var mongoose = require('mongoose');
var c = require('../lib/configuration');
var getSlug = require('speakingurl');
var prompt = require('prompt');
var generator = require('password-generator');
var pw = require('secure-password');
var q = require('q');
var mail = require('../lib/controllers/mailer');

// Print instructions
console.log('This script will create customer and one user. Please follow instructions.');

// Open db connection
var db = mongoose.createConnection(c.get('mongo:license') + '?authSource=admin');
db.once('open', function () {
  // Load models
  db.accounts = require('../lib/models/accounts')(mongoose, db, []);
  db.users = require('../lib/models/users')(mongoose, db, []);
  // Gather data about customer
  console.log('Provide company data:');
  prompt.start();
  prompt.get(['company', 'address', 'city', 'post', 'country', 'vatId'], function (err, accountData) {
    var opt = {
      separator: '_',
      symbols: false
    };
    var userData = {};
    userData.email = 'support+' + getSlug(accountData.company, opt) + '@mightyfields.com';
    // Create account
    accountData.email = userData.email;
    createAccount(accountData).then(function(accountResult) {
      // Customer created
      console.log(accountResult.msg);
      userData.accountId = accountResult.account._id;
      //Create support user
      createComlandUser(userData, accountResult.account.company).then(function (comlandUserResult) {
        //Support user created
        console.log(comlandUserResult.msg);
        //Send email
        sendWelcomeEmail(comlandUserResult.user,
                         comlandUserResult.password,
                         accountResult.account
        ).then(function() {
          console.log('Support welcome email sent');
          process.exit();
        }).fail(function() {
          console.log('There was a problem sending welcome email');
          process.exit();
        });
      }).fail (function (comlandUserError) {
        console.log(comlandUserError.msg);
        console.log(comlandUserError.error);
        process.exit();
      });
    }).fail(function(accountError) {
      // Error creating customer
      console.log(accountError.msg);
      console.log(accountError.error);
      process.exit();
    });
  });

});

function sendWelcomeEmail(user, password, account) {
  var tmpl, subj;
  switch (account.country) {
    case 'US':
      tmpl = 'welcome-mail';
      subj = 'Welcome to MightyFields';
      break;
    case 'Slovenija':
      tmpl = 'welcome-mail-slo';
      subj = 'Dobrodošli na MightyFields';
      break;
    default:
      tmpl = 'welcome-mail';
      subj = 'Welcome to MightyFields';
  }
  console.log(user.email);
  // 4. Send welcome email
  var config = {
    template    : tmpl,
    email       : user.email,
    subject     : subj,
    data        : {
      user      : user,
      account   : account,
      password  : password
    }
  };
  return mail.send(config);
}

function createComlandUser(data, company) {
  var deferred = q.defer();
  var password = generator(12, false);
  console.log('Support user password: ' + password);
  var user = db.users({
    account       : data.accountId,
    firstName     : 'MightyFields',
    lastName      : 'Support',
    email         : data.email,
    password      : pw.makePassword(password),
    active        : true,
    locked        : false,
    role          : 'superadmin',
    accessTokens  : [ ],
    group         : [ ],
    devices       : [ ],
    meta          : {
      cases: 0,
      devices: 0
    }
  });
  user.save(function (err) {
    var result = {};
    result.user = user;
    result.password = password;
    if (!err) {
      result.msg = 'Support user successfully created';
      deferred.resolve(result);
    } else {
      result.msg = 'Error creating user';
      result.error = err;
      deferred.reject(result);
    }
  });
  return deferred.promise;
}

function createAccount(data) {
  var deferred = q.defer();
  var account = new db.accounts({
    company   : data.company,
    address   : data.address,
    city      : data.city,
    post      : data.post,
    country   : data.country,
    vadId     : data.vadId,
    payment   : 'invoice',
    license   : {
      users        : 5,
      active       : 0,
      forms        : -1,
      cases        : -1,
      controls     : 'basic:all|advanced:all|special:none',
      pricePerUser : 15,
    },
    phone     : '',
    email     : data.email,
    database  : generateDbName(data.company),
    active    : true,
    locked    : false
  });
  account.save(function (err) {
    var result = {};
    result.account = account;
    if (!err) {
      result.msg = 'Account successfully created';
      deferred.resolve(result);
    } else {
      result.error = err;
      result.msg = 'Error creating account';
      deferred.reject(result);
    }
  });
  return deferred.promise;
}

function generateDbName(company) {
  var opt = {
    separator: '_',
    symbols: false
  };
  return 'mf_customer_' + getSlug(company, opt);
}
