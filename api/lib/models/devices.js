module.exports = function (mongoose, dbconn, plugins) {

  var schema = new mongoose.Schema({
    hardwareId    : {type: String, required: true, trim: true},
    label         : {type: String, required: true, trim: true},
    details       : {type: String},
    active        : {type: Boolean, default: true}
  }, {
    timestamps: true
  });

  // Apply plugins
  for (var i = 0; i < plugins.length; i++) {
    schema.plugin(plugins[i]);
  }

  return dbconn.model('Devices', schema, 'devices');
};
