module.exports = function (mongoose,  dbconn, plugins) {

  var schema = new mongoose.Schema({
    name      : {type: String},
    account   : {type: String},
    deletable : {type: String, default: true},
    users     : [{type: mongoose.Schema.Types.ObjectId, ref: 'Users'}]
  }, {
    timestamps: true
  });

  // Apply plugins
  for (var i = 0; i < plugins.length; i++) {
    schema.plugin(plugins[i]);
  }

  return dbconn.model('Groups', schema, 'groups');
};
