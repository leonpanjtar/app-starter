var debug           = require('debug')('mailer'),
    path            = require('path'),
    log             = require('../log')(module),
    c               = require('../configuration'),
    nodemailer      = require('nodemailer'),
    EmailTemplate   = require('email-templates').EmailTemplate,
    Handlebars      = require('handlebars'),
    htmlToText      = require('html-to-text'),
    q               = require('q');

var transporter = nodemailer.createTransport({
  host: c.get('sendgrid:host'),
  port: c.get('sendgrid:port'),
  auth: {
    user: c.get('sendgrid:user'),
    pass: c.get('sendgrid:apiKey')
  }
});

/*
  config = {
    template,
    email,
    subject,
    data
  };
*/

exports.send = function(config) {
  var deferred = q.defer();
  var templateDir = path.join(__dirname,'../', 'templates', config.template);
  var tpl         = new EmailTemplate(templateDir);

  tpl.render(config.data, function (err, results) {

    // setup e-mail data with unicode symbols
    var mailOptions = {
      from      : c.get('appName') + ' <' + c.get('appMail') + '>',
      to        : config.email,
      subject   : config.subject,
      text      : htmlToText.fromString(results.html, {}),
      html      : results.html,
    };

    // send mail with defined transport object
    transporter.sendMail(mailOptions, function(error, info) {
      if (error) {
        deferred.reject(error);
        return console.log(error);
      }
      deferred.resolve('Message successfully sent');
      log.info('Message sent: ' + info.response);
    });
  });
  return deferred.promise;
};
