var jwt           = require('jwt-simple'),
    log           = require('../log')(module),
    pw            = require('secure-password'),
    c             = require('../configuration'),
    crypto        = require('crypto'),
    mail          = require('./mailer'),
    getSlug       = require('speakingurl'),
    generator     = require('password-generator'),
    _             = require('underscore');

/**
 * @api {post} /login Login
 * @apiName Login
 * @apiGroup Authentication
 * @apiDescription This request can be used without token.
 * It generates you one for all future requests.
 *
 * @apiParam {String} email Your email.
 * @apiParam {String} password Your password.
 *
 * @apiSuccess {Object} user Your user data.
 */
exports.login = function(req, res) {

  var email    = req.body.email || ''; // leon.panjtar@gmail.com
  var password = req.body.password || ''; // tImV2FTzHx_I

  if (email === '' || password === '') {
    res.status(401);
    res.json({
      'status': 401,
      'message': 'Invalid credentials'
    });
    return;
  }

  // Fire a query to your DB and check if the credentials are valid
  var f = {
    'email': email,
    '$or': [{deleted: {'$exists' : false}}, {deleted: false}], //if user is deleted he shouldn't be able to login
    '$and': [{active: true}, {locked: false}], //if user is locked or unactive he shouldn't be able to login
  };
  req.db.users
    .findOne(f)
    .populate({path: 'account', model: req.db.accounts})
    .exec(function (err, user) {
      if (err) { return log.error(err); }
      // If authentication fails, we send a 401 back
      if (!user || !pw.verifyPassword(password, user.password)) {
        res.status(401);
        res.json({
          'status': 401,
          'message': 'Invalid credentials'
        });
        return;
      }

      if (user) {
        res.json(genToken(user));
      }

      return res;
    });
};

/**
 * @api {post} /login-long Login mobile
 * @apiName LoginLong
 * @apiGroup Authentication
 * @apiDescription This request can be used without token.
 * It generares you one for all future requests.
 *
 * @apiParam {String} email Your email.
 * @apiParam {String} password Your password.
 *
 * @apiSuccess {Object} user Your user data.
 */
exports.loginLong = function(req, res) {

  var email    = req.body.email || '';
  var password = req.body.password || '';

  if (email === '' || password === '') {
    res.status(401);
    res.json({
      'status': 401,
      'message': 'Invalid credentials'
    });
    return;
  }

  // Fire a query to your DB and check if the credentials are valid
  var f = {
    'email': email,
    '$or': [{deleted: {'$exists' : false}}, {deleted: false}], //if user is deleted he shouldn't be able to login
    '$and': [{active: true}, {locked: false}], //if user is locked or unactive he shouldn't be able to login
  };
  req.db.users
    .findOne(f)
    .populate({path: 'account', model: req.db.accounts})
    .populate({path: 'groups', model: req.db.groups})
    .exec(function (err, user) {
      if (err) { return log.error(err); }
      // If authentication fails, we send a 401 back
      if (!user || !pw.verifyPassword(password, user.password)) {
        res.status(401);
        res.json({
          'status': 401,
          'message': 'Invalid credentials'
        });
        return;
      }

      if (user) {
        res.json(genLongToken(user));
      }

      return res;
    });
};

/**
 * @api {get} /user-info Request user information mobile
 * @apiName UserInfo
 * @apiGroup Authentication
 *
 * @apiSuccess {Object} user User information.
 */
exports.userInfo = function(req, res) {
  console.log(req.user, req.token);
  if (req.user === undefined) {
    res.status(401);
    res.json({
      'status': 401,
      'message': 'User not found'
    });
    return;
  }

  return req.db.users
    .findById(req.user.uid)
    .populate({path: 'account', model: req.db.accounts})
    .exec(function (err, user) {
      if (err) { return log.error(err); }
      if (user) { res.send(user); }
      return res;
    });
};

/**
 * @api {get} /db/:aid Get account's database name
 * @apiName GetDB
 * @apiGroup Authentication
 *
 * @apiParam {String} aid Unique account id.
 *
 * @apiSuccess {String} database Account's database name.
 */
exports.accountDB = function(req, res) {

  return req.db.accounts
    .findById(req.params.aid)
    .select('database')
    .exec(function (err, account) {
      if (err) { return log.error(err); }
      if (account) { res.send(account.database); }
      return res;
    });
};

exports.logout = function(req, res) {
  // Set activation code
  req.body.active = false;
};

/**
 * @api {post} /reset Request password reset mail
 * @apiName ResetMail
 * @apiGroup Authentication
 * @apiDescription This request can be used without token.
 * It sends password reset link on requested email.
 *
 * @apiParam {String} email Your email.
 *
 * @apiSuccess {String} status Response status.
 * @apiSuccess {String} message Response description.
 */
exports.resetPassword = function(req, res) {
  // Fire a query to your DB and check if the user exists
  req.db.users
    .findOne({'email': req.body.email})
    .populate({path: 'account', model: req.db.accounts})
    .exec(function (err, user) {
      if (err) { return log.error(err); }

      // If no user we send normal reply otherwise users can check if other users
      // user MF.
      if (!user) {
        res.send({status: 'OK', message: 'Password mail sent.'});
      }

      if (user) {
        crypto.randomBytes(20, function(err, buf) {
          var token = buf.toString('hex');

          user.resetPwdToken    = token;
          user.resetPwdExpires  = Date.now() + 3600000; // 1 hour

          user.save(function(err) {
            if (!err) {
              log.info('user password token updated');

              var tmpl, subj;
              switch (user.account.Country) {
                case 'US':
                  tmpl = 'password-reset-mail';
                  subj = 'MightyFields account password reset';
                  break;
                default:
                  tmpl = 'password-reset-mail-slo';
                  subj = 'MightyFields ponastavitev gesla računa';
              }

              // 4. Send password reset email
              var config = {
                template    : tmpl,
                email       : user.email,
                subject     : subj,
                data        : {
                  user      : user,
                  studio    : c.get('studio')
                }
              };

              mail.send(config);

              res.send({status: 'OK', message: 'Password mail sent.'});
            }
            else {
              if (err.name === 'ValidationError') {
                res.statusCode = 400;
                res.send({error: 'Validation error'});
              } else {
                res.statusCode = 500;
                res.send({error: 'Server error'});
              }
              log.error('Internal error(%d): %s', res.statusCode, err.message);
            }
          });
        });
      }

      return res;
    });
};

/**
 * @api {post} /set/:token Reset password
 * @apiName ResetPwd
 * @apiGroup Authentication
 *
 * @apiParam {String} token Password reset token recieved in mail by
 * <a href="#api-Authentication-ResetMail">Request password reset mail</a>
 * @apiParam {String} password New password.
 *
 * @apiSuccess {String} status Response status.
 * @apiSuccess {String} message Response description.
 */
exports.setPassword = function(req, res) {
  // Fire a query to your DB and check if the user exists
  req.db.users
    .findOne({'resetPwdToken': req.params.token, 'resetPwdExpires': {$gt: Date.now()}})
    .populate({path: 'account', model: req.db.accounts})
    .exec(function (err, user) {
      if (err) { return log.error(err); }

      // If no user we send a 401 back
      if (!user) {
        res.status(400);
        res.json({
          'status': 400,
          'message': 'Password reset token is invalid or has expired.'
        });
        return;
      }

      if (user) {
        user.password             = pw.makePassword(req.body.password);
        user.resetPasswordToken   = undefined;
        user.resetPasswordExpires = undefined;

        user.save(function(err) {
          if (!err) {
            log.info('user password changed');

            var tmpl, subj;
            switch (user.account.Country) {
              case 'US':
                tmpl = 'password-reset-confirm-mail';
                subj = 'MightyFields account password has been changed';
                break;
              default:
                tmpl = 'password-reset-confirm-mail-slo';
                subj = 'MightyFields geslo računa je bilo spremenjeno';
            }

            // 4. Send password reset email
            var config = {
              template    : tmpl,
              email       : user.email,
              subject     : subj,
              data        : {
                user      : user,
                studio    : c.get('studio')
              }
            };

            mail.send(config);

            res.send({status: 'OK', message: 'Password successfully changed.'});
          }
          else {
            if (err.name === 'ValidationError') {
              res.statusCode = 400;
              res.send({error: 'Validation error'});
            } else {
              res.statusCode = 500;
              res.send({error: 'Server error'});
            }
            log.error('Internal error(%d): %s', res.statusCode, err.message);
          }
        });
      }

      return res;
    });
};

exports.activate = function(req, res) {
  req.body.active = false;
};

/**
 * Create a new user account
 */
/**
 * @apiIgnore Not used we are using script for this.
 * @api {post} /add-customer Add new customer
 * @apiName AddCustomer
 * @apiGroup Authentication
 *
 * @apiParam {String} company Company name.
 * @apiParam {String} email Company's director email.
 * @apiParam {String="US","Slovenija"} country Company's country.
 * @apiParam {String} firstName Company's director first name.
 * @apiParam {String} lastName Company's director last name.
 * @apiParam {String} address Company's address.
 * @apiParam {String} city Company's city.
 * @apiParam {String} post Company's post.
 * @apiParam {String} vadId Company's VAT identification.
 * @apiParam {String} phone Company's phone.
 * @apiParam {String} users Amount of users your company wants to have.
 *
 * @apiSuccess {String} status Response status.
 * @apiSuccess {Object} account Created account.
 * @apiSuccess {String} status Created first user.
 */
exports.createAccount = function(req, res) {

  if ((_.has(req.body, 'company')   && req.body.company !== '') &&
     (_.has(req.body, 'email')     && req.body.email !== '') &&
     (_.has(req.body, 'country') && req.body.country !== '') &&
     (_.has(req.body, 'firstName') && req.body.firstName !== '') &&
     (_.has(req.body, 'lastName')  && req.body.lastName !== '')) {

    // 1. Create Account
    var accountData = {
      company   : req.body.company,
      country   : req.body.country,
      license   : {
        users        : 5,
        active       : 1,
        forms        : -1,
        cases        : -1,
        controls     : 'basic:all|advanced:all|special:none',
        pricePerUser : 15,
      },
      email     : req.body.email,
      database  : generteDbName(req.body.company),
      active    : true,
      locked    : false
    };
    if (_.has(req.body, 'address') && req.body.address !== '') { accountData.address = req.body.address; }
    if (_.has(req.body, 'city')    && req.body.city !== '') { accountData.city = req.body.city; }
    if (_.has(req.body, 'post')    && req.body.post !== '') { accountData.post = req.body.post; }
    if (_.has(req.body, 'vadId')   && req.body.vadId !== '') { accountData.vadId = req.body.vadId; }
    if (_.has(req.body, 'phone')   && req.body.phone !== '') { accountData.phone = req.body.phone; }
    if (_.has(req.body, 'users')   && req.body.users !== '') { accountData.license.users = req.body.users; }

    var account = new req.db.accounts(accountData);

    return account.save(function (err) {
      if (!err) {
        log.info('Account for company %s successfully created.', req.body.company);

        // 2. Create new user
        var password = generator(12, false);
        //log.info('User password: ' + password);
        var user = new req.db.users({
          account       : account._id,
          firstName     : req.body.firstName,
          lastName      : req.body.lastName,
          email         : req.body.email,
          password      : pw.makePassword(password),
          active        : true,
          locked        : false,
          accessTokens  : [ ],
          group         : [ ],
          devices       : [ ],
          meta          : {
            cases: 0,
            devices: 0
          }
        });

        return user.save(function(err) {
          if (!err) {
            log.info('User with email %s for company %s successfully created.', req.body.email, req.body.company);

            var tmpl, subj;
            switch (req.body.country) {
              case 'US':
                tmpl = 'welcome-mail';
                subj = 'Welcome to MightyFields';
                break;
              default:
                tmpl = 'welcome-mail-slo';
                subj = 'Dobrodošli na MightyFields';
            }

            // 4. Send welcome email
            var config = {
              template    : tmpl,
              email       : user.email,
              subject     : subj,
              data        : {
                user      : user,
                account   : account,
                password  : password
              }
            };

            mail.send(config);

            return res.send({status: 'OK', account: account, user: user});
          }
          else {
            if (err.name === 'ValidationError') {
              res.statusCode = 400;
              res.send({error: 'Validation error', e: err});
            }
            else {
              res.statusCode = 500;
              res.send({error: 'Server error', e: err});
            }
            log.error('Internal error(%d): %s', res.statusCode, err.message);
          }
        });
      }
      else {
        if (err.name === 'ValidationError') {
          res.statusCode = 400;
          res.send({error: 'Validation error', e: err});
        }
        else {
          res.statusCode = 500;
          res.send({error: 'Server error', e: err});
        }
        log.error('Internal error(%d): %s', res.statusCode, err.message);
      }
    });
  }
  else {
    log.error('Cannot create account. Required account details not provided.');
    res.statusCode = 500;
    res.send({error: 'Cannot create account. Required account details not provided.'});
  }
};

/**
 * @api {get} /authenticated/:uid Check if user has rights to access studio
 * @apiName Check
 * @apiGroup Authentication
 *
 * @apiParam {String} uid Unique user id.
 *
 * @apiSuccess {Number} status Response status code.
 * @apiSuccess {String} message Response description.
 * @apiSuccess {Boolean} auth Is user authenticated.
 */
exports.check = function(req, res) {
  return req.db.users.findById(req.params.uid, function (err, user) {
    if (!user) {
      res.status(401);
      res.json({
        'status': 401,
        'message': 'User does not exist',
        'auth': false
      });
      return;
    }
    if (err) {
      res.status(500);
      res.json({
        'status': 500,
        'message': 'Oops something went wrong',
        'error': err,
        'auth': false
      });
      return;
    }
    if (!user.active || user.deleted) {
      res.status(401);
      res.json({
        'status': 401,
        'message': 'Token expired',
        'auth': false
      });
      return;
    }

    res.status(200);
    res.json({
      'status': 200,
      'message': 'OK',
      'auth': true
    });
    return;
  });
};

/**
 * @api {get} /authenticatedlong/:uid Check if user has rights to access mobile
 * @apiName CheckMobile
 * @apiGroup Authentication
 *
 * @apiParam {String} uid Unique user id.
 *
 * @apiSuccess {Number} status Response status code.
 * @apiSuccess {String} message Response description.
 * @apiSuccess {Boolean} auth Is user authenticated.
 */
exports.checkLong = function(req, res) {
  return req.db.users.findById(req.params.uid, function (err, user) {
    if (!user) {
      res.status(404);
      res.json({
        'status': 404,
        'message': 'Not found',
        'auth': false
      });
      return;
    }
    if (err) {
      res.status(500);
      res.json({
        'status': 500,
        'message': 'Oops something went wrong',
        'error': err,
        'auth': false
      });
      return;
    }

    if (!user.active || user.deleted) {
      res.status(401);
      res.json({
        'status': 401,
        'message': 'Token expired',
        'auth': false
      });
      return;
    }

    res.status(200);
    res.json({
      'status': 200,
      'message': 'OK',
      'auth': true
    });
    return;
  });
};

// private method
function genToken(user) {
  var created = new Date();
  var expires = expiresIn(created, 7); // 7 days
  var token = jwt.encode({
    exp   : expires,
    acc   : user.account._id,
    sub   : user.email,
    uid   : user._id,
    db    : user.account.database, //'mf_client'
    grp   : user.role,
    iss   : 'http://api.mightyfields.com'
  }, c.get('secret'));

  return {
    token   : token,
    created : created,
    expires : expires,
    user    : user
  };
}

// Generate a unique database name for company
function generteDbName(company) {
  var opt = {
    separator: '_',
    titleCase: true,
  };

  return 'mf_' + getSlug(company, opt);
}

// private method
function genLongToken(user) {
  var created = new Date();
  var expires = expiresIn(created, 120); // 120 days
  var token = jwt.encode({
    exp   : expires,
    acc   : user.account._id,
    sub   : user.email,
    uid   : user._id,
    db    : user.account.database,
    grp   : user.role,
    iss   : 'http://api.mightyfields.com'
  }, c.get('secret'));

  return {
    token   : token,
    created : created,
    expires : expires,
    user    : user
  };
}

function expiresIn(created, numDays) {
  return created.setDate(created.getDate() + numDays);
}
