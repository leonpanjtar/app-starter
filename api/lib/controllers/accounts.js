var debug           = require('debug')('accounts'),
    log             = require('../log')(module),
    c               = require('../configuration'),
    getSlug         = require('speakingurl'),
    pw              = require('secure-password'),
    generator       = require('password-generator'),
    mail            = require('./mailer');

// List all accounts
/**
 * @apiIgnore Critical. Someon outside could see all accounts data.
 * @api {get} /account Request accounts list
 * @apiName List
 * @apiGroup Accounts
 *
 * @apiSuccess {Object[]} accounts List of all accounts
 */
exports.list = function(req, res) {
  var f = {
    $or:[{deleted: {'$exists' : false}}, {deleted: false}]
  };
  return req.db.accounts.find(f, function (err, accounts) {
    if (!err) {
      return res.send(accounts);
    }
    else {
      res.statusCode = 500;
      log.error('Internal error(%d): %s', res.statusCode, err.message);
      return res.send({error: 'Server error'});
    }
  });
};

// Get one account
/**
 * @api {get} /account/:aid Request account information
 * @apiName Get
 * @apiGroup Accounts
 *
 * @apiParam {String} aid Unique account id.
 *
 * @apiSuccess {String} status Response code.
 * @apiSuccess {Object} account Account information.
 */
exports.edit = function(req, res) {
  return req.db.accounts.findById(req.params.aid, function (err, account) {
    if (!account) {
      res.statusCode = 404;
      return res.send({error: 'Not found'});
    }
    if (!err) {
      return res.send({status: 'OK', account: account});
    }
    else {
      res.statusCode = 500;
      log.error('Internal error(%d): %s',res.statusCode,err.message);
      return res.send({error: 'Server error'});
    }
  });
};

// Update account
/**
 * @api {put} /account/:aid Edit existing account
 * @apiName Edit
 * @apiGroup Accounts
 *
 * @apiParam {String} aid Unique account id.
 * @apiParam {String} [company] Account company.
 * @apiParam {String} [address] Company address.
 * @apiParam {String} [city] Company city.
 * @apiParam {String} [post] Company post.
 * @apiParam {String} [country] Company country.
 * @apiParam {String} [vatId] Company VAT identification.
 * @apiParam {String} [phone] Company phone.
 * @apiParam {String} [email] Company email.
 * @apiParam {String="en","sl_SI"} [language] Wanted language.
 * @apiParam {String="D.M.Y",
 *                   "D. M. Y",
 *                   "D. MMMM Y",
 *                   "D/M/Y",
 *                   "D-M-Y",
 *                   "D MMM Y",
 *                   "DD.MM.Y",
 *                   "DD.MM.YY",
 *                   "dddd, D.M.Y",
 *                   "dddd, MMMM D, Y",
 *                   "MMMM D, Y",
 *                   "MMM D, Y",
 *                   "M/D/Y",
 *                   "M-D-Y",
 *                   "DD.MM.YYYY",
 *                   "Y/M/D",
 *                   "Y/MM/DD",
 *                   "Y",
 *                   "YY",
 *                   "MMM",
 *                   "MMMM",
 *                   "D",
 *                   "DD",
 *                   "ddd",
 *                   "dddd",
 *                   "w"} [dateFormat] Wanted date format.
 *
 * @apiSuccess {String} status Response status.
 * @apiSuccess {Object} account Updated account info.
 */
exports.update = function(req, res) {
  return req.db.accounts.findById(req.params.aid, function (err, account) {
    if (!account) {
      res.statusCode = 404;
      return res.send({error: 'Not found'});
    }

    account.company             = req.body.company;
    account.address             = req.body.address;
    account.city                = req.body.city;
    account.post                = req.body.post;
    account.country             = req.body.country;
    account.vadId               = req.body.vatId;
    account.phone               = req.body.phone;
    account.email               = req.body.email;
    account.language            = req.body.language;
    account.dateFormat          = req.body.dateFormat;

    return account.save(function (err) {
      if (!err) {
        log.info('account updated');
        return res.send({status: 'OK', account: account});
      }
      else {
        if (err.name === 'ValidationError') {
          res.statusCode = 400;
          res.send({error: 'Validation error'});
        } else {
          res.statusCode = 500;
          res.send({error: 'Server error'});
        }
        log.error('Internal error(%d): %s', res.statusCode, err.message);
      }
    });
  });
};

var generateDbName = function (company) {
  var opt = {
    separator: '_',
    symbols: false,
  };
  return 'mf_customer_' + getSlug(company, opt);
};

// Add account
/**
 * @apiIgnore Not used we are using script for this.
 * @api {post} /account Add new customer
 * @apiName Add
 * @apiGroup Accounts
 *
 * @apiParam {String} company Company name.
 * @apiParam {String} email Company's director email.
 * @apiParam {String="US","Slovenija"} country Company's country.
 * @apiParam {String} firstName Company's director first name.
 * @apiParam {String} lastName Company's director last name.
 * @apiParam {String} address Company's address.
 * @apiParam {String} city Company's city.
 * @apiParam {String} post Company's post.
 * @apiParam {String} vadId Company's VAT identification.
 * @apiParam {String} phone Company's phone.
 * @apiParam {String} users Amount of users your company wants to have.
 *
 * @apiSuccess {String} status Response status.
 * @apiSuccess {Object} account Created account.
 * @apiSuccess {String} status Created first user.
 */
exports.add = function(req, res) {

  // 1. Create Account
  var account = new req.db.accounts({
    company             : req.body.company,
    address             : req.body.address,
    city                : req.body.city,
    post                : req.body.post,
    country             : req.body.country,
    vadId               : req.body.vatId,
    payment             : 'invoice',
    license             : {
      users         : 5,
      active        : 0,
      forms         : -1,
      cases         : -1,
      controls      : 'basic:all|advanced:all|special:none',
      pricePerUser  : 15,
    },
    phone               : req.body.phone,
    email               : req.body.email,
    database            : generateDbName(req.body.company),
    language            : req.body.language || 'en_GB',
    dateFormat          : req.body.dateFormat || 'dd.mm.YYYY',
    active              : true,
    locked              : false
  });

  //2. Create support user
  var password = generator(12, false);
  var opt = {
    separator: '_',
    symbols: false,
  };
  var supEmail = 'support+' + getSlug(req.body.company, opt) + '@mightyfields.com';
  log.info('Support user password: ' + password);
  var supUser = new req.db.users({
    account       : account._id,
    firstName     : 'MightyFields',
    lastName      : 'Support',
    email         : supEmail,
    password      : pw.makePassword(password),
    language      : req.body.language || 'en_GB',
    active        : true,
    locked        : false,
    role          : 'superadmin',
    accessTokens  : [ ],
    group         : [ ],
    devices       : [ ],
    meta          : {
      cases: 0,
      devices: 0
    }
  });

  return account.save(function (err) {
    if (!err) {
      log.info('Account successfully created.');

      return supUser.save(function (err) {
        if (!err) {
          log.info('Support user created.');

          var tmpl, subj;
          switch (account.country) {
            case 'US':
              tmpl = 'welcome-mail';
              subj = 'Welcome to MightyFields';
              break;
            case 'Slovenija':
              tmpl = 'welcome-mail-slo';
              subj = 'Dobrodošli na MightyFields';
              break;
            default:
              tmpl = 'welcome-mail';
              subj = 'Welcome to MightyFields';
          }

          var config = {};
          config = {
            template    : tmpl,
            email       : supUser.email,
            subject     : subj,
            data        : {
              user      : supUser,
              account   : account,
              password  : password
            }
          };

          mail.send(config);

          return res.send({status: 'OK', account: account, user: supUser});
        } else {
          if (err.name === 'ValidationError') {
            res.statusCode = 400;
            res.send({error: 'Validation error', e: err});
          }
          else {
            res.statusCode = 500;
            res.send({error: 'Server error', e: err});
          }
          log.error('Internal error(%d): %s', res.statusCode, err.message);
        }
      });
    }
    else {
      if (err.name === 'ValidationError') {
        res.statusCode = 400;
        res.send({error: 'Validation error', e: err});
      }
      else {
        res.statusCode = 500;
        res.send({error: 'Server error', e: err});
      }
      log.error('Internal error(%d): %s', res.statusCode, err.message);
    }
  });
};

// Activate account
/**
 * @api {get} /account/:aid/activate Activate account
 * @apiName Activate
 * @apiGroup Accounts
 *
 * @apiParam {String} aid Unique account id.
 *
 * @apiSuccess {String} status Response status.
 * @apiSuccess {Object} account Activated account information.
 */
exports.activate = function(req, res) {
  return req.db.accounts.findById(req.params.aid, function (err, account) {
    if (!account) {
      res.statusCode = 404;
      return res.send({error: 'Not found'});
    }

    account.active = true;

    return account.save(function (err) {
      if (!err) {
        log.info('account activated');
        return res.send({status: 'OK', account: account});
      }
      else {
        if (err.name === 'ValidationError') {
          res.statusCode = 400;
          res.send({error: 'Validation error'});
        } else {
          res.statusCode = 500;
          res.send({error: 'Server error'});
        }
        log.error('Internal error(%d): %s', res.statusCode, err.message);
      }
    });
  });
};

// Block account
/**
 * @api {get} /account/:aid/block Block account
 * @apiName Block
 * @apiGroup Accounts
 *
 * @apiParam {String} aid Unique account id.
 *
 * @apiSuccess {String} status Response status.
 * @apiSuccess {Object} account Blocked account information.
 */
exports.block = function(req, res) {
  return req.db.accounts.findById(req.params.aid, function (err, account) {
    if (!account) {
      res.statusCode = 404;
      return res.send({error: 'Not found'});
    }

    account.locked = true;

    return account.save(function (err) {
      if (!err) {
        log.info('account locked');
        return res.send({status: 'OK', account: account});
      }
      else {
        if (err.name === 'ValidationError') {
          res.statusCode = 400;
          res.send({error: 'Validation error'});
        } else {
          res.statusCode = 500;
          res.send({error: 'Server error'});
        }
        log.error('Internal error(%d): %s', res.statusCode, err.message);
      }
    });
  });
};

/**
 * @api {get} /account/:aid/lastupdate Get last database update date
 * @apiName GetLast
 * @apiGroup Accounts
 *
 * @apiParam {String} aid Unique account id.
 *
 * @apiSuccess {String} lastDBUpdate Date of last database update made from studio
 */
exports.lastUpdate = function(req, res) {
  return req.db.accounts.findById(req.params.aid, function (err, account) {
    if (!account) {
      res.statusCode = 404;
      return res.send({error: 'Not found'});
    }

    if (err) {
      res.statusCode = 500;
      return res.send({error: 'Server error'});
    }

    return res.send({
      'lastDBUpdate': account.lastDBUpdate
    });
  });
};

/**
 * @api {get} /account/:aid/updatedb Update lastDBUpdate
 * @apiName UpdateDB
 * @apiGroup Accounts
 * @apiDescription This call updates property lastDBUpdate in account information to current date.
 * It is used so <a href="#api-Accounts-GetLast">get last database update date</a> returns last update date.
 *
 * @apiParam {String} aid unique account id.
 *
 * @apiSuccess {String} status Response status.
 * @apiSuccess {Object} account Account information.
 */
exports.updateLastUpdate = function(req, res) {
  return req.db.accounts.findById(req.params.aid, function (err, account) {
    if (!account) {
      res.statusCode = 404;
      return res.send({error: 'Not found'});
    }

    if (err) {
      res.statusCode = 500;
      return res.send({error: 'Server error'});
    }

    account.lastDBUpdate = new Date();

    return account.save(function (err) {
      if (!err) {
        log.info('account lastDBUpdate updated');
        return res.send({status: 'OK', account: account});
      }
      else {
        if (err.name === 'ValidationError') {
          res.statusCode = 400;
          res.send({error: 'Validation error'});
        } else {
          res.statusCode = 500;
          res.send({error: 'Server error'});
        }
        log.error('Internal error(%d): %s', res.statusCode, err.message);
      }
    });
  });
};
