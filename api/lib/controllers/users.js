var debug           = require('debug')('users'),
    log             = require('../log')(module),
    c               = require('../configuration'),
    pw              = require('secure-password'),
    generator       = require('password-generator'),
    mail            = require('./mailer');

// List all users for account
/**
 * @api {get} /users/:aid Request users list
 * @apiName List
 * @apiGroup Users
 *
 * @apiParam {String} aid Unique account id.
 *
 * @apiSuccess {Object[]} users List of users.
 */
exports.list = function(req, res) {

  var options = {
    conditions : {
      '$or': [{deleted: {'$exists' : false}}, {deleted: false}],
      account :  req.params.aid
    },
    select : [
      'active',
      'groups'
    ]
  };

  if (req.params.data) {
    req.db.users.dataTable(JSON.parse(req.params.data), options, function(err, data) {
      if (err) {
        res.statusCode = 500;
        log.error('Internal error(%d): %s',res.statusCode,err.message);
      } else {
        return res.send(data);
      }
    });
  } else {
    var f = {
        'account': req.params.aid,
        '$or': [{deleted: {'$exists' : false}}, {deleted: false}]
      };
    return req.db.users
    .find(f)
    .lean()
    .exec(function (err, users) {
      if (!err) {
        return res.send(users);
      }
      else {
        res.statusCode = 500;
        log.error('Internal error(%d): %s',res.statusCode,err.message);
        return res.send({error: 'Server error'});
      }
    });
  }
};

// Get one user
/**
 * @api {get} /users/:aid/:uid Request user information
 * @apiName Get
 * @apiGroup Users
 *
 * @apiParam {String} aid Unique account id.
 * @apiParam {String} uid Unique user id.
 *
 * @apiSuccess {String} status Response status.
 * @apiSuccess {Object} user User information.
 */
exports.edit = function(req, res) {
  return req.db.users.find({'account': req.params.aid, '_id': req.params.uid}, function (err, user) {
    if (!user) {
      res.statusCode = 404;
      return res.send({error: 'Not found'});
    }
    if (!err) {
      return res.send({status: 'OK', user: user});
    }
    else {
      res.statusCode = 500;
      log.error('Internal error(%d): %s',res.statusCode,err.message);
      return res.send({error: 'Server error'});
    }
  });
};

// Update user
/**
 * @api {put} /users/:aid/:uid Edit existing user
 * @apiName Edit
 * @apiGroup Users
 *
 * @apiParam {String} aid Unique account id.
 * @apiParam {String} uid Unique user id.
 * @apiParam {String} [firstName] User's first name.
 * @apiParam {String} [lastName] User's last name.
 * @apiParam {String} [email] User's email.
 * @apiParam {String[]} [groups] Id's of groups you want to put the user in.
 * @apiParam {String="superadmin","admin","user"} [role] Role you want the user in.
 * @apiParam {String} [newPassword] New password.
 * (This will only be considered if user id match with tokens user id)
 * @apiParam {String} [confirmNewPassword] New password confirmation.
 * (This will only be considered if user id match with tokens user id)
 *
 * @apiSuccess {String} status Response status.
 * @apiSuccess {Object} user Updated user.
 */
exports.update = function(req, res) {
  return req.db.users.findOne({'account': req.params.aid, '_id': req.params.uid}, function (err, user) {
    var passChanged = false;
    if (!user) {
      res.statusCode = 404;
      return res.send({error: 'Not found'});
    }

    user.firstName  = req.body.firstName ? req.body.firstName : user.firstName;
    user.lastName   = req.body.lastName ? req.body.lastName : user.lastName;
    user.email      = req.body.email ? req.body.email : user.email;

    if (req.body.groups && req.body.groups.length > 0) {
      user.groups = req.body.groups;
    }

    if (req.user.uid !== req.params.uid &&
        req.user.grp === 'superadmin' &&
        user.email.search('@mightyfields.com') === -1 &&
        req.body.role &&
        req.body.role !== user.role) {
      user.role = req.body.role;
    }

    if (req.user.uid === req.params.uid && req.body.newPassword && req.body.confirmNewPassword) {
      if (req.body.newPassword.length > 7 && req.body.newPassword === req.body.confirmNewPassword) {
        user.password = pw.makePassword(req.body.newPassword);
        passChanged = true;
      }
    }

    user.save(function (err) {
      if (!err) {
        log.info('user updated');

        if (passChanged) {
          var tmpl, subj;
          switch (user.account.Country) {
            case 'US':
              tmpl = 'password-reset-confirm-mail';
              subj = 'MightyFields account password has been changed';
              break;
            default:
              tmpl = 'password-reset-confirm-mail-slo';
              subj = 'MightyFields geslo računa je bilo spremenjeno';
          }

          // Send password reset email
          var config = {
            template    : tmpl,
            email       : user.email,
            subject     : subj,
            data        : {
              user      : user,
              studio    : c.get('studio')
            }
          };

          mail.send(config);
        }
        return res.send({status: 'OK', user: user});
      }
      else {
        if (err.name === 'ValidationError') {
          res.statusCode = 400;
          res.send({error: 'Validation error'});
        } else if (err.code === 11000) {
          res.statusCode = 400;
          res.send({error: 'User already exists', code: 11000});
        } else {
          res.statusCode = 500;
          res.send({error: 'Server error'});
        }
        log.error('Internal error(%d): %s', res.statusCode, err.message);
      }
    });
  });
};

// Add user
/**
 * @api {post} /users/:aid Add new user
 * @apiName Add
 * @apiGroup Users
 *
 * @apiParam {String} firstName User first name.
 * @apiParam {String} lastName User last name.
 * @apiParam {String} email User email.
 * @apiParam {String[]} [groups] User groups.
 * @apiParam {String="superadmin","admin","user"} [role="admin"] User role.
 *
 * @apiSuccess {String} status Request status.
 * @apiSuccess {Object} user Created object.
 */
exports.add = function(req, res) {
  var password = generator(12, false);

  // Get account info first to check license details
  req.db.accounts.findById(req.params.aid, function(err, account) {
    if (account) {

      if (account.license.active >= account.license.users) {
        res.statusCode = 500;
        return res.send({code: 9000, error: 'License exceeded. Please buy more licenses to add more users.'});
      }

      req.db.users.findOne({'email': req.body.email}, function (err, user) {
        //Check if user already exists
        if (user) {
          account.license.active = account.license.active + 1;
          if (account.license.active > account.license.users) {
            account.license.active = account.license.users;
            res.statusCode = 500;
            return res.send({code: 9000, error: 'License exceeded. Please buy more licenses to add more users.'});
          }

          if (user.deleted) {
            // 1. Restore user
            return user.restore(function (err) {
              if (!err) {
                account.save(function() {
                  log.info('user restored');
                  return res.send({status: 'OK', user: user, code: 2000});
                });
              }
              else {
                res.statusCode = 500;
                log.error('Internal error(%d): %s', res.statusCode, err.message);
                return res.send({error: 'Server error'});
              }
            });
          } else {
            res.statusCode = 400;
            log.error('User already exists');
            return res.send({error: 'User already exists', code: 11000});
          }

        } else {
          // 1. Create user
          user = new req.db.users({
            account     : req.params.aid,
            firstName   : req.body.firstName,
            lastName    : req.body.lastName,
            email       : req.body.email,
            active      : true,
            locked      : false,
            meta        : {cases: 0, devices: 0},
            password    : pw.makePassword(password)
          });

          if (req.body.groups && req.body.groups.length > 0) {
            user.groups = req.body.groups;
          }

          if (req.user.grp === 'superadmin' &&
              req.body.role &&
              req.body.role !== user.role) {
            user.role = req.body.role;
          } else if (req.user.grp === 'admin') {
            user.role = 'user';
          }

          user.save(function (err) {
            if (!err) {
              log.info('user created');

              var tmpl, subj;
              switch (account.Country) {
                case 'US':
                  tmpl = 'welcome-mail';
                  subj = 'Welcome to MightyFields';
                  break;
                default:
                  tmpl = 'welcome-mail-slo';
                  subj = 'Dobrodošli na MightyFields';
              }

              // TODO: 3. Send emails
              var config = {
                template    : tmpl,
                email       : user.email,
                subject     : subj,
                data        : {
                  account   : account,
                  password  : password,
                  user      : user
                }
              };
              mail.send(config);

              // Update license
              account.license.active = account.license.active + 1;
              account.save(function(err) {
                return res.send({status: 'OK', user: user});
              });
            }
            else {
              if (err.name === 'ValidationError') {
                res.statusCode = 400;
                res.send({error: 'Validation error', e: err});
              }
              else {
                res.statusCode = 500;
                if (err.code === 11000) {
                  res.send({code: err.code, error: 'User with this email address already exists.'});
                }
                else { res.send({error: 'Server error'}); }
              }
              log.error('Internal error(%d): %s', res.statusCode, err.message);
            }
          });
        }
      });
    }
    else {
      res.statusCode = 500;
      log.error('Internal error(%d): %s',res.statusCode,err.message);
      return res.send({error: 'Server error'});
    }
  });
};

// Delete user
/**
 * @api {delete} /users/:aid/:uid Delete existing user
 * @apiName Delete
 * @apiGroup Users
 *
 * @apiParam {String} aid Unique account id.
 * @apiParam {String} uid Unique user id.
 *
 * @apiSuccess {String} status Response status.
 */
exports.delete = function (req, res) {
      return req.db.users.findOne({'account': req.params.aid, '_id': req.params.uid}, function (err, user) {
        if (req.user.uid === req.params.uid) {
          res.statusCode = 500;
          return res.send({code: 9001, error: 'Can not delete this user.'});
        }
        if (!user) {
          res.statusCode = 404;
          return res.send({error: 'Not found'});
        }
        if (user.email.substr('@mightyfields.com') > -1) {
          res.statusCode = 500;
          return res.send({code: 9001, error: 'Can not delete this user.'});
        }

        req.db.accounts.findById(req.params.aid, function(err, account) {
          // 1. Remove user
          return user.softdelete(function (err) {
            if (!err) {
              account.license.active = account.license.active - 1;
              account.save(function() {
                log.info('user removed');
                return res.send({status: 'OK'});
              });
            }
            else {
              res.statusCode = 500;
              log.error('Internal error(%d): %s', res.statusCode, err.message);
              return res.send({error: 'Server error'});
            }
          });

        });
      });
    };

// Lock/unlock user
/**
 * @api {put} /users/lock/:aid/:uid Lock/unlock current user
 * @apiName Lock
 * @apiGroup Users
 *
 * @apiParam {String} aid Unique account id.
 * @apiParam {String} uid Unique user id.
 *
 * @apiSuccess {String} status Request status.
 */
exports.lock = function (req, res) {
    var f = {
      'account': req.params.aid,
      '_id': req.params.uid
    };
    return req.db.users.findOne(f, function (err, user) {
      if (req.user.uid === req.params.uid) {
        res.statusCode = 500;
        return res.send({code: 9001, error: 'Can not lock this user.'});
      }
      if (!user) {
        res.statusCode = 404;
        return res.send({error: 'Not found'});
      }

      req.db.accounts.findById(req.params.aid, function(err, account) {
          if (user.email.substr('@mightyfields.com') === -1) {
            if (user.active) {
              if (account.license.active === 1) {
                res.statusCode = 500;
                return res.send({code: 9001, error: 'Can not lock this user.'});
              }
              account.license.active = account.license.active - 1;
            } else {
              account.license.active = account.license.active + 1;
            }
            if (account.license.active > account.license.users) {
              account.license.active = account.license.users;
              res.statusCode = 500;
              return res.send({code: 9000, error: 'License exceeded. Please buy more licenses to add more users.'});
            }
          }

          // lock/unlock user
          user.active = !user.active;
          log.info(user.active);

          return user.save(function (err) {
            if (!err) {
              account.save(function() {
                log.info('user lock');
                return res.send({status: 'OK', user: user});
              });
            }
            else {
              res.statusCode = 500;
              log.error('Internal error(%d): %s', res.statusCode, err.message);
              return res.send({error: 'Server error'});
            }
          });

        });
    });
  };

/**
 * @api {put} /user/updateCase Update user's cases
 * @apiName UpdateCase
 * @apiGroup Users
 *
 * @apiParam {String} id Id of user for whom you want to update cases.
 *
 * @apiSuccess {String} status Response status.
 * @apiSuccess {Object} user Updated user.
 */
exports.updateCase = function (req, res) {
  return req.db.users.findOne({_id: req.body.id}, function (err, user) {
    if (!user) {
      res.statusCode = 404;
      return res.send({error: 'Not found'});
    }
    if (req.body.delete) {
      user.meta.cases -= 1;
    } else {
      user.meta.cases += 1;
    }
    user.save(function (err) {
      if (!err) {
        return res.send({status: 'OK', user: user});
      }
    });
  });
};
