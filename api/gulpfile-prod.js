// Include gulp utils
var gulp              = require('gulp'),
    apidoc            = require('gulp-apidoc');

gulp.task('apidoc', function (done) {
  return apidoc({
    src: 'lib/controllers/',
    dest: 'documentation/',
    template: 'documentation/template/',
    config: '',
    includeFilters: ['.*\\.js$']
  }, done);
});
