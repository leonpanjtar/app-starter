#!/bin/bash


echo "------------------------------------------------------------------------------------"
echo ""
# print status
kubectl get secret --all-namespaces
echo ""
kubectl get svc --all-namespaces
echo ""
kubectl get rc --all-namespaces
echo ""
kubectl get po --all-namespaces

echo "-- Done --"