#!/bin/bash

# store env variables so we can restore them later
pre_DOCKER_TLS_VERIFY=$DOCKER_TLS_VERIFY
pre_DOCKER_CERT_PATH=$DOCKER_CERT_PATH
pre_DOCKER_HOST=$DOCKER_HOST


# build code for all components
../../scripts/build_code.bash ../../../

# use docker deamon from VM (but local client)
# the vesion of local client must watch that of the deamon (currently docker 1.83)
export DOCKER_HOST="tcp://127.0.0.1:2375"

unset DOCKER_TLS_VERIFY
unset DOCKER_CERT_PATH

# build images for all components
../../scripts/build_images.bash ../../../

# because we use docker deamon from local cluster (pangaea) there is no need to send the images to the repositroy as they will be already available in the cluster

# check which images are available in cluster
docker images


# restore env variables
DOCKER_TLS_VERIFY=$pre_DOCKER_TLS_VERIFY
DOCKER_CERT_PATH=$pre_DOCKER_CERT_PATH
DOCKER_HOST=$pre_DOCKER_HOST