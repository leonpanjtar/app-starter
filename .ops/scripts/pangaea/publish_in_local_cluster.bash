#!/bin/bash

function create_components_in_namespaces {
	path=$1
	namespace=$2
	components=$3
	for component in $components;
	do
		kubectl create -f $path/$component --namespace=$namespace
	done
}


echo "-- namespaces"
kubectl create -f ../../components/namespaces
sleep 5

echo ""
echo "-- monitoring:"

create_components_in_namespaces ../../components/monitoring "monitoring" "secrets monitoring logging proxy"  # logging-es-kibana

sleep 60

echo ""
echo "-- mf"
create_components_in_namespaces ../../components/mightyfields "mightyfields-production" "secrets auth api studio proxy"

sleep 20

echo ""
bash dump_k8s_info.bash 
