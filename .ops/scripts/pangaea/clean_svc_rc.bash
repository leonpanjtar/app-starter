#!/bin/bash

# list of namespaces
namespaces="monitoring mightyfields-production default"
# list of types of objects (services should be first)
types="service rc pod secret"

# for all namespaces
for namespace in $namespaces;
do
	# # for all types of elements
	# for type in $types;
	# do
	# 	# get list of entities
	# 	for element in $(kubectl get $type --namespace="$namespace" | tail -n +2 | cut -d " " -f 1); 
	# 	do
	# 		# delete entity
	# 		kubectl delete $type --namespace="$namespace" $element
	# 	done
	# done

	# delete namespace once entities are removed
	if [ "$namespace" != "default" ]
	then
		kubectl delete namespace $namespace
	fi
done

# k8s needs some time to delete namespaces!
sleep 8
echo "-- Done --"