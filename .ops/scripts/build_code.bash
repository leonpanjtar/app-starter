#!/bin/bash

# the first argument should be the root of the repository
cd $1

DOCKER_REPO=gcr.io/mightyfields
#DOCKER_IMAGE_PREFIX="alpha-"
DOCKER_IMAGE_PREFIX=""

CO='\033[0;32m'
NC='\033[0m' # No Color


function build_component {
    component=$1
    pushd . > /dev/null
    cd $component
    printf     "${CO}> $component${NC}\n"
    for f in "${@:2}"; do
		printf "${CO}> ${NC} $f\n"
		$f;
		status=$?
		if [[ $status -gt 0 ]]; then
		    printf "    ${CO}> ${NC} FAILED: Ending due to error in previous command\n"
		    exit $?
		fi
    done
    popd > /dev/null
}

npm set progress=false
build_component "auth" 'npm install'
build_component "api" 'bash build.bash build'
build_component "sync" 'npm install'
build_component "worker" 'npm install'
build_component "cron" 'npm install'
build_component "public" 'npm install'
build_component "static" 'npm install'
build_component "studio" 'npm install' 'bower install --config.interactive=false' 'gulp build'
