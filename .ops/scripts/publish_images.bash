#!/bin/bash

# the first argument should be the root of the repository
cd $1

DOCKER_REPO=gcr.io/quantum-tracker-121210
#DOCKER_IMAGE_PREFIX="alpha-"
DOCKER_IMAGE_PREFIX=""

CO='\033[0;32m'
NC='\033[0m' # No Color


function push_image {
    component=$1
    pushd . > /dev/null
    cd $component
    printf     "\n"
    printf     "${CO}                   PUSH IMAGE FOR${NC}     $component\n"
    printf "    ${CO}=====================================================${NC} \n"
    gcloud docker push ${DOCKER_REPO}/${DOCKER_IMAGE_PREFIX}$component
    if [[ ! $? ]]; then
	printf "    ${CO}==${NC} FAILED: Ending due to error in previous command\n"
	printf "    ${CO}=====================================================${NC} \n"
	exit $?
    fi
    popd > /dev/null
}

push_image "auth"
push_image "api"
push_image "studio"
push_image "sync"
#build_image "public"
push_image "static"
push_image "worker"
push_image "cron"
push_image "proxy"


printf "\n"
printf "    ${CO}==${NC}\n"
printf "    ${CO}==${NC} SUCCESS\n"
printf "    ${CO}=====================================================${NC} \n"
