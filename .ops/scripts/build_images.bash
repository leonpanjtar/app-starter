        #!/bin/bash

# the first argument should be the root of the repository
cd $1

DOCKER_REPO=gcr.io/round-runner-126807
#DOCKER_REPO=comland


#DOCKER_IMAGE_PREFIX="alpha-"
DOCKER_IMAGE_PREFIX="mightyfields-"

CO='\033[0;32m'
NC='\033[0m' # No Color


function build_image {
    component=$1
    pushd . > /dev/null
    cd $component
    printf     "${CO}>> $component${NC}\n"
    docker build --rm=true -t ${DOCKER_REPO}/${DOCKER_IMAGE_PREFIX}$component -f .ops/Dockerfile .

    if [[ $? -gt 0 ]]; then
	   printf "    ${CO}>> ${NC}FAILED: Ending due to error in previous command\n"
	   exit 1
    fi
    popd > /dev/null
}

build_image "studio"
build_image "auth"
build_image "api"
build_image "sync"
build_image "worker"
build_image "cron"
build_image "static"
build_image "proxy"

cd .ops/components/monitoring/
DOCKER_IMAGE_PREFIX="monitoring-" build_image "proxy"
