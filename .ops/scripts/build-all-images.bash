#!/bin/bash

# the first argument should be the root of the repository
cd $1

DOCKER_REPO=gcr.io/mightyfields
#DOCKER_IMAGE_PREFIX="alpha-"
DOCKER_IMAGE_PREFIX=""

function build_and_publish_component {
    component=$1
    echo "##"
    echo "## Building component $component"
    echo "##"
    pushd . > /dev/null    
    cd ${component}
    echo ""
    echo "## Prepare $component (compile & stuff)"
    echo "##"
    for f in "${@:2}"; do
	echo "## Build step: $f"
	$f;
    done
    echo ""
    echo "## Build docker image for $component"
    echo "##"
    echo docker build -t ${DOCKER_REPO}/${DOCKER_IMAGE_PREFIX}${component} -f .ops/Dockerfile
    echo ""
    echo "## Public image for $component to ${DOCKER_REPO}"
    echo "##"
    gcloud docker push ${DOCKER_REPO}/${DOCKER_IMAGE_PREFIX}${component}
    echo ""
    echo "## Finished ($component)"
    echo "##"
    popd > /dev/null
}

build_and_publish_component "auth" 'npm install'
build_and_publish_component "api" 'npm install'
build_and_publish_component "studio" 'npm install' 'bower install'
#build_and_publish_component "sync" 'npm install'
#build_and_publish_component "public" 'npm install'
