module.exports = function (grunt) {

    // Project configuration.
    grunt.initConfig(
        {
            pkg: grunt.file.readJSON('package.json'),
            project: {
                app: 'www',
                scss_ionic: 'scss/ionic.app.scss',
                scss_custom: 'scss/custom.app.scss',
                css_ionic: '<%= project.app %>/css/ionic.app.css',
                css_custom: '<%= project.app %>/css/custom.app.css',
                css_ionicMin: '<%= project.app %>/css/ionic.app.min.css',
                css_customMin: '<%= project.app %>/css/custom.app.min.css'
            },

            compass: {
                dist: {
                    options: {
                        sassDir: './scss',
                        cssDir: './www/css',
                        environment: 'production',
                    }
                },
                dev: {
                    options: {
                        sassDir: './scss',
                        cssDir: './www/css',
                        importPath: 'www/lib/ionic/scss',

                        specify: [
                            './scss/custom.app.scss',
                            './scss/ionic.app.scss'
                        ],

                    }
                }
            },

            sass: {
                dev: {
                    options: {
                        style: 'expanded',
                        sourcemap: 'none'
                    },
                    files:
                    {
                        '<%= project.css_ionic %>': '<%= project.scss_ionic %>',
                        '<%= project.css_custom %>': '<%= project.scss_custom %>',
                    }
                },
                dist: {
                    options: {
                        style: 'compressed',
                        sourcemap: 'none'
                    },
                    files:
                    {
                        '<%= project.css_ionicMin %>': '<%= project.scss_ionic %>',
                        '<%= project.css_customMin %>': '<%= project.scss_custom %>'
                    }

                }
            },

            uglify: {
                options: {
                    banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
                },
                build: {
                    src: 'node_modules/**/*.min.js',
                    dest: 'build/<%= pkg.name %>.min.js'
                }
            },

            useminPrepare: {
                foo: {
                    src: ['index.html', 'another.html']
                }
            },

            karma:
            {
                unit:
                {
                    options:
                    {
                        frameworks: ['jasmine'],
                        singleRun: true,
                        browsers: ['Chrome'],
                        files:
                        [
                            'www/lib/angular/angular.js',
                            'www/lib/devicejs/lib/device.min.js',
                            //'www/lib/ionic/js/*.js',
                            /** */
                            'www/lib/ionic/js/ionic.js',
                            'www/lib/ionic/js/ionic-angular.js',
                            'www/lib/ionic/js/ionic.bundle.js',                                                        
                            'www/lib/forge.min.js',
                            'www/lib/ngCordova/dist/ng-cordova.js',
                            'www/lib/angular-gettext/dist/angular-gettext.js',
                            'www/lib/underscore/underscore.js',
                            'www/lib/angular-underscore-module/angular-underscore-module.js',
                            'www/lib/ng-debounce/angular-debounce.js',
                            'www/js/**/*.js',
                            'www/lib/angular-mocks/angular-mocks.js',
                            'www/lib/angular-filter/dist/angular-filter.min.js',
                            'www/lib/angular-ui-mask/dist/mask.min.js',
                            'www/lib/angular-messages/angular-messages.min.js',
                            'www/lib/angular-filter/dist/angular-filter.js',
                            'www/lib/angular-sanitize/angular-sanitize.js',
                            'www/lib/sweetalert/dist/sweetalert.min.js',
                            'www/lib/angular-sweetalert/SweetAlert.min.js',
                            'www/lib/ionic-timepicker/dist/templates.js',
                            'www/lib/ionic-timepicker/dist/ionic-timepicker.js',
                            'www/lib/ionic-datepicker/dist/templates.js',
                            'www/lib/ionic-datepicker/dist/ionic-datepicker.js',
                            'test/cordova.mock.js',
                            'test/**/*.tests.js'
                        ]
                    }
                },
                options: {
                    plugins: [
                        'karma-jasmine',
                        'karma-chrome-launcher',
                        //'karma-phantomjs-launcher',
                        'karma-junit-reporter',
                        'karma-coverage'
                    ],
                    frameworks: ['jasmine'],
                    files: ['www/js/**\/*.js', 'test/**\/*.tests.js'],
                    reporters: ['junit', 'coverage'],
                    junitReporter: {
                        outputFile: './junitReport.xml'
                    },
                }
                
            },
            jshint: {
                files: ['www/js/**/*.js'],
                options: {
                    jshintrc: '.jshintrc',
                    reporter: 'checkstyle',
                    reporterOutput: '.jshint_result'
                }
            },
            concat:
            {
                options: {},
                dist:
                {
                    files:
                    {
                        '../utilisation/scripts/app.js':
                        [
                            '../utilisation/app/scripts/**/*.js',
                            '!../utilisation/app/scripts/**/*.tests.js'
                        ]
                    }
                }
            },
        }
        );

    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-compass');
    grunt.loadNpmTasks('grunt-karma');
    grunt.registerTask('checkCode', ['jshint']);

};
