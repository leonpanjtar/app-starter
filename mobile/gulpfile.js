var appName     = 'MightyFields';
var gulp        = require('gulp');
var gutil       = require('gulp-util');
var bower       = require('bower');
var concat      = require('gulp-concat');
var sass        = require('gulp-sass');
var minifyCss   = require('gulp-minify-css');
var rename      = require('gulp-rename');
var sh          = require('shelljs');
var gettext     = require('gulp-angular-gettext');
var karma       = require('karma').server;
var path        = require('path');
var plugins     = require('gulp-load-plugins')();
var runSequence = require('run-sequence');
var streamqueue = require('streamqueue');
var del         = require('del');
var browserify  = require('browserify');
var reactify    = require('reactify');
var source      = require('vinyl-source-stream');
var buffer      = require('vinyl-buffer');
var sourcemaps  = require('gulp-sourcemaps');
var uglify      = require('gulp-uglify');
var glob        = require('glob');
var vendor      = require('gulp-concat-vendor');
var htmlbuild   = require('gulp-htmlbuild');
var domSrc      = require('gulp-dom-src');
var gulpIgnore  = require('gulp-ignore');
var print       = require('gulp-print');
var minifyCss   = require('gulp-minify-css');
var jshint      = require('gulp-jshint');
var jshintXML   = require('gulp-jshint-xml-file-reporter');
var jscs        = require('gulp-jscs');
var stylish     = require('gulp-jscs-stylish');

var paths       = {
                    sass: ['./scss/**/*.scss']
                  };
var args        = require('yargs')
                  .alias('s', 'serve')
                  .alias('e', 'emulate')
                  .alias('b', 'build')
                  .alias('r', 'run')
                  .default('build', false)
                  .default('port', 9000)
                  .argv;

// emulate or run would also mean build
var build       = args.build || args.emulate || args.run;
var emulate     = args.emulate;
var run         = args.run;
var port        = args.port;
var serve       = args.serve;

var targetDir   = path.resolve('dist');

// if we just use emualate or run without specifying platform, we assume iOS
// in this case the value returned from yargs would just be true
if (emulate === true) {
  emulate = 'android';
}
if (run === true) {
  run = 'android';
}

//gulp.task('default', ['sass']);

gulp.task('default', function(done) {
  runSequence(
    run ? 'ionic:run' : 'noop',
    done);
});

//executes ionic serve
gulp.task('serve', ['ionic:serve']);

//prepares (minifies, concencates) files and overwrites app files
gulp.task('prepare_data', function(done) {
  runSequence(
    ['clean',
    'javascript',
    'javascript_vendor',
    'index_replacer',
    'css_all',
    /*'copy_to_dest'*/],
    done);
});

//prepares app js files
gulp.task('javascript', function () {
  var condition = /www\/js\//;

  domSrc({file: 'www/index.html', selector: 'script', attribute: 'src'})
    .pipe(gulpIgnore.include(condition))
    //.pipe(print())
    .pipe(concat('js/app.js'))
    .on('error', errorHandler)
    .pipe(uglify())
    .on('error', errorHandler)
    .pipe(gulp.dest('dist/'))
    .on('error', errorHandler);

  return true;
});

//prepares vendor js files
gulp.task('javascript_vendor', function () {
  var condition = /www\/lib/;

  domSrc({file: 'www/index.html', selector: 'script', attribute: 'src'})
    .pipe(gulpIgnore.include(condition))
    //.pipe(print())
    .pipe(concat('js/vendor.js'))
    .on('error', errorHandler)
    //.pipe(uglify())
    .pipe(gulp.dest('dist/'))
    .on('error',errorHandler);

  return true;
});

//prepares css files
gulp.task('css_all', function () {
  var condition = /ionicons.min.css/;

  domSrc({file: 'www/index.html', selector: 'link', attribute: 'href'})
    .pipe(gulpIgnore.exclude(condition))
    //.pipe(print())
    .pipe(concat('css/styleall.css'))
    .pipe(minifyCss({compatibility: 'ie8'}))
    .pipe(gulp.dest('dist/'));

  return true;
});

//replaces css and js indexes to index.html
gulp.task('index_replacer', function() {
  gulp.src(['www/index.html'])
    .pipe(htmlbuild({
      js: htmlbuild.preprocess.js(function (block) {

        block.write('js/vendor.js');
        block.write('js/app.js');
        block.end();

      }),

      css: htmlbuild.preprocess.css(function (block) {

        block.write('css/styleall.css');
        block.end();

      })
    }))
    .pipe(gulp.dest('dist'));

  return true;
});

//copies files from dist to www
gulp.task('copy_to_dest', function() {
  gulp.src('./dist/*')
    .pipe(gulp.dest('./www/'));
  gulp.src('./dist/js/*')
    .pipe(gulp.dest('./www/js/'));
  gulp.src('./dist/css/*')
    .pipe(gulp.dest('./www/css/'));

  return true;
});

// clean target dir
gulp.task('clean', function(done) {
  del([targetDir], done);
});

//generates ionic resources
gulp.task('resources', plugins.shell.task([
  'ionic resources'
]));

// no-op = empty function
gulp.task('noop', function() {});

// ionic run wrapper
gulp.task('ionic:run', plugins.shell.task([
  'ionic run ' + run
]));

// ionic run wrapper
gulp.task('ionic:serve', plugins.shell.task([
  'ionic serve'
]));

// ionic serve wrapper
gulp.task('ionic:serve', plugins.shell.task([
  'ionic serve '
]));

// global error handler
function errorHandler(error) {
  console.log(error);
  //beep();
  //if (build) {
  //  throw error;
  //} else {
  //  plugins.util.log(error);
  //}
};

gulp.task('sass', function(done) {

  // ionic sass compilation
  gulp.src(['./scss/ionic.app.scss','./scss/custom.app.scss'])
    .pipe(sass())
    .pipe(gulp.dest('./www/css/'))
    .pipe(minifyCss({
      keepSpecialComments: 0
    }))
    .pipe(rename({extname: '.min.css'}))
    .pipe(gulp.dest('./www/css/'))
    .on('end', done);

});

gulp.task('watch', function() {
  gulp.watch(paths.sass, ['sass']);
});

gulp.task('install', ['git-check'], function() {
  return bower.commands.install()
    .on('log', function(data) {
      gutil.log('bower', gutil.colors.cyan(data.id), data.message);
    });
});

gulp.task('git-check', function(done) {
  if (!sh.which('git')) {
    console.log(
      '  ' + gutil.colors.red('Git is not installed.'),
      '\n  Git, the version control system, is required to download Ionic.',
      '\n  Download git here:', gutil.colors.cyan('http://git-scm.com/downloads') + '.',
      '\n  Once git is installed, run \'' + gutil.colors.cyan('gulp install') + '\' again.'
    );
    process.exit(1);
  }
  done();
});

gulp.task('pot', function () {
    return gulp.src(['www/templates/**/*.html', 'www/js/**/*.js', 'www/js/**/*.html'])
        .pipe(gettext.extract('template.pot', {
      // options to pass to angular-gettext-tools...
    }))
        .pipe(gulp.dest('po/'));
  });

gulp.task('translations', function () {
    return gulp.src('po/**/*.po')
        .pipe(gettext.compile({
            // options to pass to angular-gettext-tools...
            format: 'javascript'
          }))
        .pipe(gulp.dest('www/js/translations/'));
  });

gulp.task('test', function(done) {
    return karma.start({
        configFile: __dirname + '/test/testing.conf.js',
        singleRun: true
      });
  });

// --------- CODESTYLE ---------------
// Generate codestyle report: uses project wise JSHint and JSCS
gulp.task('lint', function() {
  return gulp.src(['www/js/**/*.js', '!www/js/translations/**/*.js'])
    .pipe(jshint('.jshintrc'))
    .pipe(jscs({configPath: '../.jscsrc'}))
    .pipe(stylish.combineWithHintResults())
    .pipe(jshint.reporter('gulp-jshint-html-reporter', {
      filename: __dirname + '/lint-report.html',
      createMissingFolders : true
    }));
});

// Generate codestyle report for jenkins: uses project wise JSHint and JSCS
gulp.task('jenkins:lint', function() {
  return gulp.src(['www/js/**/*.js', '!www/js/translations/**/*.js'])
    .pipe(jshint('.jshintrc'))
    .pipe(jscs({configPath: '../.jscsrc'}))
    .pipe(stylish.combineWithHintResults())
    .pipe(jshint.reporter(jshintXML))
    .on('end', jshintXML.writeFile({
        format: 'checkstyle',
        filePath: './lint-report.xml'
      }));
});
