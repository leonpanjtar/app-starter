(function () {
  'use strict';

  /*jshint camelcase: false */

  /*
   * controllers/app/main.js
   *
   * (c) 2015 Comland d.o.o. http://www.comland.si
   *
   * Main application controller
   */

  angular
    .module('SmartForms.app.controller', ['gettext'])
    .controller('AppCtrl', AppCtrl);

  AppCtrl.$inject = [
    '$ionicPlatform',
    '$ionicLoading',
    '$ionicPopup',
    '$ionicSideMenuDelegate',
    '$ionicScrollDelegate',
    '$rootScope',
    '$scope',
    '$state',
    'Constants',
    'AuthService',
    'HomeService',
    'NetworkService',
    'SyncService',
    'SweetAlert',
    'gettextCatalog',
    '$cordovaGoogleAnalytics',
    'ErrorService',
    'DBConnection',
    '$timeout',
    '$ionicHistory',
    '$q'
  ];

  function AppCtrl($ionicPlatform, $ionicLoading, $ionicPopup, $ionicSideMenuDelegate, $ionicScrollDelegate,
                   $rootScope, $scope, $state, Constants, AuthService, HomeService,
                   NetworkService, SyncService, SweetAlert, gettextCatalog,
                   $cordovaGoogleAnalytics, ErrorService, DBConnection, $timeout, $ionicHistory, $q) {

    /* ==============================================
     * == INITIALIZE
     * ============================================== */

    var isSideMenuOpen  = $ionicSideMenuDelegate.isOpen.bind($ionicSideMenuDelegate);
    var scrollContainer, keyboardHeight, scrollPosition, popupContainer, sweetAlertContainer;
    var keyboardHidden   = true;

    $scope.user                 = AuthService.currentUser.info;
    $scope.vm                   = {};
    $scope.vm.synced            = false;
    $scope.vm.updated           = false;
    $scope.vm.currentState      = '';
    $scope.vm.menuOpen          = false;
    $scope.vm.version           = Constants.version;
    $scope.vm.pageSize          = 6;
    $scope.vm.homePageSize      = 12;
    $scope.vm.selectedCategory  = undefined;
    $scope.vm.selectedCategory  = false;
    $scope.vm.categories        = [];
    $scope.vm.procedures        = [];
    $scope.vm.isTablet          = window.localStorage.isDeviceTablet === 'true';
    $scope.vm.search            = '';
    $scope.progressText         = gettextCatalog.getString('DATA SYNC IN PROGRESS...');
    $scope.vm.loading           = false;
    $scope.vm.thumbnail         = false;

    /* ==============================================
     * == PRIVATE FUNCTIONS
     * ============================================== */

    var checkThumbnail = function () {
      var userSettings = JSON.parse(window.localStorage.getItem('app_user_settings'));

      var thumbnailSettings = _.findWhere(userSettings, {'Key':'thumbnail'});

      if (thumbnailSettings) {
        if (typeof thumbnailSettings.Value === 'string') {
          $scope.vm.thumbnail = thumbnailSettings.Value === 'true';
        } else {
          $scope.vm.thumbnail = thumbnailSettings.Value;
        }
      } else {
        $scope.vm.thumbnail = Constants.DEFAULT_THUMBNAIL;
      }
    };

    /**
     * @ngdoc function
     * @name changeUpdateTime
     * @description saves new updated time settings to database
     * @param {Date} localDBUpdate date of last update
     */
    var changeUpdateTime = function (localDBUpdate) {
      var deferred = $q.defer();

      var query = 'INSERT OR REPLACE INTO CONFIG(Id_Config,Scope,Id_User,Key,Value) VALUES(coalesce((SELECT Id_Config' +
                  ' FROM CONFIG WHERE Id_User =? AND Key=?),(SELECT max(Id_Config) FROM CONFIG)+1),?,?,?,?)';

      var tab = [
                  AuthService.currentUser.info.Id_Users, // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
                  'localDBUpdate',
                  'user',
                  AuthService.currentUser.info.Id_Users, // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
                  'localDBUpdate',
                  localDBUpdate
                ];

      var updatedTimeDB = DBConnection.query(query, tab);

      updatedTimeDB.then(
        function (response) {
          if (response.rows.length > 0) {
            deferred.resolve(response.rows.item(0));
          } else {
            deferred.resolve(undefined);
          }
        },
        function (error) {
          deferred.reject(error);
        }
        );

      return deferred.promise;
    };

    /* ==============================================
     * == PUBLIC FUNCTIONS
     * ============================================== */

    /**
     * @ngdoc function
     * @name clickStartButton
     * @description switch ngClick actions for start button
     */
    $scope.vm.clickStartButton = function() {
      if ($scope.isCase()) {
        $state.go('app.home');
      } else if ($scope.isSettings()) {
        $ionicHistory.goBack();
      } else {
        $scope.vm.startNewCasePopup();
      }
    };

    /**
     * @ngdoc function
     * @name closeCurrentAndStartNewCase
     * @description
     * Goes to home state and opens new case dialog
     */
    $scope.vm.closeCurrentAndStartNewCase = function () {
      $state.go('app.home');
      $scope.vm.startNewCasePopup();
    };

    /**
     * @ngdoc function
     * @name startNewCasePopup
     * @description
     * Opens a new popup which shows categories from database or automatically
     * relocates to popupProcedures if there is only one result.
     */
    $scope.vm.startNewCasePopup = function () {
      var success, error;

      if (window.analytics) {
        if ($scope.vm.menuOpen) {
          $cordovaGoogleAnalytics.trackEvent('/home', 'Menu', 'Start New', 1);
        }
        else {
          $cordovaGoogleAnalytics.trackEvent('/home', 'Button', 'Start New', 1);
        }
      }
      $scope.vm.selectedCategory = undefined;
      $scope.vm.categories = [];
      $scope.vm.procedures = [];

      // Success function
      success = function (response) {

        if (response instanceof Array) {

          if (window.analytics) {
            $cordovaGoogleAnalytics.trackView('/categories');
          }

          checkThumbnail();

          $scope.vm.popupCase = $ionicPopup.show({
            templateUrl: !$scope.vm.isTablet ?
                            'templates/home/popup.startCase.html' :
                            'templates/home/popup.startCaseTablet.html',
            cssClass: 'popup-full',
            scope: $scope
          });

          $scope.vm.categories = response;
          if (response.length === 1) {
            $scope.vm.popupProcedures(response[0]);
          }
        }

        else {
          SweetAlert.swal({
            title: gettextCatalog.getString('No procedures was found'),
            text: gettextCatalog.getString('There was no procedures found. You may need to update you data or you ' +
                                           'don\'t have permissions on any procedure.'),
            confirmButtonText: gettextCatalog.getString('OK')
          });
        }
      };

      // Error function
      error = function (response) {
        ErrorService.Message(response);
      };

      // Load a list of categories
      HomeService.
        getAllCategories(AuthService.currentUser.info.Id_Users) // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
        .then(success, error);
    };

    /**
     * @ngdoc function
     * @name $scope.vm.updateDatabase
     * @description
     * Calls API /update if phone has data connection and if user has real token and
     * then sends received data to HomeService.updateDatbase. If functions is successfull then $ionic popup
     * with success message is shown ,otherwise $ionic popup with error is shown.
     */
    $scope.vm.updateLocalDatabase = function (up) {

      var APIurl, token, users, user, startTime, success, error,
          successCases, errorCases, respTime, msgErr, existingBinHashs,
          i, c, userCred;

      $scope.progressText = gettextCatalog.getString('CONTENT UPDATE IN PROGRESS...');
      if (!up) {
        $ionicLoading.show({
          templateUrl : 'templates/loading.html',
          scope       : $scope
        });
      } else {
        $scope.vm.loading = true;
      }

      if (window.analytics) {
        $cordovaGoogleAnalytics.trackView('/update/');
      }

      APIurl    = window.localStorage.getItem('baseUrl');
      token     = window.localStorage.getItem('sessionToken');
      users     = [];
      user      = AuthService.currentUser.info.Id_Users; // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers

      users.push(user);

      if (NetworkService.status && token !== 'offline') {
        startTime = new Date().getTime();

        // Success callback
        success = function (successCallback) {
          HomeService.updateDatabase(successCallback).then(
            function () {
              successCases = function (res) {
                if (res instanceof Array) {
                  $scope.vm.cases = res;
                  $scope.lastIndexCases = res.length;
                }
                else {
                  $scope.vm.cases = [];
                }
              };

              errorCases = function () { };

              HomeService
                .getLastCases($scope.vm.homePageSize)
                .then(successCases, errorCases);

              $scope.vm.updated = true;
              if (!up) {
                swal.disableButtons();
                SweetAlert.swal({
                    title: gettextCatalog.getString('Update successfully'),
                    text: gettextCatalog.getString('Data was successfully update to your mobile device.'),
                    confirmButtonText: gettextCatalog.getString('OK'),
                  }, function () {
                    $ionicLoading.hide();
                  });

                $timeout(function () {
                  swal.enableButtons();
                });
              } else {
                $scope.vm.loading = false;
              }
            },
            function (err) {
              if (!up) {
                $ionicLoading.hide();
                ErrorService.Message(err);
              } else {
                $scope.vm.loading = false;
              }
            }
          );
        };

        // Error callback
        error = function (err) {
          respTime = new Date().getTime() - startTime;

          if (respTime >= Constants.timeouts.http) {
            msgErr = 'TIMEOUT_ERROR';
          }

          else {
            msgErr = err && err.message ? gettextCatalog.getString(err.message) : 'UNKNOWN_UPDATE_ERROR';
          }
          if (!up) {
            ErrorService.Message(msgErr);
            $ionicLoading.hide();
          } else {
            $scope.vm.loading = false;
          }
        };

        DBConnection.query('SELECT Hash FROM BINARIES').then(
          function (resoults) {
            existingBinHashs = [];
            for (i = 0; i < resoults.rows.length; i++) {
              c = resoults.rows.item(i);
              existingBinHashs.push(c.Hash);
            }
            SyncService.checkForNewData()
              .then(function (suc) {
                if (suc.newData) {
                  changeUpdateTime(suc.updateTime).then(function () {
                    var userSettings = window.localStorage.getItem('app_user_settings');
                    userSettings = JSON.parse(userSettings);

                    var updateDateSettingsIndex = _.findIndex(userSettings, function (s) {
                      return s.Key === 'localDBUpdate';
                    });

                    if (updateDateSettingsIndex >= 0) {
                      userSettings[updateDateSettingsIndex].Value = suc.updateTime;
                    }
                    else {
                      if (!userSettings) {
                        userSettings = [];
                      }
                      userSettings.push({
                        Key: 'localDBUpdate',
                        Value: suc.updateTime
                      });
                    }

                    window.localStorage.setItem('app_user_settings', JSON.stringify(userSettings));
                  });
                  SyncService.update(users, existingBinHashs).then(success, error);
                } else {
                  if (!up) {
                    swal.disableButtons();
                    SweetAlert.swal({
                        title: gettextCatalog.getString('Not updated'),
                        text: gettextCatalog.getString('There is no new data on server.'),
                        confirmButtonText: gettextCatalog.getString('OK'),
                      }, function () {
                        $ionicLoading.hide();
                      });

                    $timeout(function () {
                      swal.enableButtons();
                    });
                  } else {
                    $scope.vm.loading = false;
                  }
                }

              })
              .catch(error);

          },
          function (error) {
            if (!up) {
              $ionicLoading.hide();
              ErrorService.Message(error);
            } else {
              $scope.vm.loading = false;
            }
          });
      }

      else {

        userCred = AuthService.currentUser;

        AuthService.decryptPassword(userCred).then(
          function () {
            $scope.vm.updateLocalDatabase(up);
          },
          function () {
            if (!up) {
              ErrorService.Message('CONNECTION_UNSUCCESSFULL', gettextCatalog.getString('Warning'));
              $ionicLoading.hide();
            } else {
              $scope.vm.loading = false;
            }
          }
        );
      }
    };

    /**
     * @ngdoc function
     * @name $scope.vm.updateDatabase
     * @description
     * Gets the current procedure data from API and updates local database
     */
    $scope.vm.sync = function (online) {

      var successCases, errorCases, userCred, APIurl, token, startTime, success, error, respTime, msgErr;
      if (!online) {
        $ionicLoading.show({
          templateUrl: 'templates/loading.html',
          scope: $scope
        });
      }
      if (window.analytics) {
        $cordovaGoogleAnalytics.trackView('/sync/');
      }

      successCases = function (respond) {
        if (respond && _.has(respond, 'cases')) {
          APIurl  = window.localStorage.getItem('baseUrl');
          token   = window.localStorage.getItem('sessionToken');

          startTime = new Date().getTime();

          success = function () {
            if (!online) {
              $scope.vm.synced = true;
              SyncService.updateSyncValue();
              SweetAlert.swal({
                title: gettextCatalog.getString('Sync successfull'),
                text: gettextCatalog.getString('All closed cases were successfully transfered from' +
                                               ' you mobile aplication to server.'),
                confirmButtonText: gettextCatalog.getString('OK')
              }, function () {
                $ionicLoading.hide();
                $rootScope.$broadcast('updateCases');
              });
            } else {
              SyncService.updateSyncValue(online);
            }
          };

          error = function (err) {
            if (!online) {
              respTime = new Date().getTime() - startTime;
              if (respTime >= Constants.timeouts.http) {
                msgErr = gettextCatalog.getString('TIMEOUT_ERROR');
              }
              else {
                if (err && err.message) {
                  msgErr = gettextCatalog.getString(err.message);
                } else {
                  msgErr = gettextCatalog.getString('UNKNOWN_SYNC_ERROR');
                }
              }

              ErrorService.Message(msgErr);
              $ionicLoading.hide();
            }
          };

          SyncService.sync(respond).then(success, error);

        } else {
          if (!online) {
            SweetAlert.swal({
              title: gettextCatalog.getString('No data'),
              text: gettextCatalog.getString('There is no closed cases to transfer.'),
              confirmButtonText: gettextCatalog.getString('OK')
            }, function () {
              $ionicLoading.hide();
            });
            $ionicLoading.hide();
          } else {
            $rootScope.autoSynced = false;
          }
        }
      };

      errorCases = function () {
        if (!online) {
          $ionicLoading.hide();
        } else {
          $rootScope.autoSynced = false;
        }
      };

      if ((NetworkService.status && window.localStorage.getItem('sessionToken') !== 'offline') || online) {
        SyncService
          .getCasesForSync()
          .then(successCases, errorCases);
      }
      else {
        userCred = AuthService.currentUser;
        AuthService.decryptPassword(userCred).then(
          function () {
            $scope.vm.sync();
          },
          function () {
            ErrorService.Message('CONNECTION_UNSUCCESSFULL', gettextCatalog.getString('Warning'));
            $ionicLoading.hide();
          }
        );
      }

    };

    /**
     * @ngdoc function
     * @name $scope.vm.closeCasePopup
     * @description closes a popup of categories opened by vm.startNewCasePopup()
     */
    $scope.vm.closeCasePopup = function () {
      $scope.vm.popupCase.close();
      $scope.vm.search = '';
    };

    /**
     * @ngdoc function
     * @name $scope.vm.openCase
     * @description closes popup of categories/procedures and re-directs us to 'app.case' state
     */
    $scope.vm.openCase = function (Id_Procedure) { // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers

      // Track Google Analytics Events
      if (window.analytics) {
        // jscs:disable requireCamelCaseOrUpperCaseIdentifiers
        $cordovaGoogleAnalytics.trackEvent('Case', 'Start', 'Procedures', Id_Procedure);
        // jscs:enable requireCamelCaseOrUpperCaseIdentifiers
        $cordovaGoogleAnalytics.trackView('/case/loading');
      }

      // Close categories popup
      $scope.vm.closeCasePopup();

      // Start loading the case
      $ionicLoading.show({
        templateUrl: 'templates/loading.html',
        scope: $scope,
        showDelay: 100
      });

      // Redirect to new case
      $state.go('app.case', {
        procedureId: Id_Procedure, // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
        caseId: ''
      });
    };

    /**
     * @ngdoc function
     * @name $scope.vm.popupProcedures
     * @description hides results of category popup and shows results of procedure popup
     * @param {Object} categorie object from $scope.vm.categories
     */
    $scope.vm.popupProcedures = function (categorie) {
      // Track Google Analytics Events
      if (window.analytics) {
        // jscs:disable requireCamelCaseOrUpperCaseIdentifiers
        $cordovaGoogleAnalytics.trackEvent('Case', 'Start', 'Categories', categorie.Id_Category);
        // jscs:enable requireCamelCaseOrUpperCaseIdentifiers
        $cordovaGoogleAnalytics.trackView('/procedures');
      }

      // Success handler for fetching procedure data
      var success = function (response) {
        if (response instanceof Array) {
          $scope.vm.procedures = response;
          $scope.vm.selectedCategory = categorie;
          $scope.vm.search = '';
        }
      };

      // Error handler of the above
      var error = function () {
        $ionicLoading.hide();
      };

      // Clean scope vars
      $scope.vm.procedures = [];
      $scope.vm.selectedCategory = undefined;

      // Get list of categories and procedures
      HomeService.
        getProceduresOfCategoryAndUser(
          categorie.Id_Category, // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
          AuthService.currentUser.info.Id_Users // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
        ).
        then(success, error);
    };

    /**
     * @ngdoc function
     * @name $scope.vm.backButton
     * @description hides results from procedures popup and shows results of category popup
     */
    $scope.vm.backButton = function () {
      $scope.vm.selectedCategory = undefined;
      $scope.vm.procedures = [];
      $scope.vm.search = '';
    };

    /**
     * @ngdoc function
     * @name $scope.vm.cehckDataLength
     * @description returns true if table in parameter has more than 1 element in it , otherwise returns false
     * @param {Array[]} table Array of categories/procedures
     */
    $scope.vm.checkDataLength = function (table) {
      return table.length > $scope.vm.pageSize ? true : false;
    };

    /**
     * @ngdoc function
     * @name $scope.navigateTo
     * @description navigate to selected state
     * @param {object} state
     */
    $scope.navigateTo = function (state) {
      return $state.go(state);
    };

    /**
     * @ngdoc function
     * @name $scope.navigateToPrevious
     * @description navigate to previous state
     */
    $scope.navigateToPrevious = function () {
      if (!window.cordova) {
        window.history.back();
      } else {
        navigator.app.backHistory();
      }
    };

    /**
     * @ngdoc function
     * @name $scope.logout
     * @description Log the user out and destroy session data
     */
    $scope.logout = function () {
      var success, error;

      $ionicLoading.show({
        template: 'Signing out ...'
      });

      // Success callback
      success = function () {
        $ionicLoading.hide();
        return $state.go('app.auth');
      };

      // Error callback
      error = function () {
        $ionicLoading.hide();
        return $state.go('app.auth');
      };

      return AuthService
                .logout()
                .then(success, error);
    };

    /**
     * @ngdoc function
     * @name $scope.dismiss
     * @description Close modal window
     */
    $scope.dismiss = function () {
      $scope.modal.hide();
    };

    /**
     * @ngdoc function
     * @name $scope.isCase
     * @description Check if the current state is case
     */
    $scope.isCase = function () {
      return $state.current.name === 'app.case';
    };

    /**
     * @ngdoc function
     * @name $scope.isSettings
     * @description Check if the current state is settings
     */
    $scope.isSettings = function () {
      return $state.current.name === 'app.settings';
    };

    /**
     * @ngdoc function
     * @name $scope.isPrintPreview
     * @description Check if current state is print preview
     */
    $scope.isPrintPreview = function () {
      return $state.current.name === 'app.print';
    };

    /**
     * @ngdoc function
     * @name $scope.vm.handleErrors
     * @description Handle errors globally with single function
     */
    $scope.vm.handleErrors = function(error) {
      $ionicLoading.hide();
      console.log(error);
    };

    /**
     * @ngdoc function
     * @name $scope.vm.filterProcedures
     * @description custom function for filtering procedures and categories with search field
     */
    $scope.vm.filterFn = function (element) {
      if ($scope.vm.search && $scope.vm.search !== '') {
        var regex = new RegExp($scope.vm.search, 'gi');
        if (element.Description) {
          return element.Name.search(regex) > -1 || element.Description.search(regex) > -1;
        } else {
          return element.Name.search(regex) > -1;
        }
      } else {
        return true;
      }
    };

    /* ==============================================
     * == WATCHERS & EVENTS
     * ============================================== */

    /**
     * @ngdoc function
     * @name $ionicPlatform.registerBackButtonAction
     * @description Checks if on home screen and set back button to close app
     */
    $ionicPlatform.registerBackButtonAction(function () {
      if ($state.current.name === 'app.home' || $state.current.name === 'app.auth') {
        navigator.app.exitApp();
      }
      if ($state.current.name === 'app.case') {
        $rootScope.$broadcast('BackButtonClick', 'app.case');
      }
      else {
        navigator.app.backHistory();
      }
    }, 100);

    // track view in google analytics
    // TODO(Marko): Disable scroll
    $ionicPlatform.ready(function () {
      if (window.cordova) {
        window.cordova.plugins.Keyboard.disableScroll(true);
      }
      if (window.analytics) {
        $cordovaGoogleAnalytics.trackView('App Ctrl');
      }
      document.addEventListener('resume', function() {
        $scope.vm.updateLocalDatabase(true);
      }, false);
      $scope.vm.updateLocalDatabase(true);
    });

    $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState) {
      if (fromState.name === 'app.auth') {
        $scope.vm.updateLocalDatabase(true);
      }
    });

    // watch the side menu flag
    $scope.$watch(isSideMenuOpen, function (isOpen) {
      $scope.vm.menuOpen = isOpen;
    });

    // track menu open in google analytics
    $rootScope.$watch('vm.menuOpen', function (n) {
      if (window.analytics) {
        if (n) {
          $cordovaGoogleAnalytics.trackEvent('/home', 'Menu', 'Open', 1);
        }
      }
    });

    window.addEventListener('native.keyboardshow', function(e) {
      if (!keyboardHidden) {
        e.stopImmediatePropagation();
        return;
      } else {
        console.log('Keyboard show');
      }

      // Save keyboard height for later use
      keyboardHeight = e.keyboardHeight;

      if (scrollContainer) {
        if (ionic.Platform.isIOS() && keyboardHidden) {
          scrollContainer.height(scrollContainer.height() + keyboardHeight);
        }
        $ionicScrollDelegate.$getByHandle('caseContent').scrollTo(0, scrollPosition - 75, true);
      }
      if (ionic.Platform.isIOS() && keyboardHidden) {

        // Check if popup/sweetalert is opened
        popupContainer = angular.element('.popup');
        sweetAlertContainer = angular.element('.sweet-alert.visible');

        if (popupContainer.length) {
          var tempOffset = popupContainer.offset();
          popupContainer.height(popupContainer.height() - keyboardHeight);
          popupContainer.offset(tempOffset);
        }
        if (sweetAlertContainer.length) {
          sweetAlertContainer.offset({
                                        top: sweetAlertContainer.offset().top - keyboardHeight / 2,
                                        left: sweetAlertContainer.offset().left
                                      });
        }
      }
      keyboardHidden = false;
    });

    window.addEventListener('native.keyboardhide', function(e) {
      if (keyboardHidden) {
        e.stopImmediatePropagation();
        return;
      } else {
        console.log('Keyboard hidden');
      }

      if (scrollContainer) {
        if (ionic.Platform.isIOS() && !keyboardHidden) {
          scrollContainer.height('auto');
        }
        $timeout(function () {
          $ionicScrollDelegate.resize();
        }, 300);
      }
      if (ionic.Platform.isIOS() && !keyboardHidden) {
        if (popupContainer.length) {
          popupContainer.removeAttr('style');
          angular.element(popupContainer.find('.scroll-content')[0]).removeAttr('style');
        }
        if (sweetAlertContainer.length) {
          sweetAlertContainer.offset({
                                        top: sweetAlertContainer.offset().top + keyboardHeight / 2,
                                        left: sweetAlertContainer.offset().left
                                      });
        }
      }
      keyboardHidden = true;
    });

    /**
     * @ngdoc watcher
     * @name online
     * @description watches when the app goes online and fires autosync
     */
    document.addEventListener('online', function () {
      if ($rootScope.haveClosed) {
        $scope.vm.sync(true);
        $rootScope.haveClosed = false;
      }
    }, false);

    //TODO (Marko) documentation
    //catch event and manualy focus field
    $scope.$on('fieldFocused', function(data, event) {
      console.log('Field focus');
      event.preventDefault();
      event.stopImmediatePropagation();

      var parent = angular.element(event.target).closest('.case-content');

      parent.addClass('force-repaint');

      scrollContainer = parent.find('.scroll');

      // Get top offset of the form control field
      scrollPosition = angular.element(event.target).offset().top;

      // Substract it from current scroll offset
      // console.log('OLD', $(event.target).offset().top, 'NEW',
      //   scrollPosition - scrollContainer.offset().top, 'OFFSET', scrollContainer.offset().top);
      scrollPosition -= scrollContainer.offset().top;

      console.log('Position', scrollPosition);

      $ionicScrollDelegate.$getByHandle('caseContent').scrollTo(0, scrollPosition - 75, true);

      $timeout(function() {
        parent.removeClass('force-repaint');
      }, 700);
    });

    /**
     * @ngdoc $on event
     * @name on versionOutdated
     * @description
     * If mobile version is to old for our API dont allow
     * user to use the app until the app is updated
     */
    $scope.$on('versionOutdated', function(data, event) {
      console.log('Mobile version outdated');
      SweetAlert.swal({
        title: gettextCatalog.getString('Outdated app'),
        text: gettextCatalog.getString('Please update MightyFields if you want to continue using this app') + '\n\n' +
          gettextCatalog.getString('If you have any troubles please contact support at support@mightyfields.com'),
        allowEscapeKey: false,
        showConfirmButton: false
      });
    });

    /**
     * @ngdoc watcher
     * @name haveClosed
     * @description watches for if has closed cases so it starts autosyncing
     */
    $rootScope.$watch('haveClosed', function () {
      if ($rootScope.haveClosed && NetworkService.status) {
        $scope.vm.sync(true);
        $rootScope.haveClosed = false;
      }
    });
  }
})();
