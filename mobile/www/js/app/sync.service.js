(function () {
  'use strict';

  /**
   * @ngdoc service
   * @name SyncService
   * @description
   * this service takes care of sync method
   *
   * home/home.service.js
   *
   * (c) 2015 Comland d.o.o. http://www.comland.si
   *
   */

  angular
    .module('SmartForms.sync.service', [
      'SmartForms.auth.service',
      'SmartForms.common.dbconnection',
      'SmartForms.common.cipher',
    ])
    .service('SyncService', SyncService);

  SyncService.$inject = [
    '$http',
    '$rootScope',
    '$q',
    'Constants',
    'DBConnection',
    'AuthService',
    'gettextCatalog',
    'ChiperService',
    '$cordovaFileTransfer'
  ];

  function SyncService($http, $rootScope, $q, Constants, DBConnection, AuthService, gettextCatalog, ChiperService,
                       $cordovaFileTransfer) {

    var sync   = this;

    var fields = {};

    // prepere array values for sync
    var prepareArray = function (value) {
      var array = [];
      for (var i = 0; i < value.length; i++) {
        array.push(_.pick(value[i], 'Label', 'Value'));
      }
      return array;
    };

    // prepere object value for sync
    var prepareObject = function (value) {
      return _.pick(value, 'Label', 'Value');
    };

    //prepare email object for sync
    var prepareEmail = function (value) {
      var docs = [];

      for (var key in value[6].value.value) {
        if (value[6].value.value.hasOwnProperty(key)) {
          docs.push(_.pick(value[6].value.value[key], 'value', 'idReport'));
        }
      }

      var prepEmailDict = {
        to      : value[0].value.value,
        from    : value[1].value.value,
        cc      : value[2].value.value,
        bcc     : value[3].value.value,
        subject : value[4].value.value,
        body    : value[5].value.value,
        docs    : docs
      };

      return prepEmailDict;//ChiperService.EncodeUtf8(JSON.stringify(prepEmailDict));
    };

    // prepera base64 value for sync
    var prepareBase64 = function (defValue) {
      var b64Indicator = 'base64,';

      if (defValue.indexOf(b64Indicator) > -1) {
        defValue = defValue.substring(defValue.indexOf(b64Indicator) + b64Indicator.length, defValue.length);
      }

      return defValue;
    };

    /* jshint -W003 */
    // prepare table values for sync
    var prepareTable = function (values, images) {
      var result = [];

      for (var row = 0; row < values.length; row++) {
        result[row] = {};
        for (var guid in values[row]) {
          if (values[row].hasOwnProperty(guid) && guid !== '$$hashKey') {
            var field = fields[guid.toLowerCase()];
            addFieldToContent(guid, field, values[row][guid], result[row], images, true);
          }
        }
      }
      return result;
    };

    var addFieldToContent = function(guid, field, value, content, images, addWithKey) {
      if (field && field.type) {
        if (value) {
          switch (field.type) {
            case 800://date
            case 801://time
              value = value;
              break;
            case 1201://checkbox
              value = prepareArray(value);
              break;
            case 1205://table
              value = prepareTable(value, images);
              break;
            case 1200://dropdown
              if (_.isArray(value)) {
                // Multiselect dropdown
                value = prepareArray(value);
              } else {
                value = prepareObject(value);
              }
              break;
            case 1202://radio
              //TODO(rok): this is temp fix since radio value should not be saved in array
              value = prepareObject(value[0]);
              break;
            case 1206://email
              value = prepareEmail(value);
              break;
            case 1000://signature
              value = prepareBase64(value);
              break;
            default:
              value = value;
          }
        }
      }
      // Images have separate logic
      if (field && field.type && field.type === 1208) {
        // TODO: Add with key is used in tables because of studio. We should consolidate this.
        if (addWithKey) {
          content[guid] = {
            Guid: guid,
            Value: value ? value : '',
            Type: 'file'
          };
        } else {
          content.push({
            Guid: guid,
            Value: value ? value : '',
            Type: 'file'
          });
        }
        if (value) {
          _.each(value, function(image) {
            images.push(image);
          });
        }
      } else {
        // TODO: Add with key is used in tables because of studio. We should consolidate this.
        if (addWithKey) {
          content[guid] = {
            Guid: guid,
            Value: value ? value : ''
          };
        } else {
          content.push({
            Guid: guid,
            Value: value ? value : ''
          });
        }
      }
      return value;
    };
    /* jshint +W003 */

    /**
     * @ngdoc function
     * @name prepareContent
     * @description repare values for diffrent types for close case
     * @returns
     */
    var prepareContent = function (values) {
      var content  = [], field, value, images = [];

      values = JSON.parse(values);

      for (var i in values) {
        if (i !== undefined) {
          field = fields[i.toLowerCase()];
          value = values[i];
          addFieldToContent(i, field, value, content, images, false);
        }
      }
      console.log(content);
      console.log(JSON.stringify(content));
      console.log(ChiperService.EncodeUtf8(JSON.stringify(content)));
      return {
        //content: ChiperService.EncodeUtf8(JSON.stringify(content)),
        content: JSON.stringify(content),
        images: images
      };
    };

    /**
     * @ngdoc function
     * @name home.getAllFields
     * @description get all fields ofr cases that need to be synced
     * @returns deferred.promise
     */
    var getAllFields = function() {

      var deferred = $q.defer(), query;

      query = 'SELECT f.Guid, f.Type, fr.Id_Procedure FROM [FIELD] f ' +
              'INNER JOIN [SECTION] s ON f.Id_Section = s.Id_Section ' +
              'INNER JOIN [FORM] fr ON fr.Id_Form = s.Id_Form ';

      DBConnection.query(query).then(
        function (results) {
          if (results.rows.length) {
            for (var i = 0; i < results.rows.length; i++) {
              var f = results.rows.item(i);
              // jscs:disable requireCamelCaseOrUpperCaseIdentifiers
              fields[f.Guid.toLowerCase()] = {guid: f.Guid, type: f.Type, procedureId: f.Id_Procedure};
              // jscs:enable requireCamelCaseOrUpperCaseIdentifiers
            }
            deferred.resolve();
          } else {
            deferred.reject('No fields');
          }
        },

        function (error) {
          if (Constants.DEBUGMODE) {
            console.log('Error on select case from db');
            console.log(error);
          }
          deferred.reject(error);
        });

      return deferred.promise;
    };

    /**
     * @ngdoc function
     * @name home.getCasesForSync
     * @description get lest 10 cases from curren user
     * @returns deferred.promise last 10 cases or error
     *
     */
    sync.getCasesForSync = function () {

      var deferred = $q.defer();

      getAllFields().then(function() {

        var query = 'SELECT Id_Case, Id_User, Id_Device, Id_Procedure, Hash, Name, Location, Content, Created, Guid,' +
                    ' Version ' +
                    'FROM [CASE] WHERE [CASE].Id_User = ? AND [CASE].Synced IS NULL AND [CASE].Closed = 1';
        // jscs:disable requireCamelCaseOrUpperCaseIdentifiers
        DBConnection.query(query, [AuthService.currentUser.info.Id_Users]).then(
          // jscs:enable requireCamelCaseOrUpperCaseIdentifiers
          function (results) {

            if (results.rows.length) {
              var cases = [], images = [];
              for (var i = 0; i < results.rows.length; i++) {
                var c = results.rows.item(i);
                var content = prepareContent(c.Content);
                c.Content = content.content;
                c.Hash    = md5(c.Content);
                //c.Content = ChiperService.Encode64(c.Content);
                //console.log(c.Content);

                cases.push({
                  // jscs:disable requireCamelCaseOrUpperCaseIdentifiers
                  IdCase        : c.Id_Case,
                  Guid          : c.Guid,
                  IdUser        : c.Id_User,
                  IdDevice      : c.Id_Device,
                  IdProcedure   : c.Id_Procedure,
                  // jscs:enable requireCamelCaseOrUpperCaseIdentifiers
                  Hash          : c.Hash,
                  Name          : c.Name,
                  Location      : c.Location,
                  Content       : c.Content,
                  Images        : content.images,
                  Created       : c.Created,
                  Version       : c.Version
                });

                if (content.images && content.images.length > 0) {
                  _.each(content.images, function(image) {
                    images.push(image);
                  });
                }
              }
              deferred.resolve({
                cases: cases,
                images: images
              });
            }

            // if user has no cases return string
            else {
              deferred.resolve(gettextCatalog.getString('No records found'));
            }
          },

          function (error) {
            if (Constants.DEBUGMODE) {
              console.log('Error on select case from db');
              console.log(error);
            }
            deferred.reject(error);
          });

      }).catch(function() {
        deferred.resolve(gettextCatalog.getString('No records found'));
      });

      return deferred.promise;
    };

    /**
     * @ngdoc function
     * @name sync.updateSyncValue
     * @description updates synced value in CASE table for every record that was synced on server
     * @returns --------------
     *
     */
    sync.updateSyncValue = function (auto) {
      var deferred = $q.defer();
      var d = new Date();
      var n = d.toISOString();
      var query = 'UPDATE [CASE] SET Synced = ? WHERE [CASE].Synced IS NULL AND [CASE].Closed = 1';
      var tableArguments = [n];
      DBConnection.query(query, tableArguments).then(
        function () {
          if (auto) {
            $rootScope.autoSynced = true;
          }
          deferred.resolve(true);
        },
        function (error) {
          deferred.reject(error);
        }
      );
      return deferred.promise;
    };

    /**
     * @ngdoc function
     * @name sync.update
     * @description
     * calls the update service
     * @returns deferred.promise info if request was successful
     */
    sync.update = function (users, alreadyHasHashList) {
      var deferred = $q.defer();

      var success = function (response) {
        deferred.resolve(response.data);
      };

      var error = function (error) {
        deferred.reject(error.data);
      };

      var data = {
        UserIdList          : users,
        AlreadyHaveHashList : alreadyHasHashList
      };

      var req = {
        method: 'POST',
        url: baseURL + '/Update',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'bearer ' + window.localStorage.getItem('sessionToken')
        },
        data: data,
        timeout: Constants.timeouts.http
      };

      $http(req).then(success, error);

      return deferred.promise;
    };

    /**
     * @ngdoc function
     * @name sync.sync
     * @description
     * calls the sync service
     * @returns deferred.promise info if request was successful
     */
    sync.sync = function (respond) {
      var deferred = $q.defer();

      var success = function (response) {
        deferred.resolve(response.data);
      };

      var error = function (error) {
        deferred.reject(error.data);
      };

      var req = {
        method  : 'POST',
        url     : baseURL + '/Sync',
        headers : {
          'Content-Type'    : 'application/json',
          'Authorization'   : 'bearer ' + window.localStorage.getItem('sessionToken')
        },
        data    : respond.cases,
        timeout : Constants.timeouts.http
      };

      // Check if we need to upload any images
      if (_.has(respond, 'images') && respond.images.length > 0) {

        var promises = [];
        var images = [];

        angular.forEach(respond.images, function (image) {
          var inPromise = $cordovaFileTransfer.upload(
            baseURL + '/Image',
            cordova.file.dataDirectory + image.name, {
              fileName: image.name,
              fileKey: 'file',
              chunkedMode: false,
              mimeType: 'image/jpg',
              headers : {
                'Authorization': 'bearer ' + window.localStorage.getItem('sessionToken')
              }
            },
            true
          );
          inPromise.then(function(result) {
            var r = JSON.parse(result.response);
            images.push({name: image.name, id: r.image});
          }, function(err) {
            console.log(err);
          });
          promises.push(inPromise);
        });

        // Wait fo all pushed promises to become ready.
        $q.all(promises)
        .then(function() {
            $http(req).then(success, error);
          })
        .catch(function() {
            deferred.reject({message: 'File upload failed.'});
          });

      }
      else {
        $http(req).then(success, error);
      }

      return deferred.promise;
    };

    /**
     * @ngdoc function
     * @name sync.update
     * @description
     * calls the update service
     * @returns deferred.promise info if request was successful
     */
    sync.checkForNewData = function (users, alreadyHasHashList) {
      var deferred = $q.defer();

      var success = function (response) {
          deferred.resolve(response.data);
        };

      var error = function (error) {
        deferred.reject(error.data);
      };
      var userSettings = JSON.parse(window.localStorage.getItem('app_user_settings'));
      var localDBUpdate = _.findWhere(userSettings, {'Key': 'localDBUpdate'});

      var data;
      if (localDBUpdate && localDBUpdate.Value && localDBUpdate.Value !== 'undefined') {
        data = {
          'localDBUpdate': localDBUpdate.Value
        };
      } else {
        data = {
          'localDBUpdate': false
        };
      }

      var req = {
        method: 'POST',
        url: baseURL + '/lastupdate',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'bearer ' + window.localStorage.getItem('sessionToken')
        },
        data: data,
        timeout: Constants.timeouts.http
      };

      $http(req).then(success, error);

      return deferred.promise;
    };

    return sync;
  }

})();
