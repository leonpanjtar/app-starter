(function () {
  'use strict';

  /**
   * @ngdoc overview
   * @name InputDirective
   * @description
   * directive that renders an input box
   *
   * Example
   * ===================================
   *
   *
   *
   *
   * case/directives/drct.input.js
   *
   * (c) 2015 Comland d.o.o. http://www.comland.si
   */

  angular
    .module('SmartForms.case.directives.table', [])
    .directive('sfTable', sfTable);

  sfTable.$inject = [
    '$rootScope',
    '$timeout',
    '$ionicPopup',
    'CurrentService',
    'sfUtil',
    'gettextCatalog'
  ];

  function sfTable($rootScope, $timeout, $ionicPopup, CurrentService, sfUtil, gettextCatalog) {

    /* ==============================================
     * == PRIVATE VARS & FUNCTIONS
     * ============================================== */
    var compiled = CurrentService.getFieldTemplate('js/case/directives/tpls/part.sfTable.html');

    /* =====================================
     * = DIRECTIVE
     * ===================================== */
    return {
      restrict: 'E',
      require: 'ngModel',
      scope: {ngModel: '='},
      link: function (scope, element, attrs) {

        /* ==============================================
         * == INITIALIZE
         * ============================================== */

        scope.service             = CurrentService;
        scope.cs                  = CurrentService;

        // Evaluate input data
        scope.data                = scope.$eval(attrs.data);

        // Prepare field data
        scope.required            = sfUtil.findProp(scope.data.rules, 'required') ? true : false;
        scope.minvalueValue      = sfUtil.ruleValue(scope.data.rules, 'minvalues');
        scope.maxvalueValue      = sfUtil.ruleValue(scope.data.rules, 'maxvalues');
        scope.show                = CurrentService.isShown(scope.data.deps);
        scope.closed              = CurrentService.getCasesClosedValue();

        scope.viewResult          = undefined;

        /**
         *  Here we check if data of particular scope.data.guid exists in
         *  CurrentService.values if so then we give this value to scope.ngModel
         *  ( this is particulary useful in case of edit/preview )
         */
        if (CurrentService.values && CurrentService.values[scope.data.guid]) {
          scope.ngModel = CurrentService.values[scope.data.guid];
          scope.temp = scope.ngModel.slice();
        }

        /* ==============================================
         * == PUBLIC FUNCTIONS
         * ============================================== */

        /**
        * @ngdoc function
        * @name $scope.openPopover
        * @description open hint popover
        */
        scope.openPopover = function ($event) {
          sfUtil.openPopup($event, scope);
        };

        /**
         * @ngdoc function
         * @name $scope.closePopover
         * @description close hint popover
         */
        scope.closePopover = function () {
          sfUtil.closePopup(scope);
        };

        /**
         * @ngdoc function
         * @name $scope.fieldValue
         * @description Returns text value of field
         * @param {object} values
         * @param {object} field
         */
        scope.fieldValue = function (values, field, first) {

          var guid = (field instanceof Object) ? field.guid : field;
          if (values && field && values[guid]) {
            if (values[guid] instanceof Date) {
              var f = _.findWhere(scope.data.table, {guid: guid});
              if (f.type === 'time') {
                if ((typeof values[guid]) === 'number') {
                  var tempTime = moment.duration(values[guid] * 1000);
                  return (tempTime.hours().toString().length === 1 ? '0' + tempTime.hours() : tempTime.hours()) + ':' +
                         (tempTime.minutes().toString().length === 1 ? '0' + tempTime.minutes() : tempTime.minutes());
                }
                else {
                  return moment(values[guid]).format('hh:mm');
                }
              }
              else if (f.type === 'date') {
                return moment(values[guid]).format('DD.MM.YYYY');
              }
            }
            else if (values[guid] instanceof Array) {
              var vals = [];
              angular.forEach(values[guid], function(item) {
                vals.push(item.Label);
              });
              return vals.join(', ');
            }
            else if (values[guid] instanceof Object) {
              if (values[guid].Label) {
                return values[guid].Label;
              } else {
                return values[guid].Value ? gettextCatalog.getString('Yes') : gettextCatalog.getString('No');
              }
            }
            return values[guid];
          }
          return '';
        };

        /**
         * @ngdoc function
         * @name scope.first
         * @description Only return first element of object
         * @param {object} row
         */
        scope.first = function(row) {
          var obj = {};
          obj[Object.keys(row)[0]] = row[Object.keys(row)[0]];
          return obj;
        };

        /**
         * @ngdoc function
         * @name scope.deleteRow
         * @description Deletes selected row from table
         * @param {object} row
         */
        scope.deleteRow = function (row) {
          console.log(row);
          CurrentService.values[scope.data.guid].splice(row, 1);
          CurrentService.updateOne(scope.data.guid, CurrentService.values[scope.data.guid]);
          scope.temp = CurrentService.values[scope.data.guid].slice();
        };

        /**
         * @ngdoc function
         * @name scope.sizeOf
         * @description return the count of table fields
         * @param {object} table
         */
        scope.sizeOf = function(table) {
          return _.size(table);
        };

        /**
         * @ngdoc function
         * @name scope.openTableData
         * @description open table data modal
         */
        scope.openTableData = function() {
          scope.loading = false;

          scope.popupSearch = $ionicPopup.show({
            templateUrl: 'js/case/directives/tpls/part.sfTable.data.html',
            cssClass: 'popup-api',
            scope: scope
          });

        };

        /**
         * @ngdoc function
         * @name scope.closeTableData
         * @description close table data modal
         */
        scope.closeTableData = function () {
          scope.popupSearch.close();
        };

        /**
         * @ngdoc function
         * @name sscope.backButton
         * @description go back one view
         */
        scope.backButton = function () {
          scope.viewResult = undefined;
        };

        /**
         * @ngdoc function
         * @name $scope.view
         * @description set selected resoult as viewResoult
         */
        scope.view = function (r, event) {
          scope.x = event.layerX;
          scope.y = event.layerY;
          scope.viewResult = r;
        };

        scope.saveValue = function (id) {
          CurrentService.saveTable(id);
          angular.element('#' + scope.data.guid).controller('ngModel').$setDirty(true);
          scope.temp = CurrentService.values[scope.data.guid].slice();
        };

        /* ==============================================
         * == WATCHERS AND EVENTS
         * ============================================== */

        // Set dependencies events wathcers
        CurrentService.setDeps(scope.data.deps, scope);

        // Disable field on case close
        scope.$on('CaseClosed', function () {
          scope.closed = true;
        });

        // Remove hint popover on destroy
        scope.$on('$destroy', function () {
          if (scope.popover) {
            scope.popover.remove();
          }
        });

        // Reuse compiled template and append scope
        compiled(scope, function (clonedElement) {
          element.append(clonedElement);
        });

      }
    };

  }

})();
