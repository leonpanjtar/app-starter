(function () {
  'use strict';

  /*global SignaturePad */

  /**
   * @ngdoc overview
   * @name SignatureDirective
   * @description
   * directive that renders a signature box
   *
   * Example
   * ===================================
   *
   *
   *
   *
   * case/directives/drct.signature.js
   *
   * (c) 2015 Comland d.o.o. http://www.comland.si
   */

  angular
    .module('SmartForms.case.directives.sigPopup', [])
    .directive('sigPopup', sigPopup);

  sigPopup.$inject = [
    '$rootScope',
    '$ionicScrollDelegate'
  ];

  function sigPopup($rootScope, $ionicScrollDelegate) {

    /* =====================================
     * = DIRECTIVE
     * ===================================== */
    return {
      restrict: 'A',
      require: 'ngModel',

      // Get data from
      scope: {
        ngModel: '='
      },

      //does not work using http.get
      templateUrl: 'js/case/directives/tpls/part.sigPopup.html',

      link: function (scope, element, attrs, ngModelCtrl) {

        /* ==============================================
         * == INITIALIZE
         * ============================================== */

        var canvas = element.find('canvas')[0];

        scope.signaturePad = new SignaturePad(canvas, {
          onBegin: function () {
            $ionicScrollDelegate.freezeAllScrolls(true);
          },
          onEnd: function () {
            $ionicScrollDelegate.freezeAllScrolls(false);
            scope.saveCanvas();
          }
        });

        /* ==============================================
         * == PUBLIC FUNCTIONS
         * ============================================== */

        /**
         * @ngdoc function
         * @name saveCanvas
         * @description Save canvas data to ngModel
         * @param {object} field
         */
        scope.saveCanvas = function () {
          ngModelCtrl.$setViewValue(scope.signaturePad.toDataURL());
        };

        /* ==============================================
         * == WATCHERS AND EVENTS
         * ============================================== */

        /**
         * @ngdoc function
         * @name clearCanvas
         * @description Clear canvas data
         * @param {object} field
         */
        scope.$on('clear-signature', function () {
          scope.signaturePad.clear();
        });

      }
    };

  }

})();
