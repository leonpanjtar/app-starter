(function () {
  'use strict';

  /**
   * @ngdoc overview
   * @name InputDirective
   * @description
   * directive that renders an input box
   *
   * Example
   * ===================================
   *
   * <sf-result-item data="item" />
   *
   *
   * case/directives/drct.input.js
   *
   * (c) 2015 Comland d.o.o. http://www.comland.si
   */

  angular
    .module('SmartForms.case.directives.resultItem', [])
    .directive('sfResultItem', sfResultItem);

  sfResultItem.$inject = [
    '$compile',
    'CurrentService'
  ];

  function sfResultItem($compile, CurrentService) {

    /* ==============================================
     * == PRIVATE VARS & FUNCTIONS
     * ============================================== */
    var compiled = CurrentService.getFieldTemplate('js/case/directives/tpls/part.sfResultItem.html');

    /* =====================================
     * = DIRECTIVE
     * ===================================== */
    return {
      restrict      : 'AE',
      scope         : true,
      link          : function (scope, element, attrs) {

        /* ==============================================
         * == INITIALIZE
         * ============================================== */

        scope.service           = CurrentService;

        // Evaluate item data
        scope.data              = scope.$eval(attrs.data);
        scope.display           = scope.$eval(attrs.display);

        /* ==============================================
         * == PUBLIC FUNCTIONS
         * ============================================== */
        // Reuse compiled template and append scope
        compiled(scope, function(clonedElement) {
          element.append(clonedElement);
        });
      }
    };

  }

})();
