(function () {
'use strict';

/**
     * @ngdoc overview
     * @name NgValidInput
     * @description
     * directive that validate input field
     *
     * case/directives/validations/drct.ngValidInput.js
     *
     * (c) 2015 Comland d.o.o. http://www.comland.si
     */

angular
    .module('SmartForms.case.validation.ngValidInput', [])
    .directive('ngValidInput', NgValidInput);

/**
    * @ngdoc function
    * @name validate
    * @description validate input value
    * @param {$scope} $scope
    * @param {ngModel} ngModel
    * @param {value} value
    */
var validate = function ($scope, ngModel, value) {
    var rules = $scope.data.rules;
    // go through rules and validate
    for (var i = 0; i < rules.length; i++) {
      var element = rules[i];
      switch (element.name) {
        case 'required':
          ngModel.$setValidity('required', ($scope.show ? (value ? true : false) : true));
          break;
        case 'minlength':
          ngModel.$setValidity('minlength',
                               (value && $scope.show) ? (element.value > value.length ? false : true) : true);
          break;
        case 'maxlength':
          ngModel.$setValidity('maxlength',
                               (value && $scope.show) ? (element.value < value.length ? false : true) : true);
          break;
        case 'alphanum':
          var regexpAlphNum = /^[a-z0-9]+$/i;
          ngModel.$setValidity('alphanum',
                               (value && $scope.show) ? (value.match(regexpAlphNum) === null ? false : true) : true);
          break;
        case 'url':
          var regexpUrl = /((([A-Za-z]{3,9}:(?:\/\/)?)(?:[\-;:&=\+\$,\w]+@)?[A-Za-z0-9\.\-]+|(?:www\.|[\-;:&=\+\$,\w]+@)[A-Za-z0-9\.\-]+)((?:\/[\+~%\/\.\w\-_]*)?\??(?:[\-\+=&;%@\.\w_]*)#?(?:[\.\!\/\\\w]*))?)/; // jshint ignore:line
          ngModel.$setValidity('url', (value && $scope.show) ? (value.match(regexpUrl) === null ? false : true) : true);
          break;
        case 'email':
          var regexpEmail = /\S+@\S+\.\S+/;
          ngModel.$setValidity('email',
                               (value && $scope.show) ? (value.match(regexpEmail) === null ? false : true) : true);
          break;
        case 'numeric':
          ngModel.$setValidity('numeric',
                (value && $scope.show) ? (value.match(/^[+-]?[0-9]+([\.\,][0-9]*)?$/) === null ? false : true) : true);
          break;
      }
    }
  };

function NgValidInput() {
  return {
      restrict: 'A',
      require: 'ngModel',
      link: function ($scope, $element, $attrs, ngModel) {
          // validate on show change
          $scope.$watch('show', function () {
              validate($scope, ngModel, $scope.ngModel);
            });
          // validate on value change
          $scope.$watch($attrs.ngModel, function () {
              validate($scope, ngModel, $scope.ngModel);
            });
        }
    };
}
})();
