(function () {
  'use strict';

  /**
   * @ngdoc overview
   * @name NgValidDropdown
   * @description
   * directive that validate dropdown field
   *
   * case/directives/validations/drct.ngValidDropdown.js
   *
   * (c) 2015 Comland d.o.o. http://www.comland.si
   */

  angular
        .module('SmartForms.case.validation.ngValidDropdown', [])
        .directive('ngValidDropdown', NgValidDropdown);

  /**
   * @ngdoc function
   * @name validate
   * @description validate input value
   * @param {scope} $scope
   * @param {ngModel} ngModel
   * @param {value} value
   */
  var validate = function ($scope, ngModel, value) {

    var rules = $scope.data.rules;

    // go through rules and validate
    for (var i = 0; i < rules.length; i++) {
      var element = rules[i];
      switch (element.name) {
        case 'required':
          ngModel.$setValidity('required', $scope.show ? (value ? true : false) : true);
          break;
        case 'minselect':
          ngModel.$setValidity('minselect', value ? (value.length < element.value ? false : true) : true);
          break;
        case 'maxselect':
          ngModel.$setValidity('maxselect', value ? (value.length > element.value ? false : true) : true);
          break;
      }
    }
  };

  function NgValidDropdown() {
    return {
      restrict: 'A',
      require: 'ngModel',
      link: function ($scope, $element, $attrs, ngModel) {
        // validate on show change
        $scope.$watch('show', function () {
          validate($scope, ngModel, $scope.ngModel);
        });
        // validate on value change
        $scope.$watch($attrs.ngModel, function () {
          validate($scope, ngModel, $scope.ngModel);
        });
      }
    };
  }
})();
