(function () {
'use strict';

/**
      * @ngdoc overview
      * @name NgValidTime
      * @description
      * directive that validate time field
      *
      * case/directives/validations/drct.ngValidTime.js
      *
      * (c) 2015 Comland d.o.o. http://www.comland.si
      */

angular
    .module('SmartForms.case.validation.ngValidTime', [])
    .directive('ngValidTime', NgValidTime);

/**
      * @ngdoc function
      * @name validate
      * @description validate time value
      * @param {$scope} $scope
      * @param {ngModel} ngModel
      * @param {value} value
      */
var validate = function ($scope, ngModel, value) {
    if (!$scope.data.parentId) {
      if (!$scope.data.parentId) {
        var rules = $scope.data.rules;
        // go through rules and validate
        for (var i = 0; i < rules.length; i++) {
          var element = rules[i];
          var t1 = moment(value).format('HH:mm');
          var t2 = element.value;
          switch (element.name) {
            case 'required':
              ngModel.$setValidity('required', $scope.show ? (value ? true : false) : true);
              break;
            case 'mintime':
              ngModel.$setValidity('mintime', value ? (t1 >= t2 ? true : false) : true);
              break;
            case 'maxtime':
              ngModel.$setValidity('maxtime', value ? (t1 <= t2 ? true : false) : true);
              break;
          }
        }
      }
    }
  };

function NgValidTime() {
  return {
      restrict: 'A',
      require: 'ngModel',
      link: function ($scope, $element, $attrs, ngModel) {
          // validate on show change
          $scope.$watch('show', function () {
              validate($scope, ngModel, $scope.ngModel);
            });
          // validate on value change
          $scope.$watch($attrs.ngModel, function () {
              validate($scope, ngModel, $scope.ngModel);
            });
        }
    };
}
})();
