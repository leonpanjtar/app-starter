(function () {
  'use strict';

  /**
    * @ngdoc overview
    * @name NgValidSignature
    * @description
    * directive that validate signature field
    *
    * case/directives/validations/drct.ngValidSignature.js
    *
    * (c) 2015 Comland d.o.o. http://www.comland.si
    */

  angular
      .module('SmartForms.case.validation.ngValidSignature', [])
      .directive('ngValidSignature', NgValidSignature);

  /**
    * @ngdoc function
    * @name validate
    * @description validate signature value
    * @param {$scope} $scope
    * @param {ngModel} ngModel
    * @param {value} value
    */
  var validate = function ($scope, ngModel, value) {
      var rules, i, element;
      if (!$scope.data.parentId) {
        rules = $scope.data.rules;
        // go through rules and validate
        for (i = 0; i < rules.length; i++) {
          element = rules[i];
          switch (element.name) {
            case 'required':
              ngModel.$setValidity('required', $scope.show ? (value ? true : false) : true);
            break;
          }
        }
      } else {
        rules = $scope.data.rules;
        // go through rules and validate
        for (i = 0; i < rules.length; i++) {
          element = rules[i];
          switch (element.name) {
            case 'required':
              ngModel.$setValidity('required', $scope.show ? (value ? true : false) : true);
            break;
          }
        }
      }
    };

  function NgValidSignature() {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function ($scope, $element, $attrs, ngModelCtrl) {
            // validate on show change
            $scope.$watch('show', function () {
                validate($scope, ngModelCtrl, $scope.ngModel);
              });
            // validate on value change
            $scope.$watch('ngModel', function () {
                validate($scope, ngModelCtrl, $scope.ngModel);
              });
          }
      };
  }
})();
