(function () {
  'use strict';

  /**
    * @ngdoc overview
    * @name NgValidRadio
    * @description
    * directive that validate radio field
    *
    * case/directives/validations/drct.ngValidRadio.js
    *
    * (c) 2015 Comland d.o.o. http://www.comland.si
    */

  angular
      .module('SmartForms.case.validation.ngValidRadio', [])
      .directive('ngValidRadio', NgValidRadio);

  /**
    * @ngdoc function
    * @name validate
    * @description validate radio value
    * @param {$scope} $scope
    * @param {ngModel} ngModel
    * @param {value} values
    */
  var validate = function ($scope, ngModel, values) {
      var rules, i, element;
      if (!$scope.data.parentId) {
        rules = $scope.data.rules;
        // go through rules and validate
        for (i = 0; i < rules.length; i++) {
          element = rules[i];
          switch (element.name) {
            case 'required':
              ngModel.$setValidity('required', $scope.show ? (values ? true : false) : true);
            break;
          }
        }
      } else {
        rules = $scope.data.rules;
        // go through rules and validate
        for (i = 0; i < rules.length; i++) {
          element = rules[i];
          switch (element.name) {
            case 'required':
              ngModel.$setValidity('required', $scope.show ? (values ? true : false) : true);
            break;
          }
        }
      }
    };

  function NgValidRadio() {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function ($scope, $element, $attrs, ngModelCtrl) {
            // validate on show change
            $scope.$watch('show', function () {
                validate($scope, ngModelCtrl, $scope.ngModel);
              });
            // validate on value change
            $scope.$watch('ngModel', function () {
                validate($scope, ngModelCtrl, $scope.ngModel);
              });
          }
      };
  }
})();
