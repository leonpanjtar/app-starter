(function () {
  'use strict';

  /**
    * @ngdoc overview
    * @name NgValidCheckbox
    * @description
    * directive that validate checkbox field
    *
    * case/directives/validations/drct.ngValidCheckbox.js
    *
    * (c) 2015 Comland d.o.o. http://www.comland.si
    */

  angular
      .module('SmartForms.case.validation.ngValidCheckbox', [])
      .directive('ngValidCheckbox', NgValidCheckbox);

  /**
    * @ngdoc function
    * @name validate
    * @description validate checkbox value
    * @param {$scope} $scope
    * @param {ngModel} ngModel
    * @param {value} values
    */
  var validate = function ($scope, ngModel, values) {
      var rules, i, element;
      if (!$scope.data.parentId) {
        rules = $scope.data.rules;
        // go through rules and validate
        for (i = 0; i < rules.length; i++) {
          element = rules[i];
          switch (element.name) {
            case 'required':
              ngModel.$setValidity('required', $scope.show ? (values ? (values.length ? true : false) : true) : true);
              break;
            case 'minvalues':
              ngModel.$setValidity('minvalues',
                                   ($scope.show && values) ? (values.length < element.value ? false : true) : true);
              break;
            case 'maxvalues':
              ngModel.$setValidity('maxvalues',
                                   ($scope.show && values) ? (values.length > element.value ? false : true) : true);
              break;
          }
        }
      } else {
        rules = $scope.data.rules;
        // go through rules and validate
        for (i = 0; i < rules.length; i++) {
          element = rules[i];
          switch (element.name) {
            case 'required':
              ngModel.$setValidity('required', $scope.show ? (values ? (values.length ? true : false) : true) : true);
              break;
            case 'minvalues':
              ngModel.$setValidity('minvalues',
                                   ($scope.show && values) ? (values.length < element.value ? false : true) : true);
              break;
            case 'maxvalues':
              ngModel.$setValidity('maxvalues',
                                   ($scope.show && values) ? (values.length > element.value ? false : true) : true);
              break;

          }
        }
      }
    };

  function NgValidCheckbox() {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function ($scope, $element, $attrs, ngModelCtrl) {
            // validate on show change
            $scope.$watch('show', function () {
                validate($scope, ngModelCtrl, $scope.ngModel);
              });
            // validate on value change
            $scope.$watch('ngModel', function () {
                validate($scope, ngModelCtrl, $scope.ngModel);
              });
          }
      };
  }
})();
