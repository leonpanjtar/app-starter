(function () {
'use strict';

/**
     * @ngdoc overview
     * @name NgValidInput
     * @description
     * directive that validate input field
     *
     * case/directives/validations/drct.ngValidInput.js
     *
     * (c) 2015 Comland d.o.o. http://www.comland.si
     */

angular
    .module('SmartForms.case.validation.ngValidTable', [])
    .directive('ngValidTable', ngValidTable);

/**
    * @ngdoc function
    * @name validate
    * @description validate input value
    * @param {$scope} $scope
    * @param {ngModel} ngModel
    * @param {value} value
    */
var validate = function ($scope, ngModel, value) {
  var rules = $scope.data.rules;
  // go through rules and validate
  for (var i = 0; i < rules.length; i++) {
    var element = rules[i];
    switch (element.name) {
      case 'required':
        ngModel.$setValidity('required', ($scope.show ? (value ? (value.length ? true : false) : false) : true));
        break;
      case 'minvalues':
        ngModel.$setValidity('minvalues',
                             (value && $scope.show) ? (element.value > value.length ? false : true) : true);
        break;
      case 'maxvalues':
        ngModel.$setValidity('maxvalues',
                             (value && $scope.show) ? (element.value < value.length ? false : true) : true);
        break;
    }
  }
};

function ngValidTable() {
  return {
      restrict: 'A',
      require: 'ngModel',
      link: function ($scope, $element, $attrs, ngModel) {
          // validate on value change
          $scope.$watch('ngModel', function () {
              validate($scope, ngModel, $scope.$parent.service.values[$scope.data.guid]);
            });
          $scope.$on($scope.data.guid + '-change', function () {
              validate($scope, ngModel, $scope.$parent.service.values[$scope.data.guid]);
            });
        }
    };
}
})();
