(function () {
'use strict';

/**
      * @ngdoc overview
      * @name NgValidDate
      * @description
      * directive that validate date field
      *
      * case/directives/validations/drct.ngValidDate.js
      *
      * (c) 2015 Comland d.o.o. http://www.comland.si
      */

angular
    .module('SmartForms.case.validation.ngValidDate', [])
    .directive('ngValidDate', NgValidDate);

/**
     * @ngdoc function
     * @name validate
     * @description validate date value
     * @param {$scope} $scope
     * @param {ngModel} ngModel
     * @param {value} value
     */
var validate = function ($scope, ngModel, value) {
    if (!$scope.data.parentId) {
      var rules = $scope.data.rules;
      // go through rules and validate
      for (var i = 0; i < rules.length; i++) {
        var element = rules[i];
        var d1 = moment(value).format('YYYY-MM-DD');
        var d2 = element.value;
        switch (element.name) {
          case 'required':
            ngModel.$setValidity('required', $scope.show ? (value ? true : false) : true);
            break;
          case 'mindate':
            ngModel.$setValidity('mindate',
                                 ($scope.show && value) ? (moment(d1).isSameOrAfter(d2) ? true : false) : true);
            break;
          case 'maxdate':
            ngModel.$setValidity('maxdate',
                                 ($scope.show && value) ? (moment(d1).isBefore(d2) ? true : false) : true);
            break;
        }
      }
    }
  };

function NgValidDate() {
  return {
    restrict: 'A',
    require: 'ngModel',
    link: function ($scope, $element, $attrs, ngModel) {
        // validate on show change
        $scope.$watch('show', function () {
            validate($scope, ngModel, $scope.ngModel);
          });
        // validate on value change
        $scope.$watch($attrs.ngModel, function () {
            validate($scope, ngModel, $scope.ngModel);
          });
      }
  };
}

})();
