(function () {
  'use strict';

  /***
   * @ngdoc directive
   * @name MightyFields Home screen Case Directive
   *
   */
  angular
    .module('SmartForms.case.directives.case', [])
    .directive('casedirective', casedirective);

  function casedirective() {

    return {

      restrict: 'E',
      scope: true,
      templateUrl: 'js/case/directives/tpls/part.caseDrct.html',
      link: function (scope, element, attrs) {

        /**
         * @ngdoc property
         * @name scope.data
         * @description This is data passed in directive 'case-directive' as 'data' attribute.
         * We use this property/value to show data in directive. It holds data of a specific case.
        */
        scope.data = scope.$eval(attrs.data);
      }
    };

  }

})();
