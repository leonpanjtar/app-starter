(function () {
'use strict';

/***
     * @ngdoc directive
     * @name Smart Forms Plugify Directive
     *
     * Plugin compiler directive takes care of dinamically inserting
     * plugins into the DOM.
     */
angular
    .module('SmartForms.case.directives.multiselectDropdown', [])
    .directive('multiselectDropdown', MultiselectDropdown);

function MultiselectDropdown() {
  return {
      restrict: 'A',
      scope: true,
      link: function () {

      }
    };
}

})();
