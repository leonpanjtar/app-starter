(function () {
  'use strict';

  /**
   * @ngdoc overview
   * @name InputDirective
   * @description
   * directive that renders an input box
   *
   * Example
   * ===================================
   *
   *
   *
   *
   * case/directives/drct.input.js
   *
   * (c) 2015 Comland d.o.o. http://www.comland.si
   */

  angular
    .module('SmartForms.case.directives.description', [])
    .directive('sfDescription', sfDescription);

  sfDescription.$inject = [
    'CurrentService'
  ];

  function sfDescription(CurrentService) {

    /* ==============================================
     * == PRIVATE VARS & FUNCTIONS
     * ============================================== */
    var compiled = CurrentService.getFieldTemplate('js/case/directives/tpls/part.sfDescription.html');

    /* =====================================
     * = DIRECTIVE
     * ===================================== */
    return {
      restrict      : 'E',
      scope         : {},
      link          : function (scope, element, attrs) {

        /* ==============================================
         * == INITIALIZE
         * ============================================== */

        // Evaluate input data
        scope.data              = scope.$eval(attrs.data);
        scope.show              = CurrentService.isShown(scope.data.deps);
        scope.parentGuid        = CurrentService.getParentGuid(scope.data.parentId);

        /* ==============================================
         * == WATCHERS AND EVENTS
         * ============================================== */

        // Set dependencies events wathcers
        CurrentService.setDeps(scope.data.deps, scope);

        compiled(scope, function(clonedElement) {
          element.append(clonedElement);
        });
      }
    };

  }

})();
