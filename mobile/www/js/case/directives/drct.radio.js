(function () {
  'use strict';

  /**
   * @ngdoc overview
   * @name CheckboxDirective
   * @description
   * directive that renders an Checkbox
   *
   * Example
   * ===================================
   *
   *
   *
   *
   * case/directives/drct.checkbox.js
   *
   * (c) 2015 Comland d.o.o. http://www.comland.si
   */

  angular
    .module('SmartForms.case.directives.radio', [])
    .directive('sfRadio', sfRadio);

  sfRadio.$inject = [
    'CurrentService',
    'sfUtil',
  ];

  function sfRadio(CurrentService, sfUtil) {

    /* ==============================================
     * == PRIVATE VARS & FUNCTIONS
     * ============================================== */
    var compiled = CurrentService.getFieldTemplate('js/case/directives/tpls/part.sfRadio.html');

    /* =====================================
     * = DIRECTIVE
     * ===================================== */
    return {
      restrict      : 'E',
      require       : 'ngModel',
      scope         : {ngModel: '='},
      link          : function (scope, element, attrs) {

        /* ==============================================
         * == INITIALIZE
         * ============================================== */

        scope.service           = CurrentService;

        // Evaluate input data
        scope.data              = scope.$eval(attrs.data);

        // Prepare field data
        scope.required          = sfUtil.findProp(scope.data.rules, 'required') ? true : false;
        scope.inline            = sfUtil.findProp(scope.data.props, 'displayMode') === 'inline' ? true : false;
        scope.minlengthValue    = sfUtil.ruleValue(scope.data.rules, 'minlength');
        scope.maxlengthValue    = sfUtil.ruleValue(scope.data.rules, 'maxlength');
        scope.show              = CurrentService.isShown(scope.data.deps);
        scope.closed            = CurrentService.getCasesClosedValue();
        scope.parentGuid        = CurrentService.getParentGuid(scope.data.parentId);

        /**
         *  Here we check if data of particular scope.data.guid exists in
         *  CurrentService.values if so then we give this value to scope.ngModel
         *  ( this is particulary useful in case of edit/preview )
         */
        if (CurrentService.values && CurrentService.values[scope.data.guid]) {
          scope.ngModel = CurrentService.values[scope.data.guid];
        }

        /* ==============================================
         * == PRIVATE FUNCTIONS
         * ============================================== */

        /**
         * @ngdoc function
         * @name matchNgModel
         * @description Checks the selected options
         * @param {array} items
         */
        var matchNgModel = function() {
          var match, i, len;

          // Check if ngModel exists and has values
          if (scope.ngModel && scope.ngModel.length > 0) {
            if (scope.ngModel[0]) {
              // Check if first entry in ngModel has property Value
              // If it has it we assume other entries also have it
              if (scope.ngModel[0].hasOwnProperty('Value')) {
                for (i = 0, len = scope.data.dataSource.length; i < len; i++) {
                  match = _.findWhere(scope.ngModel, {Value : scope.data.dataSource[i].Value});
                  scope.data.dataSource[i].Checked = match ? match.Value : false;
                }
              }
              // If first entry does not have property Value we assume ngModel is
              // a string
              else {
                for (i = 0, len = scope.data.dataSource.length; i < len; i++) {
                  // jscs:disable requireCamelCaseOrUpperCaseIdentifiers
                  match = scope.data.dataSource[i].Id_CodeItem === scope.ngModel;
                  // jscs:enable requireCamelCaseOrUpperCaseIdentifiers
                  scope.data.dataSource[i].Checked = match ? scope.data.dataSource[i].Value : false;
                }
              }
            }
            // Put ngmodel in array if not
            else if (scope.ngModel instanceof Object) {
              for (i = 0, len = scope.data.dataSource.length; i < len; i++) {
                match = _.findWhere(scope.ngModel, {Value : scope.data.dataSource.Value});
                scope.data.dataSource[i].Checked = match ? match.Value : false;
              }
            }
          }
          // Put ngmodel in array if not
          else if (scope.ngModel instanceof Object) {
            for (i = 0, len = scope.data.dataSource.length; i < len; i++) {
              match = scope.data.dataSource[i].Label === scope.ngModel.Label;
              scope.data.dataSource[i].Checked = match ? scope.data.dataSource[i].Value : false;
            }
          }
          // If it does not exist or does not have any values set all to false
          else {
            for (i = 0, len = scope.data.dataSource.length; i < len; i++) {
              scope.data.dataSource[i].Checked = false;
            }
          }
        };

        /* ==============================================
         * == PUBLIC FUNCTIONS
         * ============================================== */

        /**
          * @ngdoc function
          * @name saveValue
          * @description Save value to global procedure service and notify about change
          * @param {object} item
          */
        scope.saveValue = function (item) {
          scope.ngModel = [item];
          matchNgModel();
          CurrentService.saveValue(scope.data.parentId, scope.data.id, scope.ngModel, scope.data.guid);
        };

        /**
         * @ngdoc function
         * @name $scope.openPopover
         * @description open hint popover
         */
        scope.openPopover = function ($event) {
          sfUtil.openPopup($event, scope);
        };

        /**
         * @ngdoc function
         * @name $scope.closePopover
         * @description close hint popover
         */
        scope.closePopover = function () {
          sfUtil.closePopup(scope);
        };

        /**
         * @ngdoc function
         * @name scope.resetToDefault
         * @description resets to default value
         */
        scope.resetToDefault = function () {
          scope.ngModel = sfUtil.findProp(scope.data.props, 'default');
          matchNgModel();
          scope.saveValue();
        };

        /* ==============================================
         * == WATCHERS AND EVENTS
         * ============================================== */

        // Set dependencies events wathcers
        CurrentService.setDeps(scope.data.deps, scope);

        // Match ngModel to selection
        matchNgModel();

        // On table reset match ngModel to selection
        scope.$on('table-reset', function (event, data) {
          if (data.id === scope.data.parentId) {
            scope.resetToDefault();
          }
        });

        // Disable field on case close
        scope.$on('CaseClosed', function () {
          scope.closed = true;
        });

        // Remove hint popover on destroy
        scope.$on('$destroy', function () {
          if (scope.popover) {
            scope.popover.remove();
          }
        });

        // Once case is opened save default value
        /*scope.$on('case-open', function () {
          var item = _.find(scope.data.dataSource, function (data) {
            return data.Checked != false;
          });
          if (item) {
            scope.saveValue(item);
          }
        });*/

        // Reuse compiled template and append scope
        compiled(scope, function(clonedElement) {
          element.append(clonedElement);
        });

      }
    };

  }

})();
