(function () {
  'use strict';

  /**
   * @ngdoc overview
   * @name InputDirective
   * @description
   * directive that renders an input box with barcode reader
   *
   * Example
   * ===================================
   *
   *
   *
   *
   * case/directives/drct.input.js
   *
   * (c) 2015 Comland d.o.o. http://www.comland.si
   */

  angular
    .module('SmartForms.case.directives.barcode', [])
    .directive('sfBarcode', sfBarcode);

  sfBarcode.$inject = [
    '$rootScope',
    '$cordovaBarcodeScanner',
    'sfUtil',
    'CurrentService'
  ];

  function sfBarcode($rootScope, $cordovaBarcodeScanner, sfUtil, CurrentService) {

    /* ==============================================
     * == PRIVATE VARS & FUNCTIONS
     * ============================================== */
    var compiled = CurrentService.getFieldTemplate('js/case/directives/tpls/part.sfBarcode.html');

    /* =====================================
     * = DIRECTIVE
     * ===================================== */
    return {
      restrict      : 'E',
      require       : 'ngModel',
      scope         : {ngModel: '='},
      link          : function (scope, element, attrs) {

        /* ==============================================
         * == INITIALIZE
         * ============================================== */

        scope.service           = CurrentService;

        // Evaluate input data
        scope.data              = scope.$eval(attrs.data);

        // Prepare field data
        scope.required          = sfUtil.findProp(scope.data.rules, 'required') ? true : false;
        scope.minlengthValue   = sfUtil.ruleValue(scope.data.rules, 'minlength');
        scope.maxlengthValue   = sfUtil.ruleValue(scope.data.rules, 'maxlength');
        scope.show              = CurrentService.isShown(scope.data.deps);
        scope.closed            = CurrentService.getCasesClosedValue();
        scope.parentGuid        = CurrentService.getParentGuid(scope.data.parentId);

        /* ==============================================
         * == PUBLIC FUNCTIONS
         * ============================================== */
        // TODO(anyone): add documentation
        scope.scanBarcode = function () {
          $cordovaBarcodeScanner
            .scan()
            .then(function (imageData) {
              scope.ngModel = imageData.text;
            },
            function (error) {
              console.log('An error happened -> ' + error);
            });
        };

        /**
         * @ngdoc function
         * @name $scope.openPopover
         * @description open hint popover
         */
        scope.openPopover = function ($event) {
          sfUtil.openPopup($event, scope);
        };

        /**
         * @ngdoc function
         * @name $scope.closePopover
         * @description close hint popover
         */
        scope.closePopover = function () {
          sfUtil.closePopup(scope);
        };

        scope.fieldFocused = function($event) {
          $rootScope.$broadcast('fieldFocused', $event);
        };

        /**
         * @ngdoc function
         * @name scope.resetToDefault
         * @description resets to default value
         */
        scope.resetToDefault = function () {
          scope.ngModel = sfUtil.findProp(scope.data.props, 'default');
          CurrentService.saveValue(scope.data.parentId, scope.data.id, scope.ngModel, scope.data.guid);
        };

        /* ==============================================
         * == WATCHERS AND EVENTS
         * ============================================== */

        // Set dependencies events wathcers
        CurrentService.setDeps(scope.data.deps, scope);

        // Disable field on case close
        scope.$on('CaseClosed', function () {
          scope.closed = true;
        });

        // Remove hint popover on destroy
        scope.$on('$destroy', function () {
          if (scope.popover) {
            scope.popover.remove();
          }
        });

        // On table reset match ngModel to selection
        scope.$on('table-reset', function(event, data) {
          if (data.id === scope.data.parentId) {
            scope.resetToDefault();
          }
        });

        // Reuse compiled template and append scope
        compiled(scope, function(clonedElement) {
          element.append(clonedElement);
        });

      }
    };

  }

})();
