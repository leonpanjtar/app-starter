(function () {
  'use strict';

  /**
   * @ngdoc overview
   * @name ImageDirective
   * @description
   * directive that renders an input box with file picker button
   *
   * Example
   * ===================================
   *
   * case/directives/drct.image.js
   *
   * (c) 2015 Comland d.o.o. http://www.comland.si
   */

  angular
    .module('SmartForms.case.directives.file', [])
    .directive('sfFile', sfFile);

  sfFile.$inject = [
    '$cordovaFile',
    '$ionicActionSheet',
    'ImageService',
    'sfUtil',
    'CurrentService',
    'SweetAlert',
    'gettextCatalog'
  ];

  function sfFile($cordovaFile, $ionicActionSheet, ImageService, sfUtil, CurrentService, SweetAlert, gettextCatalog) {

    /* ==============================================
     * == PRIVATE VARS & FUNCTIONS
     * ============================================== */
    var compiled = CurrentService.getFieldTemplate('js/case/directives/tpls/part.sfFile.html');

    /* =====================================
     * = DIRECTIVE
     * ===================================== */
    return {
      restrict      : 'E',
      require       : 'ngModel',
      scope         : {ngModel: '='},
      link          : function (scope, element, attrs) {

        /* ==============================================
         * == INITIALIZE
         * ============================================== */

        scope.service           = CurrentService;

        // Evaluate input data
        scope.data              = scope.$eval(attrs.data);

        // Prepare field data
        scope.required          = sfUtil.findProp(scope.data.rules, 'required') ? true : false;
        scope.minlengthValue   = sfUtil.ruleValue(scope.data.rules, 'minlength');
        scope.maxlengthValue   = sfUtil.ruleValue(scope.data.rules, 'maxlength');
        scope.show              = CurrentService.isShown(scope.data.deps);
        scope.closed            = CurrentService.getCasesClosedValue();

        scope.ngModel           = [];
        scope.dataDirectory     = cordova.file.dataDirectory;

        /**
         *  Here we check if data of particular scope.data.guid exists in
         *  CurrentService.values if so then we give this value to scope.ngModel
         *  ( this is particulary useful in case of edit/preview )
         */
        if (CurrentService.values && CurrentService.values[scope.data.guid]) {
          scope.ngModel = CurrentService.values[scope.data.guid];
        }

        /* ==============================================
         * == PUBLIC FUNCTIONS
         * ============================================== */

        /**
         * @ngdoc function
         * @name addMedia
         * @description Launch media adding popup
         * @param {object} form
         */
        scope.addMedia = function () {
          if (typeof Camera !== 'undefined') {
            scope.hideSheet = $ionicActionSheet.show({
              buttons: [
                {text: gettextCatalog.getString('Take photo')},
                {text: gettextCatalog.getString('Photo from library')}
              ],
              titleText: gettextCatalog.getString('Add images'),
              cancelText: gettextCatalog.getString('Cancel'),
              buttonClicked: function(index) {
                scope.addImage(index);
              }
            });
          } else {
            SweetAlert.swal(gettextCatalog.getString('Ooops...'),
                            gettextCatalog.getString('Camera is not available on this device'),
                            'error');
          }
        };

        /**
         * @ngdoc function
         * @name addImage
         * @description Add image to ngModel
         * @param {object} type
         */
        scope.addImage = function(type) {
          scope.hideSheet();
          ImageService.handleMediaDialog(type).then(function(image) {
            scope.ngModel.push({
              name      : image.info.name,
              source    : scope.dataDirectory + image.info.name
            });
            console.log(scope.ngModel);
            CurrentService.saveValue(scope.data.parentId, scope.data.id, scope.ngModel, scope.data.guid);
          });
        };

        /**
         * @ngdoc function
         * @name removeImage
         * @description Remove image from ngModel
         * @param {object} index
         */
        scope.removeImage = function(index) {
          scope.ngModel.splice(index, 1);
          CurrentService.saveValue(scope.data.parentId, scope.data.id, scope.ngModel, scope.data.guid);
        };

        /* ==============================================
         * == WATCHERS AND EVENTS
         * ============================================== */

        // Set dependencies events wathcers
        CurrentService.setDeps(scope.data.deps, scope);

        // Disable field on case close
        scope.$on('CaseClosed', function () {
          scope.closed = true;
        });

        // Remove hint popover on destroy
        scope.$on('$destroy', function () {
          if (scope.popover) {
            scope.popover.remove();
          }
        });

        //reset ngModel on table-reset
        scope.$on('table-reset', function (event, data) {
          if (data.id === scope.data.parentId) {
            scope.ngModel = [];
          }
        });

        // Reuse compiled template and append scope
        compiled(scope, function(clonedElement) {
          element.append(clonedElement);
        });

      }
    };

  }

})();
