(function () {
  'use strict';

  /**
   * @ngdoc overview
   * @name SignatureDirective
   * @description
   * directive that renders a signature box
   *
   * Example
   * ===================================
   *
   *
   *
   *
   * case/directives/drct.signature.js
   *
   * (c) 2015 Comland d.o.o. http://www.comland.si
   */

  angular
    .module('SmartForms.case.directives.signature', [])
    .directive('sfSignature', sfSignature);

  sfSignature.$inject = [
    '$rootScope',
    'CurrentService',
    'sfUtil',
    '$ionicPopup',
    'gettextCatalog',
  ];

  function sfSignature($rootScope, CurrentService, sfUtil, $ionicPopup, gettextCatalog) {

    /* ==============================================
     * == PRIVATE VARS & FUNCTIONS
     * ============================================== */
    var compiled = CurrentService.getFieldTemplate('js/case/directives/tpls/part.sfSignature.html');

    /* =====================================
     * = DIRECTIVE
     * ===================================== */
    return {
      restrict      : 'E',
      require       : 'ngModel',
      scope         : {ngModel: '='},
      link          : function (scope, element, attrs) {

        /* ==============================================
         * == INITIALIZE
         * ============================================== */

        scope.service           = CurrentService;

        // Evaluate input data
        scope.data              = scope.$eval(attrs.data);

        // Prepare field data
        scope.required          = sfUtil.findProp(scope.data.rules, 'required') ? true : false;
        scope.show              = CurrentService.isShown(scope.data.deps);
        scope.closed            = CurrentService.getCasesClosedValue();
        scope.parentGuid        = CurrentService.getParentGuid(scope.data.parentId);
        scope.signature         = {image: ''};

        /**
         *  Here we check if data of particular scope.data.guid exists in
         *  CurrentService.values if so then we give this value to scope.ngModel
         *  ( this is particulary useful in case of edit/preview )
         */
        if (CurrentService.values && CurrentService.values[scope.data.guid]) {
          scope.ngModel = CurrentService.values[scope.data.guid];
        }

        /* ==============================================
         * == PUBLIC FUNCTIONS
         * ============================================== */

        /**
         * @ngdoc function
         * @name saveValue
         * @description Save value to global procedure service and notify about change
         * @param {object} signature
         */
        scope.saveValue = function (signature) {
          scope.ngModel = signature;
          angular.element('#' + scope.data.guid).controller('ngModel').$setDirty(true);
          CurrentService.saveValue(scope.data.parentId, scope.data.id, scope.ngModel, scope.data.guid);
        };

        scope.clear = function () {
          scope.saveValue('');
        };

        /**
         * @ngdoc function
         * @name saveValue
         * @description Shows signature popup, triggered on a button click
         * @param {object} field
         */
        scope.showPopup = function () {

          // An elaborate, custom popup
          var signaturePopup = $ionicPopup.show({
            template    : '<div sig-popup ng-model="signature.image"></div>',
            title       : scope.data.name,
            subTitle    : gettextCatalog.getString('For better signature accuracy, please use a pen.'),
            cssClass    : 'signaturePad',
            scope       : scope,
            buttons     : [
              {
                text    : gettextCatalog.getString('Cancel'),
                type    : 'button button-light',
              },
              {
                text    : gettextCatalog.getString('Clear'),
                type    : 'button button-light',
                onTap   : function (e) {
                  $rootScope.$broadcast('clear-signature');
                  e.preventDefault();
                }
              }, {
                text    : gettextCatalog.getString('Save'),
                type    : 'button button-custom',
                onTap   : function (e) {
                  if (!scope.signature.image) {
                    e.preventDefault();
                  }
                  else {
                    return scope.signature.image;
                  }
                }
              }
            ]
          });

          signaturePopup.then(function (signature) {
            scope.saveValue(signature);
          });
        };

        /**
         * @ngdoc function
         * @name $scope.openPopover
         * @description open hint popover
         */
        scope.openPopover = function ($event) {
          sfUtil.openPopup($event, scope);
        };

        /**
         * @ngdoc function
         * @name $scope.closePopover
         * @description close hint popover
         */
        scope.closePopover = function () {
          sfUtil.closePopup(scope);
        };

        /* ==============================================
         * == WATCHERS AND EVENTS
         * ============================================== */

        // Set dependencies events wathcers
        CurrentService.setDeps(scope.data.deps, scope);

        // Disable field on case close
        scope.$on('CaseClosed', function () {
          scope.closed = true;
        });

        // Clear ngModel when table row is added
        scope.$on(scope.parentGuid + '-added', function () {
          CurrentService.tableTemp[scope.parentGuid][scope.data.guid] = undefined;
        });

        // Remove hint popover on destroy
        scope.$on('$destroy', function () {
          if (scope.popover) {
            scope.popover.remove();
          }
        });

        // Reuse compiled template and append scope
        compiled(scope, function(clonedElement) {
          element.append(clonedElement);
        });

      }
    };

  }

})();
