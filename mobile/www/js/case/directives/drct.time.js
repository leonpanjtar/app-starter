(function () {
  'use strict';

  /**
   * @ngdoc overview
   * @name InputDirective
   * @description
   * directive that renders an input box
   *
   * Example
   * ===================================
   *
   *
   *
   *
   * case/directives/drct.input.js
   *
   * (c) 2015 Comland d.o.o. http://www.comland.si
   */

  angular
    .module('SmartForms.case.directives.time', [])
    .directive('sfTime', sfTime);

  sfTime.$inject = [
    '$rootScope',
    'sfUtil',
    'CurrentService'
  ];

  function sfTime($rootScope, sfUtil, CurrentService) {

    /* ==============================================
     * == PRIVATE VARS & FUNCTIONS
     * ============================================== */
    var compiled = CurrentService.getFieldTemplate('js/case/directives/tpls/part.sfTime.html');

    var pad = function (n, width, z) {
      z = z || '0';
      n = n + '';
      return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
    };

    /* =====================================
     * = DIRECTIVE
     * ===================================== */

    return {
      restrict: 'E',
      require: 'ngModel',
      scope: {ngModel: '='},
      link: function (scope, element, attrs) {

        /* ==============================================
         * == INITIALIZE
         * ============================================== */

        scope.service       = CurrentService;

        // Evaluate input data
        scope.data          = scope.$eval(attrs.data);

        // Prepare field data
        scope.required      = sfUtil.findProp(scope.data.rules, 'required') ? true : false;
        scope.mask          = sfUtil.findProp(scope.data.props, 'mask');
        scope.mintimeValue = sfUtil.ruleValue(scope.data.rules, 'mintime');
        scope.maxtimeValue = sfUtil.ruleValue(scope.data.rules, 'maxtime');
        scope.show          = CurrentService.isShown(scope.data.deps);
        scope.closed        = CurrentService.getCasesClosedValue();
        scope.parentGuid    = CurrentService.getParentGuid(scope.data.parentId);

        /**
         *  Here we check if data of particular scope.data.guid exists in
         *  CurrentService.values if so then we give this value to scope.ngModel
         *  ( this is particulary useful in case of edit/preview )
         */
        if (CurrentService.values && CurrentService.values[scope.data.guid]) {
          scope.ngModel = moment(CurrentService.values[scope.data.guid]).seconds(0).milliseconds(0).toDate();
        }

        /* ==============================================
         * == PUBLIC FUNCTIONS
         * ============================================== */

        /**
         * @ngdoc function
         * @name $scope.isDevice
         * @description check if device is compatible with date directive
         */
        scope.isDevice = function () {
          return sfUtil.DeviceCompatible();
        };

        /**
         * @ngdoc function
         * @name $scope.openPopover
         * @description open hint popover
         */
        scope.openPopover = function ($event) {
          sfUtil.openPopup($event, scope);
        };

        /**
         * @ngdoc function
         * @name $scope.closePopover
         * @description close hint popover
         */
        scope.closePopover = function () {
          sfUtil.closePopup(scope);
        };

        scope.fieldFocused = function($event) {
          $rootScope.$broadcast('fieldFocused', $event);
        };

        /**
         * @ngdoc function
         * @name scope.resetToDefault
         * @description resets to default value
         */
        scope.resetToDefault = function () {
          var def = sfUtil.findProp(scope.data.props, 'default');
          scope.ngModel = (def ? moment(def).seconds(0).milliseconds(0).toDate() :
                                 moment().seconds(0).milliseconds(0).toDate());
          CurrentService.saveValue(scope.data.parentId, scope.data.id, scope.ngModel, scope.data.guid);
        };

        /* ==============================================
         * == WATCHERS AND EVENTS
         * ============================================== */

        // Set dependencies events wathcers
        CurrentService.setDeps(scope.data.deps, scope);

        // Disable field on case close
        scope.$on('CaseClosed', function () {
          scope.closed = true;
        });

        // Remove hint popover on destroy
        scope.$on('$destroy', function () {
          if (scope.popover) {
            scope.popover.remove();
          }
        });

        // On table reset match ngModel to selection
        scope.$on('table-reset', function(event, data) {
          if (data.id === scope.data.parentId) {
            scope.resetToDefault();
          }
        });

        // Reuse compiled template and append scope
        compiled(scope, function (clonedElement) {
          element.append(clonedElement);
        });
      }
    };

  }

})();
