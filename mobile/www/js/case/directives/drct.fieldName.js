(function() {
  'use strict';

  /***
   * @ngdoc directive
   * @name Smart Forms FieldName Directive
   *
   * Fixes Angular not being able to dynamically
   * assign name to a form element
   */
  angular
    .module('SmartForms.case.directives.fieldName', [])
    .directive('fieldName', fieldName);

  function fieldName() {

    return {
        // just postLink
        link: function (scope, element, attrs, ngModelCtrl) {
            // do nothing in case of no 'name' attribiute
            if (!attrs.name) {
              return;
            }
            // fix what should be fixed
            ngModelCtrl.$name = attrs.name;
          },
        // ngModel's priority is 0
        priority: '-100',
        // we need it to fix it's behavior
        require: 'ngModel'
      };

  }

})();
