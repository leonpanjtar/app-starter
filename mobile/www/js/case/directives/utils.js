(function () {
  'use strict';

  /***
   * @ngdoc service
   * @name Smart Forms Utilities
   *
   * @description
   * Helper methods used internally by the directives. Should not be called directly from user code.
   */
  angular
    .module('SmartForms.case.directives.utils', [])
    .factory('sfUtil', [
      '$q',
      '$ionicPopover',
      '$rootScope',
      '$compile',
      '$templateCache',
      function ($q, $ionicPopover) {

        /* ==============================================
         * == INITIALIZE
         * ============================================== */

        var self        = {};

        /* ==============================================
         * == PRIVATE FUNCTIONS
         * ============================================== */

        /**
         * @ngdoc function
         * @name $scope.parseProps
         * @description Prepare field properties
         */
        self.parseProps = function (props, type) {
          var definition = [], p;

          if (type === 'html') {
            return props;
          }
          else {
            // Check if rules are set
            if (!props || props === '') { return false; }

            try {
              // Parse passed field properties
              p = JSON.parse(props);
              //console.log(p, props);

              // Create properties definition
              for (var key in p) {
                if (p.hasOwnProperty(key)) {
                  definition.push({name: key, value: p[key]});
                }
              }
            }
            catch (err) {
              console.log(err);
            }
          }
          return definition;
        };

        /**
         * @ngdoc function
         * @name parseRules
         * @description Parse validation rules list
         */
        self.parseRules = function (rules) {
          var definition = [], p, i, len;

          // Check if rules are set
          if (!rules || rules === '' || rules === null) { return false; }
          try {
            p = rules.split('|');

            for (i = 0, len = p.length; i < len; i++) {

              // Check if we need to parse further - this is for
              // parsing validators with limits - e.g. minlength[2]
              if (/\[[\d-:]+\]/.test(p[i])) {
                definition.push({
                  name: p[i].replace(/\[.*\]/g, ''),
                  value: p[i].replace(/(^.*\[|\].*$)/g, '')}
                );
              }

              // Or just add and set validator to true - this
              // is for regular validators - e.g. required
              else {
                definition.push({
                  name: p[i],
                  value: true
                });
              }

            }
          }
          catch (err) {
            console.log(err);
          }
          return definition;
        };

        /**
         * @ngdoc function
         * @name $scope.findProp
         * @description Find specific field property
         */
        self.findProp = function (props, prop) {
          var data = _.findWhere(props, {name: prop});
          return data ? data.value : undefined;
        };

        /**
         * @ngdoc function
         * @name self.ruleValue
         * @description Find specific rule in rules object
         */
        self.ruleValue = function (rules, rule) {
          if (!rules) { return ''; }
          var data = _.findWhere(rules, {name: rule});
          return data ? data.value : '';
        };

        /**
         * @ngdoc function
         * @name self.DeviceCompatible
         * @description Checks if the device running is compatible
         * @returns boolean
         */
        self.DeviceCompatible = function () {
          if (window.ionic.Platform.isIOS() || window.ionic.Platform.isIPad()) {
            return true;
          }
          else if (window.ionic.Platform.isAndroid()) {
            return parseFloat(window.ionic.Platform.version()) >= 4.3;
          }
          return false;
        };

        /**
         * @ngdoc function
         * @name self.openPopup
         * @description Open field hint popup
         */
        self.openPopup = function ($event, scope) {
          $ionicPopover.fromTemplateUrl('js/case/directives/tpls/part.hintPopover.html', {
            scope: scope,
          }).then(function (popover) {
            scope.popover = popover;
            scope.popover.show($event);
          });
        };

        /**
         * @ngdoc function
         * @name self.openPopup
         * @description Hide field hint popup
         */
        self.closePopup = function (scope) {
          scope.popover.hide();
        };

        /* ==============================================
         * == MAKE SERVICE USABLE
         * ============================================== */

        return self;

      }]);
})();
