(function () {
  'use strict';

  /**
   * @ngdoc overview
   * @name InputDirective
   * @description
   * directive that renders an input box
   *
   * Example
   * ===================================
   *
   * <sf-input ng-model="input" data="ui.config" />
   *
   *
   * case/directives/drct.input.js
   *
   * (c) 2015 Comland d.o.o. http://www.comland.si
   */

  angular
    .module('SmartForms.case.directives.input', [])
    .directive('sfInput', sfInput);

  sfInput.$inject = [
    '$rootScope',
    'CurrentService',
    'sfUtil'
  ];

  function sfInput($rootScope, CurrentService, sfUtil) {

    /* ==============================================
     * == PRIVATE VARS & FUNCTIONS
     * ============================================== */
    var compiled = CurrentService.getFieldTemplate('js/case/directives/tpls/part.sfInput.html');

    /* =====================================
     * = DIRECTIVE
     * ===================================== */
    return {
      restrict      : 'E',
      require       : 'ngModel',
      scope         : {ngModel: '='},
      link          : function (scope, element, attrs) {

        /* ==============================================
         * == INITIALIZE
         * ============================================== */

        scope.service           = CurrentService;
        console.log(CurrentService.values[scope.guid]);
        // Evaluate input data
        scope.data              = scope.$eval(attrs.data);

        // Prepare field data
        scope.required          = sfUtil.findProp(scope.data.rules, 'required') ? true : false;
        scope.mask              = sfUtil.findProp(scope.data.props, 'mask');
        scope.minlengthValue   = sfUtil.ruleValue(scope.data.rules, 'minlength');
        scope.maxlengthValue   = sfUtil.ruleValue(scope.data.rules, 'maxlength');
        scope.show              = CurrentService.isShown(scope.data.deps);
        scope.closed            = CurrentService.getCasesClosedValue();
        scope.parentGuid        = CurrentService.getParentGuid(scope.data.parentId);

        /**
         *  Here we check if data of particular scope.data.guid exists in
         *  CurrentService.values if so then we give this value to scope.ngModel
         *  ( this is particulary useful in case of edit/preview )
         */
        if (CurrentService.values && CurrentService.values[scope.data.guid]) {
          scope.ngModel = CurrentService.values[scope.data.guid];
        }
        /* ==============================================
         * == PUBLIC FUNCTIONS
         * ============================================== */

        /**
         * @ngdoc function
        * @name $scope.type
        * @description get input type
        */
        scope.type = function () {
          // Default return
          if (!scope.data.rules) {
            return 'text';
          }
          if (_.findWhere(scope.data.rules, {name: 'url'})) {
            return 'url';
          }
          else if (_.findWhere(scope.data.rules, {name: 'email'})) {
            return 'email';
          }
          else if (_.findWhere(scope.data.rules, {name: 'numeric'})) {
            return 'tel';
          } // for numbers use tel - it shows the right keyboard, because number doesn't work with ui-mask
          else {
            return 'text';
          }
        };

        /**
         * @ngdoc function
         * @name $scope.openPopover
         * @description open hint popover
         */
        scope.openPopover = function ($event) {
          sfUtil.openPopup($event, scope);
        };

        /**
         * @ngdoc function
         * @name $scope.closePopover
         * @description close hint popover
         */
        scope.closePopover = function () {
          sfUtil.closePopup(scope);
        };

        scope.fieldFocused = function($event) {
          $rootScope.$broadcast('fieldFocused', $event);
        };

        /**
         * @ngdoc function
         * @name scope.resetToDefault
         * @description resets to default value
         */
        scope.resetToDefault = function () {
          scope.ngModel = sfUtil.findProp(scope.data.props, 'default');
          CurrentService.saveValue(scope.data.parentId, scope.data.id, scope.ngModel, scope.data.guid);
        };

        /* ==============================================
         * == WATCHERS AND EVENTS
         * ============================================== */

        // Set dependencies events wathcers
        CurrentService.setDeps(scope.data.deps, scope);

        // Disable field on case close
        scope.$on('CaseClosed', function () {
          scope.closed = true;
        });

        // Remove hint popover on destroy
        scope.$on('$destroy', function () {
          if (scope.popover) {
            scope.popover.remove();
          }
        });

        // On table reset match ngModel to selection
        scope.$on('table-reset', function(event, data) {
          if (data.id === scope.data.parentId) {
            scope.resetToDefault();
          }
        });

        // Reuse compiled template and append scope
        compiled(scope, function(clonedElement) {
          element.append(clonedElement);
        });

      }
    };

  }

})();
