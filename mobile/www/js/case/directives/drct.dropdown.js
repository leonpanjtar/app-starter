(function () {
  'use strict';

  /**
   * @ngdoc overview
   * @name DropdownDirective
   * @description
   * directive that renders an dropdown
   *
   * Example
   * ===================================
   *
   * case/directives/drct.dropdown.js
   *
   * (c) 2015 Comland d.o.o. http://www.comland.si
   */

  angular
    .module('SmartForms.case.directives.dropdown', [])
    .directive('sfDropdown', sfDropdown);

  sfDropdown.$inject = [
    '$rootScope',
    'CurrentService',
    'sfUtil'
  ];

  function sfDropdown($rootScope, CurrentService, sfUtil) {

    /* ==============================================
     * == PRIVATE VARS & FUNCTIONS
     * ============================================== */
    var compiled = CurrentService.getFieldTemplate('js/case/directives/tpls/part.sfDropdown.html');

    /* =====================================
     * = DIRECTIVE
     * ===================================== */
    return {
      restrict      : 'E',
      require       : 'ngModel',
      scope         : {ngModel: '='},
      link          : function (scope, element, attrs) {

        /* ==============================================
         * == INITIALIZE
         * ============================================== */

        scope.service           = CurrentService;

        // Evaluate input data
        scope.data              = scope.$eval(attrs.data);

        // Prepare field data
        scope.required          = sfUtil.findProp(scope.data.rules, 'required') ? true : false;
        scope.multiple          = sfUtil.findProp(scope.data.rules, 'multiselect') ? true : false;
        scope.minselect         = sfUtil.ruleValue(scope.data.rules, 'minselect');
        scope.maxselect         = sfUtil.ruleValue(scope.data.rules, 'maxselect');
        scope.show              = CurrentService.isShown(scope.data.deps);
        scope.closed            = CurrentService.getCasesClosedValue();
        scope.parentGuid        = CurrentService.getParentGuid(scope.data.parentId);

        /**
         *  Here we check if data of particular scope.data.guid exists in
         *  CurrentService.values if so then we give this value to scope.ngModel
         *  ( this is particulary useful in case of edit/preview )
         */
        if (CurrentService.values && CurrentService.values[scope.data.guid]) {
          scope.ngModel = CurrentService.values[scope.data.guid];
        }

        /* ==============================================
         * == PRIVATE FUNCTIONS
         * ============================================== */

        /**
         * @ngdoc function
         * @name fixNgModel
         * @description fixes ngModel for dropdown so default values work
         */
        var fixNgModel = function() {
          var tmp = [];

          // Check if ngModel exists and if it is not an object or array of objects
          if (scope.ngModel && !(scope.ngModel.hasOwnProperty('Id_CodeItem') ||
              scope.ngModel[0].hasOwnProperty('Id_CodeItem'))) {
            for (var i = 0; i < scope.data.dataSource.length; i++) {

              // If it is not object check if it is a string or array of strings
              if (typeof scope.ngModel !== 'string') {

                // If ngModel is array of strings we need to check matches with all
                for (var j = 0; j < scope.ngModel.length; j++) {
                  // jscs:disable requireCamelCaseOrUpperCaseIdentifiers
                  if (scope.data.dataSource[i].Id_CodeItem === scope.ngModel[j]) {
                    // jscs:enable requireCamelCaseOrUpperCaseIdentifiers
                    tmp.push(scope.data.dataSource[i]);
                  }
                }
              } else {
                // If it is a single string only check if ngModel matches id
                // jscs:disable requireCamelCaseOrUpperCaseIdentifiers
                if (scope.data.dataSource[i].Id_CodeItem === scope.ngModel) {
                  // jscs:enable requireCamelCaseOrUpperCaseIdentifiers
                  tmp.push(scope.data.dataSource[i]);
                }
              }
            }
            if (typeof scope.ngModel !== 'string') {

              // If ngModel is an array set it to array of objects
              scope.ngModel = tmp;
            } else {

              // Else set it to single object
              scope.ngModel = tmp[0];
            }
          }
        };

        /* ==============================================
         * == PUBLIC FUNCTIONS
         * ============================================== */

        /**
         * @ngdoc function
         * @name $scope.openPopover
         * @description open hint popover
         */
        scope.openPopover = function ($event) {
          sfUtil.openPopup($event, scope);
        };

        /**
         * @ngdoc function
         * @name $scope.closePopover
         * @description close hint popover
         */
        scope.closePopover = function () {
          sfUtil.closePopup(scope);
        };

        scope.fieldFocused = function($event) {
          $rootScope.$broadcast('fieldFocused', $event);
        };

        /**
         * @ngdoc function
         * @name scope.resetToDefault
         * @description resets to default value
         */
        scope.resetToDefault = function () {
          scope.ngModel = sfUtil.findProp(scope.data.props, 'default');
          fixNgModel();
          CurrentService.saveValue(scope.data.parentId, scope.data.id, scope.ngModel, scope.data.guid);
        };

        /* ==============================================
         * == WATCHERS AND EVENTS
         * ============================================== */

        // Set dependencies events wathcers
        CurrentService.setDeps(scope.data.deps, scope);

        // Disable field on case close
        scope.$on('CaseClosed', function () {
            scope.closed = true;
          });

        // Remove hint popover on destroy
        scope.$on('$destroy', function () {
          if (scope.popover) {
            scope.popover.remove();
          }
        });

        // On table reset match ngModel to selection
        scope.$on('table-reset', function(event, data) {
          if (data.id === scope.data.parentId) {
            scope.resetToDefault();
          }
        });

        scope.$watch('ngModel', function(n, o) {
          console.log('model watch', n, o);
        });

        // Reuse compiled template and append scope
        compiled(scope, function(clonedElement) {
          element.append(clonedElement);
        });
      }
    };
  }
})();
