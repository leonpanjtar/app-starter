(function () {
  'use strict';

  /**
   * @ngdoc overview
   * @name InputDirective
   * @description
   * directive that renders an input box
   *
   * Example
   * ===================================
   *
   *
   *
   *
   * case/directives/drct.input.js
   *
   * (c) 2015 Comland d.o.o. http://www.comland.si
   */

  angular
    .module('SmartForms.case.directives.html', [])
    .directive('sfHtml', sfHtml);

  sfHtml.$inject = [
    'CurrentService',
    '$sce'
  ];

  function sfHtml(CurrentService, $sce) {

    /* ==============================================
     * == PRIVATE VARS & FUNCTIONS
     * ============================================== */
    var compiled = CurrentService.getFieldTemplate('js/case/directives/tpls/part.sfHtml.html');

    /* =====================================
     * = DIRECTIVE
     * ===================================== */
    return {
      restrict      : 'E',
      scope         : {},
      link          : function (scope, element, attrs) {

        /* ==============================================
         * == INITIALIZE
         * ============================================== */

        // Evaluate input data
        scope.data              = scope.$eval(attrs.data);
        scope.data.props        = JSON.parse(scope.data.props);
        scope.data.html         = $sce.trustAsHtml(scope.data.props.default);
        scope.show              = CurrentService.isShown(scope.data.deps);
        scope.parentGuid        = CurrentService.getParentGuid(scope.data.parentId);

        /* ==============================================
         * == WATCHERS AND EVENTS
         * ============================================== */

        // Set dependencies events wathcers
        CurrentService.setDeps(scope.data.deps, scope);

        compiled(scope, function(clonedElement) {
          element.append(clonedElement);
        });
      }
    };

  }

})();
