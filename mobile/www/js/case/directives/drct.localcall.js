(function () {
  'use strict';

  /**
   * @ngdoc overview
   * @name CheckboxDirective
   * @description
   * directive that renders an Api Call
   *
   * Example
   * ===================================
   *
   *
   *
   *
   * case/directives/drct.apicall.js
   *
   * (c) 2015 Comland d.o.o. http://www.comland.si
   */

  angular
    .module('SmartForms.case.directives.localcall', [])
    .directive('sfLocalcall', sfLocalcall);

  sfLocalcall.$inject = [
    '$rootScope',
    'CurrentService',
    'sfUtil',
    '$ionicPopover',
    '$ionicPopup',
    'gettextCatalog',
    'SweetAlert',
    '$ionicScrollDelegate'
  ];

  function sfLocalcall($rootScope, CurrentService, sfUtil, $ionicPopover, $ionicPopup, gettextCatalog,
    SweetAlert, $ionicScrollDelegate) {

    /* ==============================================
     * == PRIVATE VARS & FUNCTIONS
     * ============================================== */
    var compiled = CurrentService.getFieldTemplate('js/case/directives/tpls/part.sfApicall.html');

    /* =====================================
     * = DIRECTIVE
     * ===================================== */
    return {
      restrict: 'E',
      require: 'ngModel',
      scope: {ngModel: '='},
      link: function (scope, element, attrs) {

        /* ==============================================
         * == INITIALIZE
         * ============================================== */

        scope.service = CurrentService;

        // Evaluate input data
        scope.data = scope.$eval(attrs.data);

        // Prepare field data
        scope.show       = CurrentService.isShown(scope.data.deps);
        scope.closed     = CurrentService.getCasesClosedValue();
        scope.parentGuid = CurrentService.getParentGuid(scope.data.parentId);

        scope.parameters  = undefined;
        scope.viewResoult = undefined;
        scope.resultAttributes = {};
        scope.src = {};
        scope.infiniteEnabled = false;

        var validCall = true, values = [];

        // parse properties
        try {
          scope.resultAttributes = _.findWhere(scope.data.props, {name: 'ResultAttributes'}).value;
          scope.parameters = _.findWhere(scope.data.props, {name: 'Parameters'}).value;
          scope.displayAttributes = _.pluck(_.where(scope.resultAttributes, {Display: 'true'}), 'Name');
          console.log(scope.data.dataSource);
          for (var i = 0; i < scope.data.dataSource.length; i++) {
            var temp = JSON.parse(scope.data.dataSource[i].Value);
            temp.Label = scope.data.dataSource[i].Label;
            values.push(temp);
          }
        }
        catch (error) {
          validCall = false;
        }

        /* =====================================
         * = PRIVATE FUNCTIONS
         * ===================================== */

        // scroll to top
        function scrollTop() {
          $ionicScrollDelegate.$getByHandle('localCall').scrollTop();
        }

        // scroll to x,y
        var scrollTo = function (x, y) {
          $ionicScrollDelegate.$getByHandle('localCall').scrollTo(x, y);
        };

        //get scroll position
        var getScrollPosition = function () {
          var pos = $ionicScrollDelegate.$getByHandle('localCall').getScrollPosition();
          scope.x = pos.left;
          scope.y = pos.top;
        };

        /**
         * @ngdoc function
         * @name $scope.select
         * @description set selected value on form
         */
        scope.select = function (r) {
          for (var index = 0; index < scope.resultAttributes.length; index++) {
            var atr = scope.resultAttributes[index];
            if (atr.Field) {
              CurrentService.updateOne(atr.Field, r[atr.Name]);
            }
            else if (atr.Guid) {
              CurrentService.updateOne(atr.Guid.toUpperCase(), r[atr.Name]);
            }
            var guid;
            if (atr.Field) {
              guid = atr.Field.guid.toUpperCase();
            } else if (atr.Guid) {
              guid = atr.Guid.toUpperCase();
            }
            $rootScope.$broadcast(guid + '-change', r[atr.Name]);
          }
          scope.viewResoult = undefined;
          scope.results = undefined;
          CurrentService.disableFormFn(false);
          scope.popupSearch.close();
        };

        /**
         * @ngdoc function
         * @name $scope.openPopup
         * @description open popup
         */
        scope.openPopup = function () {
          scope.loading = false;
          if (validCall) {
            CurrentService.disableFormFn(true);
            scope.popupSearch = $ionicPopup.show({
              templateUrl: 'js/case/directives/tpls/part.sfApicall.popup.html',
              cssClass: 'popup-api',
              scope: scope
            });
          } else {
            SweetAlert.swal({
              title: gettextCatalog.getString('Error reading search configuration'),
              text: '',
              confirmButtonText: gettextCatalog.getString('Close')
            });
          }
        };

        /**
         * @ngdoc function
         * @name $scope.closePopup
         * @description close popup
         */
        scope.closePopup = function () {
          CurrentService.disableFormFn(false);
          scope.popupSearch.close();
        };

        /**
         * @ngdoc function
         * @name $scope.backButton
         * @description go back one view
         */
        scope.backButton = function () {
          if (scope.viewResoult) {
            scope.viewResoult = undefined;
            scrollTo(scope.x, scope.y);
          } else {
            scrollTop();
            scope.results = undefined;
          }
        };

        /**
         * @ngdoc function
         * @name $scope.view
         * @description set selected resoult as viewResoult
         */
        scope.view = function (r, event) {
          getScrollPosition();
          console.log('View', event);
          scope.viewResoult = r;
          scrollTop();
        };

        /**
         * @ngdoc function
         * @name $scope.search
         * @description prepares data for api search method
         */
        scope.search = function () {
          scope.loading = true;
          var index, param, contains;
          var parameters = scope.parameters.slice();
          parameters.push({Label: 'Label', Name: 'Label'});
          scope.infinite = _.filter(values, function (obj) {
            contains = true;
            for (index = 0; index < parameters.length; index++) {
              if (!contains) {
                return false;
              }
              param = parameters[index].Name;
              if (scope.src[param]) {
                contains = obj[param].toUpperCase().indexOf(scope.src[param].toUpperCase()) !== -1;
              }
            }
            return contains;
          });
          if (scope.infinite.length >= 25) {
            scope.results = [];
            scope.infiniteEnabled = true;
          } else {
            scope.results = scope.infinite;
            scope.infiniteEnabled = false;
          }
          scope.loading = false;
        };

        scope.loadMore = function () {
          if (scope.infinite.length > 0) {
            if (scope.results.length < scope.infinite.length) {
              var remaining = scope.infinite.length - scope.results.length;
              var length;
              if (remaining >= 25) {
                length = 25;
                scope.infiniteEnabled = true;
              } else {
                length = remaining;
                scope.infiniteEnabled = false;
              }
              var last = scope.results.length;
              for (var i = 0; i < length; i++) {
                scope.results.push(scope.infinite[last + i]);
              }
              scope.$broadcast('scroll.infiniteScrollComplete');
            } else {
              scope.infiniteEnabled = false;
            }
          } else {

            scope.infiniteEnabled = false;
          }
        };

        /**
         * @ngdoc function
         * @name $scope.openPopover
         * @description open hint popover
         */
        scope.openPopover = function ($event) {
          sfUtil.openPopup($event, scope);
        };

        /**
         * @ngdoc function
         * @name $scope.closePopover
         * @description close hint popover
         */
        scope.closePopover = function () {
          sfUtil.closePopup(scope);
        };

        /* ==============================================
        * == WATCHERS AND EVENTS
        * ============================================== */

        // Set dependencies events wathcers
        CurrentService.setDeps(scope.data.deps, scope);

        // Disable field on case close
        scope.$on('CaseClosed', function () {
          scope.closed = true;
        });

        // Remove hint popover on destroy
        scope.$on('$destroy', function () {
          if (scope.popover) {
            scope.popover.remove();
          }
        });

        // Reuse compiled template and append scope
        compiled(scope, function (clonedElement) {
          element.append(clonedElement);
        });
      }
    };
  }

})();
