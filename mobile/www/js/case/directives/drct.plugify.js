(function () {
  'use strict';

  /***
   * @ngdoc directive
   * @name Smart Forms Plugify Directive
   *
   * Plugin compiler directive takes care of dinamically inserting
   * plugins into the DOM.
   */
  angular
    .module('SmartForms.case.directives.plugify', [])
    .directive('plugify', plugify);

  plugify.$inject = ['$compile', 'sfUtil', 'CurrentService', 'Constants'];

  function plugify($compile, sfUtil, CurrentService, C) {

    return {
      restrict: 'E',
      scope: true,
      priority: 1000,
      terminal: true,
      link: function (scope, element, attrs) {

        if (C.DEBUGMODE) {
          console.time('Plugify');
        }

        var compiled,
            row,
            field,
            tpl           = '',
            data          = scope.$eval(attrs.plugin),
            form          = scope.$eval(attrs.form),
            section       = scope.$eval(attrs.section),
            parent        = scope.$eval(attrs.parent),
            parentGuid    = CurrentService.getParentGuid(parent),
            rowCounter    = 1;

        scope.current = CurrentService;
        scope.fields  = parent ? data : CurrentService.procedure.fields;

        if (CurrentService.getTemplate(form, section, parent)) {
          compiled = CurrentService.getTemplate(form, section, parent);
        }

        else {
          if (parent) {
            // Start first row
            tpl += '<div class="row responsive-sm">';

            // Create the template for table
            for (var key in data) {
              if (data.hasOwnProperty(key)) {

                field = data[key];

                if (rowCounter && field.row !== rowCounter) {
                  tpl += '</div>'; // End Last row
                  tpl += '<div class="row responsive-sm">'; // Start new row
                  rowCounter++;
                }

                tpl += '<div class="col control table"><sf-' + field.type +
                       ' class="sf-element" ng-model="current.tableTemp[\'' + parentGuid + '\'][\'' + field.guid +
                       '\']" data="{{::fields[\'' + field.guid + '\']}}" /></div>';

              }
            }

            // End first row
            tpl += '</div>';
          }
          else {
            if (data) {
              // Create the template for section
              for (var i = 0, leng = _.size(data); i <= leng; i++) {
                row = data[i];
                tpl += '<div class="row responsive-sm">';

                // Create fields
                for (var j = 0, len = _.size(row); j < len; j++) {
                  field = row[j];

                  tpl += '<div class="col control ' + field.type + '"><sf-' + field.type +
                         ' class="sf-element" ng-model="current.values[\'' + field.guid + '\']" data="{{::fields[\'' +
                         field.guid + '\']}}" /></div>';
                }

                tpl += '</div>';
              }
            }
          }

          //CurrentService.setTemplate(form, section, parent, tpl);
          compiled = $compile(tpl)(scope);
          CurrentService.setTemplate(form, section, parent, compiled);
        }

        //compiled = $compile(tpl)(scope);
        element.append(compiled);
        if (C.DEBUGMODE) {
          console.timeEnd('Plugify');
          console.log('');
        }
      }
    };

  }

})();
