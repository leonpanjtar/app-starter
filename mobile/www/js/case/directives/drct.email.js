(function () {
  'use strict';

  /**
   * @ngdoc overview
   * @name InputDirective
   * @description
   * directive that renders an input box
   *
   * Example
   * ===================================
   *
   *
   *
   *
   * case/directives/drct.email.js
   *
   * (c) 2015 Comland d.o.o. http://www.comland.si
   */

  angular
    .module('SmartForms.case.directives.email', [])
    .directive('sfEmail', sfEmail);

  sfEmail.$inject = [
    '$rootScope',
    'CurrentService',
    'sfUtil'
  ];

  function sfEmail($rootScope, CurrentService, sfUtil) {

    /* ==============================================
     * == PRIVATE VARS & FUNCTIONS
     * ============================================== */
    var compiled = CurrentService.getFieldTemplate('js/case/directives/tpls/part.sfEmail.html');

    /**
     * @ngdoc function
     * @name prepareDocumentSelection
     * @description Prepare selection of documents to send
     * @param {object} data
     */
    var prepareDocumentSelection = function(data) {

      var docList = [];
      for (var i = 0; i < CurrentService.reports.length; i++) {
        var report = _.findWhere(data, {idReport: CurrentService.reports[i].idProcedureReport});
        if (report) {
          docList.push(report);
        }
        else {
          docList.push({
            name: CurrentService.reports[i].label,
            value: false,
            idReport: CurrentService.reports[i].idProcedureReport
          });
        }
      }

      return docList;
    };

    /* =====================================
     * = DIRECTIVE
     * ===================================== */
    return {
      restrict      : 'E',
      require       : 'ngModel',
      scope         : {ngModel: '='},
      link          : function (scope, element, attrs, ngModelCtrl) {

        /* ==============================================
         * == INITIALIZE
         * ============================================== */

        var temp, i, len;

        scope.service                     = CurrentService;

        // Evaluate input data
        scope.data                        = scope.$eval(attrs.data);

        scope.closed                      = CurrentService.getCasesClosedValue();

        scope.ngModel                     = scope.data.props;
        scope.data.props[6].value.value   = prepareDocumentSelection(scope.data.props[6].value.value);

        /**
         *  Here we check if data of particular scope.data.guid exists in
         *  CurrentService.values if so then we give this value to scope.ngModel
         *  ( this is particulary useful in case of edit/preview )
         */
        if (CurrentService.values && CurrentService.values[scope.data.guid]) {
          try {
            // Save current selection to temp
            temp = CurrentService.values[scope.data.guid];

            // Add control structure to CurrentValues
            CurrentService.values[scope.data.guid] = scope.data.props;

            // Copy current values
            for (i = 0, len = _.size(scope.data.props); i < len; i++) {
              if (temp[i] !== undefined) {
                CurrentService.values[scope.data.guid][i].value.value = temp[i].value.value;
              }
            }

            scope.ngModel = CurrentService.values[scope.data.guid];
          }
          catch (err) {
            console.log(err);
          }
        }

        /* ==============================================
         * == PUBLIC FUNCTIONS
         * ============================================== */

        /**
         * @ngdoc function
         * @name $scope.openPopover
         * @description open hint popover
         */
        scope.openPopover = function ($event) {
          sfUtil.openPopup($event, scope);
        };

        /**
         * @ngdoc function
         * @name $scope.closePopover
         * @description close hint popover
         */
        scope.closePopover = function () {
          sfUtil.closePopup(scope);
        };

        /* ==============================================
         * == WATCHERS AND EVENTS
         * ============================================== */

        // Disable field on case close
        scope.$on('CaseClosed', function () {
          if (scope.active) {
            scope.closed = true;
          }
        });

        // Remove hint popover on destroy
        scope.$on('$destroy', function () {
          if (scope.popover) {
            scope.popover.remove();
          }
        });

        // Watch ngModel changes in debounce mode
        scope.$watch(
            function() {
              return ngModelCtrl.$modelValue;
            },
            _.debounce(function(newValue, oldValue) {
                $rootScope.$broadcast(scope.data.guid + '-change', {value: newValue, old: oldValue});
              },
            500),
            true);

        // Reuse compiled template and append scope
        compiled(scope, function(clonedElement) {
          element.append(clonedElement);
        });

      }
    };

  }

})();
