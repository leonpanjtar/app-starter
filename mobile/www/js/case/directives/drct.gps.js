(function () {
  'use strict';

  /**
   * @ngdoc overview
   * @name GPSDirective
   * @description
   * directive that renders an input box with gps reader
   *
   * Example
   * ===================================
   *
   *
   *
   *
   * case/directives/drct.gps.js
   *
   * (c) 2015 Comland d.o.o. http://www.comland.si
   */

  angular
    .module('SmartForms.case.directives.gps', [])
    .directive('sfGps', sfGps);

  sfGps.$inject = [
    '$rootScope',
    '$cordovaGeolocation',
    'sfUtil',
    'CurrentService',
    'SweetAlert',
    'gettextCatalog'
  ];

  function sfGps($rootScope, $cordovaGeolocation, sfUtil, CurrentService, SweetAlert, gettextCatalog) {

    /* ==============================================
     * == PRIVATE VARS & FUNCTIONS
     * ============================================== */
    var compiled = CurrentService.getFieldTemplate('js/case/directives/tpls/part.sfGps.html');

    /* =====================================
     * = DIRECTIVE
     * ===================================== */
    return {
      restrict      : 'E',
      require       : 'ngModel',
      scope         : {ngModel: '='},
      link          : function (scope, element, attrs) {

        /* ==============================================
         * == INITIALIZE
         * ============================================== */

        scope.service           = CurrentService;

        // Evaluate input data
        scope.data              = scope.$eval(attrs.data);

        // Prepare field data
        scope.required          = sfUtil.findProp(scope.data.rules, 'required') ? true : false;
        scope.minlengthValue   = sfUtil.ruleValue(scope.data.rules, 'minlength');
        scope.maxlengthValue   = sfUtil.ruleValue(scope.data.rules, 'maxlength');
        scope.show              = CurrentService.isShown(scope.data.deps);
        scope.closed            = CurrentService.getCasesClosedValue();

        scope.gpsButtonText     = gettextCatalog.getString('GPS');
        scope.gpsButtonState    = false;
        scope.parentGuid        = CurrentService.getParentGuid(scope.data.parentId);

        /* ==============================================
         * == PUBLIC FUNCTIONS
         * ============================================== */

        scope.getLocation = function () {
          var opt = {
            timeout: 10000,
            enableHighAccuracy: true
          };

          scope.gpsButtonText = gettextCatalog.getString('Searching...');
          scope.gpsButtonState = true;

          $cordovaGeolocation
            .getCurrentPosition(opt)
            .then(function (position) {
              console.log(position);
              scope.ngModel = gettextCatalog.getString('Lat') + ': ' + position.coords.latitude + ', ' +
                              gettextCatalog.getString('Long') + ': ' +  position.coords.longitude;
              scope.gpsButtonText = gettextCatalog.getString('GPS');
              scope.gpsButtonState = false;
            }, function(err) {
              var desc = '';
              if (err.code === 1) { desc = gettextCatalog.getString('Permisson to access location info denied!'); }
              else if (err.code === 2) { desc = gettextCatalog.getString('Position data is currently not available.'); }
              else if (err.code === 3) { desc = gettextCatalog.getString('Request timed out!'); }

              SweetAlert.swal(gettextCatalog.getString('Ooops...'), desc, 'error');

              scope.gpsButtonText = gettextCatalog.getString('GPS');
              scope.gpsButtonState = false;
            });
        };

        scope.fieldFocused = function($event) {
          $rootScope.$broadcast('fieldFocused', $event);
        };

        /**
         * @ngdoc function
         * @name scope.resetToDefault
         * @description resets to default value
         */
        scope.resetToDefault = function () {
          scope.ngModel = sfUtil.findProp(scope.data.props, 'default');
          CurrentService.saveValue(scope.data.parentId, scope.data.id, scope.ngModel, scope.data.guid);
        };

        /* ==============================================
         * == WATCHERS AND EVENTS
         * ============================================== */

        // Set dependencies events wathcers
        CurrentService.setDeps(scope.data.deps, scope);

        // Disable field on case close
        scope.$on('CaseClosed', function () {
          scope.closed = true;
        });

        // Remove hint popover on destroy
        scope.$on('$destroy', function () {
          if (scope.popover) {
            scope.popover.remove();
          }
        });

        // On table reset match ngModel to selection
        scope.$on('table-reset', function(event, data) {
          if (data.id === scope.data.parentId) {
            scope.resetToDefault();
          }
        });

        // Reuse compiled template and append scope
        compiled(scope, function(clonedElement) {
          element.append(clonedElement);
        });

      }
    };

  }

})();
