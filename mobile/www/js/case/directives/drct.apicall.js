(function () {
  'use strict';

  /**
   * @ngdoc overview
   * @name CheckboxDirective
   * @description
   * directive that renders an Api Call
   *
   * Example
   * ===================================
   *
   *
   *
   *
   * case/directives/drct.apicall.js
   *
   * (c) 2015 Comland d.o.o. http://www.comland.si
   */

  angular
    .module('SmartForms.case.directives.apicall', [])
    .directive('sfApicall', sfApicall);

  sfApicall.$inject = [
    '$ionicPopup',
    '$ionicScrollDelegate',
    '$http',
    'gettextCatalog',
    'CurrentService',
    'sfUtil',
    'Constants',
    'NetworkService',
    'SweetAlert'
  ];

  function sfApicall ($ionicPopup, $ionicScrollDelegate, $http, gettextCatalog,
                      CurrentService, sfUtil, Constants, NetworkService, SweetAlert) {

    /* ==============================================
     * == PRIVATE VARS & FUNCTIONS
     * ============================================== */
    var compiled = CurrentService.getFieldTemplate('js/case/directives/tpls/part.sfApicall.html');

    /* =====================================
     * = DIRECTIVE
     * ===================================== */
    return {
      restrict: 'E',
      require: 'ngModel',
      scope: {ngModel: '='},
      link: function (scope, element, attrs) {

        /* ==============================================
         * == INITIALIZE
         * ============================================== */

        scope.service           = CurrentService;

        // Evaluate input data
        scope.data              = scope.$eval(attrs.data);

        // Prepare field data
        scope.show              = CurrentService.isShown(scope.data.deps);
        scope.closed            = CurrentService.getCasesClosedValue();
        scope.parentGuid        = CurrentService.getParentGuid(scope.data.parentId);

        scope.parameters        = undefined;
        scope.viewResoult       = undefined;
        scope.resultAttributes  = {};
        scope.src               = {};

        var apiCall = {}, validCall = true;

        // parse properties
        try {
          scope.resultAttributes    = _.findWhere(scope.data.props, {name: 'ResultAttributes'}).value;
          scope.parameters          = _.findWhere(scope.data.props, {name: 'Parameters'}).value;
          scope.displayAttributes    = _.pluck(_.where(scope.resultAttributes, {Display: 'true'}), 'Name');

          //prepare properties for api call
          apiCall = {
            UrlOfQueryMethod: _.findWhere(scope.data.props, {name: 'UrlOfQueryMethod'}).value,
            UrlOfQueryParamsMethod: _.findWhere(scope.data.props, {name: 'UrlOfQueryParamsMethod'}).value,
            Arguments: {}
          };
        }
        catch (error) {
          validCall = false;
        }

        /* =====================================
         * = PRIVATE FUNCTIONS
         * ===================================== */

        // scroll to top
        function scrollTop() {
          $ionicScrollDelegate.scrollTop();
        }

        // scroll to x,y
        var scrollTo = function (x, y) {
          $ionicScrollDelegate.scrollTo(x, y);
        };

        // call api
        var callApiAsync = function (data) {

          var success = function (response) {
            scope.results = response.data.data.Results;
            scope.loading = false;
          };

          var error = function () {
            scope.popover.hide();
          };

          var req = {
            method: 'POST',
            url: window.localStorage.getItem('baseUrl') + '/api/UserDefined/CallQuery',
            headers: {
              'Content-Type': 'application/json',
              'Authorization': 'bearer ' + window.localStorage.getItem('sessionToken')
            },
            data: data,
            timeout: Constants.timeouts.http
          };

          $http(req).then(success, error);

        };

        /**
         * @ngdoc function
         * @name $scope.select
         * @description set selected value on form
         */
        scope.select = function (r) {
          for (var index = 0; index < scope.resultAttributes.length; index++) {
            var atr = scope.resultAttributes[index];
            if (atr.Guid && atr.Guid !== '') {
              if (atr.subval !== undefined) {
                CurrentService.updateEmail(atr.Guid.toUpperCase(), atr.subval, r[atr.Name]);
              }
              else {
                CurrentService.updateOne(atr.Guid.toUpperCase(), r[atr.Name]);
              }
            }
          }
          scope.viewResoult = undefined;
          scope.results = undefined;
          CurrentService.disableFormFn(false);
          scope.popupSearch.close();
        };

        /**
         * @ngdoc function
         * @name $scope.openPopup
         * @description open popup
         */
        scope.openPopup = function () {
          scope.loading = false;
          CurrentService.disableFormFn(true);
          if (validCall && NetworkService.status) {
            scope.popupSearch = $ionicPopup.show({
              templateUrl: 'js/case/directives/tpls/part.sfApicall.popup.html',
              cssClass: 'popup-api',
              scope: scope
            });
          } else {
            SweetAlert.swal({
              title: (NetworkService.status ?
                gettextCatalog.getString('Error reading api configuration') :
                gettextCatalog.getString('No network connection')),
              text: (NetworkService.status ?
                '' : gettextCatalog.getString('You need network connection to search for data.')),
              confirmButtonText: gettextCatalog.getString('Close')
            });
          }
        };

        /**
         * @ngdoc function
         * @name $scope.closePopup
         * @description close popup
         */
        scope.closePopup = function () {
          CurrentService.disableFormFn(false);
          scope.popupSearch.close();
        };

        /**
         * @ngdoc function
         * @name $scope.backButton
         * @description go back one view
         */
        scope.backButton = function () {
          if (scope.viewResoult) {
            scope.viewResoult = undefined;
            scrollTo(scope.x, scope.y);
          } else {
            scrollTop();
            scope.results = undefined;
          }
        };

        /**
         * @ngdoc function
         * @name $scope.view
         * @description set selected resoult as viewResoult
         */
        scope.view = function (r, event) {
          scope.x = event.layerX;
          scope.y = event.layerY;
          scope.viewResoult = r;
          scrollTop();
        };

        /**
         * @ngdoc function
         * @name $scope.search
         * @description prepares data for api search method
         */
        scope.search = function () {
          scope.loading = true;
          for (var index = 0; index < scope.parameters.length; index++) {
            var param = scope.parameters[index];
            apiCall.Arguments[param.Name] = scope.src[param.Name] ? scope.src[param.Name] : '';
          }
          callApiAsync(apiCall);
        };

        /**
         * @ngdoc function
         * @name $scope.openPopover
         * @description open hint popover
         */
        scope.openPopover = function ($event) {
          sfUtil.openPopup($event, scope);
        };

        /**
         * @ngdoc function
         * @name $scope.closePopover
         * @description close hint popover
         */
        scope.closePopover = function () {
          sfUtil.closePopup(scope);
        };

        /* ==============================================
        * == WATCHERS AND EVENTS
        * ============================================== */

        // Set dependencies events wathcers
        CurrentService.setDeps(scope.data.deps, scope);

        // Disable field on case close
        scope.$on('CaseClosed', function () {
          scope.closed = true;
        });

        // Remove hint popover on destroy
        scope.$on('$destroy', function () {
          if (scope.popover) {
            scope.popover.remove();
          }
        });

        // Reuse compiled template and append scope
        compiled(scope, function (clonedElement) {
          element.append(clonedElement);
        });
      }
    };
  }

})();
