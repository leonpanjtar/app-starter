(function () {
  'use strict';

  /***
   * @ngdoc overview
   * @name Smart Forms Utilities
   *
   * @description
   * Helper methods used internally by the directives. Should not be called directly from user code.
   */
  angular
  .module('SmartForms.case.directives', [
    'SmartForms.case.directives.utils',
    'SmartForms.case.directives.fieldName',
    'SmartForms.case.directives.plugify',
    'SmartForms.case.directives.input',
    'SmartForms.case.directives.hidden',
    'SmartForms.case.directives.signature',
    'SmartForms.case.directives.sigPopup',
    'SmartForms.case.directives.dropdown',
    'SmartForms.case.directives.toggle',
    'SmartForms.case.directives.date',
    'SmartForms.case.directives.time',
    'SmartForms.case.directives.textarea',
    'SmartForms.case.directives.header',
    'SmartForms.case.directives.subheader',
    'SmartForms.case.directives.description',
    'SmartForms.case.directives.html',
    'SmartForms.case.directives.checkbox',
    'SmartForms.case.directives.radio',
    'SmartForms.case.directives.table',
    'SmartForms.case.directives.barcode',
    'SmartForms.case.directives.gps',
    'SmartForms.case.directives.email',
    'SmartForms.case.directives.apicall',
    'SmartForms.case.directives.localcall',
    'SmartForms.case.directives.resultItem',
    'SmartForms.case.directives.file',
    'SmartForms.case.validation.ngValidInput',
    'SmartForms.case.validation.ngValidDate',
    'SmartForms.case.validation.ngValidTime',
    'SmartForms.case.validation.ngValidDropdown',
    'SmartForms.case.validation.ngValidCheckbox',
    'SmartForms.case.validation.ngValidRadio',
    'SmartForms.case.validation.ngValidSignature',
    'SmartForms.case.validation.ngValidTable',
    'ui.mask',
    'ngMessages'
  ]);

})();
