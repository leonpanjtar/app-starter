(function () {
  'use strict';

  /**
   * @ngdoc service
   * @name CaseService
   * @description
   * this service takes care of loading and managing cases screen
   *
   * case/case.service.js
   *
   * (c) 2015 Comland d.o.o. http://www.comland.si
   *
   */

  angular
    .module('SmartForms.case.service.current', [])
    .service('CurrentService', CurrentService);

  CurrentService.$inject = [
    'sfUtil',
    '$q',
    'Constants',
    'CaseService',
    'AuthService',
    '$rootScope',
    '$compile',
    '$templateCache',
    '$timeout',
    '$ionicScrollDelegate',
    '$ionicPlatform'
  ];

  function CurrentService(sfUtil, $q, Constants, CaseService, AuthService, $rootScope, $compile, $templateCache,
                          $timeout, $ionicScrollDelegate, $ionicPlatform) {

    var service             = this;      // Service var
    service.current     = {};        // Current form
    service.procedure   = {};        // Current procedure
    service.codelists   = {};        // Codelists for current procedure
    service.templates   = {};        // Compiled templates for forms
    service.reports     = {};        // Hold reports of current procedure
    service.values      = {};        // Holds current values of all procedure fields
    service.tableTemp   = {};        // Temporary holder for tables
    service.closed      = false;     // Is current case closed
    service.name        = '';        // Case name
    service.location    = '';        // Case GPS coordinates
    service.compiledTpl = {};        // Hold compiled field templates to reuse
    service.casesClosed = false;     // Is case closed
    service.preview     = false;     // Has preview
    service.caseContent = [];        // Case content

    service.disableForm = false;

    /* ==============================================
     * == PRIVATE FUNCTIONS
     * ============================================== */

    /**
     * @ngdoc function
     * @name saveProgress
     * @description Saves or updates case in DB
     * @returns {promise} Result of function
     */
    var saveProgress = function () {
      var deferred = $q.defer();
      // Update if case exists in DB
      if (service.procedure.procedure.guid) {
        CaseService
          .updateCase(service.procedure.procedure.guid,
                      service.values)
          .then(
            function () {
              deferred.resolve('Case successfully saved.');
            },
            function (error) {
              console.log(error);
              deferred.reject(error);
            }
        );
      }

      // Save if case does not yet exists in DB
      else {
        CaseService
          .saveCase(AuthService.currentUser.info.Id_Users, // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
                    service.procedure.procedure.id,
                    service.name,
                    service.location,
                    service.procedure.procedure.version,
                    service.values)
          .then(
            function (result) {
              service.procedure.procedure.guid = result;
              deferred.resolve(result);
            },
            function (error) {
              console.log(error);
              deferred.reject(error);
            }
          );
      }
      return deferred.promise;
    };

    /**
     * @ngdoc function
     * @name self.compare
     * @description Compare values with operator
     */
    var compare = function (operator, value1, value2) {
      switch (operator) {
        case '==':
          return value1 === value2;
        case '>=':
          return value1 <= value2;
        case '<=':
          return value1 >= value2;
        case '!=':
          return value1 !== value2;
      }
    };

    /**
     * @ngdoc function
     * @name self.compareArray
     * @description Compare specific value of array
     */
    var compareArray = function (operator, value1, value2) {
      switch (operator) {
        case '==':
          return _.indexOf(value2, value1) > -1;
        case '!=':
          return _.indexOf(value2, value1) === -1;
      }
    };

    /**
     * @ngdoc function
     * @name self.minutesOfDay
     * @description Helper funcion for compareTime
     */
    var minutesOfDay = function(m) {
      return moment(m).minutes() + moment(m).hours() * 60;
    };

    /**
     * @ngdoc function
     * @name self.compareTime
     * @description Compare specific value of time
     */
    var compareTime = function (operator, value1, value2) {
      switch (operator) {
        case '>=':
          return minutesOfDay(value1) <= minutesOfDay(value2);
        case '<=':
          return minutesOfDay(value1) >= minutesOfDay(value2);
        case '==':
          return minutesOfDay(value1) === minutesOfDay(value2);
        case '!=':
          return minutesOfDay(value1) !== minutesOfDay(value2);
      }
    };

    /**
     * @ngdoc function
     * @name self.compareDate
     * @description Compare specific value of date
     */
    var compareDate = function (operator, value1, value2) {
      switch (operator) {
        case '>=':
          return moment(value1).isSameOrBefore(value2, 'day');
        case '<=':
          return moment(value1).isSameOrAfter(value2, 'day');
        case '==':
          return moment(value1).isSame(value2, 'day');
        case '!=':
          return !moment(value1).isSame(value2, 'day');
      }
    };

    /**
     * @ngdoc function
     * @name registerOnEvent
     * @description Register to dependency change event
     */
    var registerOnEvent = function (scope, deps, d) {
      var guid = d.field.guid.toUpperCase();
      $rootScope.$on(guid + '-change', function () {
        scope.show = service.isShown(deps);
        $ionicScrollDelegate.resize();
        if (!scope.show) {
          if (['subheader',
            'header',
            'description',
            'html',
            'localcall',
            'apicall',
            'file',
            'signature'].indexOf(scope.data.type) === -1) {
            scope.resetToDefault();
          }
        }
      });
    };

    /**
     * @ngdoc function
     * @name parseDataSource
     * @description Prepare data source for fields
     */
    var parseDataSource = function (data) {
      if (!data || data === '') { return false; }

      try {
        data = JSON.parse(data);
        if (_.has(data, 'codelist')) {
          return service.getCodelist(data.codelist);
        }
        else {
          return false;
        }
      }

      catch (err) {
        console.log(err);
      }
    };

    /**
     * @ngdoc function
     * @name self.parseDataSourceChecked
     * @description Parse data source for checkbox or radio
     */
    var parseDataSourceChecked = function (data, value) {
      var dataSource;

      dataSource = parseDataSource(data);

      if (dataSource) {
        if (typeof value === 'string') {
          _.find(dataSource, function (item, index) {
            // jscs:disable requireCamelCaseOrUpperCaseIdentifiers
            dataSource[index].Checked = dataSource[index].Id_CodeItem === value;
            // jscs:enable requireCamelCaseOrUpperCaseIdentifiers
          });
        } else {
          _.find(dataSource, function (item, index) {
            if (value) {
              for (var i = 0; i < value.length; i++) {
                // jscs:disable requireCamelCaseOrUpperCaseIdentifiers
                var match = dataSource[index].Id_CodeItem === value[i];
                // jscs:enable requireCamelCaseOrUpperCaseIdentifiers
                dataSource[index].Checked = match;
                if (match) {
                  break;
                }
              }
            } else {
              dataSource[index].Checked = false;
            }
          });
        }
      }

      return dataSource;
    };

    /**
     * @ngdoc function
     * @name self.compareArray
     * @description Prepare field dependencies JSON
     */
    var parseDeps = function (deps, scope) {
      var depsArray;
      try {
        if (deps) {
          depsArray = JSON.parse(deps);
        }

        if (depsArray && scope) {
          for (var i = 0; i < depsArray.length; i++) {
            registerOnEvent(scope, depsArray, depsArray[i]);
          }
        }
      }
      catch (err) {
        console.log(err);
      }

      return depsArray ? depsArray : [];
    };

    /**
     * @ngdoc function
     * @name self.dropdownDefaultValue
     * @param {array} dataSource
     * @param {array} defaultValue
     * @description sets default value for dropdown
     */
    var dropdownDefaultValue = function (dataSource, defaultValue) {
      var tmp = [];
      for (var i = 0; i < dataSource.length; i++) {
        if (defaultValue && typeof defaultValue !== 'string') {
          for (var j = 0; j < defaultValue.length; j++) {
            if (dataSource[i].Id_CodeItem === defaultValue[j]) { // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
              tmp.push(dataSource[i]);
            }
          }
        } else {
          if (dataSource[i].Id_CodeItem === defaultValue) { // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
            tmp.push(dataSource[i]);
          }
        }
      }
      if (defaultValue && typeof defaultValue !== 'string') {
        return tmp;
      } else {
        return tmp[0];
      }
    };

    /**
     * @ngdoc function
     * @name getFormData
     * @description Set data for specific form
     */
    var getFormData = function(form) {
      form.props   = sfUtil.parseProps(form.props);
      form.deps    = parseDeps(form.deps);
    };

    /**
     * @ngdoc function
     * @name getFieldData
     * @description Set data for specific field
     */
    var getFieldData = function(field, parentGuid) {
      try {
        field.props   = sfUtil.parseProps(field.props, field.type);
        field.deps    = parseDeps(field.deps);
        field.rules   = sfUtil.parseRules(field.rules);
        var defaultValue  = sfUtil.findProp(field.props, 'default') || undefined;

        switch (field.type) {
          case 'date':
            defaultValue = defaultValue ? moment(defaultValue) : moment();
            field.dataSource  = parseDataSource(field.data);
            break;

          case 'time':
            defaultValue = defaultValue ? moment(defaultValue) : moment();
            field.dataSource  = parseDataSource(field.data);
            break;

          case 'checkbox':
            field.dataSource  = parseDataSourceChecked(field.data, defaultValue);
            defaultValue = _.where(field.dataSource, {Checked: true});
            break;
          case 'radio':
            field.dataSource  = parseDataSourceChecked(field.data, defaultValue);
            var def = _.findWhere(field.dataSource, {Checked: true});
            if (def) {
              def.Checked = def.Value;
              defaultValue = [def];
            }
            break;
          case 'dropdown':
            field.dataSource  = parseDataSource(field.data);
            defaultValue = dropdownDefaultValue(field.dataSource, defaultValue);
            if (sfUtil.findProp(field.rules, 'multiselect') && !(defaultValue instanceof Array)) {
              defaultValue = [defaultValue];
            }
            break;
          case 'toggle':
            field.dataSource  = parseDataSource(field.data);
            defaultValue = {
              Value: defaultValue === 'true'
            };
            break;
          default:
            field.dataSource  = parseDataSource(field.data);
        }

        if (parentGuid) {
          service.tableTemp[parentGuid][field.guid] = defaultValue;
        }
        else {
          service.values[field.guid] = defaultValue;
        }
      } catch (err) {
        console.error(err);
      }

    };

    /* ==============================================
     * == PUBLIC FUNCTIONS
     * ============================================== */

    /**
     * @ngdoc function
     * @name service.init
     * @description
     * initilize the service when case is created or selected
     */
    service.init = function () {
      service.current     = {};
      service.codelists   = {};
      service.templates   = {};
      service.procedure   = {};
      service.reports     = {};
      service.values      = {};
      service.closed      = false;
      service.name        = undefined;
      service.location    = undefined;
    };

    /**
     * @ngdoc function
     * @name CurrentService.setValues
     * @description set values object from current procedure fields
     */
    service.setValues = function () {

      var i, j;

      // Loop trough forms and set values
      for (i in service.procedure.forms) {
        if (i !== undefined) {
          getFormData(service.procedure.forms[i]);
        }
      }

      // Loop trough fields and set values
      for (i in service.procedure.fields) {
        if (i !== undefined) {
          getFieldData(service.procedure.fields[i]);

          if (service.procedure.fields[i].table || service.procedure.fields[i].table.length) {
            for (j in service.procedure.fields[i].table) {
              if (j !== undefined) {
                getFieldData(service.procedure.fields[i].table[j]);
              }
            }
          }
        }
      }
    };

    /**
     * @ngdoc function
     * @name CurrentService.getParentGuid
     * @description Get Guid of parent element
     * @param {string} id
     * @returns Guid
     */
    service.getParentGuid = _.memoize(function (id) {
      var field = _.findWhere(service.procedure.fields, {id: id});
      return field ? field.guid : 0;
    });

    /**
     * @ngdoc function
     * @name CurrentService.getParentGuid
     * @description Get Guid of parent element
     * @param {string} id
     * @returns Guid
     */
    service.saveProgress = function () {
      return saveProgress();
    };

    /**
     * @ngdoc function
     * @name CurrentService.setCodelist
     * @description set codelists
     * @returns codeitems
     */
    service.setCodelist = function (codelists) {
      var i;

      for (i in codelists) {
        if (i !== undefined) {
          service.codelists[codelists[i].Guid.toLowerCase()] = codelists[i];
        }
      }
    };

    /**
     * @ngdoc function
     * @name CurrentService.getCodelist
     * @description get codelist codeitems
     * @returns codeitems
     */
    service.getCodelist = function (guid) {
      guid = guid.toLowerCase();
      return service.codelists[guid] ? service.codelists[guid].CodeItem : [];
    };

    /**
     * @ngdoc function
     * @name $scope.getFieldTemplate
     * @description Prepare and return compiled field template for reuse in field rendering
     */
    service.getFieldTemplate = function (url) {
      // If template exits, return it
      if (service.compiledTpl[url]) {
        return service.compiledTpl[url];
      }

      // If template not exits, parse it and save it
      var tpl = $templateCache.get(url);
      service.compiledTpl[url] = $compile(tpl[1]);

      return service.compiledTpl[url];
    };

    /**
     * @ngdoc function
     * @name $scope.setFieldTemplate
     * @description Compile html template for field
     */
    service.setFieldTemplate = function (url, html) {
      service.compiledTpl[url] = $compile(html);
    };

    /**
     * @ngdoc function
     * @name CurrentService.updateOne
     * @description update singel entry in fields table
     * @param {int} field guid
     * @param {object} value
     * @returns deffered promise of procedure details
     */
    service.updateOne = function (field, value) {
      if (service.procedure.procedure.guid) {
        var guid = field;
        if (!angular.isString(field)) {
          guid = field.guid.toUpperCase();
        }
        if (field.parentGuid) {
          service.updateTable(field.parentGuid.toUpperCase(), guid, value);
        } else {
          service.values[guid] = value;
        }
        saveProgress();
      }
    };

    /**
     * @ngdoc function
     * @name CurrentService.updateOne
     * @description update singel entry in fields table
     * @param {int} guid
     * @param {object} field
     * @param {object} value
     * @returns deffered promise of procedure details
     */
    service.updateEmail = function (guid, field, value) {
      if (service.values[guid] !== undefined) {
        service.values[guid][field].value.value = value;
      }
      else {
        service.values[guid] = [];
        service.values[guid][0] = {
          value: {
            value: value
          }
        };
      }
      saveProgress();
    };

    /**
     * @ngdoc function
     * @name service.updateTable
     * @description updates table values
     * @param {int} parentId
     * @param {int} guid
     * @param {object} value
     * @returns deffered promise of procedure details
     */
    service.updateTable = function (parentId, guid, value) {
      var parentGuid = service.getParentGuid(parentId);

      // Create array if not exists
      if (service.tableTemp[parentGuid] === undefined) {
        service.tableTemp[parentGuid] = {};
      }

      service.tableTemp[parentGuid][guid] = value;
    };

    /**
     * @ngdoc function
     * @name service.updateTable
     * @description updates table values
     * @param {int} parentGuid
     * @param {int} id
     * @param {object} value
     * @returns deffered promise of procedure details
     */
    service.saveTable = function (parentGuid) {
      var temp;

      // Create array if not exists
      if (service.values[parentGuid] === undefined) {
        service.values[parentGuid] = [];
      }

      temp = angular.copy(service.tableTemp[parentGuid]);
      var empty = true;

      if (temp) {
        for (var e in temp) {
          if (temp[e] instanceof Array) {
            if (temp[e].length === 1 && temp[e][0]) {
              empty = false;
              break;
            }
          } else if (temp[e]) {
            empty = false;
            break;
          }
        }
      }

      if (!empty) {
        service.values[parentGuid].push(temp);

        for (var i in service.tableTemp[parentGuid]) {
          if (i !== undefined) {
            service.tableTemp[parentGuid][i] = undefined;
            angular.element('#' + i).val(undefined);
          }
        }

        saveProgress();

        $timeout(
          function () {
            $rootScope.$broadcast('table-reset', {id: parentGuid});
          },
        100);
      }
    };

    /**
     * @ngdoc function
     * @name service.setValid
     * @param {boolean} value
     * @param {object} errorList
     * @description set current form as valid or invalid and counts number of errors
     */
    service.setValid = function (value, errorList) {
      var errorCount = 0, key;

      if (service.current.form.type !== 3) {
        if (errorList.$error) {
          for (key in errorList.$error) {
            if (errorList.$error.hasOwnProperty(key)) {
              errorCount += _.where(errorList.$error[key], {
                $name: 'subForm'
              }).length;
            }
          }
        }
      }

      else {
        for (key in errorList.$error) {
          if (errorList.$error.hasOwnProperty(key)) {
            for (var index = 1; index < 10; index++) {
              errorCount += _.where(errorList.$error[key], {
                $name: 'subForm' + index
              }).length;
            }
          }
        }
      }

      service.current.form.valid = errorCount <= 0;
      service.current.form.errors = errorCount;
    };

    /**
     * @ngdoc function
     * @name service.getField
     * @description findes field with specific guid
     * @param {int} guid
     * @returns field object
     */
    service.getField = function (guid) {
      return _.findWhere(service.procedure.fields, {guid: guid});
    };

    /**
     * @ngdoc function
     * @name service.isValid
     * @description checks if procedur is valid
     */
    service.isValid = function () {
      for (var index = 0; index < service.procedure.forms.length; index++) {
        if (!service.procedure.forms[index].valid) {
          return false;
        }
      }
      return true;
    };

    /**
     * @ngdoc function
     * @name $scope.isShown
     * @description Returns true/false if field is shown
     */
    service.isShown = function (deps) {

      if (deps.length <= 0) {
        return true;
      }

      var show = true,
          field,
          i,
          len,
          guid,
          operator,
          value,
          fieldType,
          parentGuid;

      // Check all field dependencies
      for (i = 0, len = deps.length; i < len; i++) {

        // Return when show is false
        if (!show) {
          return false;
        }

        // Get dependency settings
        operator   = deps[i].operator;
        value      = deps[i].value;
        guid       = deps[i].field.guid.toUpperCase();
        fieldType  = deps[i].field.type.toUpperCase();
        parentGuid = deps[i].field.parentGuid ? deps[i].field.parentGuid.toUpperCase() : undefined;

        // Get field value
        if (parentGuid) {
          field = service.tableTemp[parentGuid];
          if (field) {
            field = field[guid];
          }
        } else {
          field = service.values[guid];
        }

        // Compare values
        if (field instanceof Array && field.length > 0) {
          switch (fieldType) {
            case 'FILE':
              show = compare(operator, value, field.length);
              break;
            case 'RADIO':
              if (field[0] === undefined) {
                show = compareArray(operator, value, ['']);
              } else {
                show = compareArray(operator, value, _.pluck(field, 'Label'));
              }
              break;
            default:
              show = compareArray(operator, value, _.pluck(field, 'Label'));
          }
        }
        else if (field instanceof Object && !field.length) {
          switch (fieldType) {
            case 'TOGGLE':
              show = compare(operator, value, field.Value);
              break;
            case 'DATE':
              value = new Date(value);
              show = compareDate(operator, value, field);
              break;
            case 'TIME':
              value = new Date(value);
              show = compareTime(operator, value, field);
              break;
            case 'CHECKBOX':
              if (field.length === 0) {
                show = compare(operator, value, '');
              }
              break;
            default:
              show = compare(operator, value, field.Label);
          }
        }
        else if (field !== undefined && field !== null && field.length > 0) {
          switch (fieldType) {
            case 'SIGNATURE':
              show = compare(operator, value, true);
              break;
            default:
              show = compare(operator, value, field);
          }
        }
        else {
          switch (fieldType) {
            case 'TOGGLE':
              show = compare(operator, value, false);
              break;
            case 'SIGNATURE':
              show = compare(operator, value, false);
              break;
            default:
              show = compare(operator, value, '');
          }
        }
      }
      return show;
    };

    /**
     * @ngdoc function
     * @name $scope.setDeps
     * @description Set field dependencies change events
     */
    service.setDeps = function (deps, scope) {
      try {
        if (deps && scope) {
          for (var i = 0; i < deps.length; i++) {
            registerOnEvent(scope, deps, deps[i]);
          }
        }
      }
      catch (err) {
        console.log(err);
      }
    };

    /**
     * @ngdoc function
     * @name $scope.getCasesClosedValue
     * @description Checks if case is closed
     * @returns boolean
     */
    service.getCasesClosedValue = function () {
      return service.closed;
    };

    /**
     * @ngdoc function
     * @name $scope.setCasesClosedValue
     * @description Returns if case closed
     */
    service.setCasesClosedValue = function (value) {
      if (!service.preview) { service.casesClosed = value; }
    };

    /**
     * @ngdoc function
     * @name $scope.setPreviewValue
     * @description Sets preview value
     */
    service.setPreviewValue = function (value, data) {
      service.caseContent = data;
      service.preview     = value;
    };

    /**
     * @ngdoc function
     * @name $scope.getPreviewValue
     * @description Gets preview value
     * @returns boolean
     */
    service.getPreviewValue = function () {
      return service.preview;
    };

    /**
     * @ngdoc function
     * @name $scope.getCaseContent
     * @description Gets whole case content if any
     */
    service.getCaseContent = function () {
      if (service.preview) {
        return service.caseContent;
      }
      else {
        return undefined;
      }
    };

    /**
     * @ngdoc function
     * @name $scope.saveValue
     * @description saveValue function replacement for all directives to use
     */
    service.saveValue = function (parentId, id, value, guid) {
      //console.log(parentId, id, value, guid);
      if (parentId) {
        service.updateTable(parentId, guid, value);
      }
      else {
        service.updateOne(guid, value);
      }
      $rootScope.$broadcast(guid + '-change', value);
    };

    /**
     * @ngdoc function
     * @name service.addTemplate
     * @description adds a compiled template to cache
     */
    service.setTemplate = function (form, section, parent, template) {
      service.templates['form' + form] = angular.isObject(service.templates['form' + form]) ?
                                          service.templates['form' + form] : {};
      service.templates['form' + form]['section' + section] = angular.isObject(
                                                              service.templates['form' + form]['section' + section]) ?
                                                              service.templates['form' + form]['section' + section] : {}
                                                              ;
      service.templates['form' + form]['section' + section]['parent' + parent] = template;
    };

    /**
     * @ngdoc function
     * @name service.getTemplate
     * @description gets a compiled template from cache
     */
    service.getTemplate = function (form, section, parent) {
      if (service.templates['form' + form]) {
        if (service.templates['section' + section]) {
          return service.templates['form' + form]['section' + section]['parent' + parent];
        }
      }
      return false;
    };

    /**
     * @ngdoc function
     * @name service.clearTemplates
     * @description clears template cache
     */
    service.clearTemplates = function () {
      service.templates = {};
    };

    service.disableFormFn = function (disable) {
      service.disableForm = disable;
    };

    $ionicPlatform.onHardwareBackButton(function() {
      if (service.disableForm) {
        service.disableFormFn(false);
      }
    });

    /* ==============================================
     * == MAKE SERVICE USABLE
     * ============================================== */

    return service;
  }

})();
