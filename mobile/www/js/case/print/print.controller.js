(function () {
  'use strict';

  /**
   * @ngdoc overview
   * @name HomeCtrl
   * @description
   * controller that is responsible for renderign print layout
   *
   * home/home.controller.js
   *
   * (c) 2015 Comland d.o.o. http://www.comland.si
   */

  angular
    .module('SmartForms.case.print.controller', ['SmartForms.case.print.service'])
    .controller('PrintCtrl', PrintCtrl);

  PrintCtrl.$inject = [
    '$scope',
    '$stateParams',
    '$ionicLoading',
    '$ionicSideMenuDelegate',
    'CaseService',
  ];

  function PrintCtrl($scope, $stateParams, $ionicLoading, $ionicSideMenuDelegate, CaseService) {

    /* ==============================================
     * == INITIALIZE
     * ============================================== */

    $scope.defaultScale = 0.3;
    $scope.data         = {
      idProcedure       : $stateParams.procedureId,
      idProcedureReport : $stateParams.procedureReportId
    };

    /* ==============================================
     * == PUBLIC FUNCTIONS
     * ============================================== */

    /**
     * @ngdoc function
     * @name $scope.getReports
     * @description loads report details
     */
    $scope.getReports = function () {
      CaseService.getReports($scope.data.idProcedure,
                             $scope.data.idProcedureReport <= 0 ? undefined : $scope.data.idProcedureReport).then(
        function (result) {
          $scope.reports = result;
        },
        function (error) {
          console.log(error);
          $ionicLoading.hide();
        }
      );
    };

    /* ==============================================
     * == RUN CONTROLER
     * ============================================== */

    $ionicSideMenuDelegate.canDragContent(false);

    $scope.getReports();

  }

})();
