(function () {
  'use strict';

  /**
   * @ngdoc overview
   * @name sfPrint
   * @description
   * directive for print preview
   *
   * case/print/print.directive.js
   *
   * (c) 2015 Comland d.o.o. http://www.comland.si
   */

  angular
    .module('SmartForms.case.print.directive', [])
    .directive('sfPrint', sfPrint);

  sfPrint.$inject = [
    '$rootScope',
    'PrintService',
    '$compile',
    'ErrorService',
    '$ionicPopup',
    'gettextCatalog',
    '$cordovaPrinter',
    '$timeout'
  ];

  function sfPrint($rootScope, PrintService, $compile, ErrorService, $ionicPopup,
                   gettextCatalog, $cordovaPrinter, $timeout) {

    /* ==============================================
     * == PRIVATE VARS & FUNCTIONS
     * ============================================== */

    /* =====================================
     * = DIRECTIVE
     * ===================================== */
    return {
      restrict: 'E',
      scope: {
        report: '=',
        values: '=',
        fields: '=',
        closed: '='
      },
      templateUrl: 'js/case/print/part.print.html',
      link: function (scope) {

        /* ==============================================
         * == INITIALIZE
         * ============================================== */
        var iframe, a4, watermarkImage, iframedoc, defaultZoom, zoom;

        scope.rc   = {};

        /* ==============================================
         * == PRIVATE FUNCTIONS
         * ============================================== */

        /**
         * @ngdoc function
         * @name resize
         * @description calculate iframe height and width and defaultZoom
         */
        var resize = function () {
          // Resize iFrame
          var parent = angular.element(iframe).parent();
          iframe.style.height   = window.innerHeight * 0.9 -
                                  angular.element(parent).parent().find('.bar-header').outerHeight() -
                                  angular.element(parent).parent().find('.bar-footer').outerHeight() + 'px';
          iframe.style.width    = window.innerWidth * 0.95 + 'px';
          parent.height(iframe.style.height);
          parent.width(iframe.style.width);
          // Set default zoom
          defaultZoom = ((window.innerWidth * 0.95) / ((a4[0] * 1.33333) - 80) * 100) - 1.5;
          if (window.localStorage.isDeviceTablet === 'true') {
            defaultZoom -= 2;
          }
          zoom = defaultZoom;
          updateZoomProperty();
        };

        /**
         * @ngdoc function
         * @name runReport
         * @description prepares the report content and rendres to the iFrame
         */
        var runReport = function () {

          scope.rc.content = $compile('<div>' + PrintService.stylePageBreaks(scope.report.content) + '</div>')(scope);

          iframe = document.getElementById('printContentIframe');
          a4 = [595.28, 841.89]; // for a4 size paper width and height
          watermarkImage = PrintService.createWatermark();
          iframedoc = iframe.contentDocument || iframe.contentWindow.document;

          angular.element(iframedoc).find('body').append(scope.rc.content);

          angular.element(iframedoc).find('head').append($('<link/>', {
            rel     : 'stylesheet',
            href    : 'css/report.css',
            type    : 'text/css'
          }));

          angular.element(iframedoc).find('head').append($('<link/>', {
            rel     : 'stylesheet',
            href    : 'css/bootstrap.min.css',
            type    : 'text/css'
          }));

          iframedoc.body.childNodes[0].style.width      = ((a4[0] * 1.33333) - 80);
          iframedoc.body.childNodes[0].style.minHeight  = ((a4[1] * 1.33333) - 80);
          iframedoc.body.childNodes[0].style.padding    = '5 10';
          iframedoc.body.childNodes[0].style.wordWrap   = 'break-word';
          iframedoc.body.childNodes[0].style.whiteSpace = 'normal';
          iframedoc.body.childNodes[0].style.border     = '1px solid #ddd';
          iframedoc.body.childNodes[0].style.boxShadow  = '3px 3px 4px #ddd';
          if (!scope.closed) {
            iframedoc.body.childNodes[0].style.background = 'url("' + watermarkImage + '") repeat';
          }

          resize();

          if (window.cordova) {
            scope.defaultScale = (window.innerWidth / iframe.contentWindow.document.body.scrollWidth) -
                                 ((10 / window.innerWidth));
          }
          else {
            scope.defaultScale = 1;
          }

          $timeout(function () {
            var retval = scope.rc.content.html();
            var i = 0;
            for (i = 0; i < PrintService.searchStrings.length; i++) {
              retval = retval.replace(new RegExp(PrintService.searchStrings[i] + ' ' +
                       PrintService.additionString, 'g'), PrintService.searchStrings[i]);
            }

            scope.rc.html = retval;
          }, 0);

        };

        /**
         * @ngdoc function
         * @name $scope.rc.getFieldData
         * @description get field value
         */
        scope.rc.getFieldData = _.memoize(function (guid) {
          guid = guid.toUpperCase();
          try {
            return PrintService.getFieldData(scope.values[guid], scope.fields[guid].type, scope.fields[guid].props,
                                             scope.fields[guid]);
          }
          catch (err) {
            console.log(err);
          }
        });

        /**
         * @ngdoc function
         * @name  $scope.rc.zoomOut
         * @description zoom in on iframe
         */
        scope.rc.zoomOut = function () {
          if (zoom > 40) {
            zoom -= 10;
            updateZoomProperty();
          }
        };

        /**
         * @ngdoc function
         * @name  $scope.rc.zoomOut
         * @description zoom out on iframe
         */
        scope.rc.zoomIn = function () {
          if (zoom < 200) {
            zoom += 10;
            updateZoomProperty();
          }
        };

        /**
         * @ngdoc function
         * @name  $scope.rc.zoomReset
         * @description reset zoom
         */
        scope.rc.zoomReset = function () {
          zoom = defaultZoom;
          updateZoomProperty();
        };

        /**
         * @ngdoc function
         * @name updateZoomProperty
         * @description Updates zoom view
         */
        function updateZoomProperty() {
          var element = angular.element(iframedoc).find('body');
          var tempZoom = zoom + '%';
          angular.element(element).css('zoom', tempZoom);
          angular.element(element).css('-webkit-text-size-adjust', tempZoom);
          angular.element(element).css('-webkit-transform', 'scale(' + tempZoom + ')');
          angular.element(element).css('-webkit-transform-origin', '0 0');
          angular.element(element).css('-moz-transform', 'scale(' + tempZoom + ')');
          angular.element(element).css('-moz-transform-origin', '0 0');
          angular.element(element).css('-o-transform', 'scale(' + tempZoom + ')');
          angular.element(element).css('-o-transform-origin', '0 0');
        }

        /**
         * @ngdoc function
         * @name $scope.rc.print
         * @description print content
         */
        scope.rc.print = function () {
          console.log('starting print');
          if (!window.cordova) {
            window.alert(gettextCatalog.getString('Printing is not available on device'));
          }
          else {
            console.log($cordovaPrinter, $cordovaPrinter.isAvailable());
            /*window.cordova.plugins.printer.isAvailable(
              function () {
                try {
                  window.cordova.plugins.printer.print(scope.rc.html);
                }
                catch (err) {
                  window.console.log('Napaka ', err);
                  ErrorService.Message(err);
                }
              },
              function (error) {
                window.console.log('Napaka ', error);
                window.alert(gettextCatalog.getString('Printing is not available on device'));
              }
            );*/
            //if($cordovaPrinter.isAvailable()) {
            try {
              $cordovaPrinter.print(scope.rc.html);
            }
            catch (err) {
              window.console.log('Napaka ', err);
              ErrorService.Message(err);
            }
            //}
            //else {
            //  window.alert(gettextCatalog.getString('Printing is not available on device'));
            //}
          }
        };

        /**
         * @ngdoc function
         * @name $scope.rc.close
         * @description close print preview popup
         */
        scope.rc.close = function () {
          $rootScope.$broadcast('closePrintPopup');
        };

        /* ==============================================
         * == WATCHERS & EVENTS
         * ============================================== */

        scope.$watch('closed', function(v) {
          if (v) {
            iframedoc.body.childNodes[0].style.background = 'none';
          } else {
            iframedoc.body.childNodes[0].style.background = 'url("' + watermarkImage + '") repeat';
          }
        });

        /* ==============================================
         * == RUN CONTROLLER
         * ============================================== */

        runReport();
      }
    };

  }

})();
