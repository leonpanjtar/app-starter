(function () {
  'use strict';

  /**
   * @ngdoc service
   * @name PrintService
   * @description
   * this service takes care of print preview screen
   *
   * case/print/print.service.js
   *
   * (c) 2015 Comland d.o.o. http://www.comland.si
   *
   * REQ'D: pdfmake libraries https://github.com/bpampuch/pdfmake
   */
  angular.module('SmartForms.case.print.service', [])
    .service('PrintService', PrintService);

  PrintService.$inject = [
    'gettextCatalog',
    '$sce'
  ];

  function PrintService(gettextCatalog, $sce) {

    var srv = this;

    srv.searchStrings = ['page-break-before: always;'];
    srv.additionString = ['border-bottom: 2px dashed blue;'];

    /**
     * @ngdoc function
     * @name PrintService._createWatermark
     * @returns picture of watermark
     * @description
     * creates watermark picture
     */
    function _createWatermark() {
      var canvas = document.createElement('canvas');

      canvas.id = 'CursorLayer';
      canvas.width = 200;
      canvas.height = 200;
      canvas.style.zIndex = '8';
      canvas.style.position = 'Fixed';
      canvas.style.top = '0';
      canvas.style.border = '1px solid';
      canvas.style.backgroundColor = 'blue';

      var ctx = canvas.getContext('2d');
      ctx.save();
      ctx.fillStyle = 'rgba(0, 0, 0, 0.1)';
      ctx.rotate(-Math.PI / 4);
      ctx.textAlign = 'center';
      ctx.font = 'bold 25px Arial';
      ctx.fillText(gettextCatalog.getString('PREVIEW'), 0, (canvas.width / 1.5) + 50);
      ctx.restore();

      var dataURL = canvas.toDataURL();
      return dataURL;
    }

    /**
     * @ngdoc function
     * @name PrintService._stylePageBreaks
     * @param {string} htmlStr string for page breaks insertion
     * @returns : updated html string
     * @description
     *
     * adds style for page break markers
     */
    function _stylePageBreaks(htmlStr) {
      var retval = htmlStr;
      var i;

      for (i = 0; i < srv.searchStrings.length; i++) {
        retval = htmlStr.replace(
                          new RegExp(srv.searchStrings[i], 'g'),
                          srv.searchStrings[i] + ' ' + srv.additionString);
      }

      return retval;
    }

    /**
     * @ngdoc function
     * @name PrintService._getField
     * @param {guid} fieldValue
     * @returns : object representing field data
     * @description
     *
     * gets field data, depending on fields type
     */
    function _getFieldData(fieldValue, fieldType, fieldProps, field, table) {

      if (!fieldValue && !fieldProps && fieldType.toUpperCase() !== 'TOGGLE') {
        return '';
      }

      var tempFormat = _.findWhere(fieldProps, {name: 'format'});

      switch (fieldType.toUpperCase()) {
        case 'INPUT':
        case 'HIDDEN':
        case 'BARCODE':
        case 'GPS':
          return fieldValue;

        case 'FILE':
          console.log(fieldValue);
          return fieldValue;

        case 'TEXTAREA':
          return $sce.trustAsHtml(fieldValue.replace(/(?:\r\n|\r|\n)/g, '<br>'));

        case 'SIGNATURE':
          if (table === true) {
            return $sce.trustAsHtml('<img src="' + fieldValue + '">');
          } else {
            return fieldValue;
          }
          break;

        case 'HTML':
          return fieldProps.substring(1, fieldProps.length - 1);

        case 'DROPDOWN':
        case 'CHECKBOX':
        case 'RADIO':
          if (table === true) {
            return prepareData(fieldValue).join(', ');
          } else {
            return prepareData(fieldValue);
          }
          break;

        case 'TOGGLE':
          if (table) {
            return fieldValue ? gettextCatalog.getString('Yes') : gettextCatalog.getString('No');
          }
          return fieldValue ? fieldValue.Value : false;

        case 'TABLE':
          return prepareTableData(fieldValue, field);

        case 'DATE':
          if (tempFormat) {
            return moment(fieldValue).format(tempFormat.value);
          }
          return moment(fieldValue).format('DD.MM.YYYY');

        case 'TIME':
          if (tempFormat) {
            return moment(fieldValue).format(tempFormat.value);
          }
          return moment(fieldValue).format('HH:mm');

        default:
          return '';
      }

    }

    /**
     * @ngdoc function
     * @name prepareTableData
     * @param {object} data
     * @returns : array of objects
     * @description
     *
     * manages table data
     */
    function prepareTableData(data, field) {

      var i = 0;
      var retval = [];
      var table = [];
      //add field names as first index
      var r = [];
      var exclude = [
        'HEADER',
        'SUBHEADER',
        'HTML'
      ];
      var property;
      if (field.table) {
        r = [];
        for (property in field.table) {
          if (field.table.hasOwnProperty(property) &&
              exclude.indexOf(field.table[property].type.toUpperCase()) === -1) {
            r.push(field.table[property].name);
            table.push(property);
          }
        }
        retval.push(r);
      }
      var j = 0;
      for (i = 0; i < data.length; i++) {
        r = [];
        for (j = 0; j < table.length; j++) {
          property = table[j];
          if (data[i].hasOwnProperty(property)) {
            r.push(_getFieldData(data[i][property],
                                 field.table[property].type,
                                 field.table[property].props,
                                 field,
                                 true));
          } else {
            if (field.table[property].type.toUpperCase() === 'TOGGLE') {
              r.push(gettextCatalog.getString('No'));
            } else {
              r.push('');
            }
          }
        }
        retval.push(r);
      }
      return retval;
    }

    /**
     * @ngdoc function
     * @name prepareData
     * @param {object} data
     * @returns : array of Strings
     * @description
     *
     * returns array of Labels - for dropdown/checkbox
     */
    function prepareData(data) {
      var i = 0;
      var retval = [];

      if (data.length === undefined) {
        retval.push(data .Label);
      }
      else {
        for (i = 0; i < data.length; i++) {
          retval.push(data[i].Label);
        }
      }
      return retval;
    }

    srv.getFieldData = _getFieldData;
    srv.stylePageBreaks = _stylePageBreaks;
    srv.createWatermark = _createWatermark;

  }

})();
