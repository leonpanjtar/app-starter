(function () {
  'use strict';

  /**
   * @ngdoc service
   * @name CaseService
   * @description
   * this service takes care of loading and managing cases screen
   *
   * case/case.service.js
   *
   * (c) 2015 Comland d.o.o. http://www.comland.si
   *
   */

  angular
    .module('SmartForms.case.service', [
      'SmartForms.auth.service',
      'SmartForms.common.dbconnection',
      'SmartForms.common.cipher',
      'gettext'
    ])
    .service('CaseService', CaseService);

  CaseService.$inject = [
    '$http',
    '$rootScope',
    '$q',
    'Constants',
    'DBConnection',
    'gettext'
  ];

  function CaseService($http, $rootScope, $q, Constants, DBConnection, gettext) {

    var service = this;

    var createGUID = function () {
      var s = [];
      var hexDigits = '0123456789abcdef';
      for (var i = 0; i < 36; i++) {
        s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
      }
      s[14] = '4';  // bits 12-15 of the time_hi_and_version field to 0010
      s[19] = hexDigits.substr((s[19] && 0x3) || 0x8, 1);  // bits 6-7 of the clock_seq_hi_and_reserved to 01
      s[8] = s[13] = s[18] = s[23] = '-';

      var uuid = s.join('');
      return uuid;
    };

    service.prepareProcedure = function (id, caseId) {

      var deferred = $q.defer();

      var data, codelists, reports;

      service.getProcedure(id).then(

        function (results) {

          data = results.data;

          service.getCodelists(results.sourceGuids).then(

            function (resoults) {
              codelists = resoults;

              service.getReports(id).then(
                function (result) {
                  reports = result;

                  service.getCaseData(caseId, data).then(
                    function (result) {
                      data.procedure.guid = result.guid;

                      deferred.resolve({
                        data        : data,
                        codelists   : codelists,
                        reports     : reports,
                        closed      : result.closed,
                        values      : result.values,
                        name        : result.name
                      });
                    },
                    function (error) {
                      console.log(error);
                    });
                },
                function (error) {
                  console.log(error);
                });
            },

            function (error) {
              console.log(error);
              deferred.reject(error);
            });
        },

        function (error) {
          console.log(error);
          deferred.reject(error);
        });

      return deferred.promise;
    };
    /**
     * @ngdoc function
     * @name getCaseByGUID
     * @param {string} guid case guid
     * @description
     * Returns case data with specific GUID.
     * @returns {promise} Promise which is resolved with case data.
     */
    service.getCaseByGUID = function(guid) {
      var query = 'SELECT * FROM [CASE] WHERE [CASE].Guid = ?';
      return DBConnection.queryOne(query, [guid]);
    };

    service.getCaseData = function (caseId) {
      /**
       * @ngdoc property
       * @name values
       * @description this value stores content of a closed(for preview) or open case (for edit).
       * These values are equal to values that are stored in CurrentService.
       * So we use this values to initialize CurrentService.values with these values.
       */
      var deferred = $q.defer(),values;

      if (!caseId || caseId === '') {
        deferred.resolve({
          closed: false,
          guid: ''
        });

      } else {

        var query = 'SELECT * FROM [CASE] WHERE [CASE].Id_Case = ?';

        DBConnection.queryOne(query, [caseId]).then(
          function (results) {
            // GET CASE DATA
            // if case is not closed just set values
            values = JSON.parse(results.Content);

            deferred.resolve({
              closed    : results.Closed,
              guid      : results.Guid,
              values    : values,
              name      : results.Name,
              location  : results.Location
            });
          },

          function (error) {
            if (Constants.DEBUGMODE) {
              console.log('Error on select user from db');
              console.log(error);
            }
            deferred.reject(error);
          });
      }

      return deferred.promise;
    };

    /**
    * @ngdoc function
    * @name case.getProcedure
    * @description get procedure details and structure
    * @param {int} id
    * @returns deffered promise of procedure details
    */
    service.getProcedure = function (id) {
      var deferred = $q.defer();

      //var rows = [],
      var procedure = {},
        forms = [],
        formsReports = [],
        sections = [],
        fields = {};

      var sourceGuids = [];

      var query = 'SELECT ' +
        'p.[Id_Procedure], p.[Version], p.Name AS [ProcName], p.[Description] AS ProcDesc, ' +
        'p.[Image], f.[Id_Form], f.[Guid] AS FormGuid, f.[Label] AS FormName, f.[Description] AS FormDesc,  ' +
        'f.[Dependencies] AS FormDeps, f.[Properties] AS FormProp, f.[Order] AS FormOrder, f.Type AS FormType, ' +
        'f.[Version] AS FormVersion, s.[Id_Section], s.[Guid] AS SectGuid, s.[Label] AS SectName, s.[Description] AS ' +
        'SectDesc, s.[Dependencies] AS SectDeps, s.[Properties] AS SectProp, s.[Order] AS SectOrder, ' +
        'fl.[Id_Field], fl.[Guid] AS FieldGuid, fl.[Label] AS FieldName, fl.[Help] AS FieldHelp, fl.[Hint] AS ' +
        'FieldHint, fl.[Type] AS FieldType, fl.[DataSource] AS FieldDataSource, fl.[Dependencies] AS FieldDeps,  ' +
        'fl.[Rules] AS FieldRules, fl.[Properties] AS FieldProp, fl.[Row] AS FieldRow, fl.[Coll] AS FieldCol, ' +
        'fl.[Id_Field_Parent] AS FieldParent FROM PROCEDURE p  ' +
        'LEFT OUTER JOIN FORM f ON f.Id_Procedure = p.Id_Procedure ' +
        'LEFT OUTER JOIN SECTION s ON s.Id_Form = f.Id_Form ' +
        'LEFT OUTER JOIN FIELD fl ON fl.Id_Section = s.Id_Section ' +
        'WHERE p.Id_Procedure = ? ' +
        'AND f.Type IN (1,2,3) ' +
        'GROUP BY fl.Id_Field, s.Id_Section, f.Id_Form, p.Id_Procedure ' +
        'ORDER BY FormOrder, SectOrder, FieldParent ASC, FieldRow, FieldCol';

      DBConnection.query(query, [id]).then(
        function (results) {

          if (results.rows.length) {

            for (var index = 0; index < results.rows.length; index++) {
              var value = results.rows.item(index);

              // Add procedure if not exists
              if (!procedure.Id_procedure) { // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
                procedure = {
                  id        : value.Id_Procedure, // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
                  name      : value.ProcName,
                  desc      : value.ProcDesc,
                  image     : value.Image,
                  version   : value.Version
                };
              }

              switch (value.FormType) {
                // Add form if not exists
                case 1:
                case 3:
                  if (!_.findWhere(forms, {id: value.Id_Form})) { // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
                    forms.push({
                      procid    : value.Id_Procedure, // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
                      id        : value.Id_Form, // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
                      name      : value.FormName,
                      desc      : value.FormDesc,
                      guid      : value.FormGuid ? value.FormGuid.toUpperCase() : '',
                      deps      : value.FormDeps,
                      props     : value.FormProp,
                      type      : value.FormType,
                      order     : value.FormOrder,
                      version   : value.FormVersion,
                      valid     : undefined,
                      shown     : true,
                      sections  : []
                    });
                  }
                  break;
                case 2:
                  if (!_.findWhere(forms, {id: value.Id_Form})) { // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
                    formsReports.push({
                      procid    : value.Id_Procedure, // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
                      id        : value.Id_Form, // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
                      name      : value.FormName,
                      desc      : value.FormDesc,
                      guid      : value.FormGuid ? value.FormGuid.toUpperCase() : '',
                      deps      : value.FormDeps,
                      props     : value.FormProp,
                      type      : value.FormType,
                      order     : value.FormOrder,
                      version   : value.FormVersion,
                      valid     : undefined,
                      shown     : true,
                      sections  : []
                    });
                  }
                  break;
              }

              // Add section if not exists
              // jscs:disable requireCamelCaseOrUpperCaseIdentifiers
              if (!_.findWhere(sections, {id: value.Id_Section})) {
                // jscs:enable requireCamelCaseOrUpperCaseIdentifiers
                sections.push({
                  procId    : value.Id_Procedure, // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
                  formId    : value.Id_Form, // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
                  id        : value.Id_Section, // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
                  name      : value.SectName,
                  desc      : value.SectDesc,
                  guid      : value.SectGuid ? value.SectGuid.toUpperCase() : '',
                  deps      : value.SectDeps,
                  props     : value.SectProp,
                  order     : value.SectOrder,
                  shown     : true,
                  fields    : {}
                });
              }

              // Add field if not exists
              if (!_.findWhere(fields, {id: value.Id_Field})) { // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
                var t = _.findWhere(Constants.fieldType, {value: value.FieldType});
                var guid;
                if (value.FieldParent && value.FieldParent !== 'undefined') {
                  var parent = _.findWhere(fields, {id: value.FieldParent});

                  if (parent) {
                    guid = value.FieldGuid ? value.FieldGuid : '';
                    parent.table[guid] = {
                      procId    : value.Id_Procedure, // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
                      formId    : value.Id_Form, // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
                      sectId    : value.Id_Section, // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
                      id        : value.Id_Field, // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
                      name      : value.FieldName,
                      type      : t ? t.name : 'input',
                      help      : value.FieldHelp,
                      hint      : value.FieldHint,
                      guid      : value.FieldGuid ? value.FieldGuid : '',
                      data      : value.FieldDataSource,
                      deps      : value.FieldDeps,
                      props     : value.FieldProp,
                      rules     : value.FieldRules,
                      col       : value.FieldCol,
                      row       : value.FieldRow,
                      value     : undefined,
                      parentId  : parent.id
                    };
                  }
                  //console.log(parent);
                }
                else {
                  guid = value.FieldGuid ? value.FieldGuid : '';
                  fields[guid] = {
                    procId  : value.Id_Procedure, // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
                    formId  : value.Id_Form, // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
                    sectId  : value.Id_Section, // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
                    id      : guid,
                    name    : value.FieldName,
                    type    : t ? t.name : 'input',
                    help    : value.FieldHelp,
                    hint    : value.FieldHint,
                    guid    : value.FieldGuid ? value.FieldGuid.toUpperCase() : '',
                    data    : value.FieldDataSource,
                    deps    : value.FieldDeps,
                    props   : value.FieldProp,
                    rules   : value.FieldRules,
                    col     : value.FieldCol,
                    row     : value.FieldRow,
                    value   : undefined,
                    table   : {}
                  };
                }
                if (value.FieldDataSource && value.FieldDataSource !== '') {
                  var c = JSON.parse(value.FieldDataSource);
                  if (_.has(c, 'codelist')) {
                    sourceGuids.push(c.codelist);
                  }
                }
              }
            }

            for (var j = 0; j < sections.length; j++) {
              sections[j].fields = _.groupBy(_.filter(fields, {sectId: sections[j].id}), 'row');
            }

            for (var i = 0; i < forms.length; i++) {
              forms[i].sections = _.filter(sections, {formId: forms[i].id});
            }

            var data = {
              procedure     : procedure,
              forms         : forms,
              formsReports  : formsReports,
              fields        : fields
            };

            deferred.resolve({
              data        : data,
              sourceGuids : _.uniq(sourceGuids)
            });
          }

          else {
            deferred.reject(gettext('No records found'));
          }
        },
        function (error) {
          if (Constants.DEBUGMODE) {
            console.log('Error on select procedure from db');
            console.log(error);
          }
          deferred.reject(error);
        }
        );
      return deferred.promise;
    };

    // TODO Figure out what this is and add documentation
    var makePlaceholders = function (len) {
      var s = '?';
      for (var i = 1; i < len; i++) {
        s += ',?';
      }
      return s;
    };

    /**
    * @ngdoc function
    * @name case.getProcedure
    * @description get procedure details and structure
    * @param {int} sourceGuids
    * @returns deffered promise of procedure details
    */
    service.getCodelists = function (sourceGuids) {
      var deferred = $q.defer();

      if (!sourceGuids || sourceGuids === '' || sourceGuids === [] || sourceGuids.length < 1) {
        deferred.resolve(sourceGuids);
        return deferred.promise;
      }

      var query = 'SELECT ' +
        'l.[Id_CodeList], l.[Guid] as GuidList, i.[Id_CodeItem], i.[Id_CodeList], i.[Guid], i.[Label], i.[Value] ' +
        'FROM CODELIST l  ' +
        'LEFT OUTER JOIN CODEITEM i ON i.Id_CodeList = l.Id_CodeList ' +
        'WHERE l.[Guid] IN (' + makePlaceholders(sourceGuids.length) + ') ' +
        'ORDER BY l.Id_CodeList ';

      var codelists = [];
      var curList = {};

      DBConnection.query(query, sourceGuids).then(
        function (results) {
          if (results.rows.length) {
            // TODO Preglej logiko shranjevanja Codelist
            for (var index = 0, len = results.rows.length; index < len; index++) {
              var value = results.rows.item(index);
              if (curList.Id_CodeList !== value.Id_CodeList) { // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
                if (curList.Id_CodeList) { // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
                  codelists.push(curList);
                }
                curList = {
                  Id_CodeList: value.Id_CodeList, // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
                  Guid: value.GuidList,
                  CodeItem: []
                };
              }
              curList.CodeItem.push({
                Id_CodeItem: value.Id_CodeItem, // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
                Guid: value.Guid,
                Label: value.Label,
                Value: value.Value
              });

            }
            codelists.push(curList);
            deferred.resolve(codelists);
          }

          else {
            deferred.reject(gettext('No records found'));
          }
        },
        function (error) {
          if (Constants.DEBUGMODE) {
            console.log('Error on select procedure from db');
            console.log(error);
          }
          deferred.reject(error);
        }
        );
      return deferred.promise;
    };

    /**
    * @ngdoc function
    * @name case.getReports
    * @description get procedure reports or single report
    * @param {int} idProcedure
    * @param {int} idProcedureReport
    * @returns deffered promise of procedure details
    */
    service.getReports = function (idProcedure, idProcedureReport) {
      var deferred = $q.defer();

      var query = 'SELECT Id_ProcedureReport, Id_Procedure, Guid, Label, Description, Content, Version FROM ' +
                  'procedureReport WHERE Id_Procedure = "' + idProcedure +
                  (idProcedureReport ? '" AND Id_ProcedureReport = "' + idProcedureReport : '" ') +
                  ' ORDER BY Id_ProcedureReport ';

      var valuesTable = [];

      var procedureReport = [];

      DBConnection.query(query, valuesTable).then(
        function (results) {

          if (results.rows.length) {
            for (var index = 0; index < results.rows.length; index++) {

              var rw = results.rows.item(index) ? results.rows.item(index) : results.rows[index];

              var itm = {
                idProcedureReport   : rw.Id_ProcedureReport, // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
                idProcedure         : rw.Id_Procedure, // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
                label               : rw.Label,
                description         : rw.Description,
                content             : rw.Content
              };
              procedureReport.push(itm);
            }

            deferred.resolve(procedureReport);
          }

          else {
            deferred.resolve([]);
          }
        },
        function (error) {
          if (Constants.DEBUGMODE) {
            console.log('Error on select procedureReport from db');
            console.log(error);
          }
          deferred.reject(error);
        }
        );
      return deferred.promise;
    };

    // TODO Add documentation
    // jscs:disable requireCamelCaseOrUpperCaseIdentifiers
    service.saveCase = function (Id_User, Id_Procedure, Name, Location, Version, Content) {
      // jscs:enable requireCamelCaseOrUpperCaseIdentifiers
      var deferred = $q.defer();
      var d = new Date();
      var guid = createGUID();
      var query, valuesTable;
      if (typeof Location !== 'undefined') {
        query = 'INSERT INTO [CASE] (Guid, Id_User, Id_Device, Id_Procedure, Name, Location, Content, Created,' +
                ' Closed, Version) VALUES (?,?,34,?,?,?,?,?,0,?)';
        valuesTable = [
                        guid,
                        Id_User, // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
                        Id_Procedure, // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
                        Name,
                        Location,
                        JSON.stringify(Content),
                        d.toISOString(),
                        Version
                      ];
      }
      else {
        query = 'INSERT INTO [CASE] (Guid, Id_User, Id_Device, Id_Procedure, Name, Content, Created, Closed, Version)' +
                ' VALUES (?,?,34,?,?,?,?,0,?)';
        valuesTable = [
                        guid,
                        Id_User, // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
                        Id_Procedure, // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
                        Name,
                        JSON.stringify(Content),
                        d.toISOString(),
                        Version
                      ];
      }

      DBConnection.query(query, valuesTable).then(
        function () {
          deferred.resolve(guid);
        },
        function (error) {
          if (Constants.DEBUGMODE) {
            console.log('Error on select procedure from db');
            console.log(error);
          }
          deferred.reject(error);
        }
        );
      return deferred.promise;
    };

    // TODO Add documentation
    service.updateCase = function (Guid, Content) {
      var deferred = $q.defer();

      var query = 'UPDATE [CASE] SET Content = ?, Closed = 0 WHERE Guid = ?';

      var valuesTable = [JSON.stringify(Content), Guid];

      DBConnection.query(query, valuesTable).then(
        function (results) {
          deferred.resolve(results);
        },
        function (error) {
          if (Constants.DEBUGMODE) {
            console.log('Error on select procedure from db');
            console.log(error);
          }
          deferred.reject(error);
        }
        );
      return deferred.promise;
    };

    // TODO Add documentation
    service.closeCase = function (Guid, Content) {
      var deferred = $q.defer();

      var query = 'UPDATE [CASE] SET Content = ?, Closed = 1 WHERE Guid = ?';

      var valuesTable = [JSON.stringify(Content), Guid];

      DBConnection.query(query, valuesTable).then(
        function (results) {
          $rootScope.haveClosed = true;
          deferred.resolve(results);
        },
        function (error) {
          if (Constants.DEBUGMODE) {
            console.log('Error on select procedure from db');
            console.log(error);
          }
          deferred.reject(error);
        }
        );
      return deferred.promise;
    };

    // TODO Add documentation
    // jscs:disable requireCamelCaseOrUpperCaseIdentifiers
    service.closeNewCase = function (Id_User, Id_Procedure, Name, Content) {
      // jscs:enable requireCamelCaseOrUpperCaseIdentifiers
      var deferred = $q.defer();

      var query = 'INSERT INTO [CASE] (Guid, Id_User, Id_Procedure, Name, Content, Created, Closed, Id_Device,' +
                  ' Version) VALUES (?,?,?,?,?,1,34,?)';

      var d = new Date();
      var guid = createGUID();

      var valuesTable = [
                          guid,
                          Id_User, // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
                          Id_Procedure, // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
                          Name,
                          JSON.stringify(Content),
                          d.toISOString()
                        ];

      DBConnection.query(query, valuesTable).then(
        function () {
          deferred.resolve(guid);
        },
        function (error) {
          if (Constants.DEBUGMODE) {
            console.log('Error on select procedure from db');
            console.log(error);
          }
          deferred.reject(error);
        }
        );
      return deferred.promise;
    };

    /* ==============================================
     * == MAKE SERVICE USABLE
     * ============================================== */

    return service;
  }

})();
