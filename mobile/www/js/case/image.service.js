(function () {
  'use strict';

  /**
   * @ngdoc service
   * @name ImageService
   * @description
   * this service takes care of loading and managing images inside procedures
   *
   * case/images.service.js
   *
   * (c) 2015 Comland d.o.o. http://www.comland.si
   *
   */

  angular
    .module('SmartForms.case.service.image', [])
    .factory('ImageService', ImageService);

  ImageService.$inject = [
    '$cordovaCamera',
    '$q',
    '$cordovaFile'
  ];

  function ImageService($cordovaCamera, $q, $cordovaFile) {

    function makeid() {
      var text = '';
      var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

      for (var i = 0; i < 5; i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
      }
      return text;
    }

    function optionsForType(type) {
      var source;
      var CameraPopoverOptions; // TODO : check how picking from gallery opens on iPad and set this so it looks good
      if (typeof Camera !== 'undefined') {
        switch (type) {
          case 0:
            source = Camera.PictureSourceType.CAMERA;
            break;
          case 1:
            source = Camera.PictureSourceType.PHOTOLIBRARY;
            break;
        }
        return {
          destinationType     : Camera.DestinationType.FILE_URI,
          //destinationType     : Camera.DestinationType.DATA_URL,
          sourceType          : source,
          allowEdit           : false,
          targetWidth         : 1280,
          targetHeight        : 960,
          encodingType        : Camera.EncodingType.JPEG,
          popoverOptions      : CameraPopoverOptions,
          saveToPhotoAlbum    : false
        };
      }
    }

    function saveMedia(type) {
      return $q(function(resolve, reject) {
        var options = optionsForType(type);

        $cordovaCamera.getPicture(options).then(function(imageUrl) {
          var name      = imageUrl.substr(imageUrl.lastIndexOf('/') + 1);
          var namePath  = imageUrl.substr(0, imageUrl.lastIndexOf('/') + 1);
          var newName   = makeid() + name;

          $cordovaFile.copyFile(namePath, name, cordova.file.dataDirectory, newName)
            .then(function(info) {
              resolve({
                name: newName,
                info: info
              });
            }, function(e) {
              reject(e);
            });
        });
      });
    }
    return {
      handleMediaDialog: saveMedia
    };
  }

})();
