(function () {
  'use strict';

  /**
   * @ngdoc overview
   * @name CaseCtrl
   * @description
   * controller for cases
   *
   * case/case.controller.js
   *
   * (c) 2015 Comland d.o.o. http://www.comland.si
   */

  angular
    .module('SmartForms.case.controller', ['gettext'])
    .controller('CaseCtrl', CaseCtrl);

  CaseCtrl.$inject = [
    '$rootScope',
    '$scope',
    '$state',
    '$stateParams',
    '$q',
    '$ionicPlatform',
    '$ionicLoading',
    '$ionicSideMenuDelegate',
    '$ionicScrollDelegate',
    '$ionicPopup',
    'SweetAlert',
    'gettextCatalog',
    '$cordovaGoogleAnalytics',
    '$cordovaGeolocation',
    'CaseService',
    'CurrentService',
    'AuthService',
    '$timeout'
  ];

  function CaseCtrl($rootScope, $scope, $state, $stateParams, $q, $ionicPlatform,
                    $ionicLoading, $ionicSideMenuDelegate, $ionicScrollDelegate,
                    $ionicPopup, SweetAlert, gettextCatalog, $cordovaGoogleAnalytics,
                    $cordovaGeolocation, CaseService, CurrentService, AuthService, $timeout) {

    /* ==============================================
     * == INITIALIZE
     * ============================================== */
    var procedureId         = $stateParams.procedureId;
    var caseId              = $stateParams.caseId;

    $scope.cs               = {};             // Viewmodel for case
    $scope.cs.c             = CurrentService; // Wrapper to make CurrentService available on scope
    $scope.cs.caseOpened    = true;           // New case opened?

    $scope.cs.hasNext       = false;
    $scope.cs.hasPrev       = false;
    $scope.cs.saveStep      = false;
    $scope.cs.printStep     = false;
    $scope.cs.printPopup    = undefined;

    $scope.cs.disableForm   = false;

    // Strings from translation file
    $scope.cs.progressText  = gettextCatalog.getString('PREPARING CASE DATA...');

    /* ==============================================
     * == PRIVATE FUNCTIONS
     * ============================================== */

    var scrollLeft = function () {

      var tabScroll = angular.element('div.hscroll');
      var id;
      if (!($scope.cs.printStep || $scope.cs.saveStep)) {
        id = '#' + CurrentService.current.form.id;
      } else if ($scope.cs.printStep) {
        id = '#printStep';
      } else if ($scope.cs.saveStep) {
        id = '#saveStep';
      }
      var active = tabScroll.find(id);
      if (active.length) {
        tabScroll.animate(
          {
            scrollLeft : active.position().left - (tabScroll.width() / 2) + (active.width() / 2)
          },
          {
            always: function () {
              $timeout(function () {
                active = undefined;
                tabScroll = undefined;
              }, 0);
            }
          }
        );
      }
    };

    var init = function () {
      if (window.analytics) {
        $cordovaGoogleAnalytics.trackView('Case Ctrl');
      }
      $scope.cs.platformReady();
    };

    /**
     * @ngdoc function
     * @name service.setCurrentForm
     * @description Set currently selected form
     * @param {object} form
     */
    var setCurrentForm = function (form) {

      // Set data to CurrentService
      CurrentService.current = {
        form: form,
        fields: _.where(CurrentService.procedure.fields, {formId: form.id})
      };

      // Set display data
      $scope.cs.hasPrev = (_.indexOf(CurrentService.procedure.forms, form) - 1) > -1;
      $scope.cs.hasNext = true;
      $scope.cs.saveStep = false;
      $scope.cs.printStep = false;

      if (window.analytics) {
        $cordovaGoogleAnalytics.trackView('/case/' + form.id);
      }

      $ionicScrollDelegate.$getByHandle('caseContent').scrollTop();
      scrollLeft();
    };

    // TODO: Write documentation for this function
    var validateForm = function () {
      var defferd = $q.defer();
      // set dirty
      if ($scope.procedureForm.$error.required) {
        for (var index = 0; index < $scope.procedureForm.$error.required.length; index++) {
          $scope.procedureForm.$error.required[index].$setDirty();
        }
      }

      CurrentService.setValid(!$scope.procedureForm.$invalid, $scope.procedureForm);
      defferd.resolve();
      return defferd.promise;
    };

    /**
     * @ngdoc function
     * @name self.compare
     * @description Compare values with operator
     */
    var compare = function (operator, value1, value2) {
      switch (operator) {
        case '==':
          return value1 === value2;
        case '>=':
          return value1 <= value2;
        case '<=':
          return value1 >= value2;
        case '!=':
          return value1 !== value2;
      }
    };

    /**
     * @ngdoc function
     * @name self.compareArray
     * @description Compare specific value of array
     */
    var compareArray = function (operator, value1, value2) {
      switch (operator) {
        case '==':
          return _.indexOf(value2, value1) > -1;
        case '!=':
          return _.indexOf(value2, value1) === -1;
      }
    };

    /**
     * @ngdoc function
     * @name self.minutesOfDay
     * @description Helper funcion for compareTime
     */
    var minutesOfDay = function(m) {
      return moment(m).minutes() + moment(m).hours() * 60;
    };

    /**
     * @ngdoc function
     * @name self.compareTime
     * @description Compare specific value of time
     */
    var compareTime = function (operator, value1, value2) {
      switch (operator) {
        case '>=':
          return minutesOfDay(value1) <= minutesOfDay(value2);
        case '<=':
          return minutesOfDay(value1) >= minutesOfDay(value2);
        case '==':
          return minutesOfDay(value1) === minutesOfDay(value2);
        case '!=':
          return minutesOfDay(value1) !== minutesOfDay(value2);
      }
    };

    /**
     * @ngdoc function
     * @name self.compareDate
     * @description Compare specific value of date
     */
    var compareDate = function (operator, value1, value2) {
      switch (operator) {
        case '>=':
          return moment(value1).isSameOrBefore(value2, 'day');
        case '<=':
          return moment(value1).isSameOrAfter(value2, 'day');
        case '==':
          return moment(value1).isSame(value2, 'day');
        case '!=':
          return !moment(value1).isSame(value2, 'day');
      }
    };

    /**
     * @ngdoc function
     * @name self.resetSectionFields
     * @description resets all fields of section
     */
    var resetSectionFields = function (section) {
      _.each(section.fields, function (fields) {
        _.each(fields, function (field) {
          var def = _.findWhere(field.props, {name: 'default'});
          if (def) {
            CurrentService.values[field.guid] = def.value;
          } else {
            CurrentService.values[field.guid] = undefined;
          }
        });
      });
    };

    /**
     * @ngdoc function
     * @name self.resetFormFields
     * @description resets all fields of form
     */
    var resetFormFields = function (form) {
      _.each(form.sections, function (section) {
        resetSectionFields(section);
      });
    };

    /* ==============================================
     * == PUBLIC FUNCTIONS
     * ============================================== */

    /**
     * @ngdoc function
     * @name $scope.getProcedure
     * @description
     * loads procedure details from database,
     * prepares codelists and selects the first form
     */
    $scope.cs.getProcedure = function () {
      var form;

      CaseService.prepareProcedure(procedureId, caseId).then(

        function (result) {

          // Set CurrentService data
          CurrentService.procedure = result.data;
          CurrentService.reports   = result.reports;
          CurrentService.closed    = result.closed;
          CurrentService.name      = result.name;

          // Set codelists
          CurrentService.setCodelist(result.codelists);

          // Prepare values array
          CurrentService.setValues();

          // Find first form and set it
          form = _.findWhere(CurrentService.procedure.forms, {order: 1});

          /**
           * @ngdoc property
           * @name CurrentService.values
           * @description here we check if application is in a stage of previewing or editing a case
           * if so then we set CurrentService.values to the array of values which are calculated and passed in promise
           * by 'CaseService.prepareProcedure' function
           */
          if ((CurrentService.getPreviewValue() || CurrentService.getCasesClosedValue()) && result.values) {
            CurrentService.values = result.values;
          }

          if (form) { setCurrentForm(form); }

          if (caseId === '') {
            // try to get geolocation data
            if (window.cordova && $cordovaGeolocation) {
              var opt = {
                timeout: 5000,
                enableHighAccuracy: true
              };
              try {
                $cordovaGeolocation
                  .getCurrentPosition(opt)
                  .then(function (position) {
                    CurrentService.location = position.coords.latitude + '|' +  position.coords.longitude;
                  });
              }
              catch (err) {
                console.log(err);
              }
            }

            $scope.cs.disableForm = true;
            // Ask for case name
            SweetAlert.swal({
              title               : gettextCatalog.getString('Enter a name for this case'),
              text                : gettextCatalog.getString('Case name should uniquely identify the case.'),
              showCancelButton    : true,
              type                : 'input',
              confirmButtonText   : gettextCatalog.getString('Create'),
              cancelButtonText    : gettextCatalog.getString('Cancel'),
              closeOnConfirm      : false,
              inputPlaceholder    : gettextCatalog.getString('Case name'),
              inputValue          : result.data.procedure.name + ' - ' + moment().format('DD.MM.YYYY')
            },
            function(inputValue) {
              if (inputValue === false) {
                $scope.cs.disableForm = false;
                $state.go('app.home');
                window.swal.close();
                $ionicLoading.hide();
                return false;
              }

              if (inputValue === '') {
                window.swal.showInputError('You need to write something!');
                return false;
              }

              $ionicScrollDelegate.$getByHandle('caseContent').scrollTop();

              // Set case name
              CurrentService.name = inputValue;

              // Save current case
              CurrentService.saveProgress().then(function(guid) {
                // Get case id
                CaseService.getCaseByGUID(guid).then(function(caseData) {
                  // Change route parameters
                  $stateParams.caseId = caseData.Id_Case; // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
                  caseId = caseData.Id_Case; // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
                  // Close popup
                  window.swal.close();
                  // Hide loading && start the case
                  $ionicLoading.hide();
                  $rootScope.$broadcast('case-open');
                  $scope.cs.disableForm = false;
                });
              });
            });
          }
          else {
            // Hide loading
            $ionicLoading.hide();
          }
        },

        function (error) {
          $scope.vm.handleErrors(error);
        });
    };

    /**
     * @ngdoc function
     * @name $scope.selectForm
     * @description select's a specific form step
     */
    $scope.cs.selectForm = function (id) {
      var form;

      validateForm().then(function () {
        form = _.findWhere(CurrentService.procedure.forms, {id: id});

        if (!form) {
          form = _.findWhere(CurrentService.procedure.reports, {id: id});
        }

        if (form) {
          setCurrentForm(form);
        }
      });
    };

    /**
     * @ngdoc function
     * @name $scope.cs.next
     * @description finds the next form that can be shown
     */
    $scope.cs.next = function () {
      validateForm().then(function () {
        if ($scope.cs.saveStep) {
          return;
        }
        if ($scope.cs.printStep) {
          $scope.cs.goToSaveStep();
        }
        else {
          var index = _.indexOf(CurrentService.procedure.forms, CurrentService.current.form) + 1;
          if (index > -1) {
            for (index; index < CurrentService.procedure.forms.length; index++) {
              if ($scope.cs.showForm(CurrentService.procedure.forms[index])) {
                return setCurrentForm(CurrentService.procedure.forms[index]);
              }
            }
          }
          if (index === CurrentService.procedure.forms.length) {
            if ($scope.cs.hasReports()) {
              $scope.cs.goToPrintStep();
            } else {
              $scope.cs.goToSaveStep();
            }
          }
        }
      });
    };

    // TODO: Write documentation for this function
    $scope.cs.back = function () {
      validateForm().then(function () {
        var index;
        if ($scope.cs.saveStep) {
          if ($scope.cs.hasReports()) {
            $scope.cs.goToPrintStep();
          } else {
            index = CurrentService.procedure.forms.length - 1;
          }
        } else if ($scope.cs.printStep) {
          index = CurrentService.procedure.forms.length - 1;
        } else {
          index = _.indexOf(CurrentService.procedure.forms, CurrentService.current.form) - 1;
        }
        if (index < CurrentService.procedure.forms.length && index > -1) {
          for (index; index > -1; index--) {
            if ($scope.cs.showForm(CurrentService.procedure.forms[index])) {
              setCurrentForm(CurrentService.procedure.forms[index]);
              break;
            }
          }
        }
      });
    };

    // TODO: Write documentation for this function
    $scope.cs.goToSaveStep = function () {
      validateForm().then(function () {
        // set display data
        $scope.cs.hasNext = false;
        $scope.cs.hasPrev = true;
        $scope.cs.saveStep = true;
        $scope.cs.printStep = false;

        document.activeElement.blur();
        scrollLeft();
      });
    };

    // TODO: Write documentation for this function
    $scope.cs.goToPrintStep = function () {
      validateForm().then(function () {
        // set display data
        $scope.cs.hasNext = true;
        $scope.cs.hasPrev = true;
        $scope.cs.printStep = true;
        $scope.cs.saveStep = false;

        document.activeElement.blur();
        scrollLeft();
      });
    };

    /**
     * @ngdoc function
     * @name $scope.cs.showForm
     * @description decides whether or not to show a specific form
     */
    $scope.cs.showForm = function (form) {
      if (form.deps.length <= 0) {
        return true;
      }

      // Init variables
      var show = true,
          field,
          i,
          len,
          guid,
          operator,
          value,
          fieldType;

      // Loop trough dependencies
      for (i = 0, len = form.deps.length; i < len; i++) {

        // Return when show is false
        if (!show) {
          break;
        }

        // Set current iteration values
        guid      = form.deps[i].field.guid.toUpperCase();
        operator  = form.deps[i].operator;
        value     = form.deps[i].value;
        field     = CurrentService.values[guid];
        fieldType = form.deps[i].field.type.toUpperCase();

        // Compare values
        if (field instanceof Array && field.length > 0) {
          switch (fieldType) {
            case 'FILE':
              show = compare(operator, value, field.length);
              break;
            case 'RADIO':
              if (field[0] === undefined) {
                show = compareArray(operator, value, ['']);
              } else {
                show = compareArray(operator, value, _.pluck(field, 'Label'));
              }
              break;
            default:
              show = compareArray(operator, value, _.pluck(field, 'Label'));
          }
        }
        else if (field instanceof Object && !field.length) {
          switch (fieldType) {
            case 'TOGGLE':
              show = compare(operator, value, field.Value);
              break;
            case 'DATE':
              value = new Date(value);
              show = compareDate(operator, value, field);
              break;
            case 'TIME':
              value = new Date(value);
              show = compareTime(operator, value, field);
              break;
            case 'CHECKBOX':
              if (field.length === 0) {
                show = compare(operator, value, '');
              }
              break;
            default:
              show = compare(operator, value, field.Label);
          }
        }
        else if (field !== undefined && field !== null && field.length > 0) {
          switch (fieldType) {
            case 'SIGNATURE':
              show = compare(operator, value, true);
              break;
            default:
              show = compare(operator, value, field);
          }
        }
        else {
          switch (fieldType) {
            case 'TOGGLE':
              show = compare(operator, value, false);
              break;
            case 'SIGNATURE':
              show = compare(operator, value, false);
              break;
            default:
              show = compare(operator, value, '');
          }
        }
      }
      if (!show) {
        resetFormFields(form);
      }
      return show;
    };

    /**
     * @ngdoc function
     * @name $scope.cs.showSection
     * @description decides whether or not to show a specific section
     */
    $scope.cs.showSection = function (section) {
      // Init variables
      var deps, show, operator, value, field, i, len, guid, fieldType;

      // Set initial values
      deps    = JSON.parse(section.deps.replace(/'/g, '"'));
      show    = true;
      len     = 0;
      i       = 0;

      if (deps.length <= 0) {
        return true;
      }

      // Loop trough dependencies
      for (i = 0, len = deps.length; i < len; i++) {

        // Return when show is false
        if (!show) {
          break;
        }

        // Set current iteration values
        guid      = deps[i].field.guid.toUpperCase();
        operator  = deps[i].operator;
        value     = deps[i].value;
        field     = CurrentService.values[guid];
        fieldType = deps[i].field.type.toUpperCase();

        // Compare values
        if (field instanceof Array && field.length > 0) {
          switch (fieldType) {
            case 'FILE':
              show = compare(operator, value, field.length);
              break;
            case 'RADIO':
              if (field[0] === undefined) {
                show = compareArray(operator, value, ['']);
              } else {
                show = compareArray(operator, value, _.pluck(field, 'Label'));
              }
              break;
            default:
              show = compareArray(operator, value, _.pluck(field, 'Label'));
          }
        }
        else if (field instanceof Object && !field.length) {
          switch (fieldType) {
            case 'TOGGLE':
              show = compare(operator, value, field.Value);
              break;
            case 'DATE':
              value = new Date(value);
              show = compareDate(operator, value, field);
              break;
            case 'TIME':
              value = new Date(value);
              show = compareTime(operator, value, field);
              break;
            case 'CHECKBOX':
              if (field.length === 0) {
                show = compare(operator, value, '');
              }
              break;
            default:
              show = compare(operator, value, field.Label);
          }
        }
        else if (field !== undefined && field !== null && field.length > 0) {
          switch (fieldType) {
            case 'SIGNATURE':
              show = compare(operator, value, true);
              break;
            default:
              show = compare(operator, value, field);
          }
        }
        else {
          switch (fieldType) {
            case 'TOGGLE':
              show = compare(operator, value, false);
              break;
            case 'SIGNATURE':
              show = compare(operator, value, false);
              break;
            default:
              show = compare(operator, value, '');
          }
        }
      }
      if (!show) {
        resetSectionFields(section);
      }
      return show;
    };

    /**
     * @ngdoc function
     * @name $scope.cs.hasReports
     * @description Checks if procedure has reports
     */
    $scope.cs.hasReports = function () {
      return CurrentService.reports.length > 0;
    };

    /**
     * @ngdoc function
     * @name $scope.cs.isClosed
     * @description Checks if case is closed
     */
    $scope.cs.isClosed = function () {
      return CurrentService.getCasesClosedValue();
    };

    /**
     * @ngdoc function
     * @name $scope.cs.isClosed
     * @description Navigates to home screen
     */
    $scope.cs.toHome = function () {
      CurrentService.init();
      $state.go('app.home');
    };

    /**
     * @ngdoc function
     * @name $scope.cs.closeCase
     * @description Close current case
     */
    $scope.cs.closeCase = function () {

      var success, error;

      if (window.analytics) {
        $cordovaGoogleAnalytics.trackEvent('/case', 'Finish', 'Close', 1);
      }

      SweetAlert.swal({
        title               : gettextCatalog.getString('Are you sure you want to close case?'),
        text                : gettextCatalog.getString('You will not be able to edit this case after it is closed.'),
        showCancelButton    : true,
        confirmButtonText   : gettextCatalog.getString('Yes'),
        cancelButtonText    : gettextCatalog.getString('Cancel'),
        closeOnConfirm      : false
      },
      function (isConfirm) {
        if (isConfirm) {

          // Setup success callback
          success = function () {
            $rootScope.$broadcast('CaseClosed', true);
            CurrentService.closed = true;

            SweetAlert.swal({
              title             : gettextCatalog.getString('Case was successfully saved'),
              type              : 'success',
              confirmButtonText : gettextCatalog.getString('OK')
            });
          };

          // Setup Error callback
          error = function () {
            SweetAlert.swal({
              title             : gettextCatalog.getString('Error'),
              type              : 'error',
              confirmButtonText : gettextCatalog.getString('OK')
            });
          };
          var values = {};
          for (var i in CurrentService.values) {
            if (i !== undefined) {
              var value;
              if (CurrentService.values[i] &&
                (typeof CurrentService.values[i] === 'string') &&
                CurrentService.values[i].search(/\r\n|\r|\n/g) > -1) {
                value = CurrentService.values[i].replace(/\r\n|\r|\n/g, '<br>');
              } else {
                value = CurrentService.values[i];
              }
              values[i] = value;
            }
          }

          if (CurrentService.procedure.procedure.guid) {
            CaseService.
              closeCase(CurrentService.procedure.procedure.guid, values)
              .then(success, error);
          }
          else {
            CaseService.
              closeNewCase(AuthService.currentUser.info.Id_Users, // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
                           CurrentService.procedure.procedure.id,
                           values,
                           CurrentService.procedure.procedure.version)
              .then(success, error);
          }
        }
      });
    };

    /**
     * @ngdoc function
     * @name $scope.openPrint
     * @description open print preview
     */
    $scope.cs.openPrint = function (report) {
      $scope.report = report;
      $scope.cs.printPopup = $ionicPopup.show({
        template: '<sf-print report="report" values="cs.c.values" fields="cs.c.procedure.fields"' +
                  ' closed="cs.c.closed"></sf-print>',
        cssClass: 'popup-full',
        scope:    $scope
      });
    };

    // TODO Add documentation
    $scope.cs.greaterThan = function (prop, val) {
      return function (item) {
        return item[prop] > val;
      };
    };

    // TODO Add documentation
    $scope.cs.equals = function (prop, val) {
      return function (item) {
        return item[prop] === val;
      };
    };

    /**
     * @ngdoc function
     * @name $scope.cs.platformReady
     * @description executes init phase when evnironment is ready
     */
    $scope.cs.platformReady = function () {

      // Display loading screen
      $scope.progressText = gettextCatalog.getString('PREPARING CASE DATA...');
      $ionicLoading.show({
        templateUrl: 'templates/loading.html',
        scope: $scope
      });

      // Init CurrentService
      CurrentService.init();

      // Get procedure for this case
      $scope.cs.getProcedure();
    };

    // TODO: Write documentation for this function
    $scope.cs.doRefresh = function () {
      $scope.cs.platformReady();
    };

    /* ==============================================
     * == EVENTS & WATCHERS
     * ============================================== */

    // TODO: Write documentation for this function
    $scope.$on('BackButtonClick', function (event, data) {
      if (data === 'app.case') {
        if ($scope.hasPrev) {
          $scope.cs.back();
        } else {
          $state.go('app.home');
        }
      }
    });

    // Close print popup on close event
    $scope.$on('closePrintPopup', function () {
      $scope.cs.printPopup.close();
    });

    /* ==============================================
     * == INIT CASE CONTROLLER
     * ============================================== */
    $ionicSideMenuDelegate.canDragContent(false);

    // Init when cordova finished loading
    $ionicPlatform.ready(function () {
      init();
    });

    // Init immediately if we are in browser
    if (!window.cordova) {
      init();
    }

  }

})();
