(function() {
  'use strict';

  /**
   * @ngdoc filter
   * @name startFrom
   * @description filter for pagination
   *
   */
  angular
    .module('SmartForms.components.filters.startFrom', [])
    .filter('startFrom', [function() {
      return function(input, start) {
        if (!input || !input.length) { return; }
        start = +start; //parse to int
        return input.slice(start);
      };
    }]);

})();
