(function () {
  'use strict';

  /***
   * @ngdoc service
   * @name Smart Forms Utilities
   *
   * @description
   * Helper methods used internally by the directives. Should not be called directly from user code.
   */
  angular
  .module('SmartForms.components.filters', [
    'SmartForms.components.filters.startFrom',
    'SmartForms.components.filters.inRange',
    'SmartForms.components.filters.sortFields',
    'SmartForms.components.filters.groupFilter'
  ]);

})();
