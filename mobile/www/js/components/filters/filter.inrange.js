(function() {
  'use strict';

  /**
   * @ngdoc filter
   * @name inRange
   * @description filter for creating table of numbers from 0 to value
   *
   */
  angular
    .module('SmartForms.components.filters.inRange', [])
    .filter('inRange', [function() {
      return function(input, value) {
        value = Math.ceil(value);
        for (var i = 0; i < value; i++) {
          input.push(i + 1);
        }
        return input;
      };
    }]);

})();
