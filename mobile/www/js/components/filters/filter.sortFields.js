(function() {
  'use strict';

  /**
   * @ngdoc filter
   * @name sortFields
   * @description filter all fields to a specific section by row and column
   *
   */
  angular
    .module('SmartForms.components.filters.sortFields', [])
    .filter('sortFields', [function() {
      return function(input, value) {
        return _.where(input, {sectId: value});
        //var fields = _.where(input, {sectId: value});
        //return _.sortBy(fields, 'row');
      };
    }]);

})();
