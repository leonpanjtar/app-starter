(function () {
  'use strict';

  /**
   * @ngdoc overview
   * @name HomeCtrl
   * @description
   * controller for the home page
   *
   * home/home.controller.js
   *
   * (c) 2015 Comland d.o.o. http://www.comland.si
   */

  angular
    .module('SmartForms.loading.controller', [])
    .controller('LoadingCtrl', LoadingCtrl);

  LoadingCtrl.$inject = [];

  function LoadingCtrl() {}

})();
