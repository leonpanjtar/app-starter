(function () {
  'use strict';

  /**
   * @ngdoc overview
   * @name SettingsCtrl
   * @description
   * controller for the settings page
   *
   * settings/settings.controller.js
   *
   * (c) 2015 Comland d.o.o. http://www.comland.si
   */

  angular
    .module('SmartForms.settings.controller', ['SmartForms.settings.service'])
    .controller('SettingsCtrl', SettingsCtrl);

  SettingsCtrl.$inject = [
    '$ionicSideMenuDelegate',
    '$ionicPlatform',
    '$ionicLoading',
    '$scope',
    'Constants',
    'SettingsService',
    'AuthService',
    'ErrorService',
    'gettextCatalog',
    'NetworkService',
    'SweetAlert',
    '$cordovaGoogleAnalytics',
    'tmhDynamicLocale'
  ];

  function SettingsCtrl($ionicSideMenuDelegate, $ionicPlatform, $ionicLoading, $scope, Constants,
                        SettingsService, AuthService, ErrorService, gettextCatalog, NetworkService,
                        SweetAlert, $cordovaGoogleAnalytics, locale) {

    //disable drag menu
    $ionicSideMenuDelegate.canDragContent(false);

    $scope.pageTitle = gettextCatalog.getString('SETTINGS');

    /**
     * @ngdoc property
     * @name $scope.timeZones
     * @description
     * Table of timezones that we fill in <select>
     */
    $scope.timeZones = Constants.timeZones;

    /**
     * @ngdoc property
     * @name $scope.timeZoneAbbreviations
     * @description
     * Table of timezone abbreviations. We need this because Android phone somethimes return GMT+/-<value> for
     * default timezone, but sometimes it returns (CEST,WEST,..) abbreviations and so this table is used to help
     * application select right timezone.
     */
    $scope.timeZoneAbbreviations = Constants.timeZoneAbbreviations;

    /**
     * @ngdoc property
     * @name $scope.languages
     * @description
     * Table of available languages
     */
    $scope.languages = Constants.LANGUAGES;

    $scope.apiAccess = {};

    $scope.settingsUser = JSON.parse(window.localStorage.getItem('app_user_settings'));

    $scope.isTablet = window.localStorage.isDeviceTablet === 'true';

    $scope.newPwd = {};

    /**
     * @ngdoc function
     * @name $scope.checkUserTimeZone
     * @description
     * Function checks if timezone is set in localstorage (app_user_settings). Otherwise it calls
     * getDefaultTimeZone function from SettingsService which check the default timezone from the phone.
     */
    $scope.checkUserTimeZone = function () {

      var timeZoneSettings = _.findWhere($scope.settingsUser, {Key: 'timezone'});

      if (timeZoneSettings) {
        var tableResult = timeZoneSettings.Value.split(',');
        var idTimeZone = parseInt(tableResult[0]);
        $scope.selectedTime = _.findWhere($scope.timeZones, {
          timeZoneId: idTimeZone
        });
      }
      else if (window.cordova) {
        SettingsService
          .getDefaultTimeZone()
          .then(
            function (response) {
              if (response) {
                $scope.selectedTime = response;
              }
            },

            function (error) {
              ErrorService.Message(error);
            }
          );
      }

    };

    /**
     * @ngdoc function
     * @name $scope.checkCurrentLanguage
     * @description
     * Function checks if language is set in localstorage (app_user_settings). Otherwise it sets it
     * to defoult languge.
     */
    $scope.checkCurrentLanguage = function () {

      var langugeSettings = _.findWhere($scope.settingsUser, {Key: 'language'});

      if (langugeSettings) {
        $scope.selectedLanguage = _.findWhere($scope.languages, {
          value: langugeSettings.Value
        });
      }

      // if no languege is set select defoult languege
      else {
        $scope.selectedLanguage = _.findWhere($scope.languages, {value: Constants.DEFOULT_LANGUAGE});
      }
    };

    /**
     * @ngdoc function
     * @name $scope.updateTimeZone
     * @description
     * This functions gets selected item from the timezone dropdown and sends to function
     * SettingsService.updateTimeZoneDB
     * which updates or inserts data in DB. Also, this function updates localstorage of 'app_user_settings' if there is
     * 'timezone' setting, otherwise it inserts data into 'app_user_settings'
     * @param {Object} object object that was selected from dropdown ( object from Constants.timeZones array )
     */
    $scope.updateTimeZone = function (object) {

      SettingsService.updateTimeZoneDB(object).then(

        function () {
          var timeZoneSettingsIndex = _.findIndex($scope.settingsUser, function (s) {
            return s.Key === 'timezone';
          });

          if (timeZoneSettingsIndex >= 0) {
            $scope.settingsUser[timeZoneSettingsIndex].Value = object.timeZoneId.toString() + ',' +
                                                               object.useDaylightTime.toString() + ',' +
                                                               object.valueGMT.toString();
          }
          else {
            if (!$scope.settingsUser) {
              $scope.settingsUser = [];
            }
            $scope.settingsUser.push({
              Key: 'timezone',
              Value: object.timeZoneId.toString() + ',' +
                     object.useDaylightTime.toString() + ',' +
                     object.valueGMT.toString()
            });
          }

          window.localStorage.setItem('app_user_settings', JSON.stringify($scope.settingsUser));

        },

        function (error) {
          ErrorService.Message(error);
        });
    };

    /**
     * @ngdoc function
     * @name $scope.getApiConnectionURL
     * @description
     * This functions calls function getApiConnectionURL from settings service and sets api url on success.
     */
    $scope.getAPIConnectionURL = function () {
      var success = function (response) {
        if (response) {
          $scope.apiAccess.URL = response.Value;
        }
      };

      SettingsService.getAPIConnectionURL().then(success, function (error) {
        ErrorService.Message(error);
      });
    };

    /**
     * @ngdoc function
     * @name $scope.getApiConnectionURL
     * @description
     * This functions calls function userinfo from auth service and sets email and password on success.
     */
    $scope.getUserInfoForAPIConnection = function () {
      var success = function (response) {
        $scope.apiAccess.id = response.Id_Users; // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
        $scope.apiAccess.email = response.Email;
        $scope.apiAccess.password = response.Password;
      };

      AuthService.userInfo().then(success, function (error) {
        ErrorService.Message(error);
      });
    };

    /**
     * @ngdoc function
     * @name $scope.updateLanguage
     * @description
     * This functions gets selected item from the language dropdown and sends to function SettingsService.changeLanguage
     * which updates or inserts data in DB. Also, this function updates localstorage of 'app_user_settings' if there is
     * 'language' setting, otherwise it inserts data into 'app_user_settings'
     * @param {Object} selectedLanguage object that was selected from dropdown ( object from Constants.language array )
     */
    $scope.updateLanguage = function (selectedLanguage) {

      var success = function () {
        var languageSettingsIndex = _.findIndex($scope.settingsUser, function (s) {
          return s.Key === 'language';
        });

        if (languageSettingsIndex >= 0) {
          $scope.settingsUser[languageSettingsIndex].Value = selectedLanguage.value;
        }
        else {
          if (!$scope.settingsUser) {
            $scope.settingsUser = [];
          }
          $scope.settingsUser.push({
            Key: 'language',
            Value: selectedLanguage.value
          });
        }
        var loc = _.findWhere(Constants.LANGUAGES, {value: selectedLanguage.value});
        locale.set(loc.locale);
        window.localStorage.setItem('app_user_settings', JSON.stringify($scope.settingsUser));
      };
      SettingsService.changeLanguage(selectedLanguage.value).then(success, function (error) {
        ErrorService.Message(error);
      });
    };

    /**
     * @ngdoc function
     * @name $scope.checkAPIConnection
     * @description
     * This functions calls function checkAPIConnection from settings service (try to connect to api).
     */
    $scope.checkAPIConnection = function () {
      var error = function () {
        $ionicLoading.hide();
        //ErrorService.Message(error);
        SweetAlert.swal({
          title             : gettextCatalog.getString('CONNECTION_UNSUCCESSFULL'),
          text              : gettextCatalog.getString('CONNECTION_UNSUCCESSFULL'),
          confirmButtonText : gettextCatalog.getString('OK')
        });
      };

      var success = function () {
        $ionicLoading.hide();
        SweetAlert.swal({
          title             : gettextCatalog.getString('CONNECTION_SUCCESSFULL'),
          text              : gettextCatalog.getString('CONNECTION_TO_API_OK'),
          confirmButtonText : gettextCatalog.getString('OK')
        });
      };

      if (NetworkService.status) {

        $ionicLoading.show({
          template: gettextCatalog.getString('CONNECTING')
        });

        SettingsService.checkAPIConnection($scope.apiAccess.URL, $scope.apiAccess.email, $scope.apiAccess.password)
          .then(success, error);
      }
      else {
        SweetAlert.swal({
          title             : gettextCatalog.getString('CONNECTION_UNSUCCESSFULL'),
          text              : gettextCatalog.getString('CONNECTION_UNSUCCESSFULL'),
          confirmButtonText : gettextCatalog.getString('OK')
        });
      }
    };

    $scope.changePassword = function () {
      console.log($scope.apiAccess.password);
      if ($scope.apiAccess.password) {
        SettingsService.updateUserPassword($scope.apiAccess.password).then(
          function () {},
          function (error) {
            ErrorService.Message(error);
          }
        );
      }
    };

    /**
     * @ngdoc function
     * @name $scope.clearDB
     * @description calls SettingsService.reinitDB and then logs user out
     */
    $scope.clearDB = function () {
      SweetAlert.swal({
        title               : gettextCatalog.getString('Are you sure you want to clear the database?'),
        text                : gettextCatalog.getString('This action will delete all the data and cannot be undone!'),
        showCancelButton    : true,
        confirmButtonText   : gettextCatalog.getString('Yes'),
        cancelButtonText    : gettextCatalog.getString('Cancel'),
        closeOnConfirm      : false,
      },
      function (confirmed) {
        if (confirmed) {
          SettingsService.reinitDB().then(function() {
            SweetAlert.swal({
              title: gettextCatalog.getString('Notification'),
              text: gettextCatalog.getString('DATABASE_CLEAR'),
              confirmButtonText: gettextCatalog.getString('OK')
            });
            $scope.logout();
          });
        }
      });
    };

    /**
     * @ngdoc function
     * @name $scope.updateThumbnail
     * @description
     * This functions gets true or false from the thumbnail toggle and sends to function SettingsService.changeThumbnail
     * which updates or inserts data in DB. Also, this function updates localstorage of 'app_user_settings' if there is
     * 'thumbnail' setting, otherwise it inserts data into 'app_user_settings'
     * @param {boolean} thumbnail true or false value if user wants list view or thumbnail view
     */
    $scope.updateThumbnail = function (thumbnail) {

      var success = function () {
        var thumbnailSettingsIndex = _.findIndex($scope.settingsUser, function (s) {
          return s.Key === 'thumbnail';
        });

        if (thumbnailSettingsIndex >= 0) {
          $scope.settingsUser[thumbnailSettingsIndex].Value = thumbnail;
        }
        else {
          if (!$scope.settingsUser) {
            $scope.settingsUser = [];
          }
          $scope.settingsUser.push({
            Key: 'thumbnail',
            Value: thumbnail
          });
        }

        window.localStorage.setItem('app_user_settings', JSON.stringify($scope.settingsUser));
      };
      SettingsService.changeThumbnail(thumbnail).then(success, function (error) {
        ErrorService.Message(error);
      });
    };

    /**
     * @ngdoc function
     * @name $scope.checkCurrentThumbnail
     * @description
     * Function checks if thumbnail is set in localstorage (app_user_settings). Otherwise it sets it
     * to default thumbnail.
     */
    $scope.checkCurrentThumbnail = function () {

      var thumbnailSettings = _.findWhere($scope.settingsUser, {Key: 'thumbnail'});

      if (thumbnailSettings) {
        if (typeof thumbnailSettings.Value === 'string') {
          $scope.thumbnail = thumbnailSettings.Value === 'true';
        } else {
          $scope.thumbnail = thumbnailSettings.Value;
        }

      }

      // if no thumbnail is set select default thumbnail
      else {
        $scope.thumbnail = Constants.DEFAULT_THUMBNAIL;
      }
    };

    /**
     * @ngdoc function
     * @name $scope.changePwd
     * @description Changes password for user on API
     */
    $scope.changePwd = function () {
      var form = $scope.$$childHead.changePwdForm.password;
      var success = function () {
        SettingsService.updateUserPassword($scope.newPwd.password)
        .then(
          function () {
            $scope.newPwd = {};
            form.$setPristine();
            form.$setUntouched();
            SweetAlert.swal({
              title             : gettextCatalog.getString('Success'),
              text              : gettextCatalog.getString('Password has been changed.'),
              confirmButtonText : gettextCatalog.getString('OK')
            }, function (isConfirm) {
              if (isConfirm) {
                $ionicLoading.hide();
              }
            });
          }
        );
      };
      var error = function () {
        SweetAlert.swal({
          title             : gettextCatalog.getString('Error'),
          text              : gettextCatalog.getString('Password has not been changed.'),
          confirmButtonText : gettextCatalog.getString('OK')
        }, function (isConfirm) {
          if (isConfirm) {
            $ionicLoading.hide();
          }
        });
      };
      if (NetworkService.status) {
        if ($scope.newPwd.password2 !== $scope.newPwd.password) {
          SweetAlert.swal({
            title             : gettextCatalog.getString('Error'),
            text              : gettextCatalog.getString('Passwords do not match.'),
            confirmButtonText : gettextCatalog.getString('OK')
          });
        } else {
          $ionicLoading.show({
            template: gettextCatalog.getString('Loading...')
          });
          SettingsService.changePassword($scope.apiAccess.URL, $scope.apiAccess.id, $scope.newPwd).then(success, error);
        }
      } else {
        SweetAlert.swal({
          title             : gettextCatalog.getString('Warning'),
          text              : gettextCatalog.getString('Data connection needed for this action.'),
          confirmButtonText : gettextCatalog.getString('OK')
        });
      }
    };

    $scope.platformRady = function () {
      if (window.analytics) {
        $cordovaGoogleAnalytics.trackView('Settings Ctrl');
      }
      NetworkService.init();
      $scope.getAPIConnectionURL();
      $scope.getUserInfoForAPIConnection();
      $scope.checkCurrentLanguage();
      $scope.checkUserTimeZone();
      $scope.checkCurrentThumbnail();
    };

    $ionicPlatform.ready(function () {
      $scope.platformRady();
    });

    if (!window.cordova) {
      $scope.platformRady();
    }
  }

})();
