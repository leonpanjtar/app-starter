(function () {
  'use strict';

  /**
   * @ngdoc service
   * @name SettingsService
   * @description
   * this service takes care of settings screen
   *
   * settings/settings.service.js
   *
   * (c) 2015 Comland d.o.o. http://www.comland.si
   *
   */

  angular
    .module('SmartForms.settings.service', [
    'SmartForms.auth.service',
    'SmartForms.common.dbconnection',
    'SmartForms.common.cipher',
    'gettext'
  ])
    .service('SettingsService', SettingsService);

  SettingsService.$inject = [
    '$http',
    '$rootScope',
    '$q',
    'Constants',
    'DBConnection',
    'gettext',
    'AuthService',
    'gettextCatalog',
    'ChiperService',
    'NetworkService'
  ];

  function SettingsService($http, $rootScope, $q, Constants, DBConnection, gettext, AuthService, gettextCatalog,
                           ChiperService, NetworkService) {

    var settings = this;

    /**
     * @ngdoc function
     * @name settings.getAPIConnectionURL
     * @description gets data saved in database for api connection
     */
    settings.getAPIConnectionURL = function () {
      var deferred = $q.defer();

      var query = 'SELECT * FROM CONFIG WHERE CONFIG.Key=?';
      var valuesTable = ['apiURL'];

      DBConnection.query(query, valuesTable).then(
        function (results) {
          if (results.rows.length) {
            deferred.resolve(results.rows.item(0));
          }
          else {
            deferred.resolve(undefined);
          }
        },
        function (error) {
          if (Constants.DEBUGMODE) {
            console.log('Error on select procedure from db');
            console.log(error);
          }
          deferred.reject(error);
        }
        );
      return deferred.promise;
    };

    /**
     * @ngdoc function
     * @name settings.changeLanguage
     * @description changes language and saves new language settings to database
     * @param {Object} selectedLanguage this is the object that we selected from language dropdown list
     */
    settings.changeLanguage = function (selectedLanguage) {
      var deferred = $q.defer();

      gettextCatalog.setCurrentLanguage(selectedLanguage);

      var query = 'INSERT OR REPLACE INTO CONFIG(Id_Config,Scope,Id_User,Key,Value) VALUES(coalesce((SELECT Id_Config' +
                  ' FROM CONFIG WHERE Id_User =? AND Key=?),(SELECT max(Id_Config) FROM CONFIG)+1),?,?,?,?)';

      var tab = [
                  AuthService.currentUser.info.Id_Users, // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
                  'language',
                  'user',
                  AuthService.currentUser.info.Id_Users, // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
                  'language',
                  selectedLanguage
                ];

      var updatedLanguageDB = DBConnection.query(query, tab);

      updatedLanguageDB.then(
        function (response) {
          if (response.rows.length > 0) {
            deferred.resolve(response.rows.item(0));
          } else {
            deferred.resolve(undefined);
          }
        },
        function (error) {
          deferred.reject(error);
        }
        );

      return deferred.promise;
    };

    /**
     * @ngdoc function
     * @name settings.updateTimeZoneDB
     * @param {Object} ObjectSelected this is the object that we selected from timezone dropdown list
     * @description
     * Functions job is to insert or replace the new selected data from dropdown list into database
    */
    settings.updateTimeZoneDB = function (ObjectSelected) {
      var deferred = $q.defer();
      var query = 'INSERT OR REPLACE INTO CONFIG(Id_Config,Scope,Id_User,Key,Value) VALUES(coalesce((SELECT Id_Config' +
                  ' FROM CONFIG WHERE Id_User =? AND Key=?),(SELECT max(Id_Config) FROM CONFIG)+1),?,?,?,?)';

      var valueDB = ObjectSelected.timeZoneId.toString() + ',' +
                    ObjectSelected.useDaylightTime.toString() + ',' +
                    ObjectSelected.valueGMT.toString();
      var tab = [
                  AuthService.currentUser.info.Id_Users, // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
                  'timezone',
                  'user',
                  AuthService.currentUser.info.Id_Users, // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
                  'timezone',
                  valueDB
                ];

      var updatedTimeZoneDB = DBConnection.query(query, tab);

      updatedTimeZoneDB.then(
        function (response) {
          if (response.rows.length > 0) {
            deferred.resolve(response.rows.item(0));
          } else {
            deferred.resolve(undefined);
          }
        },
        function (error) {
          deferred.reject(error);
        }
        );

      return deferred.promise;
    };

    /**
     * @ngdoc function
     * @name settings.getDefaultTimeZone
     * @description
     * This function check the default timezone of the phone. For some timezones phone returns timezone abbreviation
     * like: CEST,CET,WEST,AKDT,.... but sometimes returns: GMT+1:00 or GMT-12:00 that's why the function first checks
     * if the returned timezone has 'GMT' values in the string.
    */
    settings.getDefaultTimeZone = function () {
      var deferred = $q.defer(), i, j;
      navigator.globalization.getDatePattern(gotpattern, errHandler);
      function gotpattern(ob) {
        var PhoneUtcOffset = ob.utc_offset; // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
        var PhoneDstOffset = ob.dst_offset / 3600; // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
        if (ob.timezone.search('GMT') !== -1) {
          var valueGMT = PhoneUtcOffset / 3600;
          for (i = 0; i < Constants.timeZones.length; i++) {
            if (Constants.timeZones[i].valueGMT === valueGMT &&
                Constants.timeZones[i].useDaylightTime === PhoneDstOffset) {
              settings.updateTimeZoneDB(Constants.timeZones[i]);
              deferred.resolve(Constants.timeZones[i]);
              break;
            }
          }
        }

        else {
          for (i = 0; i < Constants.timeZoneAbbreviations.length; i++) {
            if (Constants.timeZoneAbbreviations[i].name === ob.timezone) {
              for (j = 0; j < Constants.timeZones.length; j++) {
                if (Constants.timeZones[j].useDaylightTime === Constants.timeZoneAbbreviations[i].useDaylight &&
                  Constants.timeZones[j].valueGMT === Constants.timeZoneAbbreviations[i].gmtValue) {
                  settings.updateTimeZoneDB(Constants.timeZones[j]);
                  deferred.resolve(Constants.timeZones[j]);
                  break;
                }
              }
              break;
            }
          }
        }
      }

      function errHandler(error) {
        deferred.reject(error);
      }

      return deferred.promise;
    };

    /**
     * @ngdoc function
     * @name settings.checkAPIConnection
     * @param {string} APIurl url of api we want to connect to
     * @param {string} username username for api connection
     * @param {string} password password for api connection
     * @description
     * Check if it is possible to connect to api with user credentials
    */
    settings.checkAPIConnection = function (APIurl, username, password) {

      var deferred = $q.defer();
      var req = {
        method: 'POST',
        url: APIurl + '/token',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        dataType: 'json',
        data: 'grant_type=password&username=' + username + '&password=' + password,
        timeout: Constants.timeouts.http
      };
      if (NetworkService.status) {
        $http(req)
          .success(function () {
            deferred.resolve(gettextCatalog.getString('CONNECTION_TO_API_OK'));
          })
          .error(function (err, status) {
            console.log('Error');
            if (status === 0) {
              err = {};
              err.message = 'Status:' + status + '! Data not correct.';
            }
            deferred.reject(err);
          });
      } else {
        deferred.reject('offline');
      }

      return deferred.promise;
    };

    settings.updateUserPassword = function (password) {
      var deferred = $q.defer();

      var cipherObj = ChiperService.encryptAES(password);

      var query = 'UPDATE USERS SET Password=?, Salt=?, IV=? WHERE Id_Users=?';
      var valuesTable = [
                          cipherObj.cipher_text, // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
                          cipherObj.salt,
                          cipherObj.iv,
                          AuthService.currentUser.info.Id_Users // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
                        ];

      DBConnection.query(query, valuesTable).then(
        function (results) {
          if (results.rows.length) {
            deferred.resolve(results.rows.item(0));
          }
          else {
            deferred.resolve(undefined);
          }
        },
        function (error) {
          if (Constants.DEBUGMODE) {
            console.log('Error on select procedure from db');
            console.log(error);
          }
          deferred.reject(error);
        }
        );
      return deferred.promise;
    };

    /**
     * @ngdoc function
     * @name settings.reinitDB
     * @description closes database and reinits database
     */
    settings.reinitDB = function () {
      var deferred = $q.defer();
      DBConnection.closeDB().then(function() {
        DBConnection.init();
        deferred.resolve();
      }).catch(function() {
        deferred.reject();
      });
      return deferred.promise;
    };

    /**
     * @ngdoc function
     * @name settings.changeThumbnail
     * @description saves new thumbnail settings to database
     * @param {boolean} thumbnail this is true or fals value
     */
    settings.changeThumbnail = function (thumbnail) {
      var deferred = $q.defer();

      var query = 'INSERT OR REPLACE INTO CONFIG(Id_Config,Scope,Id_User,Key,Value) VALUES(coalesce((SELECT Id_Config' +
                  ' FROM CONFIG WHERE Id_User =? AND Key=?),(SELECT max(Id_Config) FROM CONFIG)+1),?,?,?,?)';

      var tab = [
                  AuthService.currentUser.info.Id_Users, // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
                  'thumbnail',
                  'user',
                  AuthService.currentUser.info.Id_Users, // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
                  'thumbnail',
                  thumbnail
                ];

      var updatedThumbnailDB = DBConnection.query(query, tab);

      updatedThumbnailDB.then(
        function (response) {
          if (response.rows.length > 0) {
            deferred.resolve(response.rows.item(0));
          } else {
            deferred.resolve(undefined);
          }
        },
        function (error) {
          deferred.reject(error);
        }
        );

      return deferred.promise;
    };

    settings.changePassword = function (APIurl, uid, newPwd) {
      var deferred = $q.defer();
      var req = {
        method: 'PUT',
        url: APIurl + '/reset/' + uid,
        headers: {
          'Content-Type'    : 'application/json',
          'Authorization'   : 'bearer ' + window.localStorage.getItem('sessionToken')
        },
        dataType: 'json',
        data: {
          newPassword: newPwd.password,
          confirmNewPassword: newPwd.password2
        },
        timeout: Constants.timeouts.http
      };

      $http(req)
        .success(function () {
          deferred.resolve(gettextCatalog.getString('Password reset success'));
        })
        .error(function (err, status) {
          deferred.reject(err);
        });

      return deferred.promise;
    };

  }

})();
