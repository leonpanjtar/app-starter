(function () {
  'use strict';

  /**
   *
   * @ngdoc overview
   * @name AuthCtrl
   * @description
   * controller for the authentication page
   *
   * auth/auth.controller.js
   *
   * (c) 2015 Comland d.o.o. http://www.comland.si
   *
   */

  angular
    .module('SmartForms.auth.controller', ['SmartForms.auth.service'])
    .controller('AuthCtrl', AuthCtrl);

  AuthCtrl.$inject = [
    '$ionicPlatform',
    '$ionicSideMenuDelegate',
    '$ionicLoading',
    '$timeout',
    '$scope',
    '$state',
    'Constants',
    'AuthService',
    'gettextCatalog',
    'ChiperService',
    'NetworkService',
    'SettingsService',
    'SweetAlert',
    '$cordovaGoogleAnalytics',
    'ErrorService'
  ];

  function AuthCtrl($ionicPlatform, $ionicSideMenuDelegate, $ionicLoading, $timeout, $scope,
                    $state, Constants, AuthService, gettextCatalog, ChiperService, NetworkService,
                    SettingsService, SweetAlert, $cordovaGoogleAnalytics, ErrorService) {

    /* ==============================================
     * == INITIALIZE
     * ============================================== */

    // user data passed to the authentication service
    $scope.user                 = {};
    $scope.vm                   = {};
    $scope.vm.version           = Constants.version;
    $scope.vm.loaded            = false;

    $scope.email                = gettextCatalog.getString('EMAIL');
    $scope.password             = gettextCatalog.getString('PASSWORD');
    $scope.api                  = 'Api url';

    $scope.previousApiUrlValue  = '';

    $scope.forgot               = {};

    /* ==============================================
     * == PRIVATE FUNCTIONS
     * ============================================== */

    /**
     *
     * @ngdoc function
     * @name $scope.authenticateUser
     * @description Calls service for user authentication and rederects user
     *
     */
    $scope.authenticateUser = function () {
      var startTimeLogin, respTime, error, success;
      startTimeLogin = new Date().getTime();

      // Error callback
      error = function (error) {
        $ionicLoading.hide();

        if (window.analytics) {
          $cordovaGoogleAnalytics.trackEvent('/login', 'Sign in', 'Failed', 1);
          $cordovaGoogleAnalytics.trackView('/login');
        }
        console.log(error);
        // Set error message
        if (error) {
          if (error.code === 2000) {
            $scope.error = {
              error             : gettextCatalog.getString('Outdated app'),
              error_description : // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
                gettextCatalog.getString('Please update MightyFields if you want to continue using this app') + '\n\n' +
                gettextCatalog.getString('If you have any troubles please contact support at support@mightyfields.com'),
            };
          } else {
            $scope.error = {
              error             : gettextCatalog.getString('Warning'),
              error_description : error.error_description // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
            };
          }
        }
        respTime = new Date().getTime() - startTimeLogin;

        if (error && error.error && error.error === 'invalid_grant') {
          $scope.error = {
            error               : gettextCatalog.getString('Warning'),
            // jscs:disable requireCamelCaseOrUpperCaseIdentifiers
            error_description   : gettextCatalog.getString('Wrong username or password')
            // jscs:enable requireCamelCaseOrUpperCaseIdentifiers
          };
        }
        else if (respTime >= Constants.timeouts.http) {
          $scope.error = {
            error               : gettextCatalog.getString('Error'),
            error_description   : 'TIMEOUT_ERROR' // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
          };
        }

        else if (error === null) {
          $scope.error = {
            error               : gettextCatalog.getString('Error'),
            error_description   : 'API_ENDPOINT_NOT_VALID' // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
          };
        }
        // jscs:disable requireCamelCaseOrUpperCaseIdentifiers
        ErrorService.Message($scope.error.error_description, $scope.error.error);
        // jscs:enable requireCamelCaseOrUpperCaseIdentifiers
      };

      success = function () {
        // if the authentication is successful go to home view
        if (window.analytics) {
          $cordovaGoogleAnalytics.trackEvent('/login', 'Sign in', 'First login', 1);
        }

        $ionicLoading.hide();
        $scope.checkApiUrl();
        $state.go('app.home');
      };

      $ionicLoading.show({
        template: 'Loading...'
      });

      // use AuthService to login
      /**
       * @ngdoc property
       * @name NetworkService.status
       * @description
       * checks if network data is true or false
       */
      AuthService.network = NetworkService.status;
      if (NetworkService.status) {
        /**
         * @ngdoc property
         * @name AuthService.checkAPIConnection
         * @description
         * sends token request to API and returns data of response if O.K.
         * else sends error
         */
        AuthService.checkAPIConnection(baseURL, $scope.user.email, $scope.user.password).then(
          function (res) {
            // če se izvede success callback pomeni, da smo dobili token
            /**
             * Now we need to check if user exists in local database
             * *** if not add him to local database
             * *** if exists check if password the same
             */

            /**
             * @ngdoc property
             * @name AuthService.checkIfUserExist
             * @description
             * checks if user exists in local database and returns
             * true or false as success or error
             */
            $scope.user.token = res.access_token; // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
            AuthService.checkIfUserExists($scope.user.email).then(
              function (response) {

                if (!response) {
                  // user doesn't exists in local database
                  /**
                   * @ngdoc property
                   * @name encryptedData
                   * @description
                   * Creates encrypted data (iv,salt,cipher_text) from users password
                   */
                  var encryptedData = ChiperService.encryptAES($scope.user.password);
                  AuthService.addUserInDB(encryptedData, $scope.user).then(
                    function () {
                      AuthService.login($scope.user).then(success, error);
                    }, error);
                }
                else {
                  //user exists in local database. Now checks if password from API and local database match
                  /**
                   * @ngdoc property
                   * @name AuthService.passwordDifference
                   * @description
                   * sends user credentials to auth service and recieves object
                   * { data,boolean }
                   */
                  AuthService.passwordDifference($scope.user).then(
                    function (response) {
                      if (!response.different) {
                        AuthService.login($scope.user).then(success, error);
                      }
                      else {
                        /**
                         * call function that replaces value in DB
                         */

                        /**
                         *
                         * @ngdoc property
                         * @name AuthService.changeUserPassInDB
                         * @description
                         * sends user credentials and data from promise to auth service
                         *
                         */
                        AuthService.changeUserPassInDB($scope.user, response.data).then(
                          function (response) {
                            if (response) {
                              AuthService.login($scope.user).then(success, error);
                            }
                          },
                          error
                          );
                      }
                    },
                    function (error) {
                      console.log(error);
                    }
                  );
                }

              }, error
            ); // end of former if
          },
          error
        );
      }
      else {
        AuthService.login($scope.user).then(success, error);
      }

    };

    /**
     * @ngdoc function
     * @name $scope.navigateTo
     * @description Display signup modal
     */
    $scope.navigateTo = function (state) {
      return $state.go(state);
    };

    /**
     * @ngdoc function
     * @name $scope.checkApiUrl
     * @description Check API url
     */
    $scope.checkApiUrl = function () {
      $scope.network = NetworkService.status;
      console.log('network: ' + $scope.network);
      $timeout(function () {
        AuthService.
        checkApiUrl().
        then(
          function (response) {
            $scope.user.api             = baseURL;
            $scope.previousApiUrlValue  = response.Value;
            window.localStorage.setItem('baseUrl', response.Value);
          },
          function (error) {
            console.log('Error checkApiUrl: ' + error);
          }
        );
      }, 800);
    };

    /**
     *
     * @ngdoc function
     * @name $scope.authenticateUser
     * @description Calls service for user authentication and rederects user
     *
     */
    $scope.resetPwd = function () {
      var error, success;

      // Error callback
      error = function (error) {
        if (window.analytics) {
          $cordovaGoogleAnalytics.trackEvent('/forgot', 'Reset password', 'Failed', 1);
          $cordovaGoogleAnalytics.trackView('/forgot');
        }

        // Set error message
        SweetAlert.swal({
          title: gettextCatalog.getString('Error'),
          text: gettextCatalog.getString('There was an error reseting your password.'),
          confirmButtonText: gettextCatalog.getString('OK')
        }, function (isConfirm) {
          if (isConfirm) {
            $ionicLoading.hide();
          }
        });
      };

      success = function () {
        // if the authentication is successful go to home view
        if (window.analytics) {
          $cordovaGoogleAnalytics.trackEvent('/forgot', 'Reset password', 'Sucess', 1);
        }
        $scope.forgot = {};
        SweetAlert.swal({
          title: gettextCatalog.getString('Success'),
          text: gettextCatalog.getString('Email with instructions has been sent to your email address.'),
          confirmButtonText: gettextCatalog.getString('OK')
        }, function (isConfirm) {
          if (isConfirm) {
            $state.go('app.auth');
            $ionicLoading.hide();
          }
        });
      };

      if (NetworkService.status) {
        $ionicLoading.show({
          template: 'Loading...'
        });
        AuthService.resetUserPassword(baseURL, $scope.forgot)
          .then(success)
          .catch(error);
      } else {
        SweetAlert.swal({
          title: gettextCatalog.getString('Warning'),
          text: gettextCatalog.getString('Data connection needed for this action.'),
          confirmButtonText: gettextCatalog.getString('OK')
        });
      }

    };

    /* ==============================================
     * == INIT, WATCHERS & EVENTS
     * ============================================== */

    $ionicPlatform.ready(function () {
      if (window.analytics) {
        $cordovaGoogleAnalytics.trackView('/auth');
      }
      $scope.checkApiUrl();
    });

    if (!window.cordova) {
      $scope.checkApiUrl();
    }

    $scope.$on('event:app-networkChange', function () {
      $scope.checkApiUrl();
    });

    $ionicSideMenuDelegate.canDragContent(false);

    $timeout(function () {
      $scope.vm.loaded = true;
    }, 2000);

  }

})();
