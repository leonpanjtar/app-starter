(function () {
  'use strict';

  /**
   * @ngdoc service
   * @name AuthService
   * @description
   * this service takes care of authentication
   *
   * auth/auth.service.js
   *
   * (c) 2015 Comland d.o.o. http://www.comland.si
   *
   */

  angular
    .module('SmartForms.auth.service', [
      'SmartForms.models.user',
      'SmartForms.common.dbconnection',
      'SmartForms.common.cipher',
      'underscore'
    ])
    .service('AuthService', AuthService);

  AuthService.$inject = [
    '$http',
    '$q',
    'UserModel',
    'Constants',
    'DBConnection',
    'ChiperService',
    'gettextCatalog'
  ];

  function AuthService($http, $q, UserModel, Constants, DBConnection, ChiperService, gettextCatalog) {

    /* ==============================================
     * == INITIALIZE
     * ============================================== */

    var auth = this;

    // the user currently logged in
    auth.currentUser = {};
    auth.currentUserSettings = {};

    /* ==============================================
     * == PUBLIC FUNCTIONS
     * ============================================== */

    /**
     * @ngdoc function
     * @name auth.init
     * @description initiate objects used for authentication
     */
    auth.init = function () {

      // getting the userdata if there's any from the localstorage
      var userData = window.localStorage.getItem('app_user') || {
        Id_Users    : '', // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
        FirstName   : '',
        LastName    : '',
        Email       : '',
        PhoneNumber : '',
        UserName    : ''
      };

      //quick check to make sure it's a string ()
      if (typeof (userData) === 'string') {
        userData = JSON.parse(userData);
      }

      // setting currentUser
      auth.currentUser = new UserModel(userData);
      auth.currentUserSettings = window.localStorage.getItem('app_user_settings') || {};
    };

    /**
     * @ngdoc function
     * @name auth.isLoggedIn
     * @description checks if user is logined
     * @returns {boolean} is user logined
     */
    auth.isLoggedIn = function () {
      /*var output = false;
      if (auth.currentUser.info && auth.currentUser.info.Id_Users) {
        output = true;
      }
      return output;*/
      // jscs:disable requireCamelCaseOrUpperCaseIdentifiers
      return (auth.currentUser.info && auth.currentUser.info.Id_Users);
      // jscs:enable requireCamelCaseOrUpperCaseIdentifiers
    };

    /**
     * @ngdoc function
     * @name auth.userInfo
     * @description gets user information
     * @returns {object} current user information
     */
    auth.userInfo = function () {
      var deferred = $q.defer();

      DBConnection.query('SELECT Id_Users, FirstName, LastName, Email, Password, Salt, IV, PhoneNumber, UserName' +
                         ' FROM USERS WHERE Id_Users=?',
                          [auth.currentUser.info.Id_Users]) // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
      .then(
        function (results) {
          if (results.rows.length) {
            var user = results.rows.item(0);
            var cipherObj = {
              cipher_text   : user.Password, // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
              salt          : user.Salt,
              iv            : user.IV
            };
            deferred.resolve({
              Id_Users  : user.Id_Users, // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
              Email     : user.Email,
              Password  : ChiperService.decryptAES(cipherObj)
            });
          }
          else {
            deferred.reject(new Error(gettextCatalog.getString('User was not found')));
          }
        },
        function (error) {
          deferred.reject(error);
        });

      return deferred.promise;
    };

    /**
     * @ngdoc function
     * @name auth.checkLanguage
     * @description gets the app language
     */
    auth.checkLanguage = function () {
      var deferred = $q.defer();

      if (navigator.globalization !== undefined) {
        navigator.globalization.getLocaleName(
          function (lan) {
            if (Constants.LANGUAGES[0].deviceValues.indexOf(lan.value) !== -1) {
              deferred.resolve('en');
            }
            else {
              if (lan.value === 'sl-SI') {
                deferred.resolve('sl_SI');
              }
              else {
                deferred.resolve('en');
              }
            }
          },
          function () {
            deferred.reject('en');
          }
        );
      }
      else {
        deferred.reject('en');
      }
      return deferred.promise;
    };

    /**
     * @ngdoc function
     * @name auth.login
     * @description checks if userdata is valid and then sets user data as curren user
     * @returns deferred.promise info if login was successful
     * @param {object} userData user information
     */
    auth.login = function (userData) {
      var deferred = $q.defer(),
          success,
          user,
          token,
          error;

      success = function (response) {
        user    = response.payload.user;
        token   = response.payload.token;

        auth.updateUser(user, {set: true});

        //set token on success
        window.localStorage.setItem('sessionToken', token);
        deferred.resolve(auth.currentUser);
      };

      error = function (error) {
        deferred.reject(error);
      };

      // TODO if connection is ok check user login on server othervise from DB
      // check user from DB
      DBConnection.query('SELECT Id_Users, FirstName, LastName, Email, Password, Salt, IV, PhoneNumber, UserName FROM' +
                         ' USERS WHERE Email=?', [userData.email]).then(
        function (results) {
          if (results.rows.length) {
            user = results.rows.item(0);

            if (typeof user !== 'undefined' &&
                user.hasOwnProperty('Password') &&
                user.hasOwnProperty('Salt') &&
                user.hasOwnProperty('IV')) {
              var cipherObj = {
                cipher_text : user.Password, // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
                salt        : user.Salt,
                iv          : user.IV
              };
              if (ChiperService.decryptAES(cipherObj) === userData.password) {
                auth.updateUser(user, {set: true});

                if (userData.token) {
                  window.localStorage.setItem('sessionToken', userData.token);
                }
                else {
                  window.localStorage.setItem('sessionToken', 'offline');
                }
                return deferred.resolve(auth.currentUser);
              }
            }
            return deferred.reject({
              'error'               : 'Warning',
              'error_description'   : (gettextCatalog.getString('Wrong username or password'))
            });
          }
          return deferred.reject({
            'error'             : 'Warning',
            'error_description' : (gettextCatalog.getString('USER_NOT_FOUND') + '. ' +
                                   gettextCatalog.getString('Data connection needed'))
          });
        },
        function (error) {
          deferred.reject(error);
        });

      return deferred.promise;
    };

    /**
     * @ngdoc function
     * @name auth.logout
     * @description on logout calls reset user data
     * @returns deferred.promise info if logout was successful
     */
    auth.logout = function () {
      var deferred = $q.defer(),
          success,
          error;

      success = function (response) {
        auth.resetCookie();
        deferred.resolve(response.payload);
      };

      error = function (error) {
        if (error.status === 400) {
          auth.resetCookie();
        }
        deferred.reject(error);
      };

      auth.resetCookie();
      deferred.resolve(success);

      return deferred.promise;
    };

    /**
     * @ngdoc function
     * @name auth.resetCookie
     * @description resets user data
     */
    auth.resetCookie = function () {
      window.localStorage.removeItem('sessionToken');
      window.localStorage.removeItem('HomeStateCases');
      window.sessionStorage.removeItem('updated');

      auth.updateUser({
          Id_Users      : '', // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
          FirstName     : '',
          LastName      : '',
          Email         : '',
          PhoneNumber   : '',
          UserName      : ''
        }, {
          remove        : true
        }
      );
    };

    /**
     * @ngdoc function
     * @name auth.updateUse
     * @description updates user data
     * @param {object} user user to set
     * @param {string} options if remove or set
     */
    auth.updateUser = function (user, options) {
      var auth = this,
          opts = {
            remove: false,
            set: false
          };

      angular.extend(opts, options);
      angular.extend(auth.currentUser.info, user);

      if (opts.remove === true) {
        window.localStorage.removeItem('app_user');
        window.localStorage.removeItem('app_user_settings');
      }

      if (opts.set === true) {
        window.localStorage.setItem('app_user', JSON.stringify(auth.currentUser.info));
        auth.getUserSettings(user);
      }
    };

    /**
     * @ngdoc function
     * @name auth.getUserSettings
     * @description get user settings from DB
     * @param {object} user
     * @return {object} user settings
     */
    auth.getUserSettings = function (user) {
      var settings = [], languageSettings;
      // jscs:disable requireCamelCaseOrUpperCaseIdentifiers
      DBConnection.query('SELECT Key, Value FROM CONFIG WHERE Id_User=?', [user.Id_Users]).then(
        // jscs:enable requireCamelCaseOrUpperCaseIdentifiers
        function (results) {
          if (results.rows.length) {
            for (var i = 0; i < results.rows.length; i++) {
              settings.push(results.rows.item(i));
            }
            auth.currentUserSettings = settings;

            languageSettings = _.findWhere(auth.currentUserSettings, {Key: 'language'});

            if (languageSettings) {
              gettextCatalog.setCurrentLanguage(languageSettings.Value);
            }

            window.localStorage.setItem('app_user_settings', JSON.stringify(settings));
          }
        },
        function () {});
    };

    /**
     * @ngdoc function
     * @name auth.checkAPIConnection
     * @description
     * Sends token request to API
     * with password and username(email)
     * @param {string} APIurl url of API to connect
     * @param {string} username email of a user
     * @param {string} password plaint text password of a user
     * @returns {object} deferred deffered is a promise which returns either results of response or error
     */
    auth.checkAPIConnection = function (APIurl, username, password) {
      username = encodeURIComponent(username);
      var deferred = $q.defer();
      var req = {
        method      : 'POST',
        url         : baseURL + '/token',
        headers     : {'Content-Type': 'application/x-www-form-urlencoded'},
        dataType    : 'json',
        data        : 'grant_type=password&username=' + username + '&password=' + password,
        timeout     : Constants.timeouts.http
      };

      // Get auth token from server
      if (auth.network) {
        $http(req)
          .success(function (successCallback) {
            deferred.resolve(successCallback);
          })
          .error(function (err) {
            deferred.reject(err);
          });
      } else {
        deferred.reject('offline');
      }

      return deferred.promise;
    };

    /**
     * @ngdoc function
     * @name auth.checkApiUrl
     * @description
     * Checks if API url exists in database
     * @returns {object} deferred deferred is a promise which returns either api url if found either nothing or error
     */
    auth.checkApiUrl = function () {
      var deferred, query, table, apiDB, success, error;

      deferred  = $q.defer();
      query     = 'SELECT * FROM CONFIG WHERE CONFIG.Scope = ? AND CONFIG.key = ? ';
      table     = ['app', 'apiURL'];
      apiDB     = DBConnection.query(query, table);

      success = function (response) {
        if (response.rows.length) {
          deferred.resolve(response.rows.item(0));
        }
        else {
          deferred.reject('No value');
        }
      };

      error = function (error) {
        console.log(error);
        deferred.reject(error);
      };

      apiDB.
        then(success, error);

      return deferred.promise;
    };

    /**
     * @ngdoc function
     * @name auth.changeUserPassInDB
     * @description
     * Updates user password in DB
     * @param {object} userObject object which represents user credentials
     * @param {object} userDB object which represents user from database
     * @returns {object} deferred deferred is a promise which returns either 'true' if data was inserted or error
     */
    auth.changeUserPassInDB = function (userObject, userDB) {
      var newUserHash, deferred, query, table;

      deferred    = $q.defer();
      newUserHash = ChiperService.encryptAES(userObject.password);
      query       = 'INSERT OR REPLACE INTO USERS ' +
                      '(Id_Users, ' +
                       'FirstName, ' +
                       'LastName, ' +
                       'Email, ' +
                       'Password, ' +
                       'Salt, ' +
                       'IV, ' +
                       'PhoneNumber, ' +
                       'UserName) ' +
                    'VALUES ' +
                      '(coalesce( (SELECT Id_Users FROM USERS WHERE Email = ?), (SELECT max(Id_Users) FROM USERS)+1),' +
                       '?,?,?,?,?,?,?,?)';
      table       = [
        userObject.email,
        userDB.FirstName,
        userDB.LastName,
        userObject.email,
        newUserHash.cipher_text, // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
        newUserHash.salt,
        newUserHash.iv,
        userDB.PhoneNumber,
        userDB.UserName];

      DBConnection.query(query, table).then(
        function (response) {
          console.log('OK', response, query, table);
          deferred.resolve(true);
        },
        function (error) {
          console.log('Reject', error, query, table);
          deferred.reject(error);
        });

      return deferred.promise;
    };

    /**
     * @ngdoc function
     * @name auth.passwordDifference
     * @description
     * checks if password sent to API matches password in local database
     * @param {object} userObject object represents user credentials
     * @returns {object} deferred is a promise which returns either object with data and boolean flag if data found
     * or error
     */
    auth.passwordDifference = function (userObject) {
      var deferred, query, table, success, error, user, userLoged, userDBHash;

      deferred  = $q.defer();
      query     = 'SELECT * FROM USERS WHERE USERS.Email == ?';
      table     = [userObject.email];

      // Success callback
      success = function (response) {
        if (response.rows.length) {
          user                  = response.rows.item(0);
          userLoged             = {};
          userLoged.salt        = user.Salt;
          userLoged.iv          = user.IV;
          userLoged.cipher_text = user.Password; // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
          userDBHash            = ChiperService.decryptAES(userLoged);

          // passwords match
          if (userDBHash === userObject.password) {
            deferred.resolve({
              different: false,
              data: response.rows.item(0)
            });
          }
          // passwords don't match
          else {
            deferred.resolve({
              different: true,
              data: response.rows.item(0)
            });
          }
        }
      };

      // Error callback
      error = function (error) {
        deferred.reject(error);
      };

      DBConnection
        .query(query, table)
        .then(success, error);

      return deferred.promise;
    };

    /**
     * @ngdoc function
     * @name auth.decryptPassword
     * @description
     * Decrypts password of a user logged in
     * @param {object} userCredentials object represents user credentials
     * @returns {object} deferred deferred is a promise which returns boolean flag if process OK or error
     */
    auth.decryptPassword = function (userCredentials) {
      var deferred, userToDecrypt, userDBHash;

      deferred = $q.defer();

      // Prepare user object
      userToDecrypt = {
        salt        : userCredentials.info.Salt,
        iv          : userCredentials.info.IV,
        cipher_text : userCredentials.info.Password // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
      };

      userDBHash = ChiperService.decryptAES(userToDecrypt);

      auth.
        checkAPIConnection(window.localStorage.getItem('baseUrl'), userCredentials.info.Email, userDBHash).
        then(
        function (response) {
          // jscs:disable requireCamelCaseOrUpperCaseIdentifiers
          window.localStorage.setItem('sessionToken', response.access_token);
          deferred.resolve(response.access_token);
          // jscs:enable requireCamelCaseOrUpperCaseIdentifiers
        },
        function (error) {
          deferred.reject(error);
        }
      );

      return deferred.promise;
    };

    /**
     * @ngdoc function
     * @name auth.addUserInDB
     * @description
     * Inserts new user to database after it receives token from API
     * @param {object} encryptedObject object which has encrypted data ( salt,iv,cipher_text)
     * @param {object} userObj email of a user signing in
     * @returns {object} deferred is a promise which returns either 'true' if data inserted ,'false' if not or error
     */
    auth.addUserInDB = function (encryptedObject, userObj) {
      var deferred = $q.defer();
      console.log(userObj);
      var req = {
        method: 'GET',
        url: baseURL + '/user-info',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'bearer ' + userObj.token
        },
        timeout: Constants.timeouts.http
      };
      $http(req)
        .success(function (data) {
          var query = 'INSERT INTO USERS (Id_Users,FirstName,LastName,Email,Password,Salt,IV,PhoneNumber,UserName)' +
                      ' VALUES (?,?,?,?,?,?,?,?,?)';
          var userId = data.IdUser;
          var table = [
                        userId,
                        '',
                        '',
                        userObj.email,
                        encryptedObject.cipher_text, // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
                        encryptedObject.salt,
                        encryptedObject.iv,
                        '',
                        ''
                      ];
          var execution = DBConnection.query(query, table);
          var success = function (response) {
            if (response.rows.length) {
              deferred.resolve(true);
            } else {
              deferred.resolve(false);
            }
          };
          var error = function (error) {
            deferred.reject(error);
          };
          execution.then(success, error);
        })
        .error(function (error) {
          deferred.reject(error);
        });

      return deferred.promise;
    };

    /**
     * @ngdoc function
     * @name auth.checkIfUserExists
     * @description
     * Checks if user singning in already exists in local database
     * @param {string} email email of a user signing in
     * @returns {object} deferred is a promise which returns either 'true' if data foudn ,'false' if not or error
     */
    auth.checkIfUserExists = function (email) {
      var deferred = $q.defer();
      var query = 'SELECT * FROM USERS WHERE USERS.Email == ?';
      var table = [email];
      var execution = DBConnection.query(query, table);
      var success = function (response) {
        if (response.rows.length) {
          deferred.resolve(true);
        } else {
          deferred.resolve(false);
        }
      };
      var error = function (error) {
        deferred.reject(error);
      };
      execution.then(success, error);

      return deferred.promise;

    };

    /**
     * @ngdoc function
     * @name auth.checkAPIConnection
     * @description
     * Sends token request to API
     * with password and username(email)
     * @param {string} APIurl url of API to connect
     * @param {string} email of a user
     * @param {string} password plaint text password of a user
     * @returns {object} deferred deffered is a promise which returns either results of response or error
     */
    auth.resetUserPassword = function (APIurl, email) {
      var deferred = $q.defer();
      var req = {
        method      : 'POST',
        url         : baseURL + '/forgot',
        dataType    : 'json',
        data        : email,
        timeout     : Constants.timeouts.http
      };

      // Get auth token from server
      $http(req)
        .success(function (successCallback) {
          deferred.resolve(successCallback);
        })
        .error(function (err) {
          deferred.reject(err);
        });

      return deferred.promise;
    };

    /* ==============================================
     * == MAKE THE SERVICE PUBLIC
     * ============================================== */
    return auth;
  }
})();
