/*jslint browser: true, loopfunc: true */
(function () {
  'use strict';

  var devicejs = window.device.noConflict();

  /*
   * app.js
   *
   * (c) 2015 Comland d.o.o. http://www.comland.si
   *
   * Point of entry for the app
   */

  angular
    .module('SmartForms', [
      'ionic',
      'components.http-interceptor',
      'ngCordova',
      'gettext',
      'ngIOS9UIWebViewPatch',
    //controllers
      'SmartForms.app.controller',
      'SmartForms.auth.controller',
      'SmartForms.home.controller',
      'SmartForms.case.directives',
      'SmartForms.case.directives.case',
      'SmartForms.case.controller',
      'SmartForms.settings.controller',
      'SmartForms.loading.controller',
      'SmartForms.case.print.controller',
    //services
      'SmartForms.auth.service',
      'SmartForms.case.service',
      'SmartForms.case.service.current',
      'SmartForms.case.service.image',
      'SmartForms.sync.service',
      'SmartForms.case.print.service',
    //commons
      'SmartForms.common.constants',
      'SmartForms.common.dbconnection',
      'SmartForms.components.filters',
      'SmartForms.common.error',
      'SmartForms.common.network',
    //directives
      'SmartForms.common.directives.multipleemails',
      'SmartForms.common.directives.keyboardnext',
      'SmartForms.common.directives.ngmodel',
      'SmartForms.common.directives.passwordcheck',
      'SmartForms.case.print.directive',
    //filters
      'SmartForms.components.filters.startFrom',
      'SmartForms.components.filters.inRange',
      'debounce',
      'angular.filter',
      'oitozero.ngSweetAlert',
      'QuickList',
      'truncate',
      'tmh.dynamicLocale'
    ])

  .run([
      '$ionicPlatform',
      'AuthService',
      'Constants',
      '$state',
      '$rootScope',
      '$ionicLoading',
      'DBConnection',
      '$ionicSideMenuDelegate',
      'gettextCatalog',
      'NetworkService',
      '$cordovaGoogleAnalytics',
      'CurrentService',
      'tmhDynamicLocale',

      function ($ionicPlatform, AuthService, Constants, $state, $rootScope, $ionicLoading,
                DBConnection, $ionicSideMenuDelegate, gettextCatalog, NetworkService,
                $cordovaGoogleAnalytics, CurrentService, locale) {

        function pageLoaded() {
          if (window.StatusBar) {
            window.StatusBar.styleDefault();
          }

          if (window.analytics)
           {
            $cordovaGoogleAnalytics.debugMode();
            $cordovaGoogleAnalytics.startTrackerWithId('UA-66150520-2');
            $cordovaGoogleAnalytics.setUserId('testUser');
          }

          // TODO(rok): monitor ionic for fixing ghosting.
          // Temporary fix for ghosting. Ghosting occurs because sometimes both tapTouchEnd and
          // tapMouseUp are fired. Issue seems to occur on random occasions. Since we cannot find
          // the reason behind it we are disabling mouseup event on iOS and Android devices.
          if (ionic.Platform.isIOS() || ionic.Platform.isAndroid()) {
            document.addEventListener('mouseup', function(e) {
              e.stopPropagation();
              e.preventDefault();
              return false;
            }, true);
          }

          //SQLite init script
          DBConnection.init();
          NetworkService.init();

          if (AuthService.isLoggedIn()) {
            /**
             * ATTENTION
             * AuthService.currentUserSettings doesn't hold object but instead it holds a JSON.stringify string of
             * app_user_settings.
             * This is why we need to parse this data to JSON object.
             * The error occurs in AuthService, because there, we declare pure string to auth.currentUserSettings and
             * not parsed object
             * from string stored in local storage
             */
            var langugeSettings = _.findWhere(JSON.parse(AuthService.currentUserSettings), {Key: 'language'});

            if (langugeSettings && langugeSettings.Value) {
              gettextCatalog.setCurrentLanguage(langugeSettings.Value);
              var loc = _.findWhere(Constants.LANGUAGES, {value: langugeSettings.Value});
              if (loc) {
                locale.set(loc.locale);
              } else {
                locale.set(Constants.DEFAULT_LOCALE);
              }
            }
            else {
              gettextCatalog.setCurrentLanguage(Constants.DEFOULT_LANGUAGE);
              locale.set(Constants.DEFAULT_LOCALE);
            }
          } else {
            AuthService.checkLanguage().then(
              function(data) {
                Constants.DEFOULT_LANGUAGE = data;
                gettextCatalog.setCurrentLanguage(data);
              },
              function() {
                gettextCatalog.setCurrentLanguage(window.mfLang);
              }
            );
          }
        }

        $ionicPlatform.ready(function () {
          pageLoaded();
        });

        if (!window.cordova) {
          pageLoaded();
        }

        // init Authentication service
        AuthService.init();

        $rootScope.$on('event:auth-loginRequired', function () {
          //if login is required redirect to the app.auth page
          AuthService.resetCookie();
          $state.go('app.auth');
        });

        // on state change you want to check whether or not the state.
        // I'm trying to reach is protected
        $rootScope.$on('$stateChangeStart', function (event, toState) {
          $ionicLoading.show({
            template: 'Loading...'
          });
          if (!toState.authenticate && AuthService.isLoggedIn()) {
            $state.transitionTo('app.home');
            event.preventDefault();

          }
          else if (toState.authenticate && !AuthService.isLoggedIn()) {
            // User isn’t authenticated
            $state.transitionTo('app.auth');
            event.preventDefault();
          }

        });

        $rootScope.$on('$stateChangeSuccess', function () {
          $ionicLoading.hide();
        });

      }])

  .run(['$http', '$templateCache', 'CurrentService', 'DBConnection', function($http, $templateCache, CurrentService) {
      var templates = [
        'js/case/directives/tpls/part.sfApicall.html',
        'js/case/directives/tpls/part.sfBarcode.html',
        'js/case/directives/tpls/part.sfCheckbox.html',
        'js/case/directives/tpls/part.sfDate.html',
        'js/case/directives/tpls/part.sfDescription.html',
        'js/case/directives/tpls/part.sfDropdown.html',
        'js/case/directives/tpls/part.sfEmail.html',
        'js/case/directives/tpls/part.sfHeader.html',
        'js/case/directives/tpls/part.sfInput.html',
        'js/case/directives/tpls/part.sfRadio.html',
        'js/case/directives/tpls/part.sfResultItem.html',
        'js/case/directives/tpls/part.sfSignature.html',
        'js/case/directives/tpls/part.sigPopup.html',
        'js/case/directives/tpls/part.sfSubheader.html',
        'js/case/directives/tpls/part.sfTable.html',
        'js/case/directives/tpls/part.sfTextarea.html',
        'js/case/directives/tpls/part.sfTime.html',
        'js/case/directives/tpls/part.sfToggle.html',
        'js/case/directives/tpls/part.sfHidden.html',
        'js/case/directives/tpls/part.sfGps.html',
        'js/case/directives/tpls/part.sfHtml.html',
        'js/case/directives/tpls/part.sfFile.html'
      ];

      for (var i = 0; i < templates.length; i++) {
        $http.get(templates[i], {cache:$templateCache})
             .success(function (html) {
                $templateCache.put(templates[i], html);
                CurrentService.setFieldTemplate(templates[i], html);
              });
      }
    }])

  .config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {

    // loging function
    var _onEnter = [
        'Constants',
        '$state',
        '$stateParams',
        function () {}];

    var _onExit = [
        'Constants',
        '$state',
        function (C) {
          if (C.DEBUGMODE) {}
        }];

    var _isDeviceTablet = function () {
      var tablet = false;

      if (devicejs && devicejs.tablet()) {
        tablet = devicejs.tablet();
      }

      var app = document.URL.indexOf('http://') === -1 && document.URL.indexOf('https://') === -1;

      if (!app) {
        tablet = true;
      }

      localStorage.setItem('isDeviceTablet', tablet);
      return tablet;
    };

    document.addEventListener('deviceready',function () {
      _isDeviceTablet();
      /*
     Local Storage holds "STRING" value in item
     */
      if (window.localStorage.getItem('isDeviceTablet') &&
          JSON.parse(window.localStorage.getItem('isDeviceTablet')) === true) {
        screen.lockOrientation('landscape');
      }
      else {
        screen.lockOrientation('portrait');
      }
    });

    /*
        // quick hack to load before the controller gets called
        var _ctrlFilter = function (options) {
          var opts = angular.extend({
            dependencies: ['Constants', '$state', '$stateParams'],
            type: 'data', // or params
            data: {}
          }, options);

          opts.dependencies.push(function (C, $state, $stateParams, $q, Model) {
            var deferred = $q.defer();

            if (opts.type === 'data') {
              // if we need to fetch some data get the model and $stageParams id then get the data
              var model = new Model();
              model.get($stateParams.id).then(function (response) {
                if (C.DEBUGMODE) {
                  console.log('success fetching model\'s data');
                }

                return deferred.resolve({
                  type: 'data',
                  params: {
                    model: model.info
                  }
                });

              }, function (error) {
                if (C.DEBUGMODE) {
                  console.log('error fetching model\'s data');
                }

                return deferred.reject();
              });

            } else {
              // otherwise we're merely passing in the data
              return deferred.resolve({
                type: 'data',
                params: {
                  id: $stateParams.id
                }
              });
            }

            return deferred.promise;
          });

          return opts.dependencies;
        };
    */
    $stateProvider
    // routing
      .state('app', {
      url: '/app',
      abstract: false,
      templateUrl: 'templates/app.html',
      controller: 'AppCtrl'
    })

    // authentication page
    .state('app.auth', {
      url: '/auth',
      views: {
        'content@app': {
          controller: 'AuthCtrl',
          templateUrl: !_isDeviceTablet() ? 'templates/auth/index.html' : 'templates/auth/indextab.html'
        }
      },
      authenticate: false,
      onEnter: _onEnter,
      onExit: _onExit
    })

    // signup page
    .state('app.auth.signup', {
      url: '/signup',
      views: {
        'content@app': {
          controller: 'AuthCtrl',
          templateUrl: 'templates/auth/signup.html'
        }
      },
      authenticate: false,
      onEnter: _onEnter,
      onExit: _onExit
    })

    .state('app.auth.forgot', {
      url: '/forgot',
      views: {
        'content@app': {
          controller: 'AuthCtrl',
          templateUrl: !_isDeviceTablet() ? 'templates/auth/forgot.html' : 'templates/auth/forgottab.html'
        }
      },
      authenticate: false,
      onEnter: _onEnter,
      onExit: _onExit
    })

    // main page with cases
    .state('app.home', {
      url: '/home',
      views: {
        'content@app': {
          controller: 'HomeCtrl',
          templateUrl: !_isDeviceTablet() ? 'templates/home/index.html' : 'templates/home/indextab.html'
        }
      },
      authenticate: true,
      onEnter: _onEnter,
      onExit: _onExit
    })

    //page for cases
    .state('app.case', {
      url: '/case/:procedureId/:caseId',
      views: {
        'content@app': {
          controller: 'CaseCtrl',
          templateUrl: !_isDeviceTablet() ? 'templates/case/index.html' : 'templates/case/indextab.html'
        }
      },
      authenticate: true,
      onEnter: _onEnter,
      onExit: _onExit
    })

    //page for printPreview
    .state('app.print', {
      url: '/print/:procedureId/:procedureReportId',
      views: {
        'content@app': {
          controller: 'PrintCtrl',
        }
      },
      authenticate: true,
      onEnter: _onEnter,
      onExit: _onExit
    })

    //page for settings
    .state('app.settings', {
      url: '/settings',
      views: {
        'content@app': {
          controller: 'SettingsCtrl',
          templateUrl: 'templates/settings/index.html'
        }
      },
      authenticate: true,
      onEnter: _onEnter,
      onExit: _onExit
    });

    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise(function ($injector) {
      var $state = $injector.get('$state');
      $state.go('app.home');
    });
  }])
  .config(['tmhDynamicLocaleProvider', function(tmhDynamicLocaleProvider) {
    tmhDynamicLocaleProvider.localeLocationPattern('js/translations/angular-locale_{{locale}}.js');
  }]);

})();
