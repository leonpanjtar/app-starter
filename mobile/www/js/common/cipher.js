(function () {
  'use strict';
  /*global forge*/

  /**
   * @ngdoc service
   * @name ChiperService
   * @description
   * this service is used for encryption/decryption
   *
   * common/cipher.js
   *
   * (c) 2015 Comland d.o.o. http://www.comland.si
   */

  angular
    .module('SmartForms.common.cipher', ['SmartForms.common.constants'])
    .service('ChiperService', ChiperService);

  ChiperService.$inject = ['Constants'];

  function ChiperService(Constants) {

    var crypt = this;

    /**
     * @ngdoc function
     * @name crypt.encryptAES
     * @description encrypts text with AES chiper
     * @param {string} plainText text to encrypt
     * @return {object} encrypted object (ciperText, salt, iv)
     */
    crypt.encryptAES = function (plainText) {
      //ganerate random salt, key and iv
      var salt = forge.random.getBytesSync(128);
      var key = forge.pkcs5.pbkdf2(Constants.PHRASE, salt, 40, 16);
      var iv = forge.random.getBytesSync(16);

      //generate crypted password
      var cipher = forge.cipher.createCipher('AES-CBC', key);
      cipher.start({iv: iv});
      cipher.update(forge.util.createBuffer(plainText));
      cipher.finish();

      //save data to object
      var cipherText = forge.util.encode64(cipher.output.getBytes());
      var cipherObj = {
        cipher_text: cipherText, // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
        salt: forge.util.encode64(salt),
        iv: forge.util.encode64(iv)
      };

      return cipherObj;
    };

    /**
     * @ngdoc function
     * @name crypt.decrypt
     * @description decrypts cipher object which is encrypted with AES chiper
     * @param {object} cipherObj encrypted object (ciperText, salt, iv) to decrypt
     * @return {string} decrypted text
     */
    crypt.decryptAES = function (cipherObj) {
      //Reads salt, iv and key from object
      var salt = forge.util.decode64(cipherObj.salt);
      var iv = forge.util.decode64(cipherObj.iv);
      var key = forge.pkcs5.pbkdf2(Constants.PHRASE, salt, 40, 16);

      //decrypts password
      var decipher = forge.cipher.createDecipher('AES-CBC', key);
      decipher.start({iv: iv});
      // jscs:disable requireCamelCaseOrUpperCaseIdentifiers
      decipher.update(forge.util.createBuffer(forge.util.decode64(cipherObj.cipher_text)));
      // jscs:enable requireCamelCaseOrUpperCaseIdentifiers
      decipher.finish();

      return decipher.output.toString();
    };

    crypt.Hash = function (text) {
      var md = forge.md.sha256.create();
      md.update(text, 'utf8');
      return md.digest().toHex();
    };

    crypt.Encode64 = function (text) {
      return forge.util.encode64(text);
    };

    crypt.Decode64 = function (encoded) {
      return forge.util.decode64(encoded);
    };

    crypt.EncodeUtf8 = function (text) {
      return forge.util.encodeUtf8(text);
    };

    crypt.DecodeUtf8 = function (encoded) {
      return forge.util.decodeUtf8(encoded);
    };

    return crypt;
  }

})();
