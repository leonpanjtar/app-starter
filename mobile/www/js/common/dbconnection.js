(function () {
'use strict';
/*global baseURL*/
/**
   * @ngdoc service
   * @name DBConnection
   * @description
   * this service takes care of connection and queries to database
   *
   * common/dbconnection.js
   *
   * (c) 2015 Comland d.o.o. http://www.comland.si
   *
   */
angular
    .module('SmartForms.common.dbconnection', ['SmartForms.common.constants'])
    .factory('DBConnection', DBConnection);

DBConnection.$inject = ['$rootScope', '$cordovaSQLite', '$ionicPlatform', 'Constants', '$q'];

function DBConnection($rootScope, $cordovaSQLite, $ionicPlatform, Constants, $q) {
  /**
  * @ngdoc object
  * @name database
  */
  var database = this;

  /**
   * @ngdoc property
   * @name database.connection
   * @description open connection to SmartFormsDB database
   */
  database.connection = null;
  database.open = false;

  var apiURL = baseURL;

  var list = [
    'INSERT INTO CONFIG(Id_Config,Scope,Id_User,Key,Value) VALUES (1,\'app\',\'\',\'apiURL\',\'' + apiURL + '\');',
  ];

  var updates = {
    '94'  : ['ALTER TABLE CASE ADD COLUMN Name TEXT;'],
    '200' : ['ALTER TABLE CASE ADD COLUMN Location TEXT;'],
    '201' : ['CREATE TABLE PROCEDUREHASCATEGORY (	Id_Procedure	TEXT, Id_Category	TEXT, ' +
      'FOREIGN KEY(Id_Procedure) REFERENCES PROCEDURE(Id_Procedure), ' +
      'FOREIGN KEY(Id_Category) REFERENCES CATEGORY(Id_Category));',
      'CREATE TABLE PROCEDURE_NEW ( Id_Procedure INT, Name TEXT, Description TEXT, ' +
      'Image TEXT, Icon TEXT, Id_Binaries INTEGER, Version	INTEGER);',
      'INSERT INTO PROCEDURE_NEW SELECT Id_Procedure, Name, Description, ' +
      'Image, Icon, Id_Binaries, Version FROM [PROCEDURE]',
      'DROP TABLE [PROCEDURE]', 'ALTER TABLE PROCEDURE_NEW RENAME TO PROCEDURE',
      'DELETE FROM CONFIG WHERE KEY = localDBUpdate']
  };

  /**
   * @ngdoc function
   * @name openSuccess
   * @description
   * Executes prepared queries and updates the database if necessary.
   */
  var openSuccess = function () {
    // Execute prepared queries
    angular.forEach(list, function (query) {
      try {
        $cordovaSQLite.execute(database.connection, query);
      } catch (err) {
        console.log(err);
      }
    });
    var allPromises = [];
    // Upgrade database if needed
    $cordovaSQLite.execute(database.connection, 'PRAGMA user_version').then(
      function (result) {
        var v = result.rows.item(0).user_version; // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
        console.log('DB version', v);
        if (v < Constants.intversion) {
          angular.forEach(updates, function (querys, version) {
            // If current object has bigger version than DB
            // execute queris for current object
            if (v <= version) {
              try {
                console.log(version);
                angular.forEach(querys, function(query) {
                  var prom = $cordovaSQLite.execute(database.connection, query);
                  allPromises.push(prom);
                });
              }
              catch (err) {
                console.log(err);
              }
            }
          });
        }
      },
      function (error) { console.log('error ' + error);}
    );
    $q.all(allPromises).then(
      function () {
        $cordovaSQLite.execute(database.connection, 'PRAGMA user_version = ' + Constants.intversion);
        // Mark database as open
        database.open = true;
        $rootScope.$broadcast('db.open');
      },
      function (err) {
        console.log('error ' + err);
      }
    );
  };

  /**
   * @ngdoc function
   * @name openOnPhone
   * @description
   * Opens connection to database.
   */
  var openOnPhone = function () {
    var db = {
      name: Constants.DB_CONFIG.name,
      location: 'default'
    };
    database.connection = window.sqlitePlugin.openDatabase(db, openSuccess, function () {
      console.log('Database could not be opened.');
    });
  };

  /**
   * @ngdoc function
   * @name initOnPhone
   * @description
   * Tries to copy the database if it doesn't exist already. Then it opens it.
   */
  var initOnPhone = function () {
    window.plugins.sqlDB.copy(Constants.DB_CONFIG.name, 2, function () {
      openOnPhone();
    }, function (error) {
      console.log('Database already exists: ', error);
      openOnPhone();
    });
  };

  /**
   * @ngdoc function
   * @name initOnBrowser
   * @description
   * Opens the database on a web browser and creates necessary tables.
   */
  var initOnBrowser = function() {
    database.connection = openDatabase(Constants.DB_CONFIG.name, '1.0', 'SmartForms Content Database', 2 * 1024 * 1024);

    angular.forEach(Constants.DB_CONFIG.tables, function (table) {
      var columns = [];

      angular.forEach(table.columns, function (column) {
        columns.push(column.name + ' ' + column.type);
      });

      var query = 'DROP TABLE ' + table.name + ';'; // delete when db design complited
      $cordovaSQLite.execute(database.connection, query);
      query = 'CREATE TABLE IF NOT EXISTS ' + table.name + ' (' + columns.join(',') + ')';
      $cordovaSQLite.execute(database.connection, query);
      angular.forEach(list, function (query) {
        $cordovaSQLite.execute(database.connection, query).then(
          function () {},
          function (error) {
            console.log('error ' + error.message);
          }
        );
      });
    });

    // Add DB version
    $cordovaSQLite.execute(database.connection, 'PRAGMA user_version = ' + Constants.intversion);
    // Mark database as open
    database.open = true;
    $rootScope.$broadcast('db.open');
  };

  /**
   * @ngdoc function
   * @name database.init
   * @description initializes connection to SmartFormsDB database and assigns it to database.connection
   */
  database.init = function () {
    if (window.cordova && window.plugins) {
      initOnPhone();
    } else {
      initOnBrowser();
    }
  };

  /**
   * @ngdoc function
   * @name database.query
   * @description Executes a query to database. If database is not ready query waits until
   * database becomes ready.
   * @param {string} query a string that represents query we want to execute
   * @param {array} QueryArguments values that replace '?' in query string
   * @returns {object} promise with query result
   */
  /* Query example -> .query(database.connection,'SELECT user.id from user where user.id==? AND user.name==?'
   * ,[50,'Janez'])
   */
  database.query = function () {
    var query = arguments[0];
    var tableQueryArgs = arguments[1];
    // Check if open
    if (database.open) {
      return database.executeQuery(query, tableQueryArgs);
    } else {
      // If not open wait for db.open event and then execute query
      var deferred = $q.defer();
      var removeListener = $rootScope.$on('db.open', function() {
        removeListener();
        database.executeQuery(query, tableQueryArgs).then(function(result) {
          deferred.resolve(result);
        }, function(error) {
          deferred.reject(error);
        });
      });
      return deferred.promise;
    }
  };

  /**
   * @ngdoc function
   * @name database.executeQuery
   * @description executes a query to database
   * @param {string} query a string that represents query we want to execute
   * @param {array} tableQueryArgs values that replace '?' in query string
   * @returns {promise} query results
   */
  database.executeQuery = function(query, tableQueryArgs) {
    return $cordovaSQLite.execute(database.connection, query, tableQueryArgs);
  };

  /**
   * @ngdoc function
   * @name database.queryOne
   * @description executes a query to database
   * @param {string} query - a string that represents query we want to execute
   * @param {Array[]} QueryArguments - values that replace '?' in query string
   * @returns {object} one object from query
   */
  database.queryOne = function () {
      var query = arguments[0];
      var tableQueryArgs = arguments[1];
      return database.query(query, tableQueryArgs).then(
          function (resoults) {
              if (resoults.rows.length) {
                return resoults.rows.item(0);
              }
              else {
                throw new Error('No results');
              }
            },
          function (error) {
              throw error;
            }
          );
    };

  /**
   * @ngdoc function
   * @name database.closeDB
   * @description deletes database
   */
  database.closeDB = function () {
    var deferred = $q.defer();
    window.sqlitePlugin.deleteDatabase(
      {name: Constants.DB_CONFIG.name, location: 'default'},
      function() {
        database.connection = undefined;
        database.open = false;
        deferred.resolve();
      },
      function() {
        deferred.reject();
      }
    );
    return deferred.promise;
  };

  return database;
}

})();
