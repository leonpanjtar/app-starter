(function () {
  'use strict';

  /**
   * @ngdoc overview
   * @name ngModelFormat
   * @description
   * directive to change the format of time
   *
   * Example
   * ===================================
   * common/directives/drct.formatfix.js
   *
   * (c) 2015 Comland d.o.o. http://www.comland.si
   */

  angular
    .module('SmartForms.common.directives.ngmodel', [])
    .directive('ngModel', ngModelFormat);

  ngModelFormat.$inject = [
    '$filter'
  ];

  function ngModelFormat($filter) {
    return {
        require: '?ngModel',
        link: function(scope, elem, attr, ngModel) {
            if (!ngModel) {
              return;
            }
            if (attr.type !== 'time') {
              return;
            }
            ngModel.$formatters.unshift(function(value) {
                return value.replace(/:\d{2}[.,]\d{3}$/, '');
              });
          }
      };
  }

})();
