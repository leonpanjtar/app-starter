(function () {
  'use strict';

  /**
   * @ngdoc overview
   * @name keyboardNext
   * @description
   * directive that checks if user pressed Return key
   * and focuses next field
   *
   * (c) 2015 Comland d.o.o. http://www.comland.si
   */

  angular
    .module('SmartForms.common.directives.keyboardnext', [])
    .directive('keyboardNext', keyboardNext);

  keyboardNext.$inject = [
    '$ionicScrollDelegate'
  ];

  function keyboardNext($ionicScrollDelegate) {

    /* =====================================
     * = DIRECTIVE
     * ===================================== */
    return {
      link: function (scope, element, attrs) {

        element.on('keydown', function(e) {
              if (e.keyCode === 13 || e.keyCode === 9) {
                e.preventDefault();
                angular.element(this).blur();

                // UNCOMENT FOR HONDA
                // console.log('ENTER PRESSED');
                // var parent = angular.element(this).closest('.scroll');
                // var fields = angular.element('input[type!=hidden], select, textarea, .button.button-custom', parent);
                // var nextField;
                //
                // // Find current field and focus next one
                // for (var i = 0; i < fields.length - 1; i++) {
                //   if (fields[i] === this) {
                //     nextField = fields[i + 1];
                //     console.log(nextField);
                //     angular.element(nextField).focus();
                //     break;
                //   }
                // }
                //
                // if (!nextField || _.contains(['checkbox', 'radio'], nextField.type)) {
                //   angular.element(this).blur();
                // }

                // USELESS
                // if (nextField) {
                //   // Get top offset of the form control field
                //   var scrollPosition = angular.element(nextField).offset().top;
                //   // Substract it from current scroll offset
                //   scrollPosition -= parent.offset().top;
                //   $ionicScrollDelegate.scrollTo(0, scrollPosition - 75, true);
                // } else {
                //   angular.element(this).blur();
                // }
              }
            });
      }
    };
  }
})();
