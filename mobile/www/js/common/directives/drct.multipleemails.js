(function () {
  'use strict';

  /**
   * @ngdoc overview
   * @name multipleEmails
   * @description
   * directive that validates multiple email for input
   *
   * Example
   * ===================================
   * case/directives/drct.email.js
   *
   * (c) 2015 Comland d.o.o. http://www.comland.si
   */

  angular
    .module('SmartForms.common.directives.multipleemails', [])
    .directive('multipleEmails', multipleEmails);

  function multipleEmails() {

    /* =====================================
     * = PRIVATE FUNCTIONS
     * ===================================== */

    /* =====================================
     * = DIRECTIVE
     * ===================================== */
    return {
      require: 'ngModel',
      link: function (scope, element, attrs, ctrl) {
        ctrl.$parsers.unshift(function (viewValue) {

          var emails = viewValue.split(',');
          // define single email validator here
          var re = /^[a-zA-Z]+[a-zA-Z0-9._]+@[a-zA-Z]+\.[a-zA-Z.]{2,5}$/;

          // escape empty string
          if (viewValue.trim() === '') {
            ctrl.$setValidity('multipleEmails', true);
            return viewValue;
          }

          var validityArr = emails.map(function (str) {
            return re.test(str.trim());
          });

          var atLeastOneInvalid = false;

          angular.forEach(validityArr, function (value) {
            if (value === false) {
              atLeastOneInvalid = true;
            }
          });

          if (!atLeastOneInvalid) {
            // ^ all I need is to call the angular email checker here, I think.
            ctrl.$setValidity('multipleEmails', true);
            return viewValue;
          }
          else {
            ctrl.$setValidity('multipleEmails', false);
            return undefined;
          }
          // })
        });
      }
    };

  }

})();
