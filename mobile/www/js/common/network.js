(function () {
  'use strict';
  /**
    * @ngdoc service
    * @name NetworkService
    * @description
    * this service takes care of phone network information
    *
    * common/network.js
    *
    * (c) 2015 Comland d.o.o. http://www.comland.si
    *
    */
  angular
    .module('SmartForms.common.network', [])
    .service('NetworkService', ['$cordovaNetwork', '$ionicPlatform', '$rootScope', 'Constants',
      function ($cordovaNetwork, $ionicPlatform, $rootScope, Constants) {

        /**
        * @ngdoc object
        * @name connect
        */
        var connect = {};

        connect.status = false;
        connect.type = 'unknown';

        /**
         * @ngdoc function
         * @name connect.init
         * @description initiate objects used for network service
         */
        connect.init = function () {
          if (Constants.identifyDevice()) {
            connect.type = $cordovaNetwork.getNetwork();
            connect.status = $cordovaNetwork.isOnline();
          } else {
            connect.status = true;
            connect.type = 'WiFi Connection';
          }

        };

        /**
         * @ngdoc function@name connectionType
         * @description checks what kind of connection does phone provide
         * @param {Object} network - Object provided by $cordovaNetwork.getNetwork()
         * @returns {string} string from one of three categories ('NO CONNECTION','DATA','WIFI','UNKNOWN')
         */
        /*function connectionType(network) {
          console.log($cordovaNetwork.getNetwork());
          if (network.UNKNOWN) {
            return 'UNKNOWN';
          }
          else if (network.WIFI || network.ETHERNET) {
            return 'WIFI';
          }
          else if (network.NONE) {
            return 'NO CONNECTION';
          }
          else if (network.CELL_2G || network.CELL_3G || network.CELL_4G || network.CELL) {
            return 'DATA';
          }
          else {
            return 'UNKNOWN';
          }
        };*/

        /**
         * @ngdoc event
         * @name $cordovaNetwork:online
         * @description waits for $cordovaNetwork:online and then informs mobile phone of network being available
         */
        $rootScope.$on('$cordovaNetwork:online', function (event, networkState) {
          connect.status = true;
          connect.type = networkState;
          //connect.type = connectionType(networkState);
          $rootScope.$broadcast('event:app-networkChange', connect.status);
        });

        /**
         * @ngdoc event
         * @name $cordovaNetwork:offline
         * @description waits for $cordovaNetwork:offline and then informs mobile phone of network not being
         * available anymore
         */
        $rootScope.$on('$cordovaNetwork:offline', function (event, networkState) {
          connect.status = false;
          connect.type = networkState;
          //connect.type = "NO CONNECTION";
          $rootScope.$broadcast('event:app-networkChange', connect.status);
        });

        return connect;
      }]);

})();
