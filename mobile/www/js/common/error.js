(function () {
  'use strict';

  /**
   * @ngdoc overview
   * @name ErrorService
   * @description
   * service for handeling errors
   *
   * common/error.service.js
   *
   * (c) 2015 Comland d.o.o. http://www.comland.si
   */

  angular
    .module('SmartForms.common.error', [])
    .service('ErrorService', ErrorService);

  ErrorService.$inject = [
    '$ionicPlatform',
    '$ionicPopup',
    '$timeout',
    'gettext',
    'gettextCatalog',
    'SweetAlert'
  ];

  function ErrorService($ionicPlatform, $ionicPopup, $timeout, gettext, gettextCatalog, SweetAlert) {

    var error = this;

    error.Message = function (errorMsg, title) {

      var err = !errorMsg ? '???' : (errorMsg && !errorMsg.message ? errorMsg : errorMsg.message);

      SweetAlert.swal({
        title: !title ? gettextCatalog.getString('Error') : gettextCatalog.getString(title),
        text: error.translateRequestResult(err, true),
        confirmButtonText: gettextCatalog.getString('CLOSE')
      }, function () {});

    };

    /**
     * @ngdoc function
     * @name error.translateRequestResult
     * @description
     *
     * attempts to translate request result message
     */
    error.translateRequestResult = function (err, returnUnknown) {
      switch (err.toUpperCase()) {

        case 'ERROR_INVALID_TOKEN':
          return gettextCatalog.getString('ERROR_INVALID_TOKEN');
        case 'NOT_AUTHORISED_FOR_THIS_ACTION':
          return gettextCatalog.getString('NOT_AUTHORISED_FOR_THIS_ACTION');
        case 'PASSWORD_CHANGED':
          return gettextCatalog.getString('PASSWORD_CHANGED');
        case 'ERROR_CHANGING_PASSWORD':
          return gettextCatalog.getString('ERROR_CHANGING_PASSWORD');
        case 'NEW_PASSWORD_WAS_SET':
          return gettextCatalog.getString('NEW_PASSWORD_WAS_SET');
        case 'ERROR_SETTING_PASSWORD':
          return gettextCatalog.getString('ERROR_SETTING_PASSWORD');
        case 'ERROR_SYNC':
          return gettextCatalog.getString('ERROR_SYNC');
        case 'ERROR_UPDATE':
          return gettextCatalog.getString('ERROR_UPDATE');
        case 'TIMEOUT_ERROR':
          return gettextCatalog.getString('TIMEOUT_ERROR');
        case 'UNKNOWN_UPDATE_ERROR':
          return gettextCatalog.getString('UNKNOWN_UPDATE_ERROR');
        case 'UNKNOWN_SYNC_ERROR':
          return gettextCatalog.getString('UNKNOWN_SYNC_ERROR');
        case 'CONNECTION_UNSUCCESSFULL':
          return gettextCatalog.getString('CONNECTION_UNSUCCESSFULL');
        case 'NO_USER_ONDEVICE':
          return gettextCatalog.getString('NO_USER_ONDEVICE');
        case 'API_ENDPOINT_NOT_VALID':
          return gettextCatalog.getString('API_ENDPOINT_NOT_VALID');

        case '???':
        case 'UNKNOWN_ERROR':
          return gettextCatalog.getString('UNKNOWN_ERROR');

        default:
          if (returnUnknown) {
            return err;
          } else {
            return '';
          }
      }
    };

    error.Log = function () {
      //TODO: error logging
    };

  }

})();
