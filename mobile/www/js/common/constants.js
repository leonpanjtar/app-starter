(function () {
  'use strict';

  /**
   * @ngdoc service
   * @name Constants
   * @description
   * this service holds the application wide constants
   *
   * common/constants.js
   *
   * (c) 2015 Comland d.o.o. http://www.comland.si
   */

  angular
    .module('SmartForms.common.constants', [])
    .service('Constants', [function () {

      var _version    = '2.0.0';
      var _intversion = 201;

      var _timeZones = [
        {
          'timeZoneId': 1,
          'offset': 'GMT-12:00',
          'value': 'International Date Line West',
          'useDaylightTime': 0,
          'valueGMT': -12
        },
        {
          'timeZoneId': 2,
          'offset': 'GMT-11:00',
          'value': 'Midway Island, Samoa',
          'useDaylightTime': 0,
          'valueGMT': -11
        },
        {
          'timeZoneId': 3,
          'offset': 'GMT-10:00',
          'value': 'Hawaii',
          'useDaylightTime': 0,
          'valueGMT': -10
        },
        {
          'timeZoneId': 4,
          'offset': 'GMT-09:00',
          'value': 'Alaska',
          'useDaylightTime': 1,
          'valueGMT': -9
        },
        {
          'timeZoneId': 5,
          'offset': 'GMT-08:00',
          'value': 'Pacific Time (US & Canada)',
          'useDaylightTime': 1,
          'valueGMT': -8
        },
        {
          'timeZoneId': 6,
          'offset': 'GMT-08:00',
          'value': 'Tijuana, Baja California',
          'useDaylightTime': 1,
          'valueGMT': -8
        },
        {
          'timeZoneId': 7,
          'offset': 'GMT-07:00',
          'value': 'Arizona',
          'useDaylightTime': 0,
          'valueGMT': -7
        },
        {
          'timeZoneId': 8,
          'offset': 'GMT-07:00',
          'value': 'Chihuahua, La Paz, Mazatlan',
          'useDaylightTime': 1,
          'valueGMT': -7
        },
        {
          'timeZoneId': 9,
          'offset': 'GMT-07:00',
          'value': 'Mountain Time (US & Canada)',
          'useDaylightTime': 1,
          'valueGMT': -7
        },
        {
          'timeZoneId': 10,
          'offset': 'GMT-06:00',
          'value': 'Central America',
          'useDaylightTime': 0,
          'valueGMT': -6
        },
        {
          'timeZoneId': 11,
          'offset': 'GMT-06:00',
          'value': 'Central Time (US & Canada)',
          'useDaylightTime': 1,
          'valueGMT': -6
        },
        {
          'timeZoneId': 12,
          'offset': 'GMT-06:00',
          'value': 'Guadalajara, Mexico City, Monterrey',
          'useDaylightTime': 1,
          'valueGMT': -6
        },
        {
          'timeZoneId': 13,
          'offset': 'GMT-06:00',
          'value': 'Saskatchewan',
          'useDaylightTime': 0,
          'valueGMT': -6
        },
        {
          'timeZoneId': 14,
          'offset': 'GMT-05:00',
          'value': 'Bogota, Lima, Quito, Rio Branco',
          'useDaylightTime': 0,
          'valueGMT': -5
        },
        {
          'timeZoneId': 15,
          'offset': 'GMT-05:00',
          'value': 'Eastern Time (US & Canada)',
          'useDaylightTime': 1,
          'valueGMT': -5
        },
        {
          'timeZoneId': 16,
          'offset': 'GMT-05:00',
          'value': 'Indiana (East)',
          'useDaylightTime': 1,
          'valueGMT': -5
        },
        {
          'timeZoneId': 17,
          'offset': 'GMT-04:00',
          'value': 'Atlantic Time (Canada)',
          'useDaylightTime': 1,
          'valueGMT': -4
        },
        {
          'timeZoneId': 18,
          'offset': 'GMT-04:00',
          'value': 'Caracas, La Paz',
          'useDaylightTime': 0,
          'valueGMT': -4
        },
        {
          'timeZoneId': 19,
          'offset': 'GMT-04:00',
          'value': 'Manaus',
          'useDaylightTime': 0,
          'valueGMT': -4
        },
        {
          'timeZoneId': 20,
          'offset': 'GMT-04:00',
          'value': 'Santiago',
          'useDaylightTime': 1,
          'valueGMT': -4
        },
        {
          'timeZoneId': 21,
          'offset': 'GMT-03:30',
          'value': 'Newfoundland',
          'useDaylightTime': 1,
          'valueGMT': -3.5
        },
        {
          'timeZoneId': 22,
          'offset': 'GMT-03:00',
          'value': 'Brasilia',
          'useDaylightTime': 1,
          'valueGMT': -3
        },
        {
          'timeZoneId': 23,
          'offset': 'GMT-03:00',
          'value': 'Buenos Aires, Georgetown',
          'useDaylightTime': 0,
          'valueGMT': -3
        },
        {
          'timeZoneId': 24,
          'offset': 'GMT-03:00',
          'value': 'Greenland',
          'useDaylightTime': 1,
          'valueGMT': -3
        },
        {
          'timeZoneId': 25,
          'offset': 'GMT-03:00',
          'value': 'Montevideo',
          'useDaylightTime': 1,
          'valueGMT': -3
        },
        {
          'timeZoneId': 26,
          'offset': 'GMT-02:00',
          'value': 'Mid-Atlantic',
          'useDaylightTime': 1,
          'valueGMT': -2
        },
        {
          'timeZoneId': 27,
          'offset': 'GMT-01:00',
          'value': 'Cape Verde Is.',
          'useDaylightTime': 0,
          'valueGMT': -1
        },
        {
          'timeZoneId': 28,
          'offset': 'GMT-01:00',
          'value': 'Azores',
          'useDaylightTime': 1,
          'valueGMT': -1
        },
        {
          'timeZoneId': 29,
          'offset': 'GMT+00:00',
          'value': 'Casablanca, Monrovia, Reykjavik',
          'useDaylightTime': 0,
          'valueGMT': 0
        },
        {
          'timeZoneId': 30,
          'offset': 'GMT+00:00',
          'value': 'Greenwich Mean Time : Dublin, Edinburgh, Lisbon, London',
          'useDaylightTime': 1,
          'valueGMT': 0
        },
        {
          'timeZoneId': 31,
          'offset': 'GMT+01:00',
          'value': 'Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna',
          'useDaylightTime': 1,
          'valueGMT': 1
        },
        {
          'timeZoneId': 32,
          'offset': 'GMT+01:00',
          'value': 'Belgrade, Bratislava, Budapest, Ljubljana, Prague',
          'useDaylightTime': 1,
          'valueGMT': 1
        },
        {
          'timeZoneId': 33, 'offset':
          'GMT+01:00', 'value': 'Brussels, Copenhagen, Madrid, Paris',
          'useDaylightTime': 1,
          'valueGMT': 1
        },
        {
          'timeZoneId': 34,
          'offset': 'GMT+01:00',
          'value': 'Sarajevo, Skopje, Warsaw, Zagreb',
          'useDaylightTime': 1,
          'valueGMT': 1
        },
        {
          'timeZoneId': 35,
          'offset': 'GMT+01:00',
          'value': 'West Central Africa',
          'useDaylightTime': 1,
          'valueGMT': 1
        },
        {
          'timeZoneId': 36,
          'offset': 'GMT+02:00',
          'value': 'Amman',
          'useDaylightTime': 1,
          'valueGMT': 2
        },
        {
          'timeZoneId': 37,
          'offset': 'GMT+02:00',
          'value': 'Athens, Bucharest, Istanbul', 'useDaylightTime': 1,
          'valueGMT': 2
        },
        {
          'timeZoneId': 38,
          'offset': 'GMT+02:00',
          'value': 'Beirut',
          'useDaylightTime': 1,
          'valueGMT': 2
        },
        {
          'timeZoneId': 39,
          'offset': 'GMT+02:00',
          'value': 'Cairo',
          'useDaylightTime': 1,
          'valueGMT': 2
        },
        {
          'timeZoneId': 40,
          'offset': 'GMT+02:00',
          'value': 'Harare, Pretoria',
          'useDaylightTime': 0,
          'valueGMT': 2
        },
        {
          'timeZoneId': 41,
          'offset': 'GMT+02:00',
          'value': 'Helsinki, Kyiv, Riga, Sofia, Tallinn, Vilnius',
          'useDaylightTime': 1,
          'valueGMT': 2
        },
        {
          'timeZoneId': 42,
          'offset': 'GMT+02:00',
          'value': 'Jerusalem',
          'useDaylightTime': 1,
          'valueGMT': 2
        },
        {
          'timeZoneId': 43,
          'offset': 'GMT+02:00', 'value': 'Minsk',
          'useDaylightTime': 1,
          'valueGMT': 2
        },
        {
          'timeZoneId': 44,
          'offset': 'GMT+02:00',
          'value': 'Windhoek',
          'useDaylightTime': 1,
          'valueGMT': 2
        },
        {
          'timeZoneId': 45,
          'offset': 'GMT+03:00',
          'value': 'Kuwait, Riyadh, Baghdad',
          'useDaylightTime': 0,
          'valueGMT': 3
        },
        {
          'timeZoneId': 46,
          'offset': 'GMT+03:00',
          'value': 'Moscow, St. Petersburg, Volgograd',
          'useDaylightTime': 1,
          'valueGMT': 3
        },
        {
          'timeZoneId': 47,
          'offset': 'GMT+03:00',
          'value': 'Nairobi',
          'useDaylightTime': 0,
          'valueGMT': 3
        },
        {
          'timeZoneId': 48,
          'offset': 'GMT+03:00',
          'value': 'Tbilisi',
          'useDaylightTime': 0,
          'valueGMT': 3
        },
        {
          'timeZoneId': 49,
          'offset': 'GMT+03:30',
          'value': 'Tehran',
          'useDaylightTime': 1,
          'valueGMT': 3.5
        },
        {
          'timeZoneId': 50,
          'offset': 'GMT+04:00',
          'value': 'Abu Dhabi, Muscat',
          'useDaylightTime': 0,
          'valueGMT': 4},
        {
          'timeZoneId': 51,
          'offset': 'GMT+04:00',
          'value': 'Baku',
          'useDaylightTime': 1,
          'valueGMT': 4
        },
        {
          'timeZoneId': 52,
          'offset': 'GMT+04:00',
          'value': 'Yerevan',
          'useDaylightTime': 1,
          'valueGMT': 4
        },
        {
          'timeZoneId': 53,
          'offset': 'GMT+04:30',
          'value': 'Kabul',
          'useDaylightTime': 0,
          'valueGMT': 4.5
        },
        {
          'timeZoneId': 54,
          'offset': 'GMT+05:00',
          'value': 'Yekaterinburg',
          'useDaylightTime': 1,
          'valueGMT': 5
        },
        {
          'timeZoneId': 55,
          'offset': 'GMT+05:00',
          'value': 'Islamabad, Karachi, Tashkent',
          'useDaylightTime': 0,
          'valueGMT': 5
        },
        {
          'timeZoneId': 56,
          'offset': 'GMT+05:30',
          'value': 'Sri Jayawardenapura',
          'useDaylightTime': 0,
          'valueGMT': 5.5
        },
        {
          'timeZoneId': 57,
          'offset': 'GMT+05:30',
          'value': 'Chennai, Kolkata, Mumbai, New Delhi',
          'useDaylightTime': 0,
          'valueGMT': 5.5
        },
        {
          'timeZoneId': 58,
          'offset': 'GMT+05:45',
          'value': 'Kathmandu',
          'useDaylightTime': 0,
          'valueGMT': 5.75
        },
        {
          'timeZoneId': 59,
          'offset': 'GMT+06:00',
          'value': 'Almaty, Novosibirsk',
          'useDaylightTime': 1,
          'valueGMT': 6
        },
        {
          'timeZoneId': 60,
          'offset': 'GMT+06:00',
          'value': 'Astana, Dhaka',
          'useDaylightTime': 0,
          'valueGMT': 6
        },
        {
          'timeZoneId': 61,
          'offset': 'GMT+06:30',
          'value': 'Yangon (Rangoon)',
          'useDaylightTime': 0,
          'valueGMT': 6.5
        },
        {
          'timeZoneId': 62,
          'offset': 'GMT+07:00',
          'value': 'Bangkok, Hanoi, Jakarta',
          'useDaylightTime': 0,
          'valueGMT': 7
        },
        {
          'timeZoneId': 63,
          'offset': 'GMT+07:00',
          'value': 'Krasnoyarsk',
          'useDaylightTime': 1,
          'valueGMT': 7
        },
        {
          'timeZoneId': 64,
          'offset': 'GMT+08:00',
          'value': 'Beijing, Chongqing, Hong Kong, Urumqi',
          'useDaylightTime': 0,
          'valueGMT': 8
        },
        {
          'timeZoneId': 65,
          'offset': 'GMT+08:00',
          'value': 'Kuala Lumpur, Singapore',
          'useDaylightTime': 0,
          'valueGMT': 8
        },
        {
          'timeZoneId': 66,
          'offset': 'GMT+08:00',
          'value': 'Irkutsk, Ulaan Bataar',
          'useDaylightTime': 0,
          'valueGMT': 8
        },
        {
          'timeZoneId': 67,
          'offset': 'GMT+08:00',
          'value': 'Perth',
          'useDaylightTime': 0,
          'valueGMT': 8},
        {
          'timeZoneId': 68,
          'offset': 'GMT+08:00',
          'value': 'Taipei',
          'useDaylightTime': 0,
          'valueGMT': 8
        },
        {
          'timeZoneId': 69,
          'offset': 'GMT+09:00',
          'value': 'Osaka, Sapporo, Tokyo',
          'useDaylightTime': 0,
          'valueGMT': 9
        },
        {
          'timeZoneId': 70,
          'offset': 'GMT+09:00',
          'value': 'Seoul',
          'useDaylightTime': 0,
          'valueGMT': 9
        },
        {
          'timeZoneId': 71,
          'offset': 'GMT+09:00',
          'value': 'Yakutsk',
          'useDaylightTime': 1,
          'valueGMT': 9
        },
        {
          'timeZoneId': 72,
          'offset': 'GMT+09:30',
          'value': 'Adelaide',
          'useDaylightTime': 0,
          'valueGMT': 9.5
        },
        {
          'timeZoneId': 73,
          'offset': 'GMT+09:30',
          'value': 'Darwin',
          'useDaylightTime': 0,
          'valueGMT': 9.5
        },
        {
          'timeZoneId': 74,
          'offset': 'GMT+10:00',
          'value': 'Brisbane',
          'useDaylightTime': 0,
          'valueGMT': 10
        },
        {
          'timeZoneId': 75,
          'offset': 'GMT+10:00',
          'value': 'Canberra, Melbourne, Sydney',
          'useDaylightTime': 1,
          'valueGMT': 10
        },
        {
          'timeZoneId': 76,
          'offset': 'GMT+10:00',
          'value': 'Hobart',
          'useDaylightTime': 1,
          'valueGMT': 10
        },
        {
          'timeZoneId': 77,
          'offset': 'GMT+10:00',
          'value': 'Guam, Port Moresby',
          'useDaylightTime': 0,
          'valueGMT': 10
        },
        {
          'timeZoneId': 78,
          'offset': 'GMT+10:00',
          'value': 'Vladivostok',
          'useDaylightTime': 1,
          'valueGMT': 10
        },
        {
          'timeZoneId': 79,
          'offset': 'GMT+11:00',
          'value': 'Magadan, Solomon Is., New Caledonia',
          'useDaylightTime': 1,
          'valueGMT': 11
        },
        {
          'timeZoneId': 80,
          'offset': 'GMT+12:00',
          'value': 'Auckland, Wellington',
          'useDaylightTime': 1,
          'valueGMT': 12
        },
        {
          'timeZoneId': 81,
          'offset': 'GMT+12:00',
          'value': 'Fiji, Kamchatka, Marshall Is.',
          'useDaylightTime': 0,
          'valueGMT': 12
        },
        {
          'timeZoneId': 82,
          'offset': 'GMT+13:00',
          'value': 'Nuku',
          'useDaylightTime': 0,
          'valueGMT': 13
        }
      ];

      var _timeZoneAbbreviations = [
        {'name': 'MIT', 'gmtValue': -11, 'useDaylight': 0}, {'name': 'HAST', 'gmtValue': -10, 'useDaylight': 0},
        {'name': 'AKST', 'gmtValue': -9, 'useDaylight': 1}, {'name': 'AKDT', 'gmtValue': -9, 'useDaylight': 1},
        {'name': 'PST', 'gmtValue': -8, 'useDaylight': 1}, {'name': 'PDT', 'gmtValue': -8, 'useDaylight': 1},
        {'name': 'MST', 'gmtValue': -7, 'useDaylight': 1}, {'name': 'MDT', 'gmtValue': -7, 'useDaylight': 1},
        {'name': 'CST', 'gmtValue': -6, 'useDaylight': 1}, {'name': 'CDT', 'gmtValue': -6, 'useDaylight': 1},
        {'name': 'EST', 'gmtValue': -5, 'useDaylight': 1}, {'name': 'EDT', 'gmtValue': -5, 'useDaylight': 1},
        {'name': 'PRT', 'gmtValue': -4, 'useDaylight': 0}, {'name': 'CNT', 'gmtValue': -3.5, 'useDaylight': 0},
        {'name': 'AGT', 'gmtValue': -3, 'useDaylight': 0}, {'name': 'BET', 'gmtValue': -3, 'useDaylight': 0},
        {'name': 'CAT', 'gmtValue': -1, 'useDaylight': 0}, {'name': 'WET', 'gmtValue': 0, 'useDaylight': 1},
        {'name': 'WEST', 'gmtValue': 0, 'useDaylight': 1}, {'name': 'CET', 'gmtValue': 1, 'useDaylight': 1},
        {'name': 'CEST', 'gmtValue': 1, 'useDaylight': 1}, {'name': 'EET', 'gmtValue': 2, 'useDaylight': 1},
        {'name': 'EEST', 'gmtValue': 2, 'useDaylight': 1}, {'name': 'ART', 'gmtValue': 2, 'useDaylight': 0},
        {'name': 'EAT', 'gmtValue': 3, 'useDaylight': 0}, {'name': 'MET', 'gmtValue': 3.5, 'useDaylight': 0},
        {'name': 'NET', 'gmtValue': 4, 'useDaylight': 0}, {'name': 'PLT', 'gmtValue': 5, 'useDaylight': 0},
        {'name': 'IST', 'gmtValue': 5.5, 'useDaylight': 0}, {'name': 'BST', 'gmtValue': 6, 'useDaylight': 0},
        {'name': 'ICT', 'gmtValue': 7, 'useDaylight': 0}, {'name': 'CTT', 'gmtValue': 8, 'useDaylight': 0},
        {'name': 'SGT', 'gmtValue': 8, 'useDaylight': 0}, {'name': 'AWST', 'gmtValue': 8, 'useDaylight': 0},
        {'name': 'JST', 'gmtValue': 9, 'useDaylight': 0}, {'name': 'ACST', 'gmtValue': 9.5, 'useDaylight': 0},
        {'name': 'AEST', 'gmtValue': 10, 'useDaylight': 0}, {'name': 'SST', 'gmtValue': 11, 'useDaylight': 0},
        {'name': 'NZST', 'gmtValue': 12, 'useDaylight': 1}, {'name': 'NZDT', 'gmtValue': 12, 'useDaylight': 1},
        {'name': 'CLT', 'gmtValue': -4, 'useDaylight': 1}, {'name': 'CLST', 'gmtValue': -4, 'useDaylight': 1}
      ];

      var _API = {
        baseUrl: 'http://localhost/api'
      };

      var _db = false;

      var _timeouts = {
        collection: {
          user: 0
        },
        http: 20000
      };

      var _identifyDevice = function () {
        var listOfDevices = ['Linux armv7l', 'Linux aarch64', 'Linux i686'];
        var myDevice = window.navigator.platform;
        var isDevice = false;
        angular.forEach(listOfDevices, function (device) {
          if (device === myDevice) {
            isDevice = true;
          }
        });
        return isDevice;
      };

      var _phrase = 'SmartForms';
      /*
      * Caution when inserting into SQLite!
      * When inserting data in SQLite be careful by using keywords like : ORDER,CASE,SELECT,FORM,....
      * It is recommended to avoid names for tables,columns like that, but if you really have to use them,
      * be sure to explicitly state that the word is a string and not a keyword
      * e.g.: 'CREATE TABLE 'CASE' ( 'ORDER' int, field1 text);'
      * List of reserved keywords in SQLite are found here: https://www.sqlite.org/lang_keywords.html
      */
      var _dbconfig = {
        name: 'SmartFormsDB',
        tables: [
          {
            name: 'USERS',
            columns: [
              {name: 'Id_Users', type: 'text primary key'},
              {name: 'FirstName', type: 'text'},
              {name: 'LastName', type: 'text'},
              {name: 'Email', type: 'text'},
              {name: 'Password', type: 'text'},
              {name: 'Salt', type: 'text'},
              {name: 'IV', type: 'text'},
              {name: 'PhoneNumber', type: 'text'},
              {name: 'UserName', type: 'text'}
            ]
          },
          {
            name: 'CONFIG',
            columns: [
              {name: 'Id_Config', type: 'int primary key'},
              {name: 'Scope', type: 'text'},
              {name: 'Key', type: 'text'},
              {name: 'Value', type: 'text'},
              {name: 'Id_User', type: 'int'},
              {name: 'FOREIGN KEY(Id_User) REFERENCES USERS(Id_Users)', type: ''}
            ]
          },
          {
            name: 'CODELIST',
            columns: [
              {name: 'Id_CodeList', type: 'int primary key'},
              {name: 'Guid', type: 'text'}, // here we have uniqueindentifier
              {name: 'Label', type: 'text'},
              //{name: 'Value', type: 'text'}
              {name: 'Description', type: 'text'}

            ]
          },
          {
            name: 'CODEITEM',
            columns: [
              {name: 'Id_CodeItem', type: 'int primary key'},
              {name: 'Id_CodeList', type: 'int'},
              {name: 'Id_CodeItem_Parent', type: 'int'},
              {name: 'Guid', type: 'text'}, // here we have uniqueindentifier
              {name: 'Label', type: 'text'},
              {name: 'Value', type: 'text'},
              {name: 'FOREIGN KEY(Id_CodeList) REFERENCES CODELIST(Id_CodeList)', type: ''},
              {name: 'FOREIGN KEY(Id_CodeItem_Parent) REFERENCES CODEITEM(Id_CodeItem)', type: ''}
            ]
          },
          {
            name: 'CATEGORY',
            columns: [
              {name: 'Id_Category', type: 'int primary key'},
              {name: 'Id_Category_Parent', type: 'int'},
              {name: 'Id_Binaries', type: 'int'},
              {name: 'Name', type: 'text'},
              {name: 'Description', type: 'text'},
              {name: 'Image', type: 'text'}
            ]
          },
                    {
            name: 'BINARIES',
            columns: [
              {name: 'Id_Binaries', type: 'int primary key'},
              {name: 'Data', type: 'blob'},
              {name: 'Hash', type: 'text'},
              {name: 'Type', type: 'text'}
            ]
          },
          {
            name: 'PROCEDURE',
            columns: [
              {name: 'Id_Procedure', type: 'int primary key'},
              {name: 'Id_Binaries', type: 'int'},
              {name: 'Name', type: 'text'},
              {name: 'Description', type: 'text'},
              {name: 'Image', type: 'text'},
              {name: 'Icon', type: 'text'},
              {name: 'Version', type: 'int'},
            ]
          },
          {
            name: 'PROCEDUREREPORT',
            columns: [
              {name: 'Id_ProcedureReport', type: 'int primary key'},
              {name: 'Id_Procedure', type: 'int'},
              {name: 'Guid', type: 'text'}, // here we have uniqueindentifier
              {name: 'Label', type: 'text'},
              {name: 'Description', type: 'text'},
              {name: 'Dependencies', type: 'text'},
              {name: 'Content', type: 'text'},
              {name: 'Version', type: 'int'},
              {name: 'FOREIGN KEY(Id_Procedure) REFERENCES PROCEDURE(Id_Procedure)', type: ''}
            ]
          },

          {
            name: 'FORM',
            columns: [
              {name: 'Id_Form', type: 'int primary key'},
              {name: 'Id_Procedure', type: 'int'},
              {name: 'Guid', type: 'text'}, // here we have uniqueindentifier
              {name: 'Label', type: 'text'},
              {name: 'Description', type: 'text'},
              {name: 'Dependencies', type: 'text'},
              {name: 'Properties', type: 'text'},
              {name: '\'Order\'', type: 'int'},
              {name: 'Type', type: 'int'},
              {name: 'Version', type: 'int'},
              {name: 'FOREIGN KEY(Id_Procedure) REFERENCES PROCEDURE(Id_Procedure)', type: ''}
            ]
          },
          {
            name: 'SECTION',
            columns: [
              {name: 'Id_Section', type: 'int primary key'},
              {name: 'Id_Form', type: 'int'},
              {name: 'Guid', type: 'text'}, // here we have uniqueindentifier
              {name: 'Label', type: 'text'},
              {name: 'Description', type: 'text'},
              {name: 'Dependencies', type: 'text'},
              {name: 'Properties', type: 'text'},
              {name: '\'Order\'', type: 'int'},
              {name: 'Version', type: 'int'},
              {name: 'FOREIGN KEY(Id_Form) REFERENCES FORM(Id_Form)', type: ''}
            ]
          },
          {
            name: 'FIELD',
            columns: [
              {name: 'Id_Field', type: 'int primary key'},
              {name: 'Id_Section', type: 'int'},
              {name: 'Guid', type: 'text'}, // here we have uniqueindentifier
              {name: 'Label', type: 'text'},
              {name: 'Help', type: 'text'},
              {name: 'Hint', type: 'text'},
              {name: 'Type', type: 'int'},
              {name: 'DataSource', type: 'text'},
              {name: 'Dependencies', type: 'text'},
              {name: 'Rules', type: 'text'},
              {name: 'Properties', type: 'text'},
              {name: 'Row', type: 'int'},
              {name: 'Coll', type: 'int'},
              {name: 'Id_Field_Parent', type: 'int'},
              {name: 'FOREIGN KEY(Id_Section) REFERENCES SECTION(Id_Section)', type: ''},
              {name: 'FOREIGN KEY(Id_Field_Parent) REFERENCES Id_Field', type: ''}
            ]
          },
          {
            name: '\'CASE\'',
            columns: [
              {name: 'Id_Case', type: 'INTEGER PRIMARY KEY AUTOINCREMENT'},
              {name: 'Id_User', type: 'int'},
              {name: 'Id_Device', type: 'int'},
              {name: 'Id_Procedure', type: 'int'},
              {name: 'Name', type: 'text'},
              {name: 'Location', type: 'text'},
              {name: 'Hash', type: 'text'},
              {name: 'Content', type: 'blob'}, // here we have varbinary
              {name: 'Synced', type: 'text'},
              // documentation recommends storing date in text with following format: YYYY-MM-DD HH:MM:SS.SSS
              {name: 'Created', type: 'text'},
              {name: 'Closed', type: 'int'},
              {name: 'Guid', type: 'text'},
              {name: 'Version', type: 'int'},
              {name: 'FOREIGN KEY(Id_User) REFERENCES USERS(Id_Users)', type: ''},
              {name: 'FOREIGN KEY(Id_Device) REFERENCES PROCEDURE(Id_Device)', type: ''},
              {name: 'FOREIGN KEY(Id_Procedure) REFERENCES PROCEDURE(Id_Procedure)', type: ''}
            ]
          },
          {
            name: 'USERHASPROCEDURE',
            columns: [
              // {name: 'Id', type:'int primary key'},
              {name: 'Id_User', type: 'int'},
              {name: 'Id_Procedure', type: 'int'},
              {name: 'FOREIGN KEY(Id_User) REFERENCES USERS(Id_Users)', type: ''},
              {name: 'FOREIGN KEY(Id_Procedure) REFERENCES PROCEDURE(Id_Procedure)', type: ''}
            ]
          },
          {
            name: 'PROCEDUREHASCATEGORY',
            columns: [
              {name: 'Id_Procedure', type: 'text'},
              {name: 'Id_Category', type: 'text'},
              {name: 'FOREIGN KEY(`Id_Procedure`) REFERENCES `PROCEDURE`(`Id_Procedure`)', type: ''},
              {name: 'FOREIGN KEY(`Id_Category`) REFERENCES `CATEGORY`(`Id_Category`)', type: ''}
            ]
          }
        ]
      };

      var _dbdata = {
        //tables
      };

      //var _defoultLanguage = 'sl_SI';
      var _defoultLanguage = 'en';

      var _languages = [
        {
          name: 'English',
          value: 'en',
          deviceValues:['en-US','en-AU','en-CA','en-IN','en-NZ','en-SG','en-GB'],
          locale: 'en-gb'
        },
        {
          name: 'Slovenian',
          value: 'sl_SI',
          locale: 'sl-si'
        }
      ];

      var _fieldType = [
        {name: 'header', value: 0},
        {name: 'subheader', value: 1},
        {name: 'description', value: 2},
        {name: 'apicall', value: 3},
        {name: 'localcall', value: 4},
        {name: 'input', value: 600},
        {name: 'barcode', value: 601},
        {name: 'hidden', value: 602},
        {name: 'gps', value: 603},
        {name: 'textarea', value: 700},
        {name: 'date', value: 800},
        {name: 'time', value: 801},
        {name: 'toggle', value: 900},
        {name: 'signature', value: 1000},
        {name: 'dropdown', value: 1200},
        {name: 'checkbox', value: 1201},
        {name: 'radio', value: 1202},
        {name: 'table', value: 1205},
        {name: 'email', value: 1206},
        {name: 'html', value: 1207},
        {name: 'file', value: 1208},
        {name: 'calculation', value: 1209}
      ];

      var _defaultThumbnail = true;

      var _defaultLocale = 'en-gb';

      // Export the constants
      var constants = {
        DEBUGMODE               : window.mfDebug,
        SHOWBROADCAST_EVENTS    : true,
        API                     : _API,
        DB                      : _db,
        DB_CONFIG               : _dbconfig,
        DB_DATA                 : _dbdata,
        timeouts                : _timeouts,
        identifyDevice          : _identifyDevice,
        PHRASE                  : _phrase,
        LANGUAGES               : _languages,
        DEFOULT_LANGUAGE        : _defoultLanguage,
        timeZones               : _timeZones,
        timeZoneAbbreviations   : _timeZoneAbbreviations,
        fieldType               : _fieldType,
        version                 : _version,
        intversion              : _intversion,
        DEFAULT_THUMBNAIL       : _defaultThumbnail,
        DEFAULT_LOCALE          : _defaultLocale
      };

      // Make them publically available
      return constants;

    }]);

})();
