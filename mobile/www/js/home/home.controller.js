(function () {
  'use strict';

  /**
   * @ngdoc overview
   * @name HomeCtrl
   * @description
   * controller for the home page
   *
   * home/home.controller.js
   *
   * (c) 2015 Comland d.o.o. http://www.comland.si
   */

  angular
    .module('SmartForms.home.controller', [
      'SmartForms.home.service'
    ])
    .controller('HomeCtrl', HomeCtrl);

  HomeCtrl.$inject = [
    '$rootScope',
    '$scope',
    '$state',
    '$ionicPlatform',
    '$ionicSideMenuDelegate',
    '$ionicScrollDelegate',
    '$cordovaGoogleAnalytics',
    'gettextCatalog',
    'Constants',
    'HomeService',
    'ChiperService',
    'CurrentService',
    'SweetAlert',
    '$ionicHistory'
  ];

  function HomeCtrl($rootScope, $scope, $state, $ionicPlatform, $ionicSideMenuDelegate, $ionicScrollDelegate,
                    $cordovaGoogleAnalytics, gettextCatalog, Constants,  HomeService, ChiperService, CurrentService,
                    SweetAlert, $ionicHistory) {

    // TODO: Organize code in this controller into main
    // parts: Initialization, Private Fn, Public Fn and
    // Watchers & Events

    $ionicSideMenuDelegate.canDragContent(false);

    // Set viewmodel parameters
    $scope.vm.cases = [];
    $scope.vm.synced = false;
    $scope.vm.updated = false;

    // Set controller flags
    $scope.allowInfiniteScroll = false;
    $scope.lastIndexCases = 0;
    $scope.pageSize = 12;

    // Set language specific scope variables
    $scope.pagename = gettextCatalog.getString('DASHBOARD');

    /**
     * @ngdoc function
     * @name self.alertAutoSync
     * @description shows SweetAlert to inform the user that the app was autosynced
     */
    var alertAutoSync = function () {
      SweetAlert.swal({
        title: gettextCatalog.getString('Autosync successfull'),
        text: gettextCatalog.getString('All closed cases were synced to server.'),
        confirmButtonText: gettextCatalog.getString('OK')
      }, function () {
        $rootScope.autoSynced = false;
      });
    };

    $scope.openCase = function (ca) {
      console.log(ca);
      $state.go('app.case', {
        procedureId: ca.Id_Procedure, // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
        caseId: ca.Id_Case // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
      });
    };

    /**
     * @ngdoc function
     * @name $scope.vm.editPreview
     * @description sets a case for preview or edit. Depending if case is closed or not.
     */
    $scope.vm.editPreview = function (ca) {
      if (ca.Closed) {
        $state.go('app.case',{
          procedureId   : ca.Id_Procedure, // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
          caseId        : ca.Id_Case // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
        });

      }else {
        CurrentService.setPreviewValue(true,null);
        $state.go('app.case',{
          procedureId   : ca.Id_Procedure, // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
          caseId        : ca.Id_Case // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
        });
      }
    };

    /**
     * @ngdoc function
     * @name $scope.vm.deleteCase
     * @description used to deletes case from database and sets $scope.vm.cases to new array
     */
    $scope.vm.deleteCase = function (ca) {
      SweetAlert.swal({
          title: gettextCatalog.getString('Are you sure?'),
          text: gettextCatalog.getString('You will not be able to recover this!'),
          showCancelButton: true,
          confirmButtonColor: '#3B9C55',
          confirmButtonText: gettextCatalog.getString('Yes'),
          cancelButtonText: gettextCatalog.getString('No')
        },
        function(isConfirm) {
          if (isConfirm) {
            HomeService.deleteCase(ca.Id_Case, $scope.vm.cases) // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
            .then(
              function(result) {
                $scope.vm.cases = result;
              }
            );
          }
        });
    };

    // TODO: Write function documentation
    $scope.getLastCases = function () {

      var error = function (error) {
        console.log(error);
      };

      var success = function (response) {

        if (response instanceof Array) {
          if (!$rootScope.haveClosed) {
            if (_.findWhere(response, {Closed: 1, Synced: null})) {
              $rootScope.haveClosed = true;
            }
          }
          $scope.vm.cases = response;
          $scope.lastIndexCases = response.length;
          $scope.allowInfiniteScroll = true;
        } else {
          $scope.vm.cases = [];
          $scope.lastIndexCases = 0;
          $scope.allowInfiniteScroll = true;
        }
      };

      HomeService.getLastCases($scope.pageSize).then(success, error);

    };

    // TODO: Write function documentation
    $scope.ItemSwipped = function (ca) {
      if (window.analytics) {
        // jscs:disable requireCamelCaseOrUpperCaseIdentifiers
        $cordovaGoogleAnalytics.trackEvent('/home', 'Swipe', 'Left', ca.Id_Case);
        // jscs:enable requireCamelCaseOrUpperCaseIdentifiers
      }
    };

    // TODO: Write function documentation
    $scope.loadMore = function () {
      function success(response) {
        if ($scope.vm.cases.length >= $scope.pageSize) {
          if (response instanceof Array) {
            $scope.allowInfiniteScroll = true;
            for (var i = 0; i < response.length; i++) {
              $scope.vm.cases.push(response[i]);
            }
            $scope.lastIndexCases += response.length;
          } else {
            $scope.allowInfiniteScroll = false;
          }
        } else {
          $scope.allowInfiniteScroll = false;
        }
        $scope.$broadcast('scroll.infiniteScrollComplete');
      }

      function error(response) {
        console.log('Error');
        console.log(response);
        $scope.allowInfiniteScroll = false;
        $scope.$broadcast('scroll.infiniteScrollComplete');
      }

      HomeService.getCasesForInfiniteScroll($scope.lastIndexCases, $scope.pageSize).then(success, error);

    };

    $scope.$on('$ionicView.beforeLeave', function() {
      //enable scroll on iOS
      $scope.allowInfiniteScroll = false;
    });

    // TODO: Write function documentation
    $scope.platformRady = function () {
      $scope.getLastCases();
    };

    $scope.$on('updateCases', function () {
      $scope.platformRady();
    });

    // TODO: Write documentation
    $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState) {
        if (toState.url === '/home' && fromState !== '/auth') {
          $scope.platformRady();
        }
        if (toState.url === '/home' && $rootScope.autoSynced) {
          alertAutoSync();
        }
      });

    /**
     * @ngdoc watcher
     * @name autoSynced
     * @description checks if cases were synced in the backgrount to alert
     */
    $rootScope.$watch('autoSynced', function () {
      if ($rootScope.autoSynced && $state.current.url === '/home') {
        alertAutoSync();
        $scope.platformRady();
      }
    });

    // TODO: Write documentation
    $ionicPlatform.ready(function () {
      if (window.analytics) {
        $cordovaGoogleAnalytics.trackView('/home');
      }
      HomeService.checkLanguageInDatabase().then(
        function (re) {
          //exists in database
          HomeService.storeLanguage(re, true);
        },
        function (error) {
          console.log('ERROR LANG');
          console.log(error);
          HomeService.storeLanguage(Constants.DEFOULT_LANGUAGE, false);
        }
        );
      $scope.platformRady();
    });

    if (!window.cordova) {
      $scope.platformRady();
    }

    $ionicPlatform.registerBackButtonAction(function () {
      if ($state.current.url === '/home') {
        ionic.Platform.exitApp();
      }
      else {
        $ionicHistory.goBack();
      }
    }, 100);

  }

})();
