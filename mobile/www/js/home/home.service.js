(function () {
  'use strict';

  /**
   * @ngdoc service
   * @name HomeService
   * @description
   * this service takes care of home screen
   *
   * home/home.service.js
   *
   * (c) 2015 Comland d.o.o. http://www.comland.si
   *
   */

  angular
    .module('SmartForms.home.service', [
      'SmartForms.auth.service',
      'SmartForms.common.dbconnection',
      'SmartForms.common.cipher',
      'gettext'
    ])
    .service('HomeService', HomeService);

  HomeService.$inject = [
    '$http',
    '$rootScope',
    '$q',
    'Constants',
    'DBConnection',
    'gettext',
    'AuthService',
    'gettextCatalog'
  ];

  function HomeService($http, $rootScope, $q, Constants, DBConnection, gettext, AuthService, gettextCatalog) {

    // TODO: Organize code in this controller into main
    // parts: Initialization, Private Fn, Public Fn and
    // Watchers & Events

    var home = this;

    /**
     * @ngdoc function
     * @name home.getProceduresOfCategory
     * @description get every procedure from database which has the same Id_Category as provided
     * @param {int} categoryId id from $scope.vm.category object
     * @returns deffered promise of all procedures found by query
     */
    home.getProceduresOfCategoryAndUser = function (categoryId, userid) {
      var deferred = $q.defer();

      var query = 'SELECT DISTINCT * FROM [PROCEDURE]' +
        ' LEFT JOIN USERHASPROCEDURE ON [PROCEDURE].Id_Procedure = USERHASPROCEDURE.Id_Procedure' +
        ' LEFT JOIN PROCEDUREHASCATEGORY ON [PROCEDURE].Id_Procedure = PROCEDUREHASCATEGORY.Id_Procedure' +
        ' WHERE PROCEDUREHASCATEGORY.Id_Category = \'' + categoryId + '\' ' +
        ' AND USERHASPROCEDURE.id_user = \'' + userid + '\' ' +
        ' AND PROCEDUREHASCATEGORY.Id_Procedure = [PROCEDURE].Id_Procedure';

      DBConnection.query(query).then(
        function (results) {
          var resultsToReturn = [];
          if (results.rows.length) {
            for (var i = 0; i < results.rows.length; i++) {
              resultsToReturn.push(results.rows.item(i));
            }
            deferred.resolve(resultsToReturn);
          } else {
            deferred.resolve(gettextCatalog.getString('No records found'));
          }
        },
        function (error) {
          if (Constants.DEBUGMODE) {
            console.log('Error on select procedure from db');
            console.log(error);
          }
          deferred.reject(error);
        }
        );
      return deferred.promise;
    };

    /**
     * @ngdoc function
     * @name home.getLast10Cases
     * @description get lest 10 cases from curren user
     * @returns deferred.promise last 10 cases or error
     *
     */
    home.getLastCases = function (pagesize) {
      var deferred = $q.defer();

      var query = 'SELECT ' +
                      '[CASE].*, ' +
                      '[PROCEDURE].Name as ProcedureName, ' +
                      '[PROCEDURE].Image as ProcedureImage ' +
                    'FROM [CASE] ' +
                    'LEFT JOIN [PROCEDURE] ON [CASE].Id_Procedure = [PROCEDURE].Id_Procedure ' +
                    'WHERE [CASE].Id_User = ? ' +
                    'ORDER BY [CASE].Created DESC ' +
                    'LIMIT ?';
      // jscs:disable requireCamelCaseOrUpperCaseIdentifiers
      DBConnection.query(query, [AuthService.currentUser.info.Id_Users, pagesize]).then(
        // jscs:enable requireCamelCaseOrUpperCaseIdentifiers
        function (results) {
          // if there are resoults save them to array
          if (results.rows.length) {
            var cases = [];
            for (var i = 0; i < results.rows.length; i++) {
              if (results.rows.item(i).Name === undefined || results.rows.item(i).Name === '') {
                results.rows.item(i).Name = results.rows.item(i).ProcedureName;
              }
              cases.push(results.rows.item(i));
            }
            console.log(cases);
            deferred.resolve(cases);
          }

          // if user has no cases return string
          else {
            deferred.resolve(gettextCatalog.getString('No records found'));
          }
        },

        function (error) {
          if (Constants.DEBUGMODE) {
            console.log('Error on select cases from db');
            console.log(error);
          }
          deferred.reject(error);
        });

      return deferred.promise;
    };

    /**
     * @ngdoc function
     * @name home.deleteCase
     * @description deletes case from database and gives new array of cases for $scope.vm.cases
     */
    home.deleteCase = function (caseId, cases) {
      var deferred = $q.defer(), query, tableForQuery;
      query = 'DELETE FROM [CASE] WHERE [CASE].Id_Case = ? ';
      tableForQuery = [caseId];
      DBConnection.query(query, tableForQuery).
        then(
          function () {
            for (var i = 0; i < cases.length; i++) {
              if (cases[i].Id_Case === caseId) { // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
                cases.splice(i, 1);
                break;
              }
            }
          },
          function () {
            deferred.reject('Error');
          }
          );
      return deferred.promise;
    };

    /**
     * @ngdoc function
     * @name home.getAllCategories
     * @description get every category from database which has at least one procedure
     * @returns deffered promise of all categories found by query
     */
    home.getAllCategories = function (userid) {
      var deferred = $q.defer();
      var categories = [];
      DBConnection.query('SELECT DISTINCT CATEGORY.Name, CATEGORY.Id_Category' +
        ' FROM CATEGORY' +
        ' LEFT JOIN PROCEDUREHASCATEGORY ON CATEGORY.Id_Category = PROCEDUREHASCATEGORY.Id_Category' +
        ' LEFT JOIN USERHASPROCEDURE ON PROCEDUREHASCATEGORY.Id_Procedure = USERHASPROCEDURE.Id_Procedure' +
        ' WHERE USERHASPROCEDURE.id_user = \'' + userid + '\'' +
        ' AND PROCEDUREHASCATEGORY.Id_Category = CATEGORY.Id_Category')
        .then(
          // if there are results save them to array
          function (results) {
            if (results.rows.length) {
              for (var i = 0; i < results.rows.length; i++) {
                categories.push(results.rows.item(i));
              }
              deferred.resolve(categories);
            } else {
              deferred.resolve(gettextCatalog.getString('No records found'));
            }
          },
          // if there are no categories return string
          function (error) {
            if (Constants.DEBUGMODE) {
              console.log('Error on select categories from db');
              console.log(error);
            }
            deferred.reject(error);
          }
          );
      return deferred.promise;
    };

    /**
     * @ngdoc function
     * @name home.getCasesForInfiniteScroll
     * @description get next 5 cases to show on home
     * @param {int} offsetNumber a number that represents how many results it should skip before storing cases which
     * are missing in home screen
     * @returns deffered promise of max 5 cases found by query
     */
    home.getCasesForInfiniteScroll = function (offsetNumber, pagesize) {
      var deferred = $q.defer();
      //'SELECT * FROM 'CASE', PROCEDURE WHERE 'CASE'.Id_Procedure = PROCEDURE.Id_Procedure AND 'Case'.Id_User = ?' +
      //' ORDER BY 'CASE'.Created DESC LIMIT ' + pagesize + ' OFFSET ?'
      var query = 'SELECT * FROM [CASE], PROCEDURE WHERE [CASE].Id_Procedure = PROCEDURE.Id_Procedure ' +
        'AND [CASE].Id_User = ? ORDER BY [CASE].Created DESC limit ' + pagesize + ' OFFSET ?';
      // jscs:disable requireCamelCaseOrUpperCaseIdentifiers
      DBConnection.query(query, [AuthService.currentUser.info.Id_Users, offsetNumber]).then(
        // jscs:enable requireCamelCaseOrUpperCaseIdentifiers
        function (resoults) {
          // if there are resoults save them to array
          if (resoults.rows.length) {
            var cases = [];
            for (var i = 0; i < resoults.rows.length; i++) {
              cases.push(resoults.rows.item(i));
            }
            deferred.resolve(cases);
          }

          // if user has no cases return string
          else {
            deferred.resolve(gettextCatalog.getString('No records found'));
          }
        },

        function (error) {
          if (Constants.DEBUGMODE) {
            console.log('Error on select user from db');
            console.log(error);
          }
          deferred.reject(error);
        });

      return deferred.promise;
    };
    /**
     * @ngdoc function
     * @name home.getAllUsersId
     * @description get Id_Users of all users from local database
     * @returns deffered promise that returns table of Id_Users or error if none found
     */
    home.getAllUsersId = function () {
      var deferred = $q.defer();
      DBConnection.query('SELECT * FROM USERS', []).then(
        function (resoults) {
          // if there are resoults save them to array
          if (resoults.rows.length) {
            var users = [];
            for (var i = 0; i < resoults.rows.length; i++) {
              users.push(resoults.rows.item(i));
            }
            deferred.resolve(users);
          }

          // if user has no cases return string
          else {
            deferred.resolve(gettextCatalog.getString('No records found'));
          }
        },

        function (error) {
          if (Constants.DEBUGMODE) {
            console.log('Error on select user from db');
            console.log(error);
          }
          deferred.reject(error);
        }
        );
      return deferred.promise;
    };

    home.storeLanguage = function (language, storeInDatabase) {
      var dataToStore = JSON.stringify([{
        'Key': 'language',
        'Value': language
      }]);
      window.localStorage.setItem('app_user_settings', dataToStore);
      if (storeInDatabase) {
        var query = 'INSERT OR REPLACE INTO CONFIG(Id_Config,Scope,Id_User,Key,Value) VALUES(coalesce((SELECT' +
                    ' Id_Config FROM CONFIG WHERE Id_User =? AND Key=?),(SELECT max(Id_Config) FROM CONFIG)' +
                    '+1),?,?,?,?)';
        var tab = [
                    AuthService.currentUser.info.Id_Users, // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
                    'language',
                    'user',
                    AuthService.currentUser.info.Id_Users, // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
                    'language',
                    language
                  ];
        DBConnection.query(query, tab);
      }
    };

    home.checkLanguageInDatabase = function () {
      var deferred = $q.defer();
      //$scope.settingsUser = JSON.parse(window.localStorage.getItem('app_user_settings'));
      if (window.localStorage.getItem('app_user_settings')) {
        var sett = JSON.parse(window.localStorage.getItem('app_user_settings'));
        deferred.resolve(sett[0].Value);
      } else {
        DBConnection.query('SELECT * FROM CONFIG WHERE Scope = ? AND Key = ? AND Id_User = ?',
                           [
                             'user',
                             'language',
                             AuthService.currentUser.info.Id_Users // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
                           ]).
          then(
            function (res) {
              if (res.rows.length) {
                deferred.resolve(res.rows[0]);
              }
              else {
                deferred.reject('No data');
              }
            },
            function () {
              deferred.reject('No data');
            }
            );
      }
      return deferred.promise;
    };

    /**
     * @ngdoc function
     * @name home.getIDForDelete
     * @description get section, form and fields ids to delet
     */
    var getIDForDelete = function (ids) {
      var deferred = $q.defer();
      var Id_Fields = [], Id_Forms = [], Id_Sections = []; // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers

      var query = 'SELECT f.[Id_Form], s.[Id_Section], fl.[Id_Field]' +
                  'FROM PROCEDURE p ' +
                  'LEFT OUTER JOIN FORM f ON f.Id_Procedure = p.Id_Procedure ' +
                  'LEFT OUTER JOIN SECTION s ON s.Id_Form = f.Id_Form ' +
                  'LEFT OUTER JOIN FIELD fl ON fl.Id_Section = s.Id_Section ' +
                  'WHERE p.Id_Procedure IN ' + ids;

      DBConnection.query(query, []).then(
          function (result) {
            if (result) {

              if (result.rows.length) {
                for (var index = 0; index < result.rows.length; index++) {
                  var value = result.rows.item(index);
                  // jscs:disable requireCamelCaseOrUpperCaseIdentifiers
                  if (value.Id_Section && _.indexOf(Id_Sections, value.Id_Section) === -1) {
                    Id_Sections.push('\'' + value.Id_Section + '\'');
                  }
                  if (value.Id_Form && _.indexOf(Id_Forms, value.Id_Form) === -1) {
                    Id_Forms.push('\'' + value.Id_Form + '\'');
                  }
                  if (value.Id_Field && _.indexOf(Id_Fields, value.Id_Field) === -1) {
                    Id_Fields.push('\'' + value.Id_Field + '\'');
                  }
                  // jscs:enable requireCamelCaseOrUpperCaseIdentifiers
                }
              }

              deferred.resolve({
                // jscs:disable requireCamelCaseOrUpperCaseIdentifiers
                Sections: Id_Sections.toString(),
                Forms: Id_Forms.toString(),
                Fields: Id_Fields.toString()
                // jscs:enable requireCamelCaseOrUpperCaseIdentifiers
              });
            } else {
              deferred.reject('No data');
            }
          },
          function (error) {
            deferred.reject(error);
            console.log(error);
          }
     );
      return deferred.promise;
    };

    /**
     * @ngdoc function
     * @name home.updateDatabase
     * @description update data received from API in local database
     * @param {object} dataReceived data that we get FROM API /update call
     * @returns deffered promise that returns either 'Data successfully updated' or error
     */
    home.updateDatabase = function (dataReceived) {
      var deferred = $q.defer();
      var contentToInsert = [];
      var dataR;

      /**
       * @ngdoc function
       * @name insertIntoDatabase
       * @description Purpose of this function is to get rid of repeatative code.
       * So we can use one function for multiple different calls, which have the same purpose but different data
       * @param {string} stringProperties string of columns of Table in which we insert data in local database split by
       * ',' .
       * Example: 'Id_User, Id_Procedure'
       * @param {object} dataTable Object which hold data to insert in specific table
       * @param {string} databaseTable string that specifies table in which we insert data
       * @param {string} tableId id field name of table
       * @param {string} effectedIds list of ids to insert
       * @param {bool} deviceFlag string that specifies table in which we insert data
       */
      var insertIntoDatabase = function (stringProperties, dataTable, databaseTable, tableId, effectedIds, deviceFlag) {
        var objProperties = stringProperties.split(',');

        if (dataTable && dataTable.length > 0) {
          var i;
          if (tableId && effectedIds && tableId !== '' && effectedIds !== '') {
            contentToInsert.push({
              table: [],
              query: 'DELETE from ' + databaseTable + ' WHERE ' + tableId + ' IN (' + effectedIds + ')'
            });
            // Delete codeitems for effected codelists. Otherwise they don't get deleted.
            if (databaseTable === 'CODELIST') {
              for (i = 0; i < effectedIds.length; i++) {
                contentToInsert.push({
                  table: [],
                  query: 'DELETE from CODEITEM WHERE Id_CodeList IN (' + effectedIds + ')'
                });
              }
            }
          }

          var queryToExecute, valuesTable, propertiesQuestionMark;

          for (i = 0; i < dataTable.length; i++) {
            if (deviceFlag) {
              if (i % 50 === 0) {
                if (queryToExecute) {
                  contentToInsert.push({
                    table: valuesTable,
                    query: queryToExecute
                  });
                }
                queryToExecute = 'INSERT INTO ' + databaseTable + '(' + stringProperties + ') VALUES';
                valuesTable = [];
              }
            }
            else {
              queryToExecute = 'INSERT INTO ' + databaseTable + '(' + stringProperties + ') VALUES';
              valuesTable = [];
            }
            propertiesQuestionMark = '(';
            for (var j = 0; j < objProperties.length; j++) {
              var property = objProperties[j].trim();
              property = property.replace(/[\[\]]/g, '');
              if (j > 0) {
                propertiesQuestionMark += ',?';
              }
              else {
                propertiesQuestionMark += '?';
              }
              valuesTable.push(dataTable[i][property]);
            }
            propertiesQuestionMark += ')';
            if (deviceFlag) {
              if (i % 50 !== 0) {
                queryToExecute += ',' + propertiesQuestionMark;
              } else {
                queryToExecute += propertiesQuestionMark;
              }
            }
            else {
              queryToExecute += propertiesQuestionMark;
              contentToInsert.push({
                table: valuesTable,
                query: queryToExecute
              });
            }
          }

          if (deviceFlag) {
            contentToInsert.push({
              table: valuesTable,
              query: queryToExecute
            });
          }
        }
        // If empty data, delete all records for this user
        else {
          var user_id = AuthService.currentUser.info.Id_Users; // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
          if (tableId && tableId !== '' && databaseTable === 'PROCEDURE') {

            // DELETE FIELDS
            contentToInsert.push({
              table: [],
              query: 'DELETE FROM FIELD WHERE Id_Section IN ' +
                     '(SELECT Id_Section FROM SECTION ' +
                     ' INNER JOIN FORM ON FORM.Id_Form = SECTION.Id_Form ' +
                     ' INNER JOIN USERHASPROCEDURE ON FORM.Id_Procedure = USERHASPROCEDURE.Id_Procedure ' +
                     ' WHERE USERHASPROCEDURE.Id_User = \'' +
                     user_id + '\')' // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
            });

            // DELETE SECTIONS
            contentToInsert.push({
              table: [],
              query: 'DELETE FROM SECTION WHERE Id_Form IN ' +
                     '(SELECT Id_Form FROM FORM ' +
                     ' INNER JOIN USERHASPROCEDURE ON FORM.Id_Procedure = USERHASPROCEDURE.Id_Procedure ' +
                     ' WHERE USERHASPROCEDURE.Id_User = \'' +
                     user_id + '\')' // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
            });

            // DELETE FORMS
            contentToInsert.push({
              table: [],
              query: 'DELETE FROM FORM WHERE Id_Procedure IN ' +
                     '(SELECT Id_Procedure FROM USERHASPROCEDURE WHERE Id_User = \'' +
                     user_id + '\')' // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
            });

            // DELETE PROCEDUREREPORT
            contentToInsert.push({
              table: [],
              query: 'DELETE FROM PROCEDUREREPORT WHERE Id_Procedure IN ' +
                     '(SELECT Id_Procedure FROM USERHASPROCEDURE WHERE Id_User = \'' +
                     user_id + '\')' // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
            });

            // DELETE PROCEDURES
            contentToInsert.push({
              table: [],
              query: 'DELETE FROM PROCEDURE WHERE Id_Procedure IN ' +
                     '(SELECT Id_Procedure FROM USERHASPROCEDURE WHERE Id_User = \'' +
                     user_id + '\')' // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
            });

          }
        }
      };

      /**
       * @ngdoc function
       * @name home.getAllTableIds
       * @description gets list of all ids of a specific table
       * @param {object} dataTable datatable
       * @param {string} tableId teble id field name
       * @returns formated filter string
       */
      var getAllTableIds = function (dataTable, tableId) {
        var retval = '';
        for (var i = 0; i < dataTable.length; i++) {
          if (dataTable[i][tableId]) {
            if (i === 0) {
              retval = '\'' + dataTable[i][tableId].toString() + '\'';
            } else {
              retval = retval + ', ' + '\'' + dataTable[i][tableId].toString() + '\'';
            }
          }
        }
        return retval;
      };

      /**
       * @ngdoc function
       * @name fixTableContent
       * @description changes table content so CurrentService.values will read them correctly
       * @param {array} data data for table
       */
      var fixTableContent = function (data) {
        var tableContent = [];

        // Go over all values in array
        _.each(data, function (row) {
          var contentRow = {};

          // One row in table is an object so go over all it's properties
          for (var i in row) {
            if (i !== undefined) {
              // Get the correct value
              contentRow[i] = row[i].Value;
            }
          }

          // Once row is corrected add it to tableContent
          tableContent.push(contentRow);
        });

        // Return fixed table content
        return tableContent;
      };

      /**
       * @ngdoc function
       * @name fixContent
       * @description changes content so CurrentService.values will read them correctly
       * @param {object} dataR recived data from db
       */
      var fixContent = function (dataR, fields) {
        // Go over all cases
        _.each(dataR, function (c) {
          var content = JSON.parse(c.Content);
          var newContent = {};

          // Go over each content and change it for mobile
          _.each(content, function(cont) {
            // Get field type for current field so we know how to handle it
            var fieldType = _.findWhere(fields, {Guid: cont.field}).Type;
            // Fix content for table
            if (fieldType === 1205) {
              newContent[cont.field] = fixTableContent(cont.value);
            } else {
              newContent[cont.field] = cont.value;
            }
          });
          c.Content = JSON.stringify(newContent);

        });
      };

      dataR = dataReceived.data.Categories;
      insertIntoDatabase('Id_Category,Id_Category_Parent,Name,Description,Image,Id_Binaries',
                         dataR,
                         'CATEGORY',
                         'Id_Category',
                         getAllTableIds(dataR, 'Id_Category'),
                         Constants.identifyDevice());

      dataR = dataReceived.data.Procedures;
      insertIntoDatabase('Id_Procedure,Name,Description,Image,Icon,Id_Binaries,Version',
                         dataR,
                         'PROCEDURE',
                         'Id_Procedure',
                         getAllTableIds(dataR, 'Id_Procedure'),
                         Constants.identifyDevice());

      dataR = dataReceived.data.UserHasProcedure;
      insertIntoDatabase('Id_User,Id_Procedure',
                         dataR,
                         'USERHASPROCEDURE',
                         'Id_User',
                         getAllTableIds(dataR, 'Id_User'),
                         Constants.identifyDevice());

      dataR = dataReceived.data.ProcedureHasCategory;
      insertIntoDatabase('Id_Procedure,Id_Category',
                         dataR,
                         'PROCEDUREHASCATEGORY',
                         'Id_Procedure',
                         getAllTableIds(dataR, 'Id_Procedure'),
                         Constants.identifyDevice());

      //all data gets deleted and reinserted
      dataR = dataReceived.data.CodeLists;
      insertIntoDatabase('Id_CodeList,Guid,Label,Description',
                         dataR,
                         'CODELIST',
                         'Id_CodeList',
                         getAllTableIds(dataR, 'Id_CodeList'),
                         Constants.identifyDevice());

      //all data gets deleted and reinserted
      dataR = dataReceived.data.CodeItems;
      insertIntoDatabase('Id_CodeItem,Id_CodeList,Id_CodeItem_Parent,Guid,Label,Value',
                         dataR,
                         'CODEITEM',
                         'Id_CodeItem',
                         getAllTableIds(dataR, 'Id_CodeItem'),
                         Constants.identifyDevice());

      dataR = dataReceived.data.Reports;
      insertIntoDatabase('Id_ProcedureReport,Id_Procedure,Guid,Label,Description,Dependencies,Version,Content',
                         dataR,
                         'PROCEDUREREPORT',
                         'Id_Procedure',
                         getAllTableIds(dataReceived.data.Procedures, 'Id_Procedure'),
                         Constants.identifyDevice());

      dataR = dataReceived.data.Cases.drafts;
      fixContent(dataR, dataReceived.data.Fields);
      insertIntoDatabase('Closed,Content,Created,Guid,Hash,Id_Device,Id_Procedure,Id_User,Location,Name,Version',
                         dataR,
                         '[CASE]',
                         '[CASE].Guid',
                         getAllTableIds(dataR, 'Guid'),
                         Constants.identifyDevice());

      dataR = dataReceived.data.Cases.dispatched;
      fixContent(dataR, dataReceived.data.Fields);
      insertIntoDatabase('Closed,Content,Created,Guid,Hash,Id_Device,Id_Procedure,Id_User,Location,Name,Version',
                         dataR,
                         '[CASE]',
                         '[CASE].Guid',
                         getAllTableIds(dataR, 'Guid'),
                         Constants.identifyDevice());

      dataR = dataReceived.data.Binaries;
      insertIntoDatabase('Id_Binaries,Data,Type,Hash',
                         dataR,
                         'BINARIES',
                         'Id_Binaries',
                         undefined,
                         Constants.identifyDevice());
      var allPromises = [];

      getIDForDelete('(' + getAllTableIds(dataReceived.data.Procedures, 'Id_Procedure') + ')').then(
        function (result) {

          dataR = dataReceived.data.Forms;
          insertIntoDatabase('Id_Form,Id_Procedure,Guid,Label,Description,Dependencies,Properties,[Order],[Type],' +
                             'Version',
                             dataR,
                             'FORM',
                             'Id_Form',
                             result.Forms,
                            Constants.identifyDevice());

          dataR = dataReceived.data.Sections;
          insertIntoDatabase('Id_Section,Id_Form,Guid,Label,Description,Dependencies,Properties,[Order]',
                             dataR,
                             'SECTION',
                             'Id_Section',
                             result.Sections,
                             Constants.identifyDevice());

          dataR = dataReceived.data.Fields;
          insertIntoDatabase('Id_Field,Id_Section,Guid,Label,Help,Hint,[Type],DataSource,Dependencies,Rules,' +
                             'Properties,Row,Coll,Id_Field_Parent',
                             dataR,
                             'FIELD',
                             'Id_Field',
                             result.Fields,
                             Constants.identifyDevice());

          DBConnection.connection.transaction(
            function (tx) {
              angular.forEach(contentToInsert, function (content) {
                var prom = tx.executeSql(content.query, content.table, function(tx, rs) {
                  console.log(rs, rs.rows);
                });
                allPromises.push(prom);
              });
              console.log(tx);
            },
          function (e) {
            console.log('ERROR: ', e.message);
          }
        );

          $q.all(allPromises).then(
            function () {
              deferred.resolve('Data successfully updated');
            },
            function (err) {
              deferred.reject(err);
            }
          );

        },
      function (error) {
          dataR = dataReceived.data.Forms;
          insertIntoDatabase('Id_Form,Id_Procedure,Guid,Label,Description,Dependencies,Properties,[Order],[Type],' +
                             'Version',
                             dataR,
                             'FORM',
                             'Guid',
                             getAllTableIds(dataR, 'Guid'),
                             Constants.identifyDevice());

          dataR = dataReceived.data.Sections;
          insertIntoDatabase('Id_Section,Id_Form,Guid,Label,Description,Dependencies,Properties,[Order]',
                             dataR,
                             'SECTION',
                             'Guid',
                             getAllTableIds(dataR, 'Guid'),
                             Constants.identifyDevice());

          dataR = dataReceived.data.Fields;
          insertIntoDatabase('Id_Field,Id_Section,Guid,Label,Help,Hint,[Type],DataSource,Dependencies,Rules,' +
                             'Properties,Row,Coll,Id_Field_Parent',
                             dataR,
                             'FIELD',
                             'Guid',
                             getAllTableIds(dataR, 'Guid'),
                             Constants.identifyDevice());

          console.log(error);

          DBConnection.connection.transaction(
            function (tx) {
              angular.forEach(contentToInsert, function (content) {
                //console.log(content.query);
                var prom = tx.executeSql(content.query, content.table);
                allPromises.push(prom);
              });
            }, function (e) {
              console.log('ERROR: ', e.message);
            }
          );

          $q.all(allPromises).then(
            function () {
              deferred.resolve('Data successfully updated');
            },
            function (err) {
              deferred.reject(err);
            }
          );
        }
      );

      return deferred.promise;

    };

    return home;
  }

})();
