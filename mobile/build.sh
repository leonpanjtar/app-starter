#!/bin/bash

appPath="www" 		
distPath="dist"			
backupPath="www_back"
platform=""	
isRelease=""
gulpWait="60s"	


#help information
function helpInfo {
		echo "creates optimised build"
		echo " "
		echo "attributes:"
		echo "-r 	release version"
		echo " "
		echo "options:"
		echo "-p 	platform (android, ios)"
		echo "-w 	wait time for gulp operations background processing (60s, 2m, 1h, ...)"
		echo " "
		echo "Example: build --r --p android"
		exit 0
	}
#validates modifies variables
function validateOptions {

	if [ "$platform" != '' ] && [ "$platform" != 'android' ] && [ "$platform" != 'ios' ]; then
		echo "ERROR - invalid platform - " "$platform"
		exit -1
	fi

	if [ -d "$backupPath" ]; then
  		echo "ERROR - Backup foder exists ($backupPath), this indicates a problem with last script run. Please check and fix the problem."
		exit -1
	fi

}

#tests for attributes (just help at this point)
while test $# -gt 0; do
        case "$1" in
                -h|--help)
                        helpInfo
                        ;;
                *)
                        break
                        ;;
        esac
done

#option setup loop
while getopts :rp:w: option
do
        case "${option}"
        in
		r) isRelease="1";;
		p) platform=${OPTARG};;
		w) gulpWait=${OPTARG};;
		?) helpInfo
        esac
done

#validate modify variable values
validateOptions

#backup ap data
mv "$appPath" "$backupPath"
cp -R "$backupPath" "$appPath"

#prepare new data
gulp prepare_data
echo "waiting for $gulpWait"
sleep "$gulpWait"

cp -r "$distPath"/* "$appPath"/

rm -r "$appPath"/js/**/*.js
rm -r "$appPath"/js/common/**/*.js
rm -r "$appPath"/js/components/**/*.js
rm -r "$appPath"/js/case/**/*.js
rm -r "$appPath"/js/case/directives/**/*.js


if [ "$isRelease" == "1" ]; then
	ionic build --release "$platform"
else
	ionic build "$platform"
fi

#delete temp app data and restore original app data from backup
rm -R "$appPath"
mv "$backupPath" "$appPath"

exit 0
