SmartForms Mobile app front end (ionic, angularjs, cordova)
================================================================

A starting project to achieve server side authentication for hybrid apps. This is the front end. You can either create your server side api based on the following blog post.

## iOS yoik.screenorientation plugin failing build
Note: latest version of cordova so this application will successfully build for iOS is 5.4.0 if you use newer cordova version build will fail because of this plugin. Install correct version of cordova with:

```bash
npm install -g cordova@5.4.0
```

## Update cordova to 3.6.0

Note : you'll need the latest version of cordova (3.6.0) for the inAppBrowser plugin to work correctly with ios8 otherwise it will be broken.

## Using this project

The following steps assume your environment is  android / ios ready. If you haven't downloaded the appropriate SDKs (ADT, or xcode) and setup the appropriate PATH variables (mostly for android), please do so before doing the steps below. You'll need nodejs installed as well.

### 1. Ionic / Cordova

make sure both [ionic](http://ionicframework.com/) and cordova are installed on your machine if not run the following command :

```bash
 npm install -g cordova ionic
```

Note : if npm isn't defined you'll need to install [node](http://nodejs.org/)

### 2. Clone this repo
```bash
 git clone CODE_COMLAND_URL mightyfields
```

Then navigate to the repo :
```bash
 cd mightyfields
```

### 3. Dependencies

Run :
```bash
 npm install
 bower install
```

This should install all dependencies for the project.


### 4. Add a platform

Once the dependencies installed, you'll need to add a platform (Warning this project has been tested for iOS and Android devices only)

Run :
```bash
 ionic platform add android
 ionic platform add ios
```

Then to build the project run:
```bash
 ionic run android
 ionic run ios
```

To run in browser:
```bash
 ionic serve
```

### 5. SASS
Files are monitored in live preview, so any changes to the content is instantly noticed.
If SASS is not available run the bellow command and ionic will take care of the rest:
```bash
 ionic setup sass
```

### 6. TRANSLATION
Inject translate directive to all translatable texts.
```html
 <a href="#" translate>Link</a>
```

Then run task:
```bash
 gulp pot
```
 ... and see the results in the po/ folder.
