(function() {

  'use strict';

  angular
      .module('appStarterWeb.dash.factory', [])
      .factory('Dash', Dash);

  Dash.$inject = ['$resource', 'Config'];

  function Dash($resource, Config) {
    return $resource(Config.apiUrl + 'dashboard', {}, {
      'get'     : {method:'POST', isArray: false}
    });
  }

})();
