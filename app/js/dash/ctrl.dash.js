(function() {

  'use strict';

  angular
      .module('appStarterWeb.dash.controller', [])
      .controller('DashController', DashController);

  DashController.$inject = ['$scope', '$state', 'Dash', 'gettextCatalog'];

  function DashController($scope, $state, Dash, gettextCatalog) {

    var vm              = this;
    vm.counters     = {};
    vm.data         = {};
    vm.loading      = true;
    vm.period       = '14days';
    vm.startDate    = moment().subtract(14, 'days').format('YYYY-MM-DD');
    vm.endDate      = moment().format('YYYY-MM-DD');
    vm.currentStep  = -1;

    /*
     * @ngdoc
     * @name getData
     * @desc Get dashboard data in a single call to server
     */
    vm.getData = function() {
      vm.loading = true;
      Dash.get({start: vm.startDate, end: vm.endDate}, function(c) {
        vm.counters = c.counters;
        vm.data     = c.data;
        vm.loading  = false;
      });
    };
    vm.getData();

    /**
     * @ngdoc function
     * @name setPeriod
     * @desc Set the required time period.
     */
    vm.setPeriod = function(period) {
      period = period || vm.period;
      switch (period) {
        case '7days':
          vm.startDate = moment().subtract(7, 'days');
          vm.endDate = moment().endOf('day');
        break;
        case '14days':
          vm.startDate = moment().subtract(14, 'days');
          vm.endDate = moment().endOf('day');
        break;
        case 'thisMonth':
          vm.startDate = moment().startOf('month');
          vm.endDate = moment().endOf('day');
        break;
        case 'lastMonth':
          vm.startDate = moment().subtract(1, 'month').startOf('month').startOf('day');
          vm.endDate = moment().subtract(1, 'month').endOf('month').endOf('day');
        break;
        case 'thisYear':
          vm.startDate = moment().startOf('year');
          vm.endDate = moment().endOf('day');
        break;
        case 'lastYear':
          vm.startDate = moment().subtract(1, 'year').startOf('year');
          vm.endDate = moment().subtract(1, 'year').endOf('year');
        break;
      }
      vm.period = period;
      vm.getData();
    };

    /**
     * @ngdoc function
     * @name gcbAppApp.dashboard.controller:getOrders
     * @description
     * # gcbAppApp
     *
     * Get orders.
     */
    vm.getPeriod = function () {
      var periods = {
        '7days': gettextCatalog.getString('Last 7 days'),
        '14days': gettextCatalog.getString('Last 14 days'),
        'thisMonth': gettextCatalog.getString('This Month'),
        'lastMonth': gettextCatalog.getString('Last Month'),
        'thisYear': gettextCatalog.getString('This Year'),
        'lastYear': gettextCatalog.getString('Last Year')
      };
      return periods[vm.period];
    };

    /*
     * @ngdoc
     * @name openDetails
     * @desc Redirect to details
     */
    vm.openDetails = function(state, id) {
      if (id) {
        $state.go(state, {id: id});
      }
      else {
        $state.go(state);
      }
    };

    /*
     * @ngdoc
     * @name tourEnded
     * @desc Called when the tour has ended
     */
    vm.tourEnded = function() {
      vm.currentStep  = -1;
      // TODO: Save dashboard tutorial step to user meta data
    };

    /*
     * @ngdoc
     * @name stepComplete
     * @desc Called when the tour step has been completed
     */
    vm.stepComplete = function() {};

    /*
     * @ngdoc
     * @name tourComplete
     * @desc Called when the tour has completed
     */
    vm.tourComplete = function() {
      vm.currentStep  = -1;
      // TODO: Mark dashboard tutorial as completed in user meta data
    };

    /* ====================================
     * ======== WATCHERS & EVENTS =========
     * ==================================== */

    $scope.$on('start-tour-dash', function() {
      vm.currentStep = 0;
    });
  }

})();
