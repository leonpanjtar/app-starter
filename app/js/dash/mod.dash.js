(function() {

  'use strict';

  angular
      .module('appStarterWeb.dash', [
        'countTo',
        'appStarterWeb.dash.controller',
        'appStarterWeb.dash.factory'
      ])
      .config(['$stateProvider',
        function($stateProvider) {

          // Application routes
          $stateProvider
            .state('dash', {
              url: '/dash',
              controller: 'DashController as dash',
              templateUrl: 'js/dash/tpl.dash.html',
              label: 'Dashboard'
            });
        }
      ]);

})();
