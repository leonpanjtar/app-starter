(function() {

  'use strict';

  angular
    .module('satellizer')
    .decorator('SatellizerStorage', SatellizerStorage);

  SatellizerStorage.$inject = ['$delegate',
                               '$cookies'];

  /**
  * @ngdoc decorator
  * @name satellizer.decorator:SatellizerStorage
  * @requires $delegate
  * @requires $cookies
  * @description
  * SatellizerStorage decorator overrides default behaviour of Satellizer and saves token in
  * cookie instead of local storage. In this way we are able to use single token across multiple
  * subdomains. Note that token is still sent in API request via authentication header. Cookie is
  * used only for storing the token.
  * @returns {Object} Decorator object with cookie storage applied.
  */

  function SatellizerStorage($delegate, $cookies) {
    var storage = {
      get: getCookie,
      set: setCookie,
      remove: removeCookie
    };
    return storage;

    function getCookie(key) {
      return $cookies.get(key);
    }

    function setCookie(key, value) {
      $cookies.put(key, value, {path: '/', domain: getCookieDomain(location.hostname)});
    }

    function removeCookie(key) {
      $cookies.remove(key, {path: '/', domain: getCookieDomain(location.hostname)});
    }

    function getCookieDomain(host) {
      var parts = host.split('.');
      switch (parts.length) {
        case 1:
          return parts[0]; // localhost -> localhost
        case 2:
          return parts[0] + '.' + parts[1]; // mightyfields.com -> mightyfields.com
        case 3:
          return parts[1] + '.' + parts[2]; // studio.mightyfields.com -> mightyfields.com
        default:
          return host; // 127.0.0.1 -> 127.0.0.1
      }
    }
  }
})();
