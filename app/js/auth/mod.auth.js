(function() {

  'use strict';

  angular
      .module('appStarterWeb.auth', [
        'appStarterWeb.auth.controller',
        'appStarterWeb.auth.factory',
        'appStarterWeb.auth.factory.interceptor',
        'appStarterWeb.auth.factory.session',
        'satellizer'
      ])
      .config(['$stateProvider',
        function($stateProvider) {

          // Application routes
          $stateProvider
            .state('login', {
                url: '/login?redirect',
                templateUrl: 'js/auth/tpl.auth.html',
                controller: 'AuthController as auth'
              })
            .state('signup', {
                url: '/signup',
                templateUrl: 'js/auth/tpl.signup.html',
                controller: 'AuthController as auth'
              })
            .state('forgot', {
                url: '/forgot',
                templateUrl: 'js/auth/tpl.forgot.html',
                controller: 'AuthController as auth'
              })
            .state('change', {
                url: '/change/:token',
                templateUrl: 'js/auth/tpl.change.html',
                controller: 'AuthController as auth'
              });
        }
      ]);

})();
