// public/scripts/authController.js

(function() {

  'use strict';

  angular
      .module('appStarterWeb.auth.controller', [])
      .controller('AuthController', AuthController);

  AuthController.$inject = ['$rootScope',
                            '$auth',
                            '$state',
                            'toastr',
                            'Auth',
                            'Session',
                            'gettextCatalog',
                            '$stateParams',
                            'Constants',
                            '$window',
                            'Config'
                          ];

  function AuthController($rootScope, $auth, $state, toastr, Auth, Session, gettextCatalog, $stateParams, Constants, // jshint ignore:line
                          $window, Config) {

    var vm = this;
    vm.register = {};
    vm.forgot   = {};
    vm.change   = {};

    vm.login = function() {
      var credentials = {
        email   : vm.email,
        password: vm.password
      };

      // Use Satellizer's $auth service to login
      $auth.login(credentials).then(function(data) {
        // Save logged in user to session
        Session.set(data.data.user);
        if (Session.get('user').role !== 'user') {
          $rootScope.$broadcast('$accountUpdate');
          // If login is successful, redirect to the users state
          if ($stateParams.redirect) {
            $window.location = decodeURIComponent($stateParams.redirect);
          } else {
            $state.go('dash');
          }
        } else {
          // If user redirect to web app
          $window.location = Config.appUrl;
        }

      }).catch(function(error) {
        var msg;
        if (error.data.code) {
          msg = gettextCatalog.getString(Constants.errorcodes[error.data.code]);
          toastr.warning(msg, gettextCatalog.getString('Login failed'));
        } else {
          msg = gettextCatalog.getString('Username and password do not match.');
          toastr.warning(msg, gettextCatalog.getString('Login failed'));
        }
        console.log(error);
      });
    };

    vm.register = function() {
      // Use Satellizer's $auth service to login
      $auth.signup(vm.register).then(function() {
        // If login is successful, redirect to the users state and clear data
        vm.register = {};
        $state.go('login', {});
      });
    };

    vm.forgotPwd = function() {
      Auth.reset(vm.forgot,
        function() {
          var msg = gettextCatalog.getString(
                      'An email with instructions to reset your password was sent to your email address.'
                    );
          toastr.success(msg, gettextCatalog.getString('Success'));
          vm.forgot = {};
          $state.go('login', {});
        },
        function(err) {
          console.log(err);
          var msg = gettextCatalog.getString('There was an error reseting your password.');
          toastr.error(msg, gettextCatalog.getString('Error'));
        });
    };

    vm.changePwd = function() {
      Auth.set({token: $stateParams.token}, vm.change,
        function() {
          var msg = gettextCatalog.getString('Your account password was successfully changed.');
          toastr.success(msg, gettextCatalog.getString('Success'));
          vm.change = {};
          $state.go('login', {});
        },
        function(err) {
          console.log(err);
          var msg = gettextCatalog.getString('There was an error changing your password.');
          toastr.error(msg, gettextCatalog.getString('Error'));
        });
    };

  }

})();
