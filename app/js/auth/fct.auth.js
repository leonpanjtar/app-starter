(function() {

  'use strict';

  angular
      .module('appStarterWeb.auth.factory', [])
      .factory('Auth', Auth);

  Auth.$inject = ['$resource', 'Config'];

  function Auth($resource, Config) {
    return $resource(Config.authUrl, {token: '@token'}, {
      'user'    : {method:'GET', url: Config.authUrl + 'user-info'},
      'set'     : {method:'POST', url: Config.authUrl + 'set/:token'},
      'reset'   : {method:'POST', url: Config.authUrl + 'reset'}
    });
  }

})();
