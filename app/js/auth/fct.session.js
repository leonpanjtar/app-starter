(function() {

  'use strict';

  /**
   * @ngdoc service
   * @name Session
   * @description
   * # smartFormsStudioApp
   *
   * Session factory that holds logged in user data
   */
  angular
      .module('appStarterWeb.auth.factory.session', [])
      .factory('Session', Session);

  Session.$inject = ['$q', '$location', 'Auth', '$cookies', '$auth'];

  function Session($q, $location, Auth, $cookies, $auth) {

    /* ======================================
     * == Service Init
     * ====================================== */

    // Previously we used local storage, now we use cookies.
    var oldStorage = localStorage;

    var f = {};  // Factory var
    f.data = undefined;

    /* ======================================
     * == Private Methods
     * ====================================== */

    /**
     * @ngdoc function
     * @name getCookieDomain
     * @description
     * Function returns cookie domain from host. Domain/cookie is shared between
     * Studio and Web app.
     * @param  {string} host
     */
    var getCookieDomain = function(host) {
      var parts = host.split('.');
      switch (parts.length) {
        case 1:
          return parts[0]; // localhost -> localhost
        case 2:
          return parts[0] + '.' + parts[1]; // mightyfields.com -> mightyfields.com
        case 3:
          return parts[1] + '.' + parts[2]; // studio.mightyfields.com -> mightyfields.com
        default:
          return host; // 127.0.0.1 -> 127.0.0.1
      }
    };

    /**
     * @ngdoc function
     * @name save
     * @description
     * Function saves session data to storage
     * @param  {object} data
     */
    var save = function(data) {
      f.data = data;
      $cookies.put('user', JSON.stringify(data), {path: '/', domain: getCookieDomain(location.hostname)});
      return data;
    };

    /**
     * @ngdoc function
     * @name clear
     * @description
     * Function clears local storage and factory storage
     */
    var clear = function() {
      f.data = undefined;
      $cookies.remove('user', {path: '/', domain: getCookieDomain(location.hostname)});
    };

    /**
     * @ngdoc function
     * @name retrieve
     * @description
     * Function tries to retrieve data from server or local storage
     */
    var retrieve = function() {
      try {
        // To provide backward compatibility: load from local storage and save into cookie.
        var user = oldStorage.getItem('user');
        if (user) {
          oldStorage.removeItem('user');
          save(JSON.parse(user));
        }
        // Load user from cookie
        user = $cookies.get('user');
        if (user) {
          return JSON.parse(user);
        } else if ($auth.isAuthenticated()) {
          Auth.user(
           function(data) { return save(data); },
           function(e) { console.log(e); });
        }
        return undefined;
      }
      catch (err) {
        console.log(err);
      }
    };
    /* ======================================
     * == Public Methods
     * ====================================== */

    /**
     * @ngdoc function
     * @name set
     * @description
     * Function sets session data
     * @param {object} data
     */
    f.set = function(data) {
      save(data);
    };

    /**
     * @ngdoc function
     * @name clear
     * @description
     * Function alls clear private function
     */
    f.clear = function() {
      clear();
    };

    /**
     * @ngdoc function
     * @name setAccount
     * @description
     * Function sets session account data
     * @param {object} data
     */
    f.setAccount = function(data) {
      var user = $cookies.get('user');
      if (user) { user = JSON.parse(user); }
      user.account = data;
      save(user);
    };

    /**
     * @ngdoc function
     * @name get
     * @description
     * Function gets session data
     */
    f.get = function() {
      return f.data ? f.data : retrieve();
    };

    return f;
  }

})();
