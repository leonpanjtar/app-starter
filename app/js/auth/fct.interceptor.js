(function() {

  'use strict';

  /**
   * @ngdoc service
   * @name smartFormsStudioApp.auth.service:AuthInterceptor
   * @description
   * # smartFormsStudioApp
   *
   * Interceptor factory to catch http response errors.
   */
  angular
      .module('appStarterWeb.auth.factory.interceptor', [])
      .factory('AuthInterceptor', AuthInterceptor);

  AuthInterceptor.$inject = ['$q', '$rootScope'];

  function AuthInterceptor($q, $rootScope) {

    /* ======================================
     * == Service Init
     * ====================================== */

    var f = {};  // Factory var

    /* ======================================
     * == Public Methods
     * ====================================== */

    /**
     * @ngdoc function
     * @name responseError
     * @description
     * Function sets response error & action
     * based on status code
     * @param  {object} rejection
     * @returns (object) promise
     */
    f.responseError = function(rejection) {
      if (rejection.status === 401) {
        $rootScope.$broadcast('$logout');
      }
      return $q.reject(rejection);
    };

    return f;
  }

})();
