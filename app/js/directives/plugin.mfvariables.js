/**
 * plugin.js
 *
 * Released under LGPL License.
 * Copyright (c) 1999-2015 Ephox Corp. All rights reserved
 *
 * License: http://www.tinymce.com/license
 * Contributing: http://www.tinymce.com/contributing
 */

/* global tinymce:true */

tinymce.PluginManager.add('mfvariables', function(editor, url) {

  var field = new RegExp('{{\s*([^}]+)\s*}}', 'g');

  /* ====================================== */
  /* == EDITOR FUNCTIONS ================== */
  /* ====================================== */

  /**
    * WYSIWYG -> source
    * Substitutes the placeholders with the tokens.
   */
  function _toSrc(s) {
    var m, a, i, re;

    // Regexes
    var spans = new RegExp('<span.*?class="mceNonEditable".*?>(.*?)<\/span.*?>', 'gi');
    var attrs = /(\S+)=["']?((?:.(?!["']?\s+(?:\S+)=|[>"']))+.)["']?/g;

    // Match all spans
    var matches = s.match(spans);

    if (matches && matches.length) {
      for (i = 0; i < matches.length; i++) {

        // Set element and value
        var el  = matches[i];
        var att = {};

        // Get span element attributes
        while ((a = attrs.exec(el))) {
          att[a[1]] = a[2];
        }
        if (att.mfvariables_data) { // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
          // Replace with presentation template
          // jscs:disable requireCamelCaseOrUpperCaseIdentifiers
          s = s.replace(el, '<span class="mfvariables_token" label="' + att.label + '">' +
                        atob(att.mfvariables_data.split('-').join('=')) + '</span>');
          // jscs:enable requireCamelCaseOrUpperCaseIdentifiers
        }
      }
    }
    return s;
  }

  /**

    * source -> WYSIWYG
    * Substitutes the tokens with the placeholders.
   */
  function _fromSrc(s) {
    var m, a, i, re;

    // Regexes
    var spans = new RegExp('<span.*?class="mfvariables_token".*?>(.*?)<\/span.*?>', 'gi');
    var attrs = /(\S+)=["']?((?:.(?!["']?\s+(?:\S+)=|[>"']))+.)["']?/g;
    var base64regex = /^([0-9a-zA-Z+/]{4})*(([0-9a-zA-Z+/]{2}==)|([0-9a-zA-Z+/]{3}=))?$/;

    // Find all spans that need replacing
    while ((m = spans.exec(s))) {

      // Set element and value
      var el  = m[0];
      var val = m[1];
      var att = {};

      // Get span element attributes
      while ((a = attrs.exec(m[0]))) {
        att[a[1]] = a[2];
      }

      try {
        if (val.indexOf('{{') > -1) {
          val = btoa(val).split('=').join('-');
        }
      }
      catch (e) {
        console.log(e);
      }

      // Replace with presentation template
      s = s.replace(m[0], '<span class="mceNonEditable" label="' + att.label + '" mfvariables_data="' + (val) + '">' +
                    unescape(att.label) + '</span>');
    }
    return s;
  }

  /* ====================================== */
  /* == EDITOR INIT ======================= */
  /* ====================================== */

  // BEGIN - Add the attribute mfvaribles_data as a valid one for span
  // jscs:disable requireCamelCaseOrUpperCaseIdentifiers
  if (editor.settings.extended_valid_elements === undefined) {
    editor.settings.extended_valid_elements = 'span[style|class|mfvariables_data|label]';
  }
  else {
    var i = editor.settings.extended_valid_elements.search(/span[\s]*\[[^\[\]]+\]/i);

    if (i === -1) {
      editor.settings.extended_valid_elements += ',span[style|class|mfvariables_data|label]';
    }
    else {
      var a = editor.settings.extended_valid_elements.substr(i);
      a = a.substr(a.indexOf('[') + 1);

      editor.settings.extended_valid_elements = editor.settings.extended_valid_elements.substring(0,i) +
                                                'span[mfvariables_data|label|' + a;
    }
    // jscs:enable requireCamelCaseOrUpperCaseIdentifiers
  }

  /* ====================================== */
  /* == EDITOR EVENTS ===================== */
  /* ====================================== */

  // Convert from Source to Editor Content
  editor.on('beforeSetContent', function(e) {
    e.content = _fromSrc(e.content);
  });

  // Convert from Editor Content to Source
  editor.on('postProcess', function(e) {
    e.content = _toSrc(e.content);
  });

});
