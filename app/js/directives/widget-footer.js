(function() {

  'use strict';

  /**
   * Widget Footer Directive
   */

  angular
      .module('appStarterWeb.directives.wfooter', [])
      .directive('rdWidgetFooter', rdWidgetFooter);

  function rdWidgetFooter() {
    var directive = {
        requires: '^rdWidget',
        transclude: true,
        template: '<div class="widget-footer" ng-transclude></div>',
        restrict: 'E'
      };
    return directive;
  }

})();
