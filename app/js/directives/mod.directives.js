(function() {

  'use strict';

  angular
      .module('appStarterWeb.directives', [
        'appStarterWeb.directives.loading',
        'appStarterWeb.directives.widget',
        'appStarterWeb.directives.wbody',
        'appStarterWeb.directives.wfooter',
        'appStarterWeb.directives.wheader',
        'fillHeight',
        'appStarterWeb.directives.pwCheck',
        'appStarterWeb.directives.affix'
      ]);

})();
