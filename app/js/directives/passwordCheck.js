(function() {

  'use strict';

  /**
   * @ngdoc overview
   * @name pwCheck
   * @description Directive that checks two fields for password match
   */

  angular
      .module('appStarterWeb.directives.pwCheck', [])
      .directive('pwCheck', pwCheck);

  function pwCheck() {
    var directive = {
      require: 'ngModel',
      restrict: 'A',
      link: function (scope, elem, attrs, ctrl) {

        var firstPassword = '#' + attrs.pwCheck;

        elem.add(firstPassword).on('keyup', function () {

          scope.$apply(function () {
            var v = elem.val() === $(firstPassword).val();
            ctrl.$setValidity('pwmatch', v);
          });
        });
      }
    };
    return directive;
  }

})();
