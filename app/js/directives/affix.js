(function() {

  'use strict';

  /**
   * Angular Affix Directive
   */

  angular
      .module('appStarterWeb.directives.affix', [])
      .directive('affix', affix);

  affix.$inject = ['$window'];

  function affix() {
    return {
      restrict: 'A',
      link: function ($scope, $element, $attrs) {

        var el = angular.element('.' + $attrs.selector);
        var topOffset = $element[0].offsetTop;

        function affixElement() {
          if (el[0].offsetTop > topOffset) {
            $element.css('top', el[0].scrollTop + 'px');
          }
          else {
            $element.css('top', '0px');
          }
        }

        $scope.$on('$stateChangeStart', function() {
          el.unbind('scroll', affixElement);
        });

        el.bind('scroll', affixElement);
      }
    };
  }

})();
