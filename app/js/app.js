(function() {

  'use strict';

  angular
    .module('appStarterWeb', [
      'ngResource',
      'ngAnimate',
      'ngSanitize',
      'ui.router',
      'satellizer',
      'ui.bootstrap',
      'ui.bootstrap.datetimepicker',
      'ngCookies',
      'datatables',
      'datatables.buttons',
      'checklist-model',
      'gettext',
      'ngFileUpload',
      'toastr',
      'dndLists',
      'ui.tab.scroll',
      'ui.tinymce',
      'tmh.dynamicLocale',
      'ui.select',
      'angular-tour',
      'angular-loading-bar',
      'ngPatternRestrict',
      'oitozero.ngSweetAlert',
      'infinite-scroll',
      'appStarterWeb',
      'appStarterWeb.config',
      'appStarterWeb.directives',
      'appStarterWeb.main',
      'appStarterWeb.auth',
      'appStarterWeb.dash',
      'appStarterWeb.users',
      'appStarterWeb.settings',
    ])

    .config(['$urlRouterProvider', '$authProvider', 'Config',
      function($urlRouterProvider, $authProvider, c) {

        // Satellizer configuration that specifies which API
        // route the JWT should be retrieved from
        $authProvider.authHeader = 'x-access-token';
        $authProvider.authToken  = '';
        $authProvider.loginUrl   = c.apiUrl + 'login';
        $authProvider.signupUrl  = c.apiUrl + 'register';

        // Redirect to the auth state if any other states
        // are requested other than users
        $urlRouterProvider.otherwise('/dash');
      }])

    .config(['$httpProvider', function ($httpProvider) {
      $httpProvider.interceptors.push([
        '$injector',
        function ($injector) {
          return $injector.get('AuthInterceptor');
        }
      ]);
      $httpProvider.interceptors.push([
        '$injector',
        function ($injector) {
          return $injector.get('serverErrorInterceptor');
        }
      ]);
    }])

    .config(['scrollableTabsetConfigProvider', function(scrollableTabsetConfigProvider) {
      scrollableTabsetConfigProvider.setShowTooltips(false);
      scrollableTabsetConfigProvider.setAutoRecalculate(true);
    }])

    .config(['tourConfig', function(tourConfig) {
      tourConfig.backDrop = true;
    }])

    .config(['tmhDynamicLocaleProvider', function(tmhDynamicLocaleProvider) {
      var pattern = 'https://code.angularjs.org/1.4.9/i18n/angular-locale_{{locale}}.js';
      tmhDynamicLocaleProvider.localeLocationPattern(pattern);
    }])

    .config(['cfpLoadingBarProvider', function(cfpLoadingBarProvider) {
      cfpLoadingBarProvider.includeSpinner = false;
    }])

    .config(['$uibTooltipProvider', function ($uibTooltipProvider) {
      $uibTooltipProvider.options({appendToBody: true});
    }])

    .run(['gettextCatalog', 'Config', function(gettextCatalog, c) {
      gettextCatalog.debug = c.debug;
    }]);
  angular.module('infinite-scroll').value('THROTTLE_MILLISECONDS', 250);
})();
