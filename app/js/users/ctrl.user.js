(function () {
  'use strict';

  /**
   * @ngdoc controller
   * @name AddUsersCtrl
   * @description controller for popup for adding user
   */
  angular
    .module('appStarterWeb.users.controller.add', [])
    .controller('AddUsersCtrl', AddUsersCtrl);

  AddUsersCtrl.$inject = ['$uibModalInstance', 'user', 'Groups', 'Session', '$state'];

  function AddUsersCtrl($uibModalInstance, user, Groups, Session, $state) {

    var vm = this;
    vm.user = user || {};
    vm.groups = [];
    vm.newGroup  = {name: '', active: false};
    vm.loggedUser = Session.get('user')._id;
    vm.loggedUserRole = Session.get('user').role;
    vm.roles = ['user', 'admin', 'superadmin'];

    /**
     * @ngdoc function
     * @name getGroups
     * @description load all groups
     */
    vm.getGroups = function() {
      Groups.query(function(groups) {
        vm.groups = groups;
      });
    };
    vm.getGroups();

    /**
     * @ngdoc function
     * @name addGroup
     * @description add new group
     */
    vm.addGroup = function() {
      Groups.save(vm.newGroup, function() {
        vm.getGroups();
        vm.newGroup  = {name: '', active: false};
      });
    };

    /**
     * @ngdoc function
     * @name smartFormsStudioApp.codelists.controller.add:cancel
     * @description cancels record adding
     */
    vm.cancel = function () {
      $uibModalInstance.dismiss('Canceled');
      if ($state.current.name === 'profile' || $state.current.name === 'user') {
        $state.go('users');
      }
    };

    /**
     * @ngdoc function
     * @name smartFormsStudioApp.codelists.controller.add:save
     * @description executes record adding
     */
    vm.save = function () {
      $uibModalInstance.close(vm.user);
    };

  }

})();
