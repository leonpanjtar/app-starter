(function() {

  'use strict';

  angular
      .module('appStarterWeb.users.factory', [])
      .factory('Users', Users);

  Users.$inject = ['cachedResource', 'Config'];

  function Users(cachedResource, Config) {
    return cachedResource(Config.apiUrl + 'users/:id', {id: '@id'}, {
      'query':   {method:'GET', isArray: true},
      'get':     {method:'GET', isArray: false},
      'getData': {method:'GET', url: Config.apiUrl + 'users/data/:id', isArray: false},
      'save':    {method:'POST'},
      'update':  {method:'PUT'},
      'lock':    {method:'PUT', url: Config.apiUrl + 'users/lock/:id'},
      'remove':  {method:'DELETE'}
    });
  }

})();
