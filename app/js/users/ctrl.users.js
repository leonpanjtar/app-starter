// public/scripts/userController.js

(function() {

  'use strict';

  angular
    .module('appStarterWeb.users.controller', [])
    .controller('UserController', UserController);

  UserController.$inject = ['Config',
                            '$uibModal',
                            'Users',
                            'Groups',
                            'DTOptionsBuilder',
                            'DTColumnBuilder',
                            'toastr',
                            'gettextCatalog',
                            'ConfirmDelete',
                            '$state',
                            'Session',
                            '$stateParams',
                            '$q',
                            '$rootScope',
                            '$scope',
                            '$compile',
                            'Constants'];

  function UserController(c, $uibModal, Users, Groups, DTOptionsBuilder, DTColumnBuilder, toastr, gettextCatalog, // jshint ignore:line
                          ConfirmDelete, $state, Session, $stateParams, $q, $rootScope, $scope, $compile, Constants) {

    var vm = this;

    vm.users = [];
    vm.groups = [];
    vm.loggedUser = Session.get('user');
    //vm.error;

    vm.dtInstance = {};
    vm.dtOptions = DTOptionsBuilder.newOptions()
                    .withDOM('<"row"<"col-sm-7"<"col-sm-6"l><"col-sm-6"f>><"col-sm-5 text-right"B>>' +
                             '<"row"<"col-sm-12"tr>>' +
                             '<"row"<"col-sm-5"i><"col-sm-7"p>>')
                    .withPaginationType('full_numbers')
                    .withOption('stateSave', true)
                    .withOption('autoWidth', false)
                    .withOption('ajax', function (data, callback, settings) {
                      Users.getData({id: JSON.stringify(data)}, function(users) {
                        vm.users = users.data;
                        callback(users);
                      });
                    })
                    .withDataProp('data')
                    .withOption('processing', true)
                    .withOption('serverSide', true)
                    .withOption('createdRow', function(row, data, dataIndex) {
                      if (!data.active) {
                        angular.element(row).addClass('deleted');
                      }
                      $compile(angular.element(row).contents())($scope);
                    })
                    .withButtons([
                      {
                        text: gettextCatalog.getString('Add new'),
                        className: 'btn btn-sm btn-primary',
                        key: '1',
                        action: function () {
                            vm.openModal();
                          }
                      }
                    ]);

    vm.dtColumns = [
      DTColumnBuilder.newColumn('firstName', gettextCatalog.getString('First Name'))
      .renderWith(function(data, type, full) {
        return '<a href="" ng-click="user.openModal(\'' + full._id + '\')">' + full.firstName + '</a>';
      }),
      DTColumnBuilder.newColumn('lastName', gettextCatalog.getString('Last Name')),
      DTColumnBuilder.newColumn('email', gettextCatalog.getString('Email')),
      DTColumnBuilder.newColumn('role', gettextCatalog.getString('Role')).renderWith(function(data, type, full) {
        return data ? data : 'admin';
      }),
      DTColumnBuilder.newColumn('meta.cases', gettextCatalog.getString('Cases')),
      //DTColumnBuilder.newColumn('meta.devices', gettextCatalog.getString('Devices')),
      DTColumnBuilder.newColumn('createdAt', gettextCatalog.getString('Created'))
      .renderWith(function(data, type, full) {
        if ($scope.$parent) {
          return $scope.$parent.$parent.main.getDate(data);
        }
        return data;
      }),
      DTColumnBuilder.newColumn(-1).withTitle(gettextCatalog.getString('Actions'))
        .notSortable().withOption('width', 80).renderWith(function(data, type, full) {
          var lockBtn = '';
          if (full.active) {
            lockBtn = '<button type="button" ng-click="user.lock(\'' + full._id + '\')" ng-disabled="' +
            (full._id === vm.loggedUser._id) + '" class="btn btn-xs btn-default" popover-trigger="mouseenter" ' +
            'popover-placement="bottom" uib-popover="' + gettextCatalog.getString('Lock') +
            '"> <i class="table-icon fa fa-lock"></i> </button>';
          } else {
            lockBtn = '<button type="button" ng-click="user.lock(\'' + full._id + '\')" ' +
              'class="btn btn-xs btn-default" popover-trigger="mouseenter" popover-placement="bottom" uib-popover="' +
              gettextCatalog.getString('Unlock') + '"> <i class="table-icon fa fa-unlock"></i> </button>';
          }
          var editBtn = '<button type="button" ng-click="user.openModal(\'' + full._id + '\')" ' +
            'class="btn btn-primary btn-xs" popover-trigger="mouseenter" popover-placement="bottom" uib-popover="' +
            gettextCatalog.getString('Edit') + '"> <i class="table-icon fa fa-edit"></i> </button>';
          var deleteBtn = '<button type="button" ng-click="user.remove(\'' + full._id + '\')" ng-disabled="' +
            (full._id === vm.loggedUser._id) + '" class="btn btn-default btn-xs" popover-trigger="mouseenter" ' +
            'popover-placement="bottom" uib-popover="' + gettextCatalog.getString('Delete') +
            '"> <i class="table-icon fa fa-trash-o"></i> </button>';
          return lockBtn + '&nbsp;' + editBtn + '&nbsp;' + deleteBtn;
        })
    ];

    /**
     * @ngdoc function
     * @name getGroups
     * @description load all groups
     */
    vm.getGroups = function() {
      Groups.query(function(groups) {
        vm.groups = groups;
      });
    };
    vm.getGroups();

    /**
     * @ngdoc function
     * @name getUsers
     * @description load all users
     */
    vm.getUser = function(uid) {
      var deffered = $q.defer();
      Users.get({id: uid}, function(res) {
        deffered.resolve(res.user[0]);
      }, function(err) {
        deffered.reject();
      });
      return deffered.promise;
    };

    /**
     * @ngdoc function
     * @name openModal
     * @description launch modal for adding and editing
     */
    vm.openModal = function (user, haveUser) {

      if (!haveUser) {
        if (user) {
          user = _.findWhere(vm.users, {_id: user});
        } else {
          user = {};
        }
      }

      var old = angular.copy(user);

      var modalInstance = $uibModal.open({
        animation: true,
        templateUrl: 'js/users/part.add.html',
        controller: 'AddUsersCtrl as user',
        windowClass: 'add-user-modal',
        resolve: {
          user: function () {
            return user;
          }
        }
      });

      modalInstance.result.then(function (user) {
        var msg;
        if (user._id !== undefined) {
          Users.update({id: user._id}, user,
            function(res) {
              var msg = gettextCatalog.getString('User successfully edited.');
              toastr.success(msg, gettextCatalog.getString('Success'));
              vm.dtInstance.reloadData(null, false);
            },
            function(err) {
              if (err.data.code === 11000) {
                msg = gettextCatalog.getString(Constants.errorcodes[err.data.code]);
                toastr.error(msg, gettextCatalog.getString('Error'));
                vm.dtInstance.reloadData(null, false);
              } else {
                toastr.error(gettextCatalog.getString(err.data.error), gettextCatalog.getString('Error'));
              }
            });
        }
        else {
          Users.save(user,
            function (res) {
              if (res.data.code && res.data.code === 2000) {
                msg = gettextCatalog.getString(Constants.errorcodes[res.data.code]);
                toastr.success(msg, gettextCatalog.getString('Success'));
              }
              else {
                toastr.success(
                  gettextCatalog.getString('User successfully saved.'),
                  gettextCatalog.getString('Success'));
              }
              vm.dtInstance.reloadData(null, false);
            },
            function(err) {
              if (err.data.code === 9000) {
                msg = gettextCatalog.getString(Constants.errorcodes[err.data.code]);
                toastr.error(msg, gettextCatalog.getString('Error'));
              } else if (err.data.code === 11000) {
                msg = gettextCatalog.getString(Constants.errorcodes[err.data.code]);
                toastr.error(msg, gettextCatalog.getString('Error'));
              } else {
                toastr.error(gettextCatalog.getString(err.data.error), gettextCatalog.getString('Error'));
              }

            });
        }
      }, function() {
        angular.copy(old, user);
      });
    };

    /**
     * @ngdoc function
     * @name openModal
     * @description remove user
     */
    vm.remove = function (id) {
      ConfirmDelete.cd().then(function () {
        Users.remove({id: id}, function(res) {

          // Save current page for datatable and check if last one to go back one page
          var page = vm.dtInstance.DataTable.page();
          if (vm.dtInstance.DataTable.page(page).data().length === 1 && page !== 0) {
            vm.dtInstance.DataTable.page(page - 1).draw(false);
          }
          vm.dtInstance.reloadData(null, false);

          if (res.data.error) {
            toastr.error(gettextCatalog.getString('Could not remove user.'), gettextCatalog.getString('Error'));
          } else {
            toastr.success(gettextCatalog.getString('User successfully removed.'), gettextCatalog.getString('Success'));
          }
        });
      }).catch(function () {

      });
    };

    /**
     * @ngdoc function
     * @name lock
     * @description Lock/unlock user
     */
    vm.lock = function (id) {
      var msg;
      Users.lock({id: id}, function(res) {
        vm.dtInstance.reloadData(null, false);
        if (res.data.code === 9000) {
          msg = gettextCatalog.getString(Constants.errorcodes[res.data.code]);
          toastr.error(msg,gettextCatalog.getString('Error'));
        } else if (res.data.error) {
          toastr.error(
            gettextCatalog.getString('Can not lock this user.'),
            gettextCatalog.getString('Error')
          );
        } else {
          var message = gettextCatalog.getString('User succesfully') + ' ';
          if (res.data.user.active) {
            message += gettextCatalog.getString('unlocked.');
          } else {
            message += gettextCatalog.getString('locked.');
          }
          toastr.success(message, gettextCatalog.getString('Success'));
        }
      });
    };

    if ($state.current.name === 'profile') {
      vm.openModal(Session.get('user'), true);
    } else if ($state.current.name === 'user') {
      vm.getUser($stateParams.id).then(function(data) {
        vm.openModal(data, true);
      });
    }
  }

})();
