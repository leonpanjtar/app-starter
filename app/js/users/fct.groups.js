(function() {

  'use strict';

  angular
      .module('appStarterWeb.users.factory.groups', [])
      .factory('Groups', Groups);

  Groups.$inject = ['cachedResource', 'Config'];

  function Groups(cachedResource, Config) {
    return cachedResource(Config.apiUrl + 'groups/:id', {id: '@id'}, {
      'query':  {method:'GET', isArray: true},
      'get':    {method:'GET', isArray: false},
      'save':   {method:'POST'},
      'update': {method:'PUT'},
      'remove': {method:'DELETE'}
    });
  }

})();
