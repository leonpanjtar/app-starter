(function() {

  'use strict';

  angular
      .module('appStarterWeb.users', [
        'appStarterWeb.users.controller',
        'appStarterWeb.users.controller.add',
        'appStarterWeb.users.factory',
        'appStarterWeb.users.factory.groups'
      ])
      .config(['$stateProvider',
        function($stateProvider) {

          // Application routes
          $stateProvider
            .state('users', {
              url: '/users',
              templateUrl: 'js/users/tpl.users.html',
              controller: 'UserController as user',
              label: 'Users'
            })
            .state('profile', {
              url: '/users/profile',
              templateUrl: 'js/users/tpl.users.html',
              controller: 'UserController as user',
              label: 'Users'
            })
            .state('user', {
              url: '/users/user/:id',
              templateUrl: 'js/users/tpl.users.html',
              controller: 'UserController as user',
              label: 'Users'
            });
        }
      ]);

})();
