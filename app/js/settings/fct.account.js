(function() {

  'use strict';

  angular
      .module('appStarterWeb.settings.factory', [])
      .factory('Account', Account);

  Account.$inject = ['$resource', 'Config'];

  function Account($resource, Config) {
    return $resource(Config.apiUrl + 'account', {}, {
      'get':    {method:'GET', isArray: false},
      'update': {method:'PUT'}
    });
  }

})();
