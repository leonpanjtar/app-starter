(function() {

  'use strict';

  angular
      .module('appStarterWeb.settings', [
        'appStarterWeb.settings.controller',
        'appStarterWeb.settings.factory',
      ])
      .config(['$stateProvider',
        function($stateProvider) {

          // Application routes
          $stateProvider
            .state('settings', {
              url: '/settings',
              templateUrl: 'js/settings/tpl.settings.html',
              controller: 'SettingsController as settings',
              label: 'Settings'
            });
        }
      ]);

})();
