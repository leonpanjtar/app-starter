(function() {

  'use strict';

  angular
      .module('appStarterWeb.settings.controller', [])
      .controller('SettingsController', SettingsController);

  SettingsController.$inject = ['$rootScope',
                                'Account',
                                'toastr',
                                'gettextCatalog',
                                'Session',
                                'Constants',
                                '$stateParams',
                                'Auth'
                              ];

  function SettingsController($rootScope, Account, toastr, gettextCatalog, Session, Constants, $stateParams, Auth) {

    var vm = this;

    vm.account        = {};
    vm.c              = Constants;
    vm.disableCompany = Session.get('user').role !== 'superadmin';            // disable editing Company info

    /**
     * @ngdoc function
     * @name getAccount
     * @description load account data
     */
    vm.getAccount = function() {
      Account.get(function(response) {
        vm.account = response.account;
      });
    };
    vm.getAccount();

    /**
     * @ngdoc function
     * @name save
     * @description save account data
     */
    vm.save = function() {
      var success = true;
      Account.update(vm.account,
        function(response) {
          vm.account = response.account;
          Session.setAccount(response.account); // Update account data in session
          $rootScope.$broadcast('$accountUpdate');
          var msg = gettextCatalog.getString('Settings were successfully saved.');
          toastr.success(msg, gettextCatalog.getString('Success'));
        },
        function() {
          toastr.error(gettextCatalog.getString('Error saving settings.'), gettextCatalog.getString('Error'));
        });
    };

  }

})();
