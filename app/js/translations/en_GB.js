angular.module('gettext').run(['gettextCatalog', function (gettextCatalog) {
/* jshint -W100 */
    gettextCatalog.setStrings('en', {"2active users":"active users","2more users":"more users","2users":"users","3active users":"active users","3more users":"more users","3users":"users"});
/* jshint +W100 */
}]);