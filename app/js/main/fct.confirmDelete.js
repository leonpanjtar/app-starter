(function() {

  'use strict';

  angular
      .module('appStarterWeb.main.factory.confirmDelete', [])
      .factory('ConfirmDelete', ConfirmDelete);

  ConfirmDelete.$inject = ['$rootScope', 'SweetAlert', 'gettextCatalog', '$q'];

  function ConfirmDelete($rootScope, SweetAlert, gettextCatalog, $q) {

    var f = {
      cd: cd,
    };
    return f;

    /**
     * @ngdoc function
     * @name cd
     * @description opens sweetAlert if you are sure to delete this.
     */
    function cd () {
      var deffered = $q.defer();
      SweetAlert.swal({
          title: gettextCatalog.getString('Are you sure?'),
          text: gettextCatalog.getString('You will not be able to recover this!'),
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3B9C55',
          confirmButtonText: gettextCatalog.getString('Yes'),
          cancelButtonText: gettextCatalog.getString('No')
        },
        function(isConfirm) {
          if (isConfirm) {
            deffered.resolve();
          } else {
            deffered.reject();
          }
        });
      return deffered.promise;
    }

  }
})();
