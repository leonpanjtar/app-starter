// public/scripts/userController.js

(function() {

  'use strict';

  angular
    .module('appStarterWeb.main.controller', [])
    .controller('MainController', MainController);

  MainController.$inject = [
    '$rootScope',
    '$scope',
    '$cookieStore',
    '$state',
    '$timeout',
    'gettextCatalog',
    '$auth',
    'Session',
    'tmhDynamicLocale',
    'Constants',
    'DTDefaultOptions',
    'SweetAlert',
    'Config',
    'toastr',
    '$window',
    '$cacheFactory'
  ];

  function MainController($rootScope, $scope, $cookieStore, $state, $timeout, gettextCatalog, $auth, Session, locale, // jshint ignore:line
                          Constants, DTDefaultOptions, SweetAlert, Config, toastr, $window, $cacheFactory) {

    var vm            = this;
    vm.mobileView     = 992;
    vm.toggle         = false;
    vm.state          = $state;
    vm.isAuth         = false;
    vm.currentDate    = new Date();
    vm.lang           = 'en';
    vm.title          = gettextCatalog.getString('Dashboard');
    vm.user           = Session.get();
    vm.config         = Config;
    vm.previousState  = undefined;

    var sweetAlertOpen = false;
    var changeState = false;


    /* ====================================
     * ======== PRIVATE FUNCTIONS =========
     * ==================================== */

    function setPreviousState(toState, fromState) {
      //vm.previousState = undefined;
    }


    /* ====================================
     * ======== PUBLIC FUNCTIONS ==========
     * ==================================== */

    /**
     * @ngdoc function
     * @name vm.getWidth
     * @description return window inner width
     */
    vm.getWidth = function() {
      return window.innerWidth;
    };

    /**
     * @ngdoc function
     * @name vm.getHeight
     * @description return window inner height
     */
    vm.getHeight = function() {
      return window.innerHeight;
    };

    /**
     * @ngdoc function
     * @name vm.currentPath
     * @description toggle sidebar display
     */
    vm.toggleSidebar = function() {
      vm.toggle = !vm.toggle;
      $cookieStore.put('toggle', vm.toggle);
    };

    /**
     * @ngdoc function
     * @name vm.currentStateName
     * @description return label for current state
     */
    vm.currentStateName = function() {
      return gettextCatalog.getString($state.current.label);
    };

    /**
     * @ngdoc function
     * @name vm.getPreviousStateName
     * @description return label for previous state if exists
     */
    vm.previousStateName = function() {
      if (vm.previousState) {
        return gettextCatalog.getString(vm.previousState.label);
      }
    };

    /**
     * @ngdoc function
     * @name vm.logout
     * @description Use Satellizer's $auth service to logout and redirect to login
     */
    vm.logout = function(redirectUrl) {
      // Logout
      $auth.logout();
      // Clear session data
      Session.clear();
      // Clear cached resources
      var caches = $cacheFactory.info();
      for (var cache in caches) {
        if (cache.indexOf('cached_resource') !== -1) {
          $cacheFactory.get(cache).removeAll();
        }
      }
      // Clear local storage
      localStorage.clear();
      // Redirect
      if (!redirectUrl) {
        redirectUrl = $window.location;
      }
      $state.go('login', {redirect: encodeURIComponent(redirectUrl)});
    };

    /**
     * @ngdoc function
     * @name vm.switchLang
     * @description switch app language
     */
    vm.switchLang = function(lang) {
      if (!lang) {
        if (vm.user && vm.user.hasOwnProperty('account') && vm.user.account.hasOwnProperty('language')) {
          lang = vm.user.account.language;
        }
        else {
          lang = 'sl_SI';
        }
      }

      locale.set(Constants.languages[lang].locale);
      moment.locale(Constants.languages[lang].locale);
      gettextCatalog.setCurrentLanguage(lang);

      var msg = gettextCatalog.getString('This website uses cookies to ensure you get the best experience on our website.');

      window.cookieconsent_options = { // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
        message: msg,
        dismiss: gettextCatalog.getString('Got it!'),
        theme: 'light-bottom'
      };

      var path = 'js/translations/datatables/';

      DTDefaultOptions.setLanguageSource(path + Constants.languages[lang].code + '.json');
    };

    vm.switchLang();

    /**
     * @ngdoc function
     * @name vm.startTour
     * @description starts the tour for the selected screen
     */
    vm.startTour = function() {
      vm.toggle = true;
      vm.toggleSidebar();
      $rootScope.$broadcast('start-tour-' + $state.current.name);
    };

    /**
     * @ngdoc function
     * @name getDate
     * @param {string} currentDate
     * @param {string} dateFormat
     * @returns {string} formatted date
     * @description returns current date in selected format or DD.MM.YYYY
     */
    vm.getDate = function(currentDate, dateFormat) {
      var date = currentDate ? currentDate : vm.currentDate;
      var format = dateFormat ? dateFormat : (vm.user.account.dateFormat ? vm.user.account.dateFormat : 'DD.MM.YYYY');
      return moment(date).format(format);
    };

    /* ====================================
     * ======== WATCHER FUNCTIONS =========
     * ==================================== */

    angular.element(window).resize(function() {
      var height = vm.getHeight();
      if (height > 500) {
        angular.element('.sidebar-info').show();
        angular.element('.sidebar-info').css('top', height - 90);
      } else {
        angular.element('.sidebar-info').hide();
      }
      angular.element('.sidebar-footer').css('top', height - angular.element('.sidebar-footer').height());
    });

    $scope.$watch(vm.getWidth, function(n) {
      if (n >= vm.mobileView) {
        if (angular.isDefined($cookieStore.get('toggle'))) {
          vm.toggle = !$cookieStore.get('toggle') ? false : true;
        }
        else {
          vm.toggle = true;
        }
      }
      else {
        vm.toggle = false;
      }
    });

    window.onresize = function() {
      $scope.$apply();
    };

    $scope.$on('$accountUpdate', function() {
      vm.user = Session.get();
      vm.switchLang();
    });

    $scope.$on('$logout', function() {
      if ($state.current.name !== 'login') {
        vm.logout();
      }
    });

    $scope.$on('minimizeSidebar', function() {
      vm.toggle = true;
      vm.toggleSidebar();
    });

    $scope.$on('currentName', function(event, args) {
      if (vm.previousState) {
        vm.previousState.current = args;
      }
    });

    // Watch state change event
    $rootScope.$on('$stateChangeStart',
      function(event, toState, toParams, fromState, fromParams) {
          var authStates = ['login', 'signup', 'change', 'reset-pwd', 'forgot'];

          // Dont't let guest access dash/cases ...
          if (!$auth.isAuthenticated()) {
            vm.isAuth = true;
            if (!_.contains(authStates, toState.name)) {
              event.preventDefault();
              vm.logout($window.location);
            }
            return true;
          }

          // Redirect users to app
          if (Session.get('user').role === 'user') {
            $window.location = Config.appUrl;
            return true;
          }

          if (authStates.indexOf(toState.name) > -1) {
            vm.isAuth = true;
          } else {
            vm.isAuth = false;
          }

          // Check if you need to ask for leave confirmation (while in formbuilder etc.)
          var needConfirm = [];

          if (_.contains(needConfirm, toState.name) && fromParams.id !== toParams.id) {
            $rootScope.changed = false;
          }

          if (_.contains(needConfirm, fromState.name) &&
            !changeState &&
            fromParams.id !== toParams.id &&
            $rootScope.changed) {
            sweetAlertOpen = true;
            event.preventDefault();
            SweetAlert.swal({
                title: gettextCatalog.getString('Are you sure?'),
                text: gettextCatalog.getString('You will lose changes you made!'),
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3B9C55',
                confirmButtonText: gettextCatalog.getString('Yes'),
                cancelButtonText: gettextCatalog.getString('No')
              },
              function (isConfirm) {
                if (isConfirm) {
                  changeState = true;
                  $state.go(toState.name, {});
                }
              });
          } else {
            changeState = false;
            sweetAlertOpen = false;
          }

          setPreviousState(toState, fromState);

        });
  }

})();
