/**
 * @ngdoc overview
 * @name appStarterWeb.main.factory.cachedResource
 * @description
 * Module containing cachedResource factory.
 */

(function() {

  'use strict';

  angular
      .module('appStarterWeb.main.factory.cachedResource', [])
      .factory('cachedResource', cachedResource);

  cachedResource.$inject = ['$resource', '$cacheFactory'];

  /**
   * @ngdoc service
   * @name appStarterWeb.main.factory.cachedResource.service:cachedResource
   * @requires $resource
   * @requires $cacheFactory
   * @description
   * cachedResource factory extends $resource factory by caching results
   * returned by `query()` and invalidating cache when `save()`, `remove()` or
   * `update()` is invoked. It is used in the same way as $resource factory.
   *
   * **Warning:** If you modify objects returned by `query()` with functions
   * other than `save()`, `remove()` or `update()` you have to invalidate
   * the cache yourself. You do this by using
   * `$cacheFactory(url).remove()`.
   * @returns {Object} A $resource object with cache logic applied.
   */

  function cachedResource($resource, $cacheFactory) {

    return function (url, paramDefaults, actions, options) {

      var cache = $cacheFactory('cached_resource_' + url);
      var interceptor = {
          response: function (response) {
              cache.removeAll();
              return response;
            }
        };

      actions = angular.merge(
          {},
          {
              'get':     {method: 'GET'},
              'query':   {method: 'GET', cache: cache, isArray: true},
              'save':    {method: 'POST', interceptor: interceptor},
              'remove':  {method: 'DELETE', interceptor: interceptor},
              'update':  {method: 'PUT', interceptor: interceptor},
              'lock':    {method: 'PUT', interceptor: interceptor}
            },
          actions
      );
      return $resource(url, paramDefaults, actions, options);
    };
  }
})();
