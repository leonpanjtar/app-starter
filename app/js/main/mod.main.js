(function() {

  'use strict';

  angular
      .module('appStarterWeb.main', [
        'appStarterWeb.main.controller',
        'appStarterWeb.main.factory.constants',
        'appStarterWeb.main.factory.cachedResource',
        'appStarterWeb.main.factory.fieldTemplates',
        'appStarterWeb.main.factory.serverErrorInterceptor',
        'appStarterWeb.main.factory.confirmDelete'
      ]);
})();
