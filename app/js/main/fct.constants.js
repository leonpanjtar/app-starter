(function() {

  'use strict';

  angular
    .module('appStarterWeb.main.factory.constants', [])
    .service('Constants', Constants);

  Constants.$inject = ['gettextCatalog'];

  function Constants(gettextCatalog) {

    /* ======================================
       * == Service Init
       * ====================================== */

    var s             = this;  // Service var

    // Available Languages
    s.languages   = {
      // jscs:disable requireCamelCaseOrUpperCaseIdentifiers
      sl_SI : {gt: 'sl_SI', locale: 'sl', code: 'sl_SI', label: 'Slovenščina'},
      // jscs:enable requireCamelCaseOrUpperCaseIdentifiers
      en : {gt: 'en', locale: 'en', code: 'en_GB', label: 'English'}
    };

    // Available date formats
    s.dateformats = [
      {format: 'D.M.Y'},           // 3.9.2010
      {format: 'D. M. Y'},         // 3. 9. 2010
      {format: 'D. MMMM Y'},       // 3. October 2010
      {format: 'D/M/Y'},           // 3/9/2010
      {format: 'D-M-Y'},           // 3-9-2010
      {format: 'D MMM Y'},         // 3 Sep 2010
      {format: 'DD.MM.Y'},         // 03.09.2010
      {format: 'DD.MM.YY'},        // 03.09.10
      {format: 'dddd, D.M.Y'},     // Friday, 3.9.2010
      {format: 'dddd, MMMM D, Y'}, // Friday, September 3, 2010
      {format: 'MMMM D, Y'},       // September 3, 2010
      {format: 'MMM D, Y'},        // Sep 3, 2010
      {format: 'M/D/Y'},           // 9/3/2010
      {format: 'M-D-Y'},           // 9-3-2010
      {format: 'DD.MM.YYYY'},      // 09.07.2007
      {format: 'Y/M/D'},           // 2010/9/3
      {format: 'Y/MM/DD'},         // 2010/09/03
      {format: 'Y'},               // 2010
      {format: 'YY'},              // 10
      {format: 'MMM'},             // Sep
      {format: 'MMMM'},            // September
      {format: 'D'},               // 3
      {format: 'DD'},              // 03
      {format: 'ddd'},             // Fri
      {format: 'dddd'},            // Friday
      {format: 'w'}                // Week
    ];

    // Available time formats
    s.timeformats = [
      {format: 'H:m'},         // 17:46
      {format: 'HH:mm'},       // 17:46
      {format: 'H:mm'},        // 17:46
      {format: 'H:m:s'},       // 17:46
      {format: 'HH:mm:ss'},    // 17:46:21
      {format: 'hh:mm A'},     // 12:05 PM
      {format: 'h:mm A'},      // 12:05 PM
      {format: 'h:m A'},       // 12:05 PM
      {format: 'h:m:s A'},     // 12:05 PM
      {format: 'hh:mm:ss A'},  // 12:05:08 PM
      {format: 'HH'},          // 17
      {format: 'H'},           // 17
      {format: 'h A'},         // 5 PM
      {format: 'hh A'},        // 05 PM
      {format: 'mm'},          // 46
      {format: 'm'},           // 46
      {format: 'ss'},          // 21
      {format: 's'}            // 21
    ];

    // Available countries
    s.countries   = [
      {id: 'sl', label: 'Slovenija', language: 'sl_SI', gt: 'sl'},
      {id: 'us', label: 'US', language: 'en_GB', gt: 'en'}
    ];

    // Available Task Types
    s.tasktypes   = [
      {
        type: 'email',
        icon: 'envelope-o',
        label: gettextCatalog.getString('Email'),
        description: gettextCatalog.getString('Sends an email with custom template and attachments.')
      },
      {
        type: 'rest',
        icon: 'globe',
        label: gettextCatalog.getString('REST'),
        description: gettextCatalog.getString('Send a REST request to provided API URL with all case data.')
      },
      {
        type: 'script',
        icon: 'file-code-o',
        label: gettextCatalog.getString('Script'),
        description: gettextCatalog.getString('Call a custom script that does some data processing.')
      },
      /*
      {
        type: 'soap',
        label: gettextCatalog.getString('SOAP'),
        label: gettextCatalog.getString('Send a SOAP request to provided service with all case data.')
      },
      {
        type: 'docs',
        label: gettextCatalog.getString('File System'),
        label: gettextCatalog.getString('Uploads case documents to desired document storge.')
      }
      */
    ];

    // Look @ http://www.sitepoint.com/web-foundations/mime-types-summary-list/
    s.docFormats = [
      {
        type: 'pdf',
        file: 'pdf',
        header: 'appliaction/pdf',
        color: 'danger'
      },
      // {
      //   type: 'excel',
      //   file: 'xlsx',
      //   header: 'appliaction/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
      //   color: 'success'
      // },
      // {
      //   type: 'word',
      //   file: 'docx',
      //   header: 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
      //   color: 'info'
      // },
      {
        type: 'html',
        file: 'html',
        header: 'text/html',
        color: 'warning'}
    ];

    // Predefined regex
    s.regex = {
        'label': '^[0-9a-zA-ZčšžćđČŠŽĆĐ?!.,_ -]*$'  // Allowing alphanumeric with ?!_.,- and space
      };

    s.errorcodes = {
      '1000': gettextCatalog.getString('Unknown error'),
      '2000': gettextCatalog.getString('Succesfully restored'),
      '4000': gettextCatalog.getString('Allowed length exceeded.'),
      '4001': gettextCatalog.getString('Invalid rights for login to studio.'),
      '4002': gettextCatalog.getString('Can not change template'),
      '4005': gettextCatalog.getString('Procedure already exists. Can restore.'),
      '9000': gettextCatalog.getString('License exceeded. Please buy more licenses to add more users.'),
      '9001': gettextCatalog.getString('Can not delete.'),
      '11000': gettextCatalog.getString('Duplicate entry.'),
      // NodeMailer error codes
      'EENVELOPE'  : gettextCatalog.getString('Invalid recipient email.'),
      'EMESSAGE'   : gettextCatalog.getString('Message failed.'),
      'ETIMEDOUT'  : gettextCatalog.getString('Connection timed out.'),
      'ECONNECTION': gettextCatalog.getString('Connection failed.'),
      'EAUTH'      : gettextCatalog.getString('Invalid login.')
    };

    return s;
  }
})();
