(function() {

  'use strict';

  angular
      .module('appStarterWeb.main.factory.fieldTemplates', [])
      .service('FieldTemplates', FieldTemplates);

  FieldTemplates.$inject = ['gettextCatalog'];

  function FieldTemplates(gettextCatalog) {

    /* ======================================
     * == Service Init
     * ====================================== */

    var s = this;  // Service var

    /**
     * @ngdoc function
     * @name getFieldtemplate
     * @description get field template for print
     * @param {Object} field
     */
    s.gft = function(field) {
      var tpl = '';
      switch (field.type.toUpperCase()) {
        case 'SIGNATURE':
          tpl = btoa('<img src="{{rc.getFieldData(\'' + field.guid + '\')}}" />');
        break;

        case 'FILE':
          tpl = btoa('<img ng-src="{{i.source}}" ng-repeat="i in rc.getFieldData(\'' + field.guid +
                     '\') track by $index">');
        break;

        case 'RADIO':
        case 'CHECKBOX':
        case 'DROPDOWN':
          tpl = btoa('<ins ng-if="rc.getFieldData(\'' + field.guid + '\').length" style="text-decoration: none"' +
                     ' ng-repeat="v in rc.getFieldData(\'' + field.guid + '\') track by $index">' +
                       '{{v}}<ins style="text-decoration: none" ng-if="$index < rc.getFieldData(\'' + field.guid +
                       '\').length - 1 && v">, </ins>' +
                     '</ins>');
        break;

        case 'TOGGLE':
          var yes = gettextCatalog.getString('Yes');
          var no = gettextCatalog.getString('No');
          tpl = btoa('<ins style="text-decoration: none">{{ rc.getFieldData(\'' + field.guid + '\') ? \'' + yes +
                     '\' : \'' + no + '\'}}</ins>');
        break;

        case 'TABLE':
          tpl = btoa('<table width="100%" class="table table-bordered">' +
                        '<tr ng-repeat="tr in rc.getFieldData(\'' + field.guid + '\') track by $index" ' +
                        'ng-init="parentIdx = $index">' +
                          '<th ng-if="parentIdx == 0" ng-repeat="td in tr track by $index" ng-bind-html="td">{{td}}' +
                          '</th>' +
                          '<td ng-if="parentIdx > 0" ng-repeat="td in tr track by $index" ng-bind-html="td">{{td}}' +
                          '</td>' +
                        '</tr>' +
                      '</table>');
        break;

        case 'TEXTAREA':
          tpl = btoa('<ins style="text-decoration: none" ng-bind-html="rc.getFieldData(\'' + field.guid +
                     '\')">{{rc.getFieldData(\'' + field.guid + '\')}}</ins>');
        break;

        default:
          tpl = btoa('{{rc.getFieldData(\'' + field.guid + '\')}}');
      }

      [].filter.call(tpl, function(c) { return c.charCodeAt(0) !== 8203;});

      return '<span class="mfvariables_token" label="' + escape(field.label) + '">' + tpl.split('=').join('-') +
             '</span>';
    };

    return s;
  }
})();
