/**
 * @ngdoc overview
 * @name appStarterWeb.main.factory.serverErrorInterceptor
 * @description
 * Module containing serverErrorInterceptor factory.
 */

(function() {

  'use strict';

  angular
      .module('appStarterWeb.main.factory.serverErrorInterceptor', [])
      .factory('serverErrorInterceptor', serverErrorInterceptor);

  serverErrorInterceptor.$inject = ['$q', '$injector'];

  /**
   * @ngdoc service
   * @name appStarterWeb.main.factory.serverErrorInterceptor.service:serverErrorInterceptor
   * @requires $q
   * @requires $injector
   * @description
   * Handles $http response errors and displays error message. Unathorized
   * requests (401) and conflicts (409) are not handled by this service.
   * @returns {Object} Object with error handling logic.
   */
  function serverErrorInterceptor($q, $injector) {
    var factory = {
        responseError: responseError
      };
    return factory;

    /**
     * @ngdoc function
     * @name responseError
     * @description
     * Checks response code and displays error. Unathorized requests (401)
     * and conflicts (409) are not handled by this function.
     * @param {Object} rejection Server response object
     * @returns {Object} $http promise
     */
    function responseError(rejection) {
      // We need to inject toastr and gettextCatalog manually because
      // of circular dependency
      var toastr = $injector.get('toastr');
      var gettextCatalog = $injector.get('gettextCatalog');
      // Check if we should handle error
      var skip = [
          400, // Bad request
          401, // Authorization
          409  // Conflict
      ];
      if (skip.indexOf(rejection.status) === -1) {
        // Display error
        var msg = gettextCatalog.getString('Cloud is stuck in hurricane');
        toastr.error(msg);
      }
      return $q.reject(rejection);
    }
  }
})();
