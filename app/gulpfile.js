// Include gulp utils
var gulp              = require('gulp'),
    connect           = require('gulp-connect'),
    gettext           = require('gulp-angular-gettext'),
    del               = require('del'),
    print             = require('gulp-print'),
    useref            = require('gulp-useref'),
    uglify            = require('gulp-uglify'),
    cssnano           = require('gulp-cssnano'),
    gulpif            = require('gulp-if'),
    templateCache     = require('gulp-angular-templatecache'),
    path              = require('path'),
    concat            = require('gulp-concat'),
    print             = require('gulp-print'),
    clean             = require('gulp-clean'),
    runSequence       = require('run-sequence'),
    jshint            = require('gulp-jshint'),
    stylish           = require('jshint-stylish'),
    ngAnnotate        = require('gulp-ng-annotate'),
    jshintXMLReporter = require('gulp-jshint-xml-file-reporter'),
    jscs              = require('gulp-jscs'),
    stylish           = require('gulp-jscs-stylish'),
    ngConstant        = require('gulp-ng-constant');

// Paths and file filters
var paths = {
    index         : './index.html',
    partials      : ['./partials/*.html', './js/**/*.html'],
    dist          : './dist',
    distScripts   : './dist/**/*.js',
    distStyles    : './dist/**/*.css',
    images        : './images/*',
    translations  : './js/translations/**/*.json',
    fonts         : ['./libs/bootstrap/fonts/*.woff',
                     './libs/bootstrap/fonts/*.woff2',
                     './libs/font-awesome/fonts/*'],
    tinymce       : ['./libs/tinymce-dist/*/**'],
    src           : ['./*.html',
                     './partials/*.html',
                     './css/*.css',
                     './js/**'],
    configInput   : './config.json',
    configOutput  : './js/',
  };

// Main module name. Used when injecting partials into templateCache.
var mainModule = 'appStarterWeb.main';

// --------- DEVELOPMENT ------------

gulp.task('default', ['connect', 'watch']);

// Create development configuration
gulp.task('dev:config', function () {
  return gulp.src(paths.configInput)
    .pipe(ngConstant({
      name: 'appStarterWeb.config',
      deps: [],
      merge: true,
      constants: {
        Config: {
          debug: true
        }
      },
      wrap: true,
    }))
    .pipe(gulp.dest(paths.configOutput));
});

// Create development server
gulp.task('connect', ['dev:config'], function() {
  connect.server({
    root: './',
    livereload: true,
    port: 8081
  });
});

// Reload page when src file change
gulp.task('reload', function() {
  gulp.src(paths.src)
    .pipe(connect.reload());
});

// Watch src files for change
gulp.task('watch', function() {
  gulp.watch([paths.src], ['reload']);
});

// --------- CODESTYLE ---------------
// Generate codestyle report: uses project wise JSHint and JSCS
gulp.task('lint', function() {
  return gulp.src(['js/**/*.js', '!js/config.js', '!js/translations/**/*.js'])
    .pipe(jshint('.jshintrc'))
    .pipe(jscs({configPath: '../.jscsrc'}))
    .pipe(stylish.combineWithHintResults())
    .pipe(jshint.reporter('gulp-jshint-html-reporter', {
      filename: __dirname + '/lint-report.html',
      createMissingFolders : true
    }));
});

// Generate codestyle report for jenkins: uses project wise JSHint and JSCS
gulp.task('jenkins:lint', function() {
  return gulp.src(['js/**/*.js', '!js/config.js', '!js/translations/**/*.js'])
    .pipe(jshint('.jshintrc'))
    .pipe(jscs({configPath: '../.jscsrc'}))
    .pipe(stylish.combineWithHintResults())
    .pipe(jshint.reporter(jshintXMLReporter))
    .on('end', jshintXMLReporter.writeFile({
        format: 'checkstyle',
        filePath: './lint-report.xml'
      }));
});

// --------- DISTRIBUTION ------------
// Create distribution server
gulp.task('dist:connect', function() {
  connect.server({
    root: './dist',
    livereload: true,
    port: 2773
  });
});
/*
Build distribution version of application.
1. Clean distribution folder
2. Create distribution configuration
3. Concatenate CSS and JS files into app and vendor files
4. Compile partials in templateCache
5. Merge compiled partials with app.js.
6. Delete compiled partials
7. Minify CSS an JS files
8. Copy images
9. Copy translations
10. Copy fonts
11. Copy tinyMCE files
*/
gulp.task('build', function() {
  runSequence('dist:clean',
              'dist:config',
              'dist:useref',
              'dist:compile-partials',
              'dist:merge-partials-with-app',
              'dist:delete-partials',
              'dist:minify',
              'dist:copy-images',
              'dist:copy-translations',
              'dist:copy-fonts',
              'dist:copy-tinymce');
});

// 1. Clean distribution folder
gulp.task('dist:clean', function() {
  return del.sync(paths.dist);
});

// 2. Create distribution configuration
gulp.task('dist:config', function () {
  return gulp.src(paths.configInput)
    .pipe(ngConstant({
      name: 'mfStudio.config',
      deps: [],
      merge: true,
      constants: {
        Config: {
          debug: false
        }
      },
      wrap: true,
    }))
    .pipe(gulp.dest(paths.configOutput));
});

// 3. Concatenate CSS and JS files into app and vendor files
gulp.task('dist:useref', function() {
  return gulp.src(paths.index)
    .pipe(useref())
    .pipe(gulp.dest(paths.dist));
});

// 4. Compile partials in templateCache
gulp.task('dist:compile-partials', function() {
  return gulp.src(paths.partials)
    .pipe(templateCache(
      {
        module: mainModule,
        filename: 'js/partials.js',
        base: function(file) {return path.relative(file.cwd, file.path);}
      }))
    .pipe(gulp.dest(paths.dist));
});

// 5. Merge compiled partials with app.js.
gulp.task('dist:merge-partials-with-app', function() {
  return gulp.src([path.join(paths.dist, 'js/app.min.js'),
                   path.join(paths.dist, 'js/partials.js')])
    .pipe(concat('js/app.min.js'))
    .pipe(gulp.dest(paths.dist));
});

// 6. Delete compiled partials
gulp.task('dist:delete-partials', function() {
  return del.sync(path.join(paths.dist, 'js/partials.js'));
});

// 7. Minify CSS an JS files
gulp.task('dist:minify', function() {
  return gulp.src([paths.distScripts, paths.distStyles])
    .pipe(gulpif('*.js', uglify({
      output: {
        ascii_only: true //jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
      }
    }
    )))
    .pipe(gulpif('*.css', cssnano()))
    .pipe(gulp.dest(paths.dist));
});

// 8. Copy images
gulp.task('dist:copy-images', function() {
  return gulp.src(paths.images)
    .pipe(gulp.dest(path.join(paths.dist, 'images')));
});

// 9. Copy translations
gulp.task('dist:copy-translations', function() {
  return gulp.src(paths.translations)
    .pipe(gulp.dest(path.join(paths.dist, 'js/translations')));
});

// 10. Copy fonts
gulp.task('dist:copy-fonts', function() {
  return gulp.src(paths.fonts)
    .pipe(gulp.dest(path.join(paths.dist, 'fonts')));
});

// 11. Copy tinyMCE files
gulp.task('dist:copy-tinymce', function() {
  return gulp.src(paths.tinymce)
    .pipe(gulp.dest(path.join(paths.dist, 'js/tinymce')));
});

gulp.task('connect-dist', function() {
  connect.server({
    root: './dist/',
    livereload: false,
    port: 2772
  });
});

// --------- LANGUAGE ------------

// Generate translations
gulp.task('translations:generate', function () {
  return gulp.src(['partials/**/*.html', 'js/**/*.js', 'js/**/*.html'])
    .pipe(gettext.extract('template.pot', {}))
    .pipe(gulp.dest('po/'));
});

// Compile translations
gulp.task('translations:compile', function () {
  return gulp.src('po/**/*.po')
    .pipe(gettext.compile({
      format: 'javascript'
    }))
    .pipe(gulp.dest('js/translations/'));
});
