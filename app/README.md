# App Starter Web #

This is the App Starter Web App basis

## Installing

Obviously you need node.js and npm. Run `npm install` and `bower install` in the `app` directory to setup the dependencies. Once installed, you can run the super exciting API with `gulp connect`. By default the app is accessible at `http://localhost:2772/`.

## Linitng the app

To lint all the JS code run `gulp lint`.

## Building distribution version

To build distribution version run `gulp build`.

## Structure

    app
      css [global app css]
    	libs [bower components folder]
    	images [holds the application images]
    	js [holdes the application logic]
    	partials [holds generic partial templates]
    	bower.json [Bower config file]
    	gulpfile.js [Gulp configuration file]
    	index.html [Main app boostrapper]
